{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<style type="text/css">
.pass {
	text-align: center;
	margin-top: 0px;
}
</style>
{/literal}
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<div class="image">
  <div><img src="images/bar1.gif" width="740
" height="30
" /></div>
  <table width="740" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="740" height="150" align="left" valign="middle"><p>この調査は、市民の社会貢献に関する実態や意識を調査し、特定非営利活動法人（NPO法人）を支援する施策の検討や「特定非営利活動促進法」改正のための基礎資料とすることを目的として実施するものです。いただいた御回答が、今後のNPO関連施策の充実へ向けた貴重な資料となります。<br />
          <br />
          御多忙の折、大変恐縮に存じますが、調査の趣旨を御理解の上、御協力を賜りますよう、よろしくお願い申し上げます。 </p></td>
    </tr>
  </table>
  <div><img src="images/border1.gif" width="740
" height="3" /></div>
  <br />
  <table width="740" border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td height="25" colspan="2" align="left" valign="middle"><p><img src="images/bar_caution1.gif" width="328" height="25" /></p></td>
    </tr>
    <tr>
      <td width="26" height="150" align="left" valign="middle">&nbsp;</td>
      <td width="714" align="left" valign="middle"><p>・この調査は、封筒の宛先に書かせていただいた方御本人が入力してください。 <br />
          <br />
          ・御回答いただいた内容は、&ldquo;○○と答えた方が○％&rdquo;というように、統計的にまとめられ、個人を特定する情報が公表されることはありません。 <br />
          <br />
          ・お答えいただく問数は、選択される回答により異なりますが、属性を除き最大で18問になり、<u>所要時間は15分程度です</u>。<br />
          <br />
          ・設問は、あてはまる御回答を選択していただく形式と、金額を御入力いただく形式（問８、問９）があります。回答選択については、「ひとつお選びください」又は「複数回答可能」の指示がありますので、御留意ください。その他、各問に書かれている注書きをお読みになってから御回答ください。 <br />
          <br />
          ・回答が困難な項目については、空欄のまま御回答いただいても差支えありませんが、全ての項目の御回答に御協力願います。<br />
          <br />
          ・この調査に御協力いただきました方には、御礼として後日「クオカード（500円分）」を送付いたします。
        </p>
  
        <p>・御回答は、途中までの回答内容を保存することが出来ますので、何回かに分けて御回答が可能です。<br />
          <br />
          【分けて回答する際の回答方法】<br />
          回答については、「次に進む」ボタンを押すことでそこまでの回答が保存されます。<br />
          一旦回答を中断するには、「次に進む」ボタンを押し、その後ブラウザの閉じるボタンで終了させてください。<br />
          調査の回答を再開したい場合は、もう一度URLにアクセスいただくとログインページが表示されますので、「ID」「パスワード」を入力すると、保存された次のページから回答を再開することができます。<br />
          再開前に保存されている回答画面に戻りたい場合は、すべての設問に御回答いただいたあと回答の確認ページが表示されますので、そのページから戻りたい回答画面までお戻りください。</p>
        <p>・複数の調査画面を同時に開きますと、正常に回答できない可能性があります。同時に複数の画面で御回答なさらないように御注意ください。</p>
        <p>・この調査への回答は、Internet Explorer 10もしくは11を推奨環境とさせていただいております。</p>
        </td>
    </tr>
  </table>
  <br />
  <table width="740" border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td height="25" colspan="2" align="left" valign="middle"><p><img src="images/bar_caution2-2.gif" width="328" height="25" /></p></td>
    </tr>
    <tr>
        <td width="26" height="150" align="left" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td  width="714" align="left" valign="middle">
          <div style="width : 740px; height : px; border : 0px; margin-bottom: 30px; font-size: 16px; margin-top: 20px;">
            <div>
              <p>※　お答えいただく問数は、選択される回答により異なりますが、属性を除き最大で18問になります。<br />
        Ⅰ．属性<br />
        Ⅱ．ボランティア活動について（最大6問）<br />
        Ⅲ．寄付について（最大7問）<br />
        Ⅳ．NPO全般について（最大5問）<br />
        <br />
              ※　特に指定のない質問に関しては回答時の状況で御入力ください。<br />
              ※　名宛人による回答をお願いいたします。<br />
                  なお、名宛人以外の第三者による回答は御遠慮願います。</p>
            </div>


            <div class="unei-link"><a href="http://navit-j.net/chousa/shimin/2014_shimin_chousa.pdf" target="_blank">■設問一覧（PDFファイル）</a><br />
              <br />
            </div>
            <div><img src="images/border1.gif" width="740" height="3" /></div>
            <div class="pdf">
              <p><strong>◆PDFファイルを御覧いただくには、PDFが閲覧できるソフトAdobe Readerをインストールしてください。</strong></p>
              <p>PDFファイルを御覧いただくためには、お持ちのパソコンに
                Adobe Reader（アドビ リーダー）というソフトをインストールする必要があります。<br />
                Adobe ReaderのボタンをクリックしてAdobe社のホームページから、ソフトをインストールしてください。<br />
                ※ダウンロードは無償です。</p>
            </div>
            <div class="section" id="installation-procedure">
              <h3><span class="adobe-reader"><a href="http://www.adobe.com/go/JP-H-GET-READER" target="_blank"><img src="images/btn_adobe_reader.png" alt="Get Adobe Reader" width="88" height="31" /></a></span></h3>
              <p><strong>&lt; インストール手順 &gt;</strong></p>
              <p>1.上のボタンをクリックするとAdobe社のAdobe Readerのページへリンクされます。<br />
                2.リンク先の画面の説明に従ってAdobe Readerをインストールしてください。<br />
                3.インストールが完了した後、PDFファイルをクリックするとAdobe Readerが自動的に立ち上がり、ファイルを開くことができます。<br />
                PDFについての詳細はAdobe社のホームページで御確認いただけます。 <a href="http://www.adobe.com/jp/products/acrobat/adobepdf.html" target="_blank">PDFについてはこちら</a></p>
            </div>
          </div>
        </td>
    </tr>
</table>
  <table width="740" border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td height="25" colspan="2" align="left" valign="middle"><p><img src="images/bar_caution3.gif" width="328" height="25" /></p></td>
    </tr>
    <tr>
      <td width="26" height="0" align="left" valign="middle">&nbsp;</td>
      <td width="714" height="80" align="left" valign="middle"><p>業務請負先である株式会社ナビットでは、あなた様の氏名、住所、年齢、性別が記載された「調査対象一覧表」を厳重に管理しています。一方、調査票には、氏名等の個人情報の記載を求めない事としており、誰がどのように回答したかがわからないようになっています。なお、「調査対象一覧表」と「回収した調査票」は調査終了から1年後に廃棄します。 </p></td>
    </tr>
  </table>
  <br />
  <table width="740" border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td height="25" colspan="2" align="left" valign="middle"><p><img src="images/bar_caution4.gif" width="328" height="25" /></p></td>
    </tr>
    <tr>
      <td width="26" height="0" align="left" valign="middle">&nbsp;</td>
      <td width="714" height="80" align="left" valign="middle"><p><strong>【調査の設問・回答方法等に関するお問い合わせ】</strong>　<u>※FAX・電子メールで調査票を送る場合の送付先</u><br />
          <br />
          <strong> 株式会社ナビット　</strong>（担当）堀田、野村、立川<br />
          所在地：〒101-0051 東京都千代田区神田神保町3-10-2共立ビル3Ｆ<br />
          電話（フリーダイヤル）：0120-964-603　（受付時間：平日10時～18時）※携帯電話からも御利用頂けます。<br />
          ＦＡＸ：03-5215-5702<br />
          Ｅメール：<a href="mail to:shimin@navit-j.com">shimin@navit-j.com</a>　／　ホームページ：<a href="http://www.navit-j.com/" target="_blank">http://www.navit-j.com/</a><br />
        
        <p>お問い合わせは<a href="./index.php?action_e25=true" target="_blank">こちら</a></p>
        <br />
        <strong>【調査の趣旨・実施体制等に関するお問い合わせ】 </strong><br />
        <br />
        <strong>内閣府</strong>　政策統括官（経済社会システム担当）付　参事官（市民活動促進担当）付<br />
        （担当）山下、立福 <br />
        電話：03-5253-2111㈹ (内線　32393，32396)<br />
        <br />
        内閣府NPOホームページ：<a href="https://www.npo-homepage.go.jp/" target="_blank">https://www.npo-homepage.go.jp/</a>　　（ 「内閣府 NPO」 で検索）
        </p></td>
    </tr>
  </table>
  <br />
  <table width="700" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
    </tr>
  </table>
  <div style="width : 740px; height : px; border : 0px; margin-bottom: 30px; font-size: 16px; margin-top: 20px;"> 
    <!--
    <div><img src="images/border1.gif" width="740
" height="3" /></div>
-->
    <div style="text-align:center; margin:10px 0 20px 0; width: 740px;">
        <form action="{$script}" name="f2" method="{$form_action}">
            <input type="hidden" name="action_e02" value="true">
            <img onClick="javascript:do_submit();" src="images/btn1.gif" alt="回答開始" width="258" height="47" style="cursor:pointer;" /><br />
        </form>
    </div>
  </div>
  <div class="section">
    <dt>&nbsp;</dt>
    <a href="http://www.adobe.com/jp/products/acrobat/adobepdf.html" target="_blank"></a> </div>
</div>
<div><img src="images/border1.gif" width="740
" height="3" /></div>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>


{$footer}
<!--フッター-->
{$app_ne.footer}
<!--フッター-->
</body>
</html>
