<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>助成金・補助金検索結果一覧｜助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」の助成金・補助金検索結果一覧">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="stylesheet" href="css/first.css" type="text/css">
<link rel="stylesheet" href="css/ditail.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<style type="text/css">
<!--
@media print {

#header,
#result_title_img,
#gnavi,
#back_result1,
#back_result2,
#interest_offer,
.re_top_first,
.link_text_box,
#footer
{
display: none;
}


#proposal_ditail {

border: #000000 solid 1px;
background-color: #ffffff;
width: 933px;
/*height: 1019px;*/
padding: 4px;
margin-left: 18px;
float: left;
box-shadow: 0px 0px 00px #ffffff;
}
.link_text {
		text-align: center;
		width:130px;
	font-size:12px;
	font-family:"メイリオ", Meiryo;
	font-weight:normal;
	border:1px solid #000000;
	padding:2px 76px;
	text-decoration:none;
	color:#000000;
	display:inline-block;
}
.ditail_box {
font-size: 11px;
width:933px;
margin:30px 0 0 15px;
}
.ditail_title_head_text {
width:850px;
font-size: 10px;
font-family:"メイリオ", Meiryo;
padding:50px 0 0 30px;
margin-bottom:-18px;
margin-top:-36px;
}
.ditail_title_text {
/*width:200px;*/
font-size: 20px;
font-family:"メイリオ", Meiryo;
font-weight:bold;
padding:20px 0 0 30px;
margin-top: -20px;
}
.ditail_midashitext {
font-size: 20px;
font-family:"メイリオ", Meiryo;
font-weight:bold;
margin:0 0 10px 0px;
}
.ditail_midashiback {
width:400px;
margin:30px 0 0 30px;
background-image: url("");
}
.ditail_midashiback_left {
width:0px;
margin:30px 0 0 30px;
background-image: url("");
}
.ditail_midashiback_right {
width:0px;
margin:30px 67px 0 30px;
background-image: url("");
}


}
-->
</style>

<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.lightbox_me.js"></script>

<script type="text/javascript">
function do_submit(){
	document.f2.submit();
}
</script>

<script type="text/javascript">
function displaySendForm(){
	$("#send_interest_confirm").lightbox_me({centered: true,closeSelector:'#send_confirm_close',overlayCSS:{background:'#C2D6CD',opacity: .8}});
}

$( document ).ready( function() {
	$( "#favorite_button" ).click( function() {

		var isFavorite = $( "#is_favorite"     ).val();
		var conentKind = $( "#in_content_type" ).val();
		var contentId  = $( "#in_contents_id"  ).val();
		var add        = true;
		if ( $( "#is_favorite" ).val() == "1" ) {
			add = false;
		}

		$.ajax( {
			type: "POST",
			url: "index.php",
			data: "action_favorite=true&in_add=" + add + "&in_content_type=" + conentKind + "&in_contents_id=" + contentId,
			success: function(msg){
				$( "#is_favorite" ).val( msg );
				setFavoriteButtonName();
			}
		} );
	} );
	setFavoriteButtonName();
});

function setFavoriteButtonName()
{
	if ( $( "#is_favorite" ).val() == "1" ) {
		$( "#favorite_button" ).val( "お気に入りから削除" );
	}
	else {
		$( "#favorite_button" ).val( "お気に入りに追加" );
	}
}

</script>
{/literal}
</head>

<body>

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}

{include file='common/navi.tpl'}


<!-- ここからメインコンテンツ -->
<!-- ここから検索結果詳細 -->
<img id="result_title_img" src="img/title_proposal_ditail.jpg" alt="案件検索結果詳細" />

{if $app.rank != 0}

<form action="index.php#favorite_autonomy_projects" method="get">
	<input type="hidden" name="action_mypage" value= "true"/>
	<input class="favorite_button2" type="submit" value= "お気に入り案件一覧"/>
</form>
{/if}

<div id="proposal_ditail">

{if $app.rank != 0}
<form name="favorite">
	<input type="hidden" id="is_favorite"     name="is_favorite"     value="{$app_ne.is_favorite}" />
	<input type="hidden" id="in_content_type" name="in_content_type" value="{$app_ne.content_type}" />
	<input type="hidden" id="in_contents_id"  name="in_contents_id"  value="{$app_ne.data.contents_id}" />
	<input class="favorite_button3" type="button" id="favorite_button"  value="お気に入りに追加" />
</form>
{/if}
	<div class="ditail_box">


		{if $app_ne.type == 'foundation'}
			<div id="mainArea">
				<div id="mainContents">
					<div id="frame">
						<div id="contents">
							<div class="titleBox clearfix">
								<div class="ditail_title_head_text">{$app_ne.data.sub_title_1}</div>
								<h1 class="ditail_title_text">{$app_ne.data.main_title}</h1>
								<div style="margin-left:42px;margin-right:78px;">
									<p class="ditail_title_subtext">{$app_ne.data.sub_title_2}</p>
								</div>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">事業コード</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.businessform_text}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">分野コード</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.field_text}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">内容</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.target|nl2br}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">募集</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.fund|nl2br}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">応募制限</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.limitation|nl2br}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">決定時期</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.judge_season|nl2br}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">選考方法</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.judge_method|nl2br}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">助成団体</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.contact|nl2br}</p>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		{elseif $app_ne.type == 'government'}
			<div id="mainArea">
				<div id="mainContents">
					<div id="frame">
						<div id="contents">

							<div class="titleBox clearfix">
								<div class="ditail_title_head_text">{$app_ne.data.sub_title_1}</div>
								<h1 class="ditail_title_text">{$app_ne.data.main_title}</h1>
								<div style="margin-left:42px;margin-right:78px;">
									<p class="ditail_title_subtext">{$app_ne.data.sub_title_2}</p>
								</div>
							</div>
							<div class="officer">登録機関：{$app_ne.data.supplier}</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">目的</h2>
							</div>

							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.purpose}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">対象者の詳細</h2>
							</div>

							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.target|nl2br}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">支援内容・支援規模</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.comments|nl2br}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">募集期間</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.season|nl2br}</p>
							</div>

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">対象期間</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.period|nl2br}</p>
							</div>

							{if $app_ne.data.web_page}
								<div class="buttonBoxGray link_text_box">
									<a href="{$app_ne.data.web_page}" target="_blank"><span class="link_text">WEBサイト</span></a>
								</div>
							{/if}

							<div class="ditail_midashiback">
								<h2 class="ditail_midashitext">問い合わせ先</h2>
							</div>
							<div style="margin-left:42px;margin-right:78px;">
								<p>{$app_ne.data.contact|nl2br}</p>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		{/if}
	</div>

	<div id="interest_offer">
		{if $app_ne.is_guest}
				<a href="javascript:void(0);" onclick="displaySendForm();"><img src="img/shinsei_supp_non.png" onmouseover="this.src='img/shinsei_supp_non_d.png'" onmouseout="this.src='img/shinsei_supp_non.png'"   alt="この案件にチャレンジしたい方はこちら" /></a>

			{else}

			{if $app.rank != 0}
				<form action="index.php" name="f2" method="post" id="f2">
					<input type="hidden" name="action_sendoffer" value="true" />
					<input type="hidden" id = "in_callkind" name="in_callkind" value="{$app_ne.in_callkind}" />
					<input type="hidden" id = "in_contents_id" name="in_contents_id" value="{$app_ne.data.contents_org_id}" />
				</form>
				<a id="send_offer" href="blog/?p=3988"><img src="img/shinsei_supp.png" onmouseover="this.src='img/shinsei_supp_d.png'" onmouseout="this.src='img/shinsei_supp.png'"   alt="この案件にチャレンジしたい方はこちら" /></a>
				</div>

			{else}
				<form action="index.php" name="f2" method="post" id="f2">
					<input type="hidden" name="action_sendoffer" value="true" />
					<input type="hidden" id = "in_callkind" name="in_callkind" value="{$app_ne.in_callkind}" />
					<input type="hidden" id = "in_contents_id" name="in_contents_id" value="{$app_ne.data.contents_org_id}" />
				</form>
				<a id="send_offer" href="blog/?p=2395"><img src="img/shinsei_supp_non.png" onmouseover="this.src='img/shinsei_supp_non_d.png'" onmouseout="this.src='img/shinsei_supp_non.png'"   alt="この案件にチャレンジしたい方はこちら" /></a>
				</div>
			{/if}
		{/if}
	</div>

	<form class="submit_form" action="index.php" method="get">
		<input type="hidden" value= "true"/>
		<input type="submit" class="submit_btn2" value= "検索画面へ戻る"/>
	</form>

</div>
<br />

<!-- ここから送信確認 -->
<div id="send_interest_confirm" style="display:none; height:330px;">

	<div style="width:100%;">
		<div style="width:24px;">
			<a href="javascript:void(0);" style="position:relative; top:-10px; left:592px;"><img id="send_confirm_close" src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
		</div>
	</div>
	<div style="font-size:20px; color:#22a90c; text-align:center;">
『助成金なう』のご利用には会員登録が必要となります。<br />まずは無料でお試しください！<br />

	<div style="text-align:center; margin-top:20px;">
		<a class="button5" href="./index.php?action_secure_registration=true"  id="registration">今すぐ無料会員登録！</a>
	</div>

	<div style="text-align:center; margin-top:20px;">
		<a class="button5" href="./index.php?action_login=true" onclick="" id="login_btn">すでに会員の方はこちらからログイン</a>
	</div>

	<div style="text-align:center; margin-top:20px;">
		<a class="button5" href="./lp.html" onclick="" id="login_btn"><img src='img/wakaba_mark.png' width="17px" height="25px" style="display:inline; margig-top:10px;"/> 助成金なうについて詳しくはこちら</a>
	</div>

	</div>

</div>
<!-- ここまで送信確認 -->


<!-- ここまで検索結果詳細 -->

<!-- ここからトップへ戻る -->

	<div class="clear"></div>
	<div class="re_top_first">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear"></div>
<!-- ここまでトップへ戻る -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->


</div>
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>
{/literal}

{include file='common/track.tpl'}

</body>
</html>

