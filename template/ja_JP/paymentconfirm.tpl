<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	$('label').balloon();
});

function do_submit(kind){
	if(kind=='back'){
		document.f2.submit();
	}else{
		document.f3.submit();
	}
}

</script>
{/literal}
</head>
<body>

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->

<!-- ここから入力フォーム --><div id="03"></div>

<br />
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">ご入力内容の確認</span></div>

{if $app.error}
<div style="margin-left: 120px;">{$app.error}</div>
{/if}

<div style="margin-left: 120px;">以下の内容でよろしければ、『送信』ボタンを押してください。<br />修正する場合は、『戻る』ボタンを押して該当箇所を修正してください。</div>
<div style="font-size:0.7em;margin-left: 120px;">
</div>

<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
	<tr>
		<td class="l_Cel_01_02">支払方法</td>
		<td class="s_Cel_01" style="text-align:left;">
			{if $app.data.payment_method == 1}
				クレジットカード決済
			{else}
				銀行振込
			{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">ご利用期間/料金</td>
		<td class="s_Cel_01" style="text-align:left;">
			12ヶ月 \12,960(税込)
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">入金予定日</td>
		<td class="s_Cel_01" style="text-align:left;">
			{if $app.data.payment_method == 1}
				即日
			{else}
				{$app.data.pay_date}
			{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">開始希望日</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.begin_date}
		</td>
	</tr>

	<tr>
		<td colspan="3">
				<!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
				<!-- ここまでトップへ戻る -->
		</td>
	</tr>
</table>
</div>
<div class="mod_form_btn">
		<div style="margin-top:20px;margin-left:280px;">
			<form action="{$script}" name="f2" method="post" id="f2">
				<input type="hidden" name="action_paymentconfirm" value="true">
				<a class="button" href="javascript:void(0)" onclick="javascript:do_submit('back');" id="back_btn">戻る</a>
				<input type="hidden" name="back" id="back" value="1">
			</form>
			</div>
		<div style="margin-top:-46px;margin-left:500px;">
			<form action="{$script}" name="f3" method="post" id="f3">
				<input type="hidden" name="action_paymentconfirm" value="true">
				<a class="button2" href="javascript:void(0)" onclick="javascript:do_submit('send');" id="send_btn">送信</a>
				<input type="hidden" name="send" id="send" value="1">
			</form>
		</div>
	</div>
</div>

<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
	// #で始まるアンカーをクリックした場合に処理
	$('a[href^=#]').click(function() {
		// スクロールの速度
		var speed = 700; // ミリ秒
		// アンカーの値取得
		var href= $(this).attr("href");
		// 移動先を取得
		var target = $(href == "#" || href == "" ? 'html' : href);
		// 移動先を数値で取得
		var position = target.offset().top;
		// スムーススクロール
		$('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	});
});
</script>
<!-- スムーズスクロール -->

{/literal}

{include file='common/track.tpl'}

</body>
</html>
