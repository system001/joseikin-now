<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal} 
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript">
//吹き出し
$(window).load(function(){
      $('label').balloon();
      
      
    if ($('#privacy_1').is(':checked')) {
        $('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
    } else {
        $('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
    }
});


  	
//全選択・全解除
$(function() {
    $('#all_check').on("click",function(){
        $('.CheckList').prop("checked", true);
    });
});
$(function() {
    $('#all_clear').on("click",function(){
        $('.CheckList').prop("checked", false);
    });
});

$(function() {
    $('#all_check2').on("click",function(){
        $('.CheckList2').prop("checked", true);
    });
});
$(function() {
    $('#all_clear2').on("click",function(){
        $('.CheckList2').prop("checked", false);
    });
});

$(function() {
    $('#all_check3').on("click",function(){
        $('.CheckList3').prop("checked", true);
    });
});
$(function() {
    $('#all_clear3').on("click",function(){
        $('.CheckList3').prop("checked", false);
    });
});

//リセット(自治体)
$(function() {
    $('#a_reset').on("click",function(){
        //ラジオボタン初期値セット
        $('#in_kind_1').prop("checked", true);
        $('#in_kind_2').prop("checked", false);
        //プルダウン初期値セット
        $('select[name="in_area1"]').val("");
        $('#in_area2').html('');//一度select内を空に
        $('#in_area2').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');
        
        //チェックボックスクリア
        $('.CheckList').prop("checked", false);
        
        $("#a_reset").blur();
    });
});
//リセット(財団)
$(function() {
    $('#f_reset').on("click",function(){
        //チェックボックスクリア
        $('.CheckList2').prop("checked", false);
        $('.CheckList3').prop("checked", false);
        $('#in_keyword').val("");
        $("#f_reset").blur();
    });
});

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit(){
    document.f2.submit();
}

/*
 　　全角->半角変換
 */
jQuery(function(){
 
    // 郵便番号の処理
    $('.zip-number').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字のみ残す
        var zenkakuDel = new String( hankaku ).match(/\d/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
 
        $(this).val(zenkakuDel);
    });
    // 電話番号の処理
    $('.tel-number').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字と+-のみ残す
        var zenkakuDel = new String( hankaku ).match(/\d|\-|\+/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
        
        $(this).val(zenkakuDel);
    });
 
    // メールアドレスの処理
    $('.mail-address').change( function(){
        var zenkigou = "＠－ー＋＿．，、";
        var hankigou = "@--+_...";
        var data = $(this).val();
        var str = "";
 
        // 指定された全角記号のみを半角に変換
        for (i=0; i<data.length; i++)
        {
            var dataChar = data.charAt(i);
            var dataNum = zenkigou.indexOf(dataChar,0);
            if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
            str += dataChar;
        }
        // 定番の、アルファベットと数字の変換処理
        var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
        $(this).val(hankaku);
    });
 
});

//確認ボタンの有効無効

jQuery(function(){
    $('#privacy_1').change(function(){
            if ($(this).is(':checked')) {
                    $('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
            } else {
                    $('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
            }
    });
});  


</script>

{/literal} 
</head>
<body>
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_login_do" value="true">
<!-- ここからconteinar -->
<div id="conteinar">
<!-- ここからwrapper -->
	<div id="wrapper">

<!-- ここからheader --><div id="01"></div>
		<div id="header">

			<div id="logo">
				<a href="index.php" ><img src="img/logo.jpg" alt="助成金なう" /></a>
			</div>

			<div id="h1">
				<h1>助成金・補助金の検索サービス「助成金なう」</h1>
			</div>

			<div id="mini_contact">
				<img src="img/m_contact.gif" alt="お問合せ" />
			</div>

			<div id="mini_contact_txt">
				<a href="https://www.navit-j.com/contactus/" target="_blank">お問合せ</a>
			</div>

			<div id="mini_sitemap">
				<img src="img/m_sitemap.gif" alt="サイトマップ" />
			</div>

			<div id="mini_sitemap_txt">
				<a href="#">サイトマップ</a>
			</div>

			<div id="freedial">
				<img src="img/freedial.jpg" alt="0120-937-781" />
			</div>

			{if $app.name !=""}
                        <div style="display:inline-block;width:360px;">
                            <table  border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#f00">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <span id="logininfo">{$app.name} 様　ログイン中</span>   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <a href="./index.php?action_logout_do=true" id="logout_btn">ログアウト</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>    
                        </div>
                    {else}
                        <div id="newaccount">
                            <a class="button3" href="./index.php?action_secure_registration=true"  id="registration">新規会員登録</a>
                        </div>
                        <div id="login">
                            <a class="button3" href="./index.php?action_login=true" onclick="" id="login_btn">ログイン</a>
                        </div>
                    {/if}

		</div>

		<div class="clear">
		</div>
<hr id="head-hr" color="#539d0a" width="100%" noshade>
<!-- ここまでheader -->

<!-- ここからG NAVI -->

<!--
<div id="gnavi">
	<ul>
		<li style="height:93px;">
			<a href="#01"><img src="img/gnavi_home.jpg" alt="ホーム" width="143px" height="94px"></a>
		</li>
		<li style="height:93px;">
			<a href="#02"><input type="image" onClick="" src="img/gnavi_news.jpg" onMouseOver="this.src='img/gnavi_news_d.jpg'" onMouseOut="this.src='img/gnavi_news.jpg'" alt="お知らせ" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="#03"><input type="image" onClick="" src="img/gnavi_a_search.jpg" onMouseOver="this.src='img/gnavi_a_search_d.jpg'" onMouseOut="this.src='img/gnavi_a_search.jpg'" alt="自治体案件検索" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="#04"><input type="image" onClick="" src="img/gnavi_f_search.jpg" onMouseOver="this.src='img/gnavi_f_search_d.jpg'" onMouseOut="this.src='img/gnavi_f_search.jpg'" alt="財団案件検索" width="142px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="#05"><input type="image" onClick="" src="img/gnavi_sem.jpg" onMouseOver="this.src='img/gnavi_sem_d.jpg'" onMouseOut="this.src='img/gnavi_sem.jpg'" alt="セミナー・イベント" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="#06"><input type="image" onClick="" src="img/gnavi_com.jpg" onMouseOver="this.src='img/gnavi_com_d.jpg'" onMouseOut="this.src='img/gnavi_com.jpg'" alt="会社概要" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="https://www.navit-j.com/contactus/" target="_blank"><input type="image" onClick="" src="img/gnavi_ccontact.jpg" onMouseOver="this.src='img/gnavi_ccontact_d.jpg'" onMouseOut="this.src='img/gnavi_ccontact.jpg'" alt="お問合せ" width="143px" height="93px"></a>
		</li>
	</ul>

</div>
		<div class="clear">
		</div>
 -->
<!-- ここまでG NAVI -->


<!-- ここからメインコンテンツ -->


<!-- ここから入力フォーム --><div id="03"></div>

	<!--<img src="img/title_a_search_line.jpg" alt="" />-->
        <br />
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">ログイン</span></div>
<div style="margin-left: 296px;">ログインIDとパスワードを入力してください。<br /></div>
<div style="font-size:0.7em;margin-left: 120px;">
    <span>
    </span>
</div>        
<br />

<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 208px;">
    <tr>
        <td style="text-align:right;">ログインID</td>
        <td>
            <input type="text" name="in_id" value="{$app.data.id}"  size="30" maxlength="64" style="font-size:20px;width:300px;">
            
        </td>
    </tr>
    <tr>
        <td style="text-align:right;">パスワード</td>
        <td>
            <input type="password" name="in_pw"  size="30" maxlength="64" style="font-size:20px;width:300px;">
            
        </td>
    </tr>
    <tr>
        <td style="text-align:right;vertical-align top;">文字認証</td>
        <td>
            <img src="index.php?action_captcha=true" style="margin-bottom:5px" id ='captcha'>
            <input type="text" name="in_captcha_code" size="15" maxlength="8" style="font-size:20px;width:300px;" /><br />
            <a href="#" onclick="document.getElementById('captcha').src = 'index.php?action_captcha=true' + Math.random(); return false">[画像変更]</a>
        </td>
    </tr>
    <tr>
        <td colspan=2>
            <div class="mod_form_btn">
                <div style="margin-top:20px;margin-left:190px;"><a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();" id="login_btn">ログイン</a></div>
            </div>
        </td>
    </tr>
</table>
<div class="request" style="font-size:16px; margin-top:20px; text-align:center;">
    <a href="https://www.navit-j.com/contactus/" target="_blank">ログインIDやパスワードを忘れた場合はこちらよりお問い合わせください。</a>
</div>
<!--エラーメッセージ-->
{if count($errors)}
<table class="error" align="center">
{foreach from=$errors item=error}
<tr class="error" style="color:red;"><td>{$error}</td></tr>
{/foreach}
</table>
{/if}
<!--エラーメッセージ-->
</div>

</div>
</form>                
	<!-- ここまで入力フォーム -->









</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">
	
</div>

<!-- ここまでfooter -->


</div>


<!-- スムーズスクロール -->
{literal}
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 700; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
</script>
<!-- スムーズスクロール -->

<!-- グローバルナビ -->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->

<!-- グローバルナビ -->

<!-- ▼************ Google Anarytics トラッキングコード ************ ▼-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- ▲ *********** Google Anarytics トラッキングコード ************ ▲-->

<!-- googleanalytics TAG -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-12', 'auto');
  ga('send', 'pageview');

</script>
<!-- googleanalytics TAG -->



<!-- LF innov TAG 20150127 -->

<script type="text/javascript">
var _trackingid = 'LFT-10394-1';

(function() {
  var lft = document.createElement('script'); lft.type = 'text/javascript'; lft.async = true;
  lft.src = document.location.protocol + '//track.list-finder.jp/js/ja/track.js';
  var snode = document.getElementsByTagName('script')[0]; snode.parentNode.insertBefore(lft, snode);
})();
</script>

<!-- LF innov TAG 20150127 -->

{/literal}


</body>
</html>