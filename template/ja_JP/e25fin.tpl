{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}



<div id="contents">
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar19.gif" width="740" height="30" /></div>

	
  <div class="box">
		<div>
			<p class="center_text">お問い合わせありがとうございます。<br />
			<br />
			お送りいただきました内容を確認の上、3営業日以内で回答致します。<br />
		3営業日過ぎても回答が来ない場合は、お手数ですが電話にてお問い合わせください。<br />
		<br />
		<strong> 株式会社ナビット　</strong>（担当）堀田、野村、立川 <br />
		電話（フリーダイヤル）：0120-964-603　（受付時間：平日10時～18時）<br />
		※携帯電話からも御利用頂けます。 </p>
		<p class="center_text">ブラウザの閉じるボタンで画面を閉じて終了してください。</p>
		</div>
	</div>
	<div class="center_text_link">
		<p><a href="https://www.npo-homepage.go.jp/">内閣府NPOホームページ</a></p>
	</div>
	
	<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>
</div>


{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<br />
<br />
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
