<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金検索結果一覧｜助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」の助成金・補助金検索結果一覧">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="stylesheet" href="css/first.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script>
	$(window).load(function(){
//		$("#gnavi").sticky({ topSpacing: 0, center:true, className:"hey" });
	});
</script>

{/literal}
</head>

<body>

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}

{include file='common/navi.tpl'}

<!-- ここからメインコンテンツ -->
<!-- ここから検索結果 -->

{if $app_ne.content_mode == 'recommend'}
	<img src="img/title_recommend.jpg" alt="旬な助成金・補助金" />
{else}
	<img src="img/title_search_result.jpg" alt="案件検索結果" />
{/if}

<input type="hidden" name="action_detail" value="true">
<div id="searchresult">

	<form method="post" action="index.php" id="f2" name="f2" style="display:none;"/>
		<input type="hidden" name="action_display" value="1" />
		<input type="hideen" name="in_callkind" value="" />
		<input type="hidden" name="in_contents_id" value="" />
	</form>

	{if $app_ne.content_mode != 'recommend'}
		<div id="result" align="center">
			<span style="font: bold 20px 'メイリオ',Meiryo; color: orange;">
				条件に一致する案件が{$app_ne.hits}件見つかりました
			</span>
		</div>
	{/if}

	{if $app_ne.hits > 0}
		{foreach from=$app_ne.list item=item name=items}
			<div class="box_left">
				<div class="matter_title">
						{$item.main_title}
				</div>
				<div class="matter_ditail">
						{$item.comments}
				</div>
				<div class="ditail_btn">
						<img class="item_detail" mode="{$item.content_mode}"  content_id="{$item.contents_org_id}" src="img/result_btn.png" onMouseOver="this.src='img/result_btn_d.png'" onMouseOut="this.src='img/result_btn.png'" alt="詳細はこちら">
				</div>
			</div>
			{if $smarty.foreach.items.index % 3 == 2}
				<div class="clear"></div>
			{/if}
		{/foreach}
	{else}
		<br />
		<div style="text-align:center;">対象の案件はありません。</div>
		<br />
	{/if}

	<div class="clear"></div>

	{if $app.rank > 0}
		<div class="re_top_first">
			<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
		</div>
		<div class="clear">
		</div>
	{/if}

{if empty($app.name) || $app.rank < 1}
<br />
<span style="font-size:70%;margin-left:20px;">※無料会員版は一度に表示できる件数を3件までとさせていただいております。</span><br />
<a href="http://www.joseikin-now.com/service/model_03.html" target="_blank"><span style="font-size:70%; color:red; margin-left:20px;">※有料サービスをご利用いただくと件数無制限で検索結果が表示されるようになります。</span></a>
{/if}

</div>
<!-- ここまでメインコンテンツ -->

</div>

<!-- ここからfooter -->
<br />
<div id="footer">
	
</div>

<!-- ここまでfooter -->

{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});

$('.item_detail').click(function(){
	document.f2.in_callkind.value = $(this).attr('mode');
	document.f2.in_contents_id.value = $(this).attr('content_id');
	document.f2.submit();
});

</script>
{/literal}

{include file='common/track.tpl'}

</body>
</html>
