{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!--
var btn_group_name1 = "in_q1";
var btn_group_name2 = "in_q2";
var trigger_id1 = "#in_q1_5";
var trigger_id2 = "#in_q2_5";
var txtarea_id1 = "#in_q1_no5_text";
var txtarea_id2 = "#in_q2_no5_text";
var zan_text_id1 = "#zan_q1_no5_text";
var zan_text_id2 = "#zan_q2_no5_text";

$(document).ready(function (){
    text_val1 = $(txtarea_id1).val();
    text_val2 = $(txtarea_id2).val();
    CountDownLength('zan_q1_no5_text', text_val1, 255);
    CountDownLength('zan_q2_no5_text', text_val2, 255);
    
    if($(trigger_id1+":checked").val()){
        $(txtarea_id1).removeAttr("disabled");
        $(txtarea_id1).css("background-color","#ffffff");
        $(zan_text_id1).css("color","#000000");
    }
    else{
        $(txtarea_id1).attr("disabled", "disabled");
        $(txtarea_id1).css("background-color","#d2d1c6");
        $(zan_text_id1).css("color","#d2d1c6");
    }

    if($(trigger_id2+":checked").val()){
        $(txtarea_id2).removeAttr("disabled");
        $(txtarea_id2).css("background-color","#ffffff");
        $(zan_text_id2).css("color","#000000");
    }
    else{
        $(txtarea_id2).attr("disabled", "disabled");
        $(txtarea_id2).css("background-color","#d2d1c6");
        $(zan_text_id2).css("color","#d2d1c6");
    }
});

$(function() {
    $("[name="+btn_group_name1+"]").on('click', TextAreaEnable1);
    $("[name="+btn_group_name2+"]").on('click', TextAreaEnable2);
    
    function TextAreaEnable1(){
        if ($(trigger_id1+":checked").val()) {
            $(txtarea_id1).removeAttr("disabled");
            $(txtarea_id1).css("background-color","#ffffff");
            $(zan_text_id1).css("color","#000000");
        } else {
            $(txtarea_id1).attr("disabled", "disabled");
            $(txtarea_id1).css("background-color","#d2d1c6");
            $(zan_text_id1).css("color","#d2d1c6");
        }
    }
    
    function TextAreaEnable2(){
        if ($(trigger_id2+":checked").val()) {
            $(txtarea_id2).removeAttr("disabled");
            $(txtarea_id2).css("background-color","#ffffff");
            $(zan_text_id2).css("color","#000000");
        } else {
            $(txtarea_id2).attr("disabled", "disabled");
            $(txtarea_id2).css("background-color","#d2d1c6");
            $(zan_text_id2).css("color","#d2d1c6");
        }
    }
});

//-->
</script>
{/literal}





<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}


<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e04" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar12.gif" width="740" height="30" /></div>

<div>
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10px;">
<div>
  <p><strong>［ボランティア活動に対する関心の有無］</strong></p>
</div>
  <p> <strong>問１</strong>　【全員の方にお聞きします】　<img src="images/m1.gif" width="128" height="19" />
    <br />
    あなたは、ボランティア活動に関心がありますか。ひとつお選びください。<br />
    {if is_error('in_q1')}<br /><span class="error">{message name="in_q1"}</span>{/if}
    <br />
    {$app_ne.radio.q1}
  </p>
<div> <br />
  <p><strong>［ボランティア活動経験の有無］</strong></p>
</div>
  <p id="q2"> <strong>問２</strong> 　【全員の方にお聞きします】<font color="red"><strong>【必須】</strong></font><img src="images/m2.gif" width="128" height="19" /><br />
    <br />
    あなたは、過去3年間にボランティア活動をしたことがありますか。ひとつお選びください。<br />
    {if is_error('in_q2')}<br /><span class="error">{message name="in_q2"}</span>{/if}
    <br />
    {$app_ne.radio.q2}
  </p>

<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e02=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->
<div><img src="images/border1.gif" width="740
" height="3" /></div>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>
</form>


                            
{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
