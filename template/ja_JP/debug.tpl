<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>debug</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal} 
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>

<script>
    $(window).load(function(){
    });
</script>
<script type="text/javascript">
//吹き出し
$('label').balloon();


$(function(){
});  
  	
//全選択・全解除
$(function() {
    $('#all_check').on("click",function(){
        $('.CheckList').prop("checked", true);
    });
});
$(function() {
    $('#all_clear').on("click",function(){
        $('.CheckList').prop("checked", false);
    });
});

$(function() {
    $('#all_check2').on("click",function(){
        $('.CheckList2').prop("checked", true);
    });
});
$(function() {
    $('#all_clear2').on("click",function(){
        $('.CheckList2').prop("checked", false);
    });
});

$(function() {
    $('#all_check3').on("click",function(){
        $('.CheckList3').prop("checked", true);
    });
});
$(function() {
    $('#all_clear3').on("click",function(){
        $('.CheckList3').prop("checked", false);
    });
});

//リセット(自治体)
$(function() {
    $('#a_reset').on("click",function(){
        //ラジオボタン初期値セット
        //$('#in_kind_1').prop("checked", true);
        //$('#in_kind_2').prop("checked", false);
        //チェックボックスクリア
        $('.CheckList4').prop("checked", false);
        
        //プルダウン初期値セット
        $('select[name="in_area1"]').val("");
        $('#in_area2').html('');//一度select内を空に
        $('#in_area2').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');
        
        //チェックボックスクリア
        $('.CheckList').prop("checked", false);
        $('#in_keyword1').val("");
        
        $("#a_reset").blur();
    });
});
//リセット(財団)
$(function() {
    $('#f_reset').on("click",function(){
        //プルダウン初期値セット
        $('select[name="in_area3"]').val("");
        $('#in_area4').html('');//一度select内を空に
        $('#in_area4').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');

        //チェックボックスクリア
        $('.CheckList2').prop("checked", false);
        $('.CheckList3').prop("checked", false);
        $('#in_keyword2').val("");
        $("#f_reset").blur();
    });
});

function changeCity2(sel){

    var pref_code = sel.options[sel.selectedIndex].value;
    if(pref_code==""){
        $('#in_area2').html('');//一度select内を空に
        $('#in_area2').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');
    }
}


function changeCity4(sel){

}

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit_with_param(param){
    document.f2.in_callkind.value = param;
    document.f2.submit();
}



</script>

{/literal} 


{if $app.name}
{literal} 
<script type="text/javascript">
$(document).ready(function(){
	alert('ready user');
});
{/literal} 
{else}
{literal} 
<script type="text/javascript">
$(document).ready(function(){
	alert('ready guest');
});
{/literal} 
{/if}


</script>
</head>
<body>
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_searchresult" value="true">
<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
	<div id="wrapper">

<!-- ここからheader --><div id="01"></div>
		<div id="header">

			<div id="logo">
				<a href="index.php" ><img src="img/logo.jpg" alt="助成金なう" /></a>
			</div>

			<div id="h1">
				<h1>助成金・補助金の検索サービス「助成金なう」</h1>
			</div>

			<div id="mini_contact">
				<img src="img/m_contact.gif" alt="お問合せ" />
			</div>

			<div id="mini_contact_txt">
				<a href="https://www.navit-j.com/contactus/" target="_blank">お問合せ</a>
			</div>

			<div id="mini_sitemap">
				<img src="img/m_sitemap.gif" alt="運営会社" />
			</div>

			<div id="mini_sitemap_txt">
				<a href="http://navit-j.com/" target="_blank">運営会社</a>
			</div>

			<div id="freedial">
				<img src="img/freedial.jpg" alt="0120-937-781" />
			</div>

                    {if $app.name !=""}
                        <div style="display:inline-block;width:360px;">
                            <table  border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#f00">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2" align="left">
                                                    <span id="logininfo">{$app.name} 様　ログイン中</span>   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <a href="./index.php?action_logout_do=true" id="logout_btn">ログアウト</a>
                                                </td>
                                                <td align="right">
                                                    <a href="./index.php?action_mypage=true" id="mypage_btn_on">マイページ</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>    
                        </div>
                    {else}
                        <div id="newaccount">
                            <a class="button3_a" href="https://www.navit-j.com/service/joseikin-now/index.php?action_secure_registration=true"  id="registration">新規会員登録</a>
                        </div>
                        <div id="login">
                            <a class="button3_a" href="./index.php?action_login=true" onclick="" id="login_btn">ログイン</a>
                        </div>
                        <div id="mypage_btn">
                            <a class="button3_a" href="./index.php?action_mypage=true" onclick="" id="mypage_btn_off">マイページ</a>
                        </div>
                    {/if}

		</div>

		<div class="clear">
		</div>

<!-- ここまでheader -->

<!-- ここからG NAVI -->


<div id="gnavi">
	<ul>
		<li style="height:93px;">
			<a href="#01"><img src="img/gnavi_home.jpg" alt="ホーム" width="143px" height="94px"></a>
		</li>
		<li style="height:93px;">
			<a href="lp.html" target="_blank"><img src="img/gnavi_news.jpg" onmouseover="this.src='img/gnavi_news_d.jpg'" onmouseout="this.src='img/gnavi_news.jpg'" /></a>
		</li>
		<li style="height:93px;">
			<a href="https://www.navit-j.com/service/joseikin-now/blog/" target="_blank" /><img src="img/gnavi_a_search.jpg" onmouseover="this.src='img/gnavi_a_search_d.jpg'" onmouseout="this.src='img/gnavi_a_search.jpg'" alt="助成金ブログ" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="https://www.facebook.com/joseikinnow" target="_blank /"><img src="img/gnavi_f_search.jpg" onmouseover="this.src='img/gnavi_f_search_d.jpg'" onmouseout="this.src='img/gnavi_f_search.jpg'" alt="助成金FB" width="142px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="seminar/index.html" target="_blank"><img src="img/gnavi_sem.jpg" onmouseover="this.src='img/gnavi_sem_d.jpg'" onmouseout="this.src='img/gnavi_sem.jpg'" alt="セミナー・イベント" width="143px" height="93px"></a>
		</li>

		<li style="height:93px;">
			<a href="https://www.navit-j.com/service/joseikin-now/blog/?cat=15" target="_blank"><img src="img/main_company.jpg" onmouseover="this.src='img/main_company_d.jpg'" onmouseout="this.src='img/main_company.jpg'" /></a>                    
		</li>

<!--入札なう-->
		<li style="height:93px;">
                    {if $app.name !=""}
			<a href="http://nyusatsu-now.com/" target="_blank"><img src="img/main_mypage.jpg" onmouseover="this.src='img/main_mypage_d.jpg'" onmouseout="this.src='img/main_mypage.jpg'" /></a>
                    {else}
			<a href="http://nyusatsu-now.com/" target="_blank"><img src="img/main_mypage.jpg" onmouseover="this.src='img/main_mypage_d.jpg'" onmouseout="this.src='img/main_mypage.jpg'" /></a>
                    {/if}   
		</li>

<!--マイページ	<li style="height:93px;">
                    {if $app.name !=""}
			<a href="./index.php?action_mypage=true"><img src="img/main_mypage.jpg" onmouseover="this.src='img/main_mypage_d.jpg'" onmouseout="this.src='img/main_mypage.jpg'" /></a>
                    {else}
			<a href="./index.php?action_login=true"><img src="img/main_mypage.jpg" onmouseover="this.src='img/main_mypage_d.jpg'" onmouseout="this.src='img/main_mypage.jpg'" /></a>
                    {/if}   
		</li>-->


<!--	<li style="height:93px;">
			<a href="/id/index.html"><img src="img/gnavi_ccontact.jpg" onmouseover="this.src='img/gnavi_ccontact_d.jpg'" onmouseout="this.src='img/gnavi_ccontact.jpg'" /></a>
		</li>-->

	</ul>

</div>
		<div class="clear">
		</div>

<!-- ここまでG NAVI -->

<!-- ここからメインビジュアル -->

<!--
	<img src="img/main_vis00.jpg" alt="助成金なうの特徴" usemap="#Map" border="0" />
    <map name="Map" id="Map">
      <area shape="circle" coords="125,120,103" href="#03" />
      <area shape="circle" coords="372,120,100" href="seminar/index.html" target="_blank" />
      <area shape="circle" coords="619,120,96" href="lp.html#7" target="_blank" />
      <area shape="circle" coords="870,120,99" href="service/model_03.html" target="_blank" />
    </map>
-->

<div id="mvis">
	<div style="float:left"><a href="#03" /><img src="img/main_vis01.jpg" onmouseover="this.src='img/main_vis01_d.jpg'" onmouseout="this.src='img/main_vis01.jpg'" alt="01助成金・補助金サーチ"></a></div>

	<div style="float:left"><a href="https://www.navit-j.com/service/joseikin-now/index.php?action_recommend=true" target="_blank" /><img src="img/main_vis02.jpg" onmouseover="this.src='img/main_vis02_d.jpg'" onmouseout="this.src='img/main_vis02.jpg'" alt="02旬な助成金・補助金"></a></div>

	<div style="float:left"><a href="https://www.navit-j.com/service/joseikin-now/blog/?p=470" target="_blank" /><img src="img/main_vis03.jpg" onmouseover="this.src='img/main_vis03_d.jpg'" onmouseout="this.src='img/main_vis03.jpg'" alt="03申請サポートサービス"></a></div>

	<div style="float:left"><a href="service/model_03.html" target="_blank" /><img src="img/main_vis04.jpg" onmouseover="this.src='img/main_vis04_d.jpg'" onmouseout="this.src='img/main_vis04.jpg'" alt="04有料会員サービス"></a></div>

	<div class="clear"></div>
<img src="img/main_vis05.jpg" alt="日本初！全国の官公庁、自治体、財団11187機関の情報を網羅中！" border="0" />

</div>

<!-- ここまでメインビジュアル -->

<!-- ここからメインコンテンツ -->

	<img src="img/title_a_search_line.jpg" alt="" />


<!-- キャンペーン img 
		<div style="margin:0 0 10px 0;">
<a href="http://www.navit-j.com/blog/?p=13190"><img src="img/jyo_ys_camp.jpg" onmouseover="this.src='img/jyo_ys_camp_on.jpg'" onmouseout="this.src='img/jyo_ys_camp.jpg'" alt="掲載記念プレゼント"></a>
	</div>

 ここまでキャンペーン img -->


<!-- 小冊子 img -->
		<div style="margin:0 0 10px 0;">
<a href="https://www.navit-j.com/service/joseikin-now/inquiry/jyo_fp_new.html"><img src="img/jyo_sassi2top_off.jpg" onmouseover="this.src='img/jyo_sassi2top_on.jpg'" onmouseout="this.src='img/jyo_sassi2top_off.jpg'" alt="助成金小冊子プレゼント"></a>

<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=387"><img src="img/jyo_sassi2bottm_off.jpg" onmouseover="this.src='img/jyo_sassi2bottm_on.jpg'" onmouseout="this.src='img/jyo_sassi2bottm_off.jpg'" alt="助成金小冊子紹介"></a>
	</div>

<!-- ここまで小冊子 img -->


<!-- 小冊子ログイン可変式 img -->
<!--		<div style="margin:20px 70px 10px 70px;">
{if $app.name == ""}
<a href="javascript:void(0)" onclick="javascript:do_submit_with_param('autonomy');"><img src="img/jyo_sassi_out.jpg" onmouseover="this.src='img/jyo_sassi_out.jpg'" onmouseout="this.src='img/jyo_sassi_out.jpg'" alt="助成金小冊子プレゼント"></a>
{else}
<a href="https://www.navit-j.com/service/joseikin-now/inquiry/jyo_fp_new.html"><img src="img/jyo_sassi.jpg" onmouseover="this.src='img/jyo_sassi_b.jpg'" onmouseout="this.src='img/jyo_sassi.jpg'" alt="助成金小冊子プレゼント"></a>
{/if}
	</div>
-->
<!-- ここまで小冊子ログイン可変式 img -->

<!-- DVD販売 img -->
<!--		<div style="margin:0 0 10px 0;">
<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=764"><img src="img/dvdsale_off.jpg" onmouseover="this.src='img/dvdsale_on.jpg'" onmouseout="this.src='img/dvdsale_off.jpg'" alt="助成金DVD販売"></a>
	</div>
-->
<!-- ここまでDVD販売 img -->


<!-- セミナーリストDL img -->
<!--		<div style="margin:20px 0px 10px 200px;">
{if $app.name == ""}
<a href="javascript:void(0)" onclick="javascript:do_submit_with_param('autonomy');"><img src="img/listdl_login_off.jpg" onmouseover="this.src='img/listdl_login_on.jpg'" onmouseout="this.src='img/listdl_login_off.jpg'" alt="対象セミナーリストDL"></a>
{else}
<a href="inquiry/s_support_new.html"><img src="img/listdl_logout_off.jpg" onmouseover="this.src='img/listdl_logout_on.jpg'" onmouseout="this.src='img/listdl_logout_off.jpg'" alt="対象セミナーリストDL"></a>
{/if}
	</div>
-->
<!-- ここまでセミナーリストDL img -->


<!-- ここから自治体フォーム --><div id="03"></div>

	<img src="img/title_a_search.jpg" alt="自治体案件検索" />

<div class="table">

		<table border="0" cellpadding="5" cellspacing="1">

		<tr>
			<td class="l_Cel"><img src="img/midashi_syubetu.jpg" alt="種別" /></td>
			<td colspan="2" class="r_Cel">
                            <table border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label><input class="CheckList4" type="checkbox" name="in_kind_1000" id="n_kind_1000" value="1000" {$app.data.kind_1000} />{$app.master_kind.1000}</label></td>
                                    <td><label><input  class="CheckList4" type="checkbox" name="in_kind_2000" id="in_kind_2000" value="2000" {$app.data.kind_2000} />{$app.master_kind.2000}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList4" type="checkbox" name="in_kind_3000" id="in_kind_3000" value="3000" {$app.data.kind_3000} />{$app.master_kind.3000}</label></td>
                                </tr>
                            </table>
                            {*$app_ne.radio.kind*}
                            {if is_error('in_kind')}<br /><br /><span class="error">{message name="in_kind"}</span>{/if}
                        </td>
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_area.jpg" alt="エリア" /></td>
			<td colspan="2" class="r_Cel">
                            
                            <select id='in_area1' name="in_area1" class="select_font" onchange="changeCity2(this);">{$app_ne.pulldown.area1}</select>
                            <select id='in_area2' name="in_area2" class="select_font"><option value="">市区町村を選択</option></select>
                            {if is_error('in_area1')}<br /><br /><span class="error">{message name="in_area1"}</span>{/if}
                            {if is_error('in_area2')}<br /><br /><span class="error">{message name="in_area2"}</span>{/if}
                            
                        </td>
                            
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_bunya.jpg" alt="分野" />
                        <a href="javascript:void(0)" id="all_check" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
                        <a href="javascript:void(0)" id="all_clear" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a>
                        </td>
			<td class="r_Cel">
                            <table border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label><input class="CheckList" type="checkbox" name="in_field_90" id="in_field_90" value="9000" {$app.data.field_90} />{$app.master_field.9000}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_91" id="in_field_91" value="9100" {$app.data.field_91} />{$app.master_field.9100}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_92" id="in_field_92" value="9200" {$app.data.field_92} />{$app.master_field.9200}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_93" id="in_field_93" value="9300" {$app.data.field_93} />{$app.master_field.9300}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_94" id="in_field_94" value="9400" {$app.data.field_94} />{$app.master_field.9400}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_95" id="in_field_95" value="9500" {$app.data.field_95} />{$app.master_field.9500}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_96" id="in_field_96" value="9600" {$app.data.field_96} />{$app.master_field.9600}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_97" id="in_field_97" value="9700" {$app.data.field_97} />{$app.master_field.9700}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_98" id="in_field_98" value="9800" {$app.data.field_98} />{$app.master_field.9800}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_99" id="in_field_99" value="9900" {$app.data.field_99} />{$app.master_field.9900}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_100" id="in_field_100" value="10000" {$app.data.field_100} />{$app.master_field.10000}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_101" id="in_field_101" value="10100" {$app.data.field_101} />{$app.master_field.10100}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_field')}<br /><span class="error">{message name="in_field"}</span>{/if}
                            
                            
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel_new"><img src="img/midashi_keyword_new.jpg" alt="キーワード" /><a style="padding:5px 30px 0 0" href="https://www.navit-j.com/service/joseikin-now/blog/?p=910" target="_blank">使い方はこちら</a>
</td>
			<td colspan="2" class="r_Cel">
                            <input type="text" id="in_keyword1" name="in_keyword1" value="{$app.data.keyword1}" size="40" maxlength="20" value="案件名などのワードを入力してください" style="font-size:20px;" />
                            <label for="keyword1"></label>
                            {if is_error('in_keyword1')}<br /><span class="error">{message name="in_keyword1"}</span>{/if}
			</td>
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td colspan="4">
				<div id="submit_btn">
					<!--<input type="image" onclick="return false;" id="a_reset" src="img/subbtn_reset.png" onMouseOver="this.src='img/subbtn_reset_d.png'" onMouseOut="this.src='img/subbtn_reset.png'" alt="リセット">-->
                                        <a class="button" href="javascript:void(0)" onclick="return false;" id="a_reset">リセット</a>
					<!--<input type="image" id="a_search" onClick="javascript:do_submit_with_param('autonomy');" src="img/subbtn_submit.png" onMouseOver="this.src='img/subbtn_submit_d.png'" onMouseOut="this.src='img/subbtn_submit.png'" alt="一覧表示">-->
                                        <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit_with_param('autonomy');" id="a_search">一覧表示</a>     
				</div>
			</td>
		  </tr>

    </table>
</div>
	<!-- ここまで自治体フォーム -->

<!-- ここからトップへ戻る -->

	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>

<!-- ここまでトップへ戻る -->

<!-- ここから財団フォーム --><div id="04"></div>

	<img src="img/title_f_search_line.jpg" alt="" />
	<img src="img/title_f_search.jpg" alt="財団案件検索" />

<div class="table">
		<table border="0" cellpadding="5" cellspacing="1">

		<!-- 20150707 -->
		<tr>
			<td class="l_Cel"><img src="img/midashi_area.jpg" alt="エリア" /></td>
			<td colspan="2" class="r_Cel">

                            <select id='in_area3' name="in_area3" class="select_font" onchange="changeCity4(this);">{$app_ne.pulldown.area3}</select>
                            <select id='in_area4' name="in_area4" class="select_font"><option value="">市区町村を選択</option></select>
                            {if is_error('in_area3')}<br /><br /><span class="error">{message name="in_area3"}</span>{/if}
                            {if is_error('in_area4')}<br /><br /><span class="error">{message name="in_area4"}</span>{/if}

                        </td>

		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>
		<!-- 20150707 -->

		<tr>
			<td class="l_Cel"><img src="img/midashi_bunya.jpg" alt="分野" />
                        <a href="javascript:void(0)" id="all_check2" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
                        <a href="javascript:void(0)" id="all_clear2" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a>
                        </td>
			<td class="r_Cel">
                            <table  border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label title="{$app.master_field2_desc.1000}"><input class="CheckList2" type="checkbox" name="in_field2_1" id="in_field2_1" value="1000" {$app.data.field2_1} />{$app.master_field2.1000}</label></td>
                                    <td><label title="{$app.master_field2_desc.2000}"><input  class="CheckList2" type="checkbox" name="in_field2_2" id="in_field2_2" value="2000" {$app.data.field2_2} />{$app.master_field2.2000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.3000}"><input  class="CheckList2" type="checkbox" name="in_field2_3" id="in_field2_3" value="3000" {$app.data.field2_3} />{$app.master_field2.3000}</label></td>
                                    <td><label title="{$app.master_field2_desc.4000}"><input  class="CheckList2" type="checkbox" name="in_field2_4" id="in_field2_4" value="4000" {$app.data.field2_4} />{$app.master_field2.4000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.5000}"><input  class="CheckList2" type="checkbox" name="in_field2_5" id="in_field2_5" value="5000" {$app.data.field2_5} />{$app.master_field2.5000}</label></td>
                                    <td><label title="{$app.master_field2_desc.6000}"><input  class="CheckList2" type="checkbox" name="in_field2_6" id="in_field2_6" value="6000" {$app.data.field2_6} />{$app.master_field2.6000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.7000}"><input  class="CheckList2" type="checkbox" name="in_field2_7" id="in_field2_7" value="7000" {$app.data.field2_7} />{$app.master_field2.7000}</label></td>
                                    <td><label title="{$app.master_field2_desc.8000}"><input  class="CheckList2" type="checkbox" name="in_field2_8" id="in_field2_8" value="8000" {$app.data.field2_8} />{$app.master_field2.8000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.9000}"><input  class="CheckList2" type="checkbox" name="in_field2_9" id="in_field2_9" value="9000" {$app.data.field2_9} />{$app.master_field2.9000}</label></td>
                                    <td><label title="{$app.master_field2_desc.10000}"><input class="CheckList2" type="checkbox" name="in_field2_10" id="in_field2_10" value="10000" {$app.data.field2_10} />{$app.master_field2.10000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.11000}"><input class="CheckList2" type="checkbox" name="in_field2_11" id="in_field2_11" value="11000" {$app.data.field2_11} />{$app.master_field2.11000}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_field2')}<br /><span class="error">{message name="in_field2"}</span>{/if}
                            
                            
			</td>

			
		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_keitai.jpg" alt="形態" />
                        <a href="javascript:void(0)" id="all_check3" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
                        <a href="javascript:void(0)" id="all_clear3" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a>
                        </td>
			<td class="r_Cel">
                            <table  border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label title="{$app.master_field3_desc.1000}"><input class="CheckList3" type="checkbox" name="in_field3_1" id="in_field3_1" value="1000" {$app.data.field3_1} />{$app.master_field3.1000}</label></td>
                                    <td><label title="{$app.master_field3_desc.2000}"><input  class="CheckList3" type="checkbox" name="in_field3_2" id="in_field3_2" value="2000" {$app.data.field3_2} />{$app.master_field3.2000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.3000}"><input  class="CheckList3" type="checkbox" name="in_field3_3" id="in_field3_3" value="3000" {$app.data.field3_3} />{$app.master_field3.3000}</label></td>
                                    <td><label title="{$app.master_field3_desc.4000}"><input  class="CheckList3" type="checkbox" name="in_field3_4" id="in_field3_4" value="4000" {$app.data.field3_4} />{$app.master_field3.4000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.5000}"><input  class="CheckList3" type="checkbox" name="in_field3_5" id="in_field3_5" value="5000" {$app.data.field3_5} />{$app.master_field3.5000}</label></td>
                                    <td><label title="{$app.master_field3_desc.6000}"><input  class="CheckList3" type="checkbox" name="in_field3_6" id="in_field3_6" value="6000" {$app.data.field3_6} />{$app.master_field3.6000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.7000}"><input  class="CheckList3" type="checkbox" name="in_field3_7" id="in_field3_7" value="7000" {$app.data.field3_7} />{$app.master_field3.7000}</label></td>
                                    <td><label title="{$app.master_field3_desc.8000}"><input  class="CheckList3" type="checkbox" name="in_field3_8" id="in_field3_8" value="8000" {$app.data.field3_8} />{$app.master_field3.8000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.9000}"><input  class="CheckList3" type="checkbox" name="in_field3_9" id="in_field3_9" value="9000" {$app.data.field3_9} />{$app.master_field3.9000}</label></td>
                                    <td><label title="{$app.master_field3_desc.10000}"><input class="CheckList3" type="checkbox" name="in_field3_10" id="in_field3_10" value="10000" {$app.data.field3_10} />{$app.master_field3.10000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.11000}"><input class="CheckList3" type="checkbox" name="in_field3_11" id="in_field3_11" value="11000" {$app.data.field3_11} />{$app.master_field3.11000}</label></td>
                                    <td><label title="{$app.master_field3_desc.12000}"><input class="CheckList3" type="checkbox" name="in_field3_12" id="in_field3_12" value="12000" {$app.data.field3_12} />{$app.master_field3.12000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.13000}"><input class="CheckList3" type="checkbox" name="in_field3_13" id="in_field3_13" value="13000" {$app.data.field3_13} />{$app.master_field3.13000}</label></td>
                                    <td><label title="{$app.master_field3_desc.14000}"><input class="CheckList3" type="checkbox" name="in_field3_14" id="in_field3_14" value="14000" {$app.data.field3_14} />{$app.master_field3.14000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.15000}"><input class="CheckList3" type="checkbox" name="in_field3_15" id="in_field3_15" value="15000" {$app.data.field3_15} />{$app.master_field3.15000}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_field3')}<br /><span class="error">{message name="in_field3"}</span>{/if}
                            
                            
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_keyword.jpg" alt="キーワード" /></td>
			<td colspan="2" class="r_Cel">
                            <input type="text" id="in_keyword2" name="in_keyword2" value="{$app.data.keyword2}" size="40" maxlength="20" value="案件名などのワードを入力してください" style="font-size:20px;" />
                            <label for="keyword2"></label>
                            {if is_error('in_keyword2')}<br /><span class="error">{message name="in_keyword2"}</span>{/if}
			</td>
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<div id="submit_btn">
					<!--<input type="image" onClick="return false;" id="f_reset" src="img/subbtn_reset.png" onMouseOver="this.src='img/subbtn_reset_d.png'" onMouseOut="this.src='img/subbtn_reset.png'" alt="リセット">-->
                                        <a class="button" href="javascript:void(0)" onclick="return false;" id="f_reset">リセット</a> 
					<!--<input type="image" onClick="javascript:do_submit_with_param('foundation');" src="img/subbtn_submit.png" onMouseOver="this.src='img/subbtn_submit_d.png'" onMouseOut="this.src='img/subbtn_submit.png'" alt="リセット">-->
                                        <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit_with_param('foundation');" id="f_search">一覧表示</a>     
				</div>
			</td>
		  </tr>

	</table>
<input type="hidden" id = "in_callkind" name="in_callkind" value="">                        
</form>
</div>

<!-- ここまで財団フォーム -->

<!-- ここからトップへ戻る -->

	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>

<!-- ここまでトップへ戻る -->
<!--
<div align="center" style="width:480px; margin:10px auto 30px;">
<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1456"><img src="img/staff_top2.png" onmouseover="this.src='img/staff_top2_on.png'" onmouseout="this.src='img/staff_top2.png'" alt="スタッフ"></a>
<img src="img/staff_top2.png" alt="スタッフ"/>

</div>
-->

<!-- ここからお知らせ -->

<div id="02"></div>

	<img src="img/title_news_line.jpg" alt="" />

			<a href="id/index.html" target="_blank"><img src="img/title_news.jpg" onmouseover="this.src='img/title_news_d.jpg'" onmouseout="this.src='img/title_news.jpg'" alt="お知らせ"></a>


<div class="topics_maincont_news">

        
<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2016.02.09
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem160224.html" target="_blank" />2016年2月24日（水）～知らないと損する【旬のクニモノ】活用術 ～「助成金・補助金・入札セミナー」 (東京会場/全国配信）開催のお知らせ。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2016.02.05
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/seminar/index.html" target="_blank" />「ものづくり補助金セミナー」(東京開催/全国配信）のお知らせ。<br />
【開催日程】2016年2月17日(水)、2月18日(木)、3月3日(木)、3月10日(木)<br /><span style="color:red;">　　　　　　※2/17東京開催は残席わずかです。</span>
</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2016.02.09
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=2198" target="_blank" />自治体案件が新しく[1件]公示され、[1件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=2198" target="_blank" />財団法人案件が新しく[0件]公示され、[0件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2016.02.08
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=2182" target="_blank" />助成金なう フェイスブックファンページの「いいね」が30,000件到達いたしました！</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2016.02.05
			</div>
			<div class="space_height">
            <a href="https://www.navit-j.com/service/joseikin-now/blog/?p=2126" target="_blank" />平成27年度ものづくり補助金の公募が開始されました。</a>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2016.02.04
			</div>
			<div class="space_height">
            <a href="https://www.navit-j.com/service/joseikin-now/blog/?p=2062" target="_blank" />この度、『東京都地域中小企業応援ファンド』に当社ナビットの「豊島区の空き家データベース構築」が採択されました。</a>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2016.02.02
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=2057" target="_blank" />自治体案件が新しく[4件]公示され、[32件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=2057" target="_blank" />財団法人案件が新しく[304件]公示され、[703件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2016.01.26
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=2027" target="_blank" />自治体案件が新しく[24件]公示され、[0件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=2027" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2016.01.19
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1974" target="_blank" />自治体案件が新しく[23件]公示され、[3件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1974" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2016.01.13
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem160127.html" target="_blank"><!--<span style="color:red;">【応募締切】</span>-->2016年1月27日（水）助成金・補助金・入札セミナー【無料】（東京開催/全国配信）のお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>


<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2016.01.13
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem160126.html" target="_blank" /><!--<span style="color:red;">【残席わずか】</span>-->2016年1月26日（火）「ものづくり補助金徹底解説2016 ～助成金・補助金活用セミナー～」(東京開催）のお知らせ。<!--※定員に達したため締め切らせていただきました。--></a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2016.01.12
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1949" target="_blank" />自治体案件が新しく[6件]公示され、[1件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1949" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2016.01.05
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1933" target="_blank" />自治体案件が新しく[26件]公示され、[31件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1933" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.12.22
			</div>
			<span class="text">
				年末年始の営業時間のご案内<br />
12/29（火）～1/3（日）まで年末年始の休暇とさせていただきます。</span>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.12.22
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1914" target="_blank" />自治体案件が新しく[39件]公示され、[2件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1914" target="_blank" />財団法人案件が新しく[1件]公示され、[0件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.12.15
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1884" target="_blank" />自治体案件が新しく[9件]公示され、[3件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1884" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.12.08
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1857" target="_blank" />自治体案件が新しく[28件]公示され、[1件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1857" target="_blank" />財団法人案件が新しく[1件]公示され、[0件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.12.03
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem151217.html" target="_blank" />2015年12月17日（木）「企業内の人材育成に助成金を！～助成金・補助金活用セミナー～（無料）」（東京開催）開催のお知らせ。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.12.01
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1831" target="_blank" />自治体案件が新しく[5件]公示され、[108件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1831" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.11.24
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1810" target="_blank" />自治体案件の新しい公示はありませんでした。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1810" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.11.20
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem151210.html" target="_blank" />2015年12月10日（木）「社員研修・育成に助成金！～助成金・補助金活用セミナー～(無料)」（東京開催）開催のお知らせ　※定員に達したため締め切らせていただきました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.11.19
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info151119" target="_blank" />当社ナビットの公式Facebookページができました。「いいね！」してファンになっていただければ、いち早く当社からのお知らせをご覧いただくことができるようになります。是非、ご利用下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.11.17
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1788" target="_blank" />自治体案件が新しく[62件]公示され、[2件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1788" target="_blank" />財団法人案件が新しく[1件]公示され、[0件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.11.11
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem151126_2.html" target="_blank" />11月26日（木）09:30「究極の売上アップセミナー（無料）～3社共催！独自性＆即効性にこだわった営業ツールをご紹介～」名古屋・大阪を開催いたします。※助成金による売上アップの事例をご紹介させていただきます。</a>
			</div>
		</div>
		<div class="clear">
		</div>


<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.11.10
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1763" target="_blank" />自治体案件が新しく[7件]公示され、[5件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1763" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.11.04
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1736" target="_blank" />自治体案件が新しく[28件]公示され、[31件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1736" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.10.29
			</div>
			<div class="text">
				<a href="http://navit-j.com/press/info.html#info151029" target="_blank" />東京都産業労働局の東京都内のオススメ企業95社の紹介サイト「東京カイシャハッケン伝！」に当社の記事がアップされました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.10.27
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1707" target="_blank" />自治体案件が新しく[22件]公示され、[0件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1707" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.10.26
			</div>
			<div class="text">
				<a href="http://navit-j.com/press/info.html#info151026" target="_blank">10/26（月）発行のThe Japan News（読売新聞英語版）の「The Pioneers」というコーナーに、弊社代表の福井が紹介されました。10/3，10/17の読売新聞夕刊に掲載された記事の英語版となります。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WebサイトThe Japan Newsにも掲載されておりますので、あわせてご覧ください。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.10.20
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1625" target="_blank" />自治体案件が新しく[40件]公示され、[8件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1625" target="_blank" />財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.10.19
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/blog/?p=12926" target="_blank">10/17（土）の読売新聞夕刊紙の「開拓者たち 下」というコーナーに、ナビットと弊社代表の福井が紹介されました。10/3に掲載された記事の後編となります。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Webサイト読売プレミアムにも掲載されておえりますので、あわせてご覧ください。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.10.14
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem151202.html" target="_blank">2015年12月2日（水）助成金・補助金・入札セミナー【無料】（東京開催/全国配信）のお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.10.13
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1568" target="_blank">自治体案件が新しく[31件]公示され、[4件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1568" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.10.09
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem151023.html" target="_blank">2015年10月23日（金）「まだ間に合う今年の補助金！助成金補助金セミナー」(東京開催/全国配信）開催のお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.10.09
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem151106.html" target="_blank">2015年11月6日（金）助成金・補助金・入札セミナー【無料】（広島/福岡）開催のお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.10.06
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1525" target="_blank">自治体案件が新しく[96件]公示され、[25件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1525" target="_blank">財団法人案件が新しく[1件]公示され、[0件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.10.05
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/blog/?p=12363" target="_blank">10/3（土）の読売新聞の夕刊の「開拓者たち上」というコーナーに、弊社代表の福井が紹介されました。この企画は連載なので、次回は10/17(土)に下が掲載されます。掲載エリアは関西、中国、四国を除く全国です。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.10.01
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem151021.html" target="_blank">2015年10月21日（水）助成金・補助金・入札セミナー【無料】（東京開催/全国配信）のお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.09.29
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem151016.html" target="_blank">2015年10月16日（金）助成金・補助金・入札セミナー【無料】（名古屋/大阪開催）のお知らせ</a><br />
<span style="margin-top:5px; color: #F33; ">※名古屋会場・大阪会場 共に満席となりました。</span>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.09.29
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1464" target="_blank">自治体案件が新しく[53件]公示され、[2件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1464" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.09.29
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1456" target="_blank">助成金なう フェイスブックファンページの「いいね」が20000件到達いたしました！</a>
			</div>
		</div>
		<div class="clear">
		</div>


<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.09.24
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1407" target="_blank">自治体案件が新しく[185件]公示され、[4件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1407" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.09.15
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1269" target="_blank">自治体案件が新しく[404件]公示され、[7件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1269" target="_blank">財団法人案件の新しい公示はございせんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.09.10
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem150925.html" target="_blank">2015年9月25日（金）、10月2日（金）、10月9日（金）<br />
「まだ間に合う今年の補助金！助成金補助金セミナー」（東京開催/全国配信）のお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.09.08
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1222" target="_blank">自治体案件が新しく[80件]公示され、[9件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1222" target="_blank">財団法人案件が新しく[3件]公示され、[2件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.09.03
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/press/150903.html" target="_blank">この度、当社の子育て中の女性へのお仕事支援サイト「Sohos-Style」の取り組みが評価され、東京都産業労働局の「ものづくりで魅力的な企業95社」に選ばれました。「東京カイシャハッケン伝」にて<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;特集していただきました。今後、都内の魅力的ものづくり企業として、内容は冊子化され、都内の大学、<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;専門学校の学生向けに配布されます。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.09.02
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1192" target="_blank">フジサンケイビジネスアイ2015年9月1日号朝刊にて助成金なうが掲載されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.09.01
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1154" target="_blank">自治体案件が新しく[19件]公示され、[220件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1154" target="_blank">財団法人案件が新しく[2件]公示され、[0件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.08.28
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem150918.html" target="_blank">9月18日（金）よく分かる！入札・補助金・助成金の仕組みと活用事例のご紹介 ～知らないと損する申請の肝～ セミナーのお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>


<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.08.25
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1097" target="_blank">自治体案件が新しく[13件]公示され、[1件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1097" target="_blank">財団法人案件の新しい公示はございせんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.08.24
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/blog/?p=10040" target="_blank">当社ナビットのエントランスの展示スペース用の、各商材ごとのキャラクターぬいぐるみが完成いたしました。SOHOの在宅ワーカーさんに作成してもらったものです。ご来社の際は、是非、ご覧ください。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.08.20
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem150911.html" target="_blank">2015年9月11日（金）「まだ間に合う今年の補助金！助成金補助金セミナー」（東京開催/全国配信）のお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.08.18
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1057" target="_blank">自治体案件が新しく[28件]公示され、[4件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1057" target="_blank">財団法人案件の新しい公示はございせんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.08.11
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1040" target="_blank">自治体案件が新しく[15件]公示され、[1件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1040" target="_blank">財団法人案件の新しい公示はございせんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.08.10
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=1029" target="_blank">助成金なうフェイスブックファンページの「いいね」が15000件到達いたしました！</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.08.04
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=965" target="_blank">自治体案件が新しく[52件]公示され、[837件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=965" target="_blank">財団法人案件の新しい公示はございせんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.08.03
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=958" target="_blank">助成金なうブログのサイドエリアにオンラインセミナー動画を設置いたしました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.07.30
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=910" target="_blank">自治体案件検索に「キ－ワード」検索の機能が追加されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.07.28
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/sem150826.html" target="_blank">2015年8月26日（水）「助成金・補助金・入札セミナー ～知らないと損する【旬のクニモノ】セミナー ～」（東京開催/全国配信）のお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.07.28
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=893" target="_blank">自治体案件が新しく[28件]公示され、[16件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=893" target="_blank">財団法人案件の更新はございませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.07.23
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/seminar/" target="_blank">2015年8月7日（金）、8月21日（金）「NiCoA主催会員向け入札＋助成金セミナー」開催のお知らせ</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.07.22
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?cat=15" target="_blank">よくある質問・用語集の項目を設けました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.07.21
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=848" target="_blank">自治体案件が新しく[51件]公示され、[8件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=848" target="_blank">財団法人案件の更新はございませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.07.15
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=764" target="_blank">「ものづくり2次募集公募開始！助成金補助金セミナー」のDVD販売を開始しました。</a><br />
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.07.14
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=829" target="_blank">自治体案件が新しく[77件]公示され、[13件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=829" target="_blank">財団法人案件の更新はありません。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.07.07
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=753" target="_blank">自治体案件が新しく[87件]公示され、[2225件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=753" target="_blank">財団法人案件が新しく[1件]公示され、[1件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.07.07
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/seminar/" target="_blank">2015年7月14日（火）「ものづくり2次募集公募開始！助成金補助金セミナー」（東京開催/全国配信）開催のお知らせ ※7月1日(水)、7月3日(金)、7月7日(火)開催は終了しました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.06.30
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=632" target="_blank">自治体案件が新しく[39件]公示され、[145件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=632" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.06.23
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=602" target="_blank">自治体案件が新しく[57件]公示され、[12件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=602" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.06.22
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150622" target="_blank">この度、『平成26年度補　ものづくり・商業・サービス革新補助金』１次公募採択の東京都800社に選ばれました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.06.16
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=590" target="_blank">自治体案件が新しく[66件]公示され、[9件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=590" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.06.09
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=585" target="_blank">自治体案件が新しく[22件]公示され、[20件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=585" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.06.02
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=541" target="_blank">自治体案件が新しく自治体案件が新しく[39件]公示され、[12件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=541" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.06.02
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/seminar/" target="_blank">「ものづくり2次募集公募開始！ 助成金補助金セミナー」を開催します。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.06.01
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/blog/?p=5232" target="_blank">この度、業務拡大につき、事務所を九段下に移転することになりました。新事務所での営業は6/8（月）を予定しております。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.05.28
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=470" target="_blank">申請サポートサービスが全国対応になりました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.05.27
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=387" target="_blank">助成金なう 小冊子「経営者のための助成金のすすめ」(無料）ができました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.05.26
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=379" target="_blank">自治体案件が新しく[40件]公示され、[6件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=379" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.05.22
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=371" target="_blank">助成金なうフェイスブックファンページの「いいね」が10000件到達いたしました！</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="media">
				2015.05.22
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150522" target="_blank">5月25日(月）TBS、CBC「ゴゴスマ」13：55～15：30の「いま知り」コーナーに当社のサービスが紹介される予定です。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;※放送内容は変更される可能性があります。予めご了承下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.05.21
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/seminar/" target="_blank">6月3日(水)「優秀な社員の確保と助成金活用 ～他社との差別化を図る、優秀な人材確保の仕組みづくり～（無料）」セミナー(東京会場）を開催いたします。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.05.19
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=367" target="_blank">自治体案件が新しく[42件]公示され、[16件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=367" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.05.12
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=352" target="_blank">自治体案件が新しく[37件]公示され、[65件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=352" target="_blank">財団法人案件が新しく[2件]追加公示され、[1件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.04.28
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=342" target="_blank">自治体案件が新しく[28件]公示され、[963件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=342" target="_blank">財団法人案件が新しく[1件]追加公示され、削除はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.04.21
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=338" target="_blank">自治体案件が新しく[10件]公示され、[14件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=338" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="media">
				2015.04.01
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150401" target="_blank">4月19日（日）日本テレビ系「スクール革命！」（11：45～12：45）の発明特集にて、当社の「のりかえ便利マップ」が関東ローカルで放送される予定です。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;※放送内容は変更される可能性があります。予めご了承下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.04.17
			</div>
			<div class="text">
				Gmailのメールアドレスではサービスがご利用いただけなくなります。マイページの会員情報編集より、メールアドレスの変更をお願いいたします。
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.04.14
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=332" target="_blank">自治体案件が新しく[21件]公示され、[19件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=332" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>


<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.04.09
			</div>
			<div class="text">
				<a href="http://navit-j.com/20150409.html" target="_blank">【お詫びとご報告】当社からのメールが一部のお客様向けに3月中頃から届いていない、という事象が判明いたしました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.04.07
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=312" target="_blank">自治体案件が新しく[29件]公示され、[110件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=312" target="_blank">財団法人案件が新しく[10件]追加公示され、[9件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>



<!-- 一行 -->
		<div class="space_height">
			<div class="media">
				2015.04.06
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150406" target="_blank">4月15日（水）フジテレビ「バイキング」11：55～12：55にて、「毎日特売」が全国放送される予定です。<br />※放送内容は変更される可能性があります。予めご了承下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.04.01
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=305" target="_blank">自治体案件が新しく[7件]公示され、[3件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=305" target="_blank">財団法人案件が新しく[1件]追加公示され、[5件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.03.31
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=286" target="_blank">会員の方向けにマイページのご利用ができるようになりました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.03.30
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/seminar/" target="_blank">4月10日(金)「ものづくり公募開始！助成金補助金セミナー(東京会場）」を開催いたします。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.03.25
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=220" target="_blank">助成金なうフェイスブックファンページの「いいね」が5000件到達いたしました！</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.03.24
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=211" target="_blank">自治体案件が新しく[99件]公示され、[431件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=211" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="media">
				2015.03.20
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150320" target="_blank">中小企業政策審議官の村本先生の書籍「元気な中小企業を育てる」（蒼天社2700円）の本の表紙として、当社の「のりかえ便利マップ」が採用されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.03.17
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=189" target="_blank">自治体案件が新しく[4件]公示され、[4件]削除されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=189" target="_blank">財団法人案件の新しい公示はありませんでした。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.03.10
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=141" target="_blank">自治体案件が新しく[20件]公示されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=141" target="_blank">財団法人案件が新しく[1件]追加公示され、[54件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="media">
				2015.03.09
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150309" target="_blank">3月11日（水）テレビ東京『L4YOUプラス』（15：35～）において、のりかえ便利マップが紹介される予定です。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.03.04
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/seminar/" target="_blank">3月13日(金)、3月17日(火)、3月26日(木)「ものづくり/創業支援 公募開始！助成金補助金セミナー(東京会場/全国配信）」を開催いたします。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.03.03
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=109" target="_blank">自治体案件が新しく[8件]公示されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=109" target="_blank">財団法人案件が新しく[757件]追加公示されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.02.24
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=102" target="_blank">自治体案件が新しく[13件]公示されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=102" target="_blank">財団法人案件が新しく[1140件]追加公示され、[757件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="matter">
				2015.02.18
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=83" target="_blank">自治体案件が新しく[13件]公示されました。</a><br />
				<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=83" target="_blank">財団法人案件が新しく[421件]追加公示され、[133件]削除されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="sem">
				2015.02.05
			</div>
			<div class="text">
				<a href="https://www.navit-j.com/service/joseikin-now/seminar/" target="_blank">新サービス【助成金なう】より「合言葉は助成金なう～日本初11187機関の助成金情報を徹底的に活用する～」無料オンラインセミナー(22分)が始まりました。全国どこでも、いつでも受講することができます。<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ご興味のある方は、まずはお申込み下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>


<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2015.02.04
			</div>
			<div class="text">
				<p>自治体案件検索に「キ－ワード」検索の機能が追加されました。</p>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="matter">
				2015.02.04
			</div>
			<div class="text">
				<a href="http://www.joseikin-now.com/id/38506.html" target="_blank">[創業支援融資資金]が公示されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="matter">
				2015.02.04
			</div>
			<div class="text">
				<a href="http://www.joseikin-now.com/id/38505.html" target="_blank">[杉並区中小企業融資制度]が公示されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>


<!-- 一行 -->
		<div class="space">
			<div class="matter">
				2015.02.04
			</div>
			<div class="text">
				<a href="http://www.joseikin-now.com/id/38504.html" target="_blank">[地域工場・中小企業等の省エネルギー設備導入補助金【資源エネルギー庁】]が公示されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="media">
				2015.02.02
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150202" target="_blank">週刊東洋経済臨時増刊「WORK AGAIN」誌のコーナー『女性こそ「企画力」で勝負』にて「のりかえ便利マップ」と弊社代表福井が紹介されております。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="matter">
				2015.01.30
			</div>
			<div class="text">
				<a href="http://www.joseikin-now.com/id/38501.html" target="_blank">[中小企業活路開拓調査・実現化事業]が公示されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="matter">
				2015.01.29
			</div>
			<div class="text">
				<a href="http://www.joseikin-now.com/id/38502.html" target="_blank">[地域商業自立促進事業]が公示されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="news">
				2015.01.19
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150119" target="_blank">「助成金なう」サービスがスタートいたしました。全国の助成金・補助金対応 日本初！官公庁、自治体、財団11187機関の情報を網羅しております。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="media">
				2015.01.07
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150107" target="_blank">雑誌「地図ジャーナル」2015年 新春号 No.176 業界消息にて、CEATEC JAPANでの弊社出展と新サービス「地番変換サービス」「雷発生予想データ」が紹介されております。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="media">
				2015.01.05
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150105" target="_blank">ランコム社内報2014年第2期号にて、当社代表福井が登壇しました9/6（土）開催の復興支援セミナー「アイズ・フォー・フィーチャーbyランコム～女性が輝く。石巻が輝く。～」が紹介されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="news">
				2014.12.26
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info141226-2" target="_blank">「2015年1月版 価格表」のダウンロードを開始いたしました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2014.12.05
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info141205" target="_blank">12月6日（土）テレビ朝日「タメになる同世代自慢バラエティ！　学年ゲーム～TAME～」（24:45～25:15）において、当社代表福井と当社の業務が紹介される予定です。 関東ローカルのみの放送となります。</a>
			</div>
		</div>
		<div class="clear">
		</div>

</div>

<!-- ここまでお知らせ -->



<!-- ここからセミナー -->
<!--
<div id="05"></div>

	<img src="img/title_sem_line.jpg" alt="" />
	<img src="img/title_sem.jpg" alt="セミナー・イベント" />
	<img src="img/con_sem.jpg" alt="セミナー・イベント" />
-->
<!-- ここまでセミナー -->

<!-- ここからトップへ戻る -->
<!-- 
	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>
-->
<!-- ここまでトップへ戻る -->

<!-- ここから会社概要 -->
<!--

<div id="06"></div>

	<img src="img/title_com_line.jpg" alt="" />
	<img src="img/title_com.jpg" alt="会社概要" />

<div id="company">

<div id="company_left">

	<div id="com_msg">

		<div id="navit_logo">
			<a href="http://www.navit-j.com/" target="_blank" /><img src="img/navit_logo.jpg" alt="株式会社ナビット"></a>
		</div>

		<div id="msg">
			入札なうを運営する「株式会社ナビット」は、様々なサービスを<br />提供しております。その実績をご評価いただき、テレビ他、<br />多様なメディアに取り上げていただいております。
		</div>

		<div class="clear">
		</div>
		<hr />
	</div>

	<div id="pre_msg">

		<div id="pre_photo">
			<img src="img/com_fukui.jpg" alt="福井泰代">
		</div>

		<div id="pmsg"><span style="font-size:1.5em; font-weight:bold; color:#FF4000;">のりかえ便利マップをご存じですか？</span><br />
ひとりの主婦が、小さな子供を抱えて駅で困った経験、階段やエスカレータ<br />ーがどこにあって、出口はどこなのか？重いベビーカーを押しながら思いつ<br />いた答えがのりかえ便利マップのきっかけでした。<br />その主婦がナビット代表の福井です。
		</div>

		<div class="clear">
		</div>

	</div>

</div>

	<div id="company_right">
		<img src="img/com_noriben.jpg" alt="のりかえ便利マップ">
	</div>

	<div class="clear">
	</div>

	<div id="company_bottom">
	<hr />
		<h3 style="padding:0 0 0 5px; font-size:19px; font-weight:bold; float:left;">
			株式会社ナビット
		</h3>

		<div style="padding:5px 150px 0 0; font-size:15px; line-height:15px; float:right;">
			本　　社：〒101-0051 東京都千代田区神田神保町3-10-2共立ビル3Ｆ<br />
	　　　　　TEL：03-5215-5701　FAX：03-5215-5702　URL：<a href="http://www.navit-j.com/" target="_blank" />http://www.navit-j.com/</a><br />
		</div>
		<div style="padding:5px 121px 0 0; font-size:15px; line-height:15px; float:right;">
			大阪支社：〒530-0001 大阪府大阪市北区梅田1丁目11番4-1100　大阪駅前第四ビル11階10号室<br />
	　　　　　TEL：06-4799-9201　FAX：06-4799-9011
		</div>
	</div>
	</div>
-->
<!-- ここまで会社概要 -->

<!-- ここからトップへ戻る -->
<!--
	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>
-->
<!-- ここまでトップへ戻る -->
</div>

<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">
	
</div>

<!-- ここまでfooter -->


</div>


<!-- スムーズスクロール -->
{literal}
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 700; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
</script>
<!-- スムーズスクロール -->

<!-- グローバルナビ -->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->

<!-- グローバルナビ -->

<!-- ▼************ Google Anarytics トラッキングコード ************ ▼-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- ▲ *********** Google Anarytics トラッキングコード ************ ▲-->

<!-- googleanalytics TAG -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-12', 'auto');
  ga('send', 'pageview');

</script>
<!-- googleanalytics TAG -->


<!-- LF innov TAG 20150127 -->

<script type="text/javascript">
var _trackingid = 'LFT-10394-1';

(function() {
  var lft = document.createElement('script'); lft.type = 'text/javascript'; lft.async = true;
  lft.src = document.location.protocol + '//track.list-finder.jp/js/ja/track.js';
  var snode = document.getElementsByTagName('script')[0]; snode.parentNode.insertBefore(lft, snode);
})();
</script>

<!-- LF innov TAG 20150127 -->


{/literal}


</body>
</html>
