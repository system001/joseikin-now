{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e12" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
		<td width="75" align="left" valign="middle">&nbsp;</td>
	</tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
	<div><img src="images/bar14.gif" width="740" height="30" /></div>
</div>
<div>
<div id="column">
	<div>
		<p><strong> ◆<font size="3">『認定・仮認定NPO法人』等に関する説明</font></strong></p>
	</div>
	<table width="710" border="0" cellspacing="3" cellpadding="3">
		<tr>
			<td width="40"><strong>（１）</strong></td>
			<td width="678"><strong>「認定・仮認定NPO法人」について</strong></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>認定・仮認定NPO法人とは、一定の基準を満たすものとして所轄庁の「認定・仮認定」を受けたNPO法人のことです。「認定・仮認定NPO法人」と<strong>「認定・仮認定NPO法人に寄附した者」は、各々税制の優遇措置を受けることができます</strong>。 <br />
				詳しくは、<a href="https://www.npo-homepage.go.jp/about/nintei.html" target="_blank">内閣府のホームページ</a>をご覧ください。 </td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><strong>（２）</strong></td>
			<td><strong>「認定・仮認定NPO法人への寄附に伴う税制優遇措置の拡大」について</strong></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><u>『新寄附税制』の施行（平成23年6月30日）に伴い、寄附金控除の還付率がアップしています！！</u></td>
		</tr>
		<tr>
			<td colspan="2"><img src="images/P13.gif" width="710" height="335" /></td>
		</tr>
	</table>
</div>
<div><img src="images/border1.gif" width="740" height="3" /></div>
<div>
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10px;">
	<div>
		<div>
			<p><strong>［認定・仮認定NPO法人への寄附］</strong>　</p>
		</div>
			<p> <strong>問１６</strong> 　【全員の方にお聞きします】<font color="red"><strong>【必須】</strong></font><img src="images/m16.gif" width="128" height="19" /><br />
				<br />
				あなたは、認定・仮認定NPO法人に寄附をしたいと思いますか。ひとつお選びください。<br />
				<br />
				{if is_error('in_q16')}<br /><span class="error">{message name="in_q16"}</span>{/if}
                                                                <br />
                                                                {$app_ne.radio.q16}
			</p>
                        
		<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
			</tr>
		</table>
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e10=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->
	</div>
</div>
<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
	</tr>
</table>
</form>



{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
