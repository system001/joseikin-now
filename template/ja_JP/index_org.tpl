{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<style type="text/css">
.pass {
	text-align: center;
	margin-top: 0px;
}
</style>
{/literal}
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<h3>

<table width="740" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td><p class="pass">調査ページにアクセスいただきましてありがとうございます。<br />
    平成26年度　市民の社会貢献に関する実態調査　Web回答ページです。</p></td>
  </tr>
</table>
</h3>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<div class="image">
<div><img src="images/bar_login.gif" width="740" height="30" /></div>

    <table width="740" >
        <tr>
            <form action="{$script}" name="f2" method="{$form_action}">
            <input type="hidden" name="action_login_do" value="true">

            <td width="739">
              <table border="0" cellpadding="2" cellspacing="0" id="table3" style="text-align:center; margin:40px auto; font-size:18px;">
                <tr>
                      <th></th>
                        <td width="300" height="5"><img src="images/login_kikan_bar.gif" width="300" height="2" /></td>
                </tr>
                      
                                          <tr>
                        <th height="30"><strong>回答期間</strong></th>
                        <td><strong>9月19日（金）～10月30日（木）</strong></td>
                </tr>
                    <tr>
                      <th></th>
                        <td width="300" height="5"><img src="images/login_kikan_bar.gif" width="300" height="2" /></td>
                    </tr>
              </table>
<p style="font-size:18px; font-weight:bold; text-align:center; margin-top:20px;">Web回答　 ログイン画面</p>
              <table border="0" cellpadding="2" cellspacing="0" id="table3" style="text-align:center; margin:40px auto; font-size:18px;">
                    <tr>
                        <th>ログインID</th>
                        <td>
                            <label for="ID"></label>
                            <input type="text" name="in_id" value="{$form.in_id}"  maxlength="64" style="height: 22px; width: 230px;">
                        </td>
                </tr>
                    <tr><td height="20"></td></tr>
                    <tr>
                        <th>パスワード</th>
                        <td>
                            <input type="password" name="in_pw"  maxlength="64" style="height: 22px; width: 230px;">
                        </td>
                    </tr>
              </table>
<!--エラーメッセージ-->
{if count($errors)}
<table class="error" align="center">
{foreach from=$errors item=error}
<tr class="error"><td>{$error}</td></tr>
{/foreach}
</table>
{/if}
<!--エラーメッセージ-->
                        
                <div class="pass">
                    <img onClick="javascript:do_submit();" border="0" src="images/login_off.png" onmouseover="this.src='images/login_on.png'" onmouseout="this.src='images/login_off.png'" style="cursor:pointer;">
                    <br />
                    <br />
              </div>
            </td>
            </form>
        </tr>
    </table>
</td>
</tr></table>

<div><img src="images/border1.gif" width="740
" height="3" /></div>
 <div class="設問内容">
<div><img src="images/bar.gif" width="740
" height="5" /></div>

<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>

{$footer}
<!--フッター-->
{$app_ne.footer}
<!--フッター-->
</body>
</html>
