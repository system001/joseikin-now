<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<style>
.state_payment_open {
	background: url("img/jyo_paybtn_d.jpg") no-repeat;
}

.state_payment_open a {
	height: 60px;
	background: url("img/jyo_paybtn_d.jpg") no-repeat;
	display: block;
	text-indent: -9999px;
}

.state_payment_open a:hover {
	background-image: url("img/jyo_paybtn.jpg");
}

.state_payment_closed {
	background: url("img/jyo_paybtn.jpg") no-repeat;
}

.state_payment_closed a {
	height: 60px;
	background: url("img/jyo_paybtn.jpg") no-repeat;
	display: block;
	text-indent: -9999px;
}

.state_payment_closed a:hover {
	background-image: url("img/jyo_paybtn_d.jpg");
}

div.ui-datepicker {
	font-size: 70%;
}
</style>
<link rel="stylesheet" href="js/jquery-ui/jquery-ui.css" type="text/css">
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script type="text/javascript">

$(window).load(function(){
	$('label').balloon();
	$('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
});

function doblur() {
	var element = document.getElementById("name");
	element.blur();
}

function do_submit(){
	document.f2.submit();
}

/*
 　　全角->半角変換
 */
jQuery(function(){

	// 郵便番号の処理
	$('.zip-number').change( function(){
		var data = $(this).val();
		var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});

		// 半角数字のみ残す
		var zenkakuDel = new String( hankaku ).match(/\d/g);
		if(zenkakuDel){
			zenkakuDel = zenkakuDel.join("");
		}else {
			zenkakuDel = "";
		}

		$(this).val(zenkakuDel);
	});
	// 電話番号の処理
	$('.tel-number').change( function(){
		var data = $(this).val();
		var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});

		// 半角数字と+-のみ残す
		var zenkakuDel = new String( hankaku ).match(/\d|\-|\+/g);
		if(zenkakuDel){
			zenkakuDel = zenkakuDel.join("");
		}else {
			zenkakuDel = "";
		}

		$(this).val(zenkakuDel);
	});

	// メールアドレスの処理
	$('.mail-address').change( function(){
		var zenkigou = "＠－ー＋＿．，、";
		var hankigou = "@--+_...";
		var data = $(this).val();
		var str = "";

		// 指定された全角記号のみを半角に変換
		for (i=0; i<data.length; i++)
		{
			var dataChar = data.charAt(i);
			var dataNum = zenkigou.indexOf(dataChar,0);
			if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
			str += dataChar;
		}
		// 定番の、アルファベットと数字の変換処理
		var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
		$(this).val(hankaku);
	});

	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#in_pay_date" ).datepicker({
		beforeShow: function(input, inst) {
			var calendar = inst.dpDiv;
			setTimeout(function() {
				calendar.position({
					my: 'left bottom',
					at: 'left top',
					of: input
				});
			}, 1);
		}
	});
	$( "#in_begin_date" ).datepicker({
		beforeShow: function(input, inst) {
			var calendar = inst.dpDiv;
			setTimeout(function() {
				calendar.position({
					my: 'left bottom',
					at: 'left top',
					of: input
				});
			}, 1);
		}
	});
});


</script>
{/literal}
</head>
<body>
<form action="{$script}" name="f2" id="f2" method="post">
<input type="hidden" name="action_payment" value="true">
<input type="hidden" name="in_check_payment" value="1">
<!-- ここからconteinar -->
<div id="conteinar">
<!-- ここからwrapper -->
	<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}
<!-- ここから入力フォーム --><div id="03"></div>

<br />
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">有料会員登録</span></div>
<div style="margin-left: 120px;">下記フォームに必要事項を入力後、確認ボタンを押してください。<br /></div>
<div style="font-size:0.7em;margin-left: 120px;">
	<span>
	【注意事項】<br />
	<span style="color:#64c601;font-size:16px;">■</span>ご登録頂いたメールアドレスにご連絡致します。<br />
	</span>
</div>
<br />

<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
	<tr class="payment_params">
		<td class="l_Cel_01_01">お支払い方法<br /><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<label><input type="radio" name="in_payment_method" value="1" {if $app.data.payment_method == 1} checked{/if}/>クレジットカード決済</label>
			<label><input type="radio" name="in_payment_method" value="0" {if $app.data.payment_method != 1} checked{/if}/>銀行振込</label>
		</td>
	</tr>

	<tr class="payment_params">
		<td class="l_Cel_01_01">ご利用期間/料金<br /></td>
		<td width="550" class="s_Cel">
		12ヶ月 \12,900(税込)
		</td>
	</tr>

	<tr class="payment_params">
		<td class="l_Cel_01_01">入金予定日<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">例：2015年04月01日<span style="font-size:0.8em;">(クレジットカード決済の場合は即日となります)</span><br />
			<input style="width:20em;" type="text" id="in_pay_date" name="in_pay_date" value="{$app.data.pay_date}" size="20" maxlength="20"  style="font-size:16px;" />
			{if is_error('in_pay_date')}<div class="error" style="color:red;">{message name="in_pay_date"}</div>{/if}
		</td>
	</tr>

	<tr class="payment_params">
		<td class="l_Cel_01_01">開始希望日<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">例：2015年04月03日<span style="font-size:0.8em;">(入金予定日から２営業日以降を指定してください)</span><br />
			<input style="width:20em;" type="text" id="in_begin_date" name="in_begin_date" value="{$app.data.begin_date}" size="20" maxlength="20" style="font-size:16px;"/>
			{if is_error('in_begin_date')}<div class="error" style="color:red;">{message name="in_begin_date"}</div>{/if}
		</td>
	</tr>
</table>
</div>

<div class="mod_form_btn">
	<div align="center" style="margin-top:20px;"><a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();" id="confirm_btn">確認</a></div>
	</div>
</div>
</form>
<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$('#in_check_payment').change(function(){
	if($(this).prop('checked')){
		$('.payment_params').each(function(){
			$(this).css('display','');
		});
	} else {
		$('.payment_params').each(function(){
			$(this).css('display','none');
		});
	}
});

$('#cmd_check_payment_open').click(function(){
	$('#in_check_payment').prop('checked', true);
	$('.payment_params').each(function(){
		$(this).css('display','');
	});
	$('#tr_check_payment_open').hide();
	$('#tr_check_payment_closed').show();
});

$('#cmd_check_payment_close').click(function(){

	$('#in_check_payment').prop('checked', false);
	$('.payment_params').each(function(){
		$(this).css('display','none');
	});
	$('#tr_check_payment_closed').hide();
	$('#tr_check_payment_open').show();
});


$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	// スクロールの速度
	var speed = 700; // ミリ秒
	// アンカーの値取得
	var href= $(this).attr("href");
	// 移動先を取得
	var target = $(href == "#" || href == "" ? 'html' : href);
	// 移動先を数値で取得
	var position = target.offset().top;
	// スムーススクロール
	$('body,html').animate({scrollTop:position}, speed, 'swing');
	return false;
   });
});
</script>
<!-- スムーズスクロール -->

{/literal}

{include file='common/track.tpl'}

</body>
</html>
