<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索、申請サポート「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="助成金・補助金の情報を簡単に検索！エリアやキーワードを入力するだけ！全国の市区町案件検索、最新情報、おすすめ案件表示などコンテンツが充実。有料会員なら申請サポートも受けられます。">
<meta name="author" content="株式会社ナビット">
<meta property="og:title" content="助成金・補助金の検索、申請サポート「助成金なう」" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.navit-j.com/service/joseikin-now/" />
<meta property="og:image" content="http://www.navit-j.com/img/tasukekun.png" />
<meta property="fb:app_id" content="584166861742163" />
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">

<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="js/jquery.jStageAligner.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>

{literal}
<script type="text/javascript">
$(window).load(function(){
	$("#gnavi").sticky({ topSpacing: 0, center:true, className:"hey" });
});
$(function(){
	$('label').balloon();
	$("#fixedbox").jStageAligner("CENTER_MIDDLE");
});

function doblur() {
	var element = document.getElementById("name");
	element.blur();
}
$( document ).ready( function() {
	$( ".favorite_button" ).click( function() {
		//var isFavorite = $(this).attr( 'favorite' );
		var conentKind = $(this).attr( 'content' );
		var contentId  = $(this).attr( 'target' );
		var add        = true;
		if ( $( this ).attr( "favorite" ) == "1" ) {
			add = false;
		}

		$.ajax( {
			type: "POST",
			url: "index.php",
			data: "action_favorite=true&in_add=" + add + "&in_content_type=" + conentKind + "&in_contents_id=" + contentId,
			success: function(msg){
				$( "#favorite_button_" + contentId ).attr( 'favorite', msg );
				setFavoriteButtonName();
			}
		} );
	} );
	setFavoriteButtonName();
});

function setFavoriteButtonName()
{
	$( ".favorite_button" ).each( function ( index, element ) {
		element = $(element);
		if ( element.attr( 'favorite' ) == "1" ) {
			element.val( "お気に入りから削除" );
		}
		else {
			element.val( "お気に入りに追加" );
		}
	} );
}
</script>
{/literal}

<!-- 有料会員バナーランダム表示 -->
<!--<script type="text/javascript">
jQuery(function($) {
$.fn.extend({
	randomdisplay : function(num) {
		return this.each(function() {
			var chn = $(this).children().hide().length;
			for(var i = 0; i < num && i < chn; i++) {
				var r = parseInt(Math.random() * (chn - i)) + i;
			$(this).children().eq(r).show().prependTo($(this));
			}
		});
	}
});
$(function(){
	$("[randomdisplay]").each(function() {
	$(this).randomdisplay($(this).attr("randomdisplay"));
	});
});
});
</script>-->
<!-- 有料会員バナーランダム表示 -->

</head>
<body>

<div id="center_box">
	<input type="hidden" id="show_modal" value="limit" />
	<div id="fixedbox" style="display:none;">
		<div style="position:relative; top:-12px; left:670px; width:24px;">
		<a id="close_fixed" href="javascript:void(0)"><img src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
		</div>

		<table style="margin-left:11%;margin-top: -3%">

			<tr>
				<td colspan="3">
						<div id="submit_btn">
							<span style="font-size:20px;color:#22a90c;">『助成金なう』のご利用には会員登録が必要となります。<br />まずは無料でお試しください！</span><br />
								<a class="button5" href="./index.php?action_secure_registration=true"  id="registration">今すぐ無料会員登録！</a>
						</div>
				</td>
			</tr>

			<tr>
				<td colspan="3">
						<div id="submit_btn">
							<span style="font-size:20px;color:#22a90c">すでに会員の方はこちらからログインしてください。</span><br />
								<a class="button5" href="./index.php?action_login=true" onclick="" id="login_btn">すでに会員の方はこちら</a>
						</div>
				</td>
			</tr>

			 <tr>
				<td colspan="3">
						<div id="submit_btn">
								<a class="button5" href="./lp.html" onclick="" id="login_btn"><img src='img/wakaba_mark.png' width="17px" height="25px" style="display:inline; margig-top:10px;"/> 助成金なうについて詳しくはこちら</a>
						</div>
				</td>
			</tr>


		</table>
	</div>
</div>

	<!-- ここからconteinar -->
	<div id="conteinar">
		<div id="filter" style="display:none;"></div>

		<!-- ここからwrapper -->
		<div id="wrapper">

			<div id="01"></div>

			{include file='common/header.tpl'}

			{include file='common/navi.tpl'}

			<!-- ココからメインコンテンツ -->
			{$app_ne.content}

			<!-- フッター -->
			<br />
			<div id="footer">
				<a href="http://navit-j.com/">運営会社</a>　｜　
				<a href="index.php?action_static=terms_index">利用規約</a>　｜　
				<a href="http://www.navit-j.com/privacy/index.html">Pマークについて</a>　｜　
				<a href="index.php?action_static=law_index">特定商取引法に基づく表記</a>　｜　
				<a href="https://www.navit-j.com/contactus/">問い合わせフォーム</a>
			</div>
			<!-- ここまでコンテンツ -->

		</div>
		<!-- ここまでwrapper -->

	</div>
	<!-- ここまでconteinar -->

	</body>
</html>
