{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>

</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="index.php" name="f2" method="{$form_action}" target="_blank">
<input type="hidden" name="action_e25" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
        <td width="75" align="left" valign="middle">&nbsp;</td>
    </tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar20.gif" width="740" height="30" /></div>

    <table width="740" >
        <tr><td>&nbsp; </td></tr>
        <tr>
            <td width="739">             
	<p style="font-size:18px; font-weight:bold; text-align:center; margin-top:20px;">調査回答は終了しております。</p>
            </td>
        </tr>
        
        <tr><td><p style="font-size:16px; text-align:center; margin-top:20px;">調査の御回答は終了しております。調査に御協力頂きまして、ありがとうございました。</p></td></tr>
        <tr><td><p style="font-size:16px; text-align:center; margin-top:20px;">※御回答いただいた内容の確認を希望される方はお問い合わせフォームから御連絡ください。</p></td></tr>
        <tr><td>&nbsp; </td></tr>
        <tr><td><p style="font-size:16px; text-align:center; margin-top:20px;"><a href="javascript:void(0)" onClick="javascript:do_submit();">お問い合わせはこちら</a></p></td></tr>
    </table>
</td>
</tr></table>

<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>
</form>




{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
