{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!--
var trigger_id2 = "#q9_9";
var txtarea_id2 = "#in_q9_no9_text";

$(document).ready(function (){
    
    if($(trigger_id2+":checked").val()){
        $(txtarea_id2).removeAttr("disabled");
        $(txtarea_id2).css("background-color","#ffffff");
        $("#in_q9_other").removeAttr("disabled");
        $("#in_q9_other").css("background-color","#ffffff");
    }
    else{
        $(txtarea_id2).attr("disabled", "disabled");
        $(txtarea_id2).css("background-color","#d2d1c6");
        $("#in_q9_other").attr("disabled", "disabled");
        $("#in_q9_other").css("background-color","#d2d1c6");
    }
    
    if($("#q9_1:checked").val()){
        $("#in_q9_gaitobokin").removeAttr("disabled");
        $("#in_q9_gaitobokin").css("background-color","#ffffff");
    }
    else{
        $("#in_q9_gaitobokin").attr("disabled", "disabled");
        $("#in_q9_gaitobokin").css("background-color","#d2d1c6");
    }

    if($("#q9_2:checked").val()){
        $("#in_q9_tewatashi").removeAttr("disabled");
        $("#in_q9_tewatashi").css("background-color","#ffffff");
    }
    else{
        $("#in_q9_tewatashi").attr("disabled", "disabled");
        $("#in_q9_tewatashi").css("background-color","#d2d1c6");
    }

    if($("#q9_3:checked").val()){
        $("#in_q9_bokinbako").removeAttr("disabled");
        $("#in_q9_bokinbako").css("background-color","#ffffff");
    }
    else{
        $("#in_q9_bokinbako").attr("disabled", "disabled");
        $("#in_q9_bokinbako").css("background-color","#d2d1c6");
    }

    if($("#q9_4:checked").val()){
        $("#in_q9_hurikomi").removeAttr("disabled");
        $("#in_q9_hurikomi").css("background-color","#ffffff");
    }
    else{
        $("#in_q9_hurikomi").attr("disabled", "disabled");
        $("#in_q9_hurikomi").css("background-color","#d2d1c6");
    }

    if($("#q9_5:checked").val()){
        $("#in_q9_card").removeAttr("disabled");
        $("#in_q9_card").css("background-color","#ffffff");
    }
    else{
        $("#in_q9_card").attr("disabled", "disabled");
        $("#in_q9_card").css("background-color","#d2d1c6");
    }

    if($("#q9_6:checked").val()){
        $("#in_q9_gift").removeAttr("disabled");
        $("#in_q9_gift").css("background-color","#ffffff");
    }
    else{
        $("#in_q9_gift").attr("disabled", "disabled");
        $("#in_q9_gift").css("background-color","#d2d1c6");
    }

    if($("#q9_7:checked").val()){
        $("#in_q9_syouhin").removeAttr("disabled");
        $("#in_q9_syouhin").css("background-color","#ffffff");
    }
    else{
        $("#in_q9_syouhin").attr("disabled", "disabled");
        $("#in_q9_syouhin").css("background-color","#d2d1c6");
    }

    if($("#q9_8:checked").val()){
        $("#in_q9_genbutsu").removeAttr("disabled");
        $("#in_q9_genbutsu").css("background-color","#ffffff");
    }
    else{
        $("#in_q9_genbutsu").attr("disabled", "disabled");
        $("#in_q9_genbutsu").css("background-color","#d2d1c6");
    }

    if($("#q9_9:checked").val()){
        $("#in_q9_other").removeAttr("disabled");
        $("#in_q9_other").css("background-color","#ffffff");
    }
    else{
        $("#in_q9_other").attr("disabled", "disabled");
        $("#in_q9_other").css("background-color","#d2d1c6");
    }
    
    $('#in_q8_person_kifu').val($('#in_q8_person_kifu').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    $('#in_q8_all_kifu').val($('#in_q8_all_kifu').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    
    $('#in_q9_gaitobokin').val($('#in_q9_gaitobokin').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    $('#in_q9_tewatashi').val($('#in_q9_tewatashi').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    $('#in_q9_bokinbako').val($('#in_q9_bokinbako').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    $('#in_q9_hurikomi').val($('#in_q9_hurikomi').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    $('#in_q9_card').val($('#in_q9_card').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    $('#in_q9_gift').val($('#in_q9_gift').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    $('#in_q9_syouhin').val($('#in_q9_syouhin').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    $('#in_q9_genbutsu').val($('#in_q9_genbutsu').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    $('#in_q9_other').val($('#in_q9_other').val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
         
});

$(function() {
    $(trigger_id2).on('click', TextAreaEnable1);
    
    function TextAreaEnable1(){
        if ($(trigger_id2+":checked").val()) {
            $(txtarea_id2).removeAttr("disabled");
            $(txtarea_id2).css("background-color","#ffffff");
            $("#in_q9_other").removeAttr("disabled");
            $("#in_q9_other").css("background-color","#ffffff");
        } else {
            $(txtarea_id2).attr("disabled", "disabled");
            $(txtarea_id2).css("background-color","#d2d1c6");
            $("#in_q9_other").attr("disabled", "disabled");
            $("#in_q9_other").css("background-color","#d2d1c6");
        }
    }
});


$(function() {
    $('#in_q8_person_kifu').bind('focus', funcFocus);
    $('#in_q8_person_kifu').bind('blur', funcBlur);
    $('#in_q8_all_kifu').bind('focus', funcFocus);
    $('#in_q8_all_kifu').bind('blur', funcBlur);
    
    $('#in_q9_gaitobokin').bind('focus', funcFocus);
    $('#in_q9_gaitobokin').bind('blur', funcBlur);
    $('#in_q9_tewatashi').bind('focus', funcFocus);
    $('#in_q9_tewatashi').bind('blur', funcBlur);
    $('#in_q9_bokinbako').bind('focus', funcFocus);
    $('#in_q9_bokinbako').bind('blur', funcBlur);
    $('#in_q9_hurikomi').bind('focus', funcFocus);
    $('#in_q9_hurikomi').bind('blur', funcBlur);
    $('#in_q9_card').bind('focus', funcFocus);
    $('#in_q9_card').bind('blur', funcBlur);
    $('#in_q9_gift').bind('focus', funcFocus);
    $('#in_q9_gift').bind('blur', funcBlur);
    $('#in_q9_syouhin').bind('focus', funcFocus);
    $('#in_q9_syouhin').bind('blur', funcBlur);
    $('#in_q9_genbutsu').bind('focus', funcFocus);
    $('#in_q9_genbutsu').bind('blur', funcBlur);
    $('#in_q9_other').bind('focus', funcFocus);
    $('#in_q9_other').bind('blur', funcBlur);
    
    function funcFocus(){
        // カンマを外してテキストボックスに戻す
        $(this).val($(this).val().replace(/,/g, ''));
    }
    
    function funcBlur(){
        // カンマを付けてテキストボックスに戻す
        $(this).val($(this).val().replace(/,/g, '').replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,'));
    }
});

$(function() {

    $("#q9_1").on('click', ProhibitionText1);
    $("#q9_2").on('click', ProhibitionText2);
    $("#q9_3").on('click', ProhibitionText3);
    $("#q9_4").on('click', ProhibitionText4);
    $("#q9_5").on('click', ProhibitionText5);
    $("#q9_6").on('click', ProhibitionText6);
    $("#q9_7").on('click', ProhibitionText7);
    $("#q9_8").on('click', ProhibitionText8);
    $("#q9_9").on('click', ProhibitionText9);
    
    
    function ProhibitionText1(){
        if($("#q9_1:checked").val()){
            $("#in_q9_gaitobokin").removeAttr("disabled");
            $("#in_q9_gaitobokin").css("background-color","#ffffff");
        }
        else{
            $("#in_q9_gaitobokin").attr("disabled", "disabled");
            $("#in_q9_gaitobokin").css("background-color","#d2d1c6");
        }
    }
    function ProhibitionText2(){
        if($("#q9_2:checked").val()){
            $("#in_q9_tewatashi").removeAttr("disabled");
            $("#in_q9_tewatashi").css("background-color","#ffffff");
        }
        else{
            $("#in_q9_tewatashi").attr("disabled", "disabled");
            $("#in_q9_tewatashi").css("background-color","#d2d1c6");
        }
    }
    function ProhibitionText3(){
        if($("#q9_3:checked").val()){
            $("#in_q9_bokinbako").removeAttr("disabled");
            $("#in_q9_bokinbako").css("background-color","#ffffff");
        }
        else{
            $("#in_q9_bokinbako").attr("disabled", "disabled");
            $("#in_q9_bokinbako").css("background-color","#d2d1c6");
        }
    }
    function ProhibitionText4(){
        if($("#q9_4:checked").val()){
            $("#in_q9_hurikomi").removeAttr("disabled");
            $("#in_q9_hurikomi").css("background-color","#ffffff");
        }
        else{
            $("#in_q9_hurikomi").attr("disabled", "disabled");
            $("#in_q9_hurikomi").css("background-color","#d2d1c6");
        }
    }
    function ProhibitionText5(){
        if($("#q9_5:checked").val()){
            $("#in_q9_card").removeAttr("disabled");
            $("#in_q9_card").css("background-color","#ffffff");
        }
        else{
            $("#in_q9_card").attr("disabled", "disabled");
            $("#in_q9_card").css("background-color","#d2d1c6");
        }
    }
    function ProhibitionText6(){
        if($("#q9_6:checked").val()){
            $("#in_q9_gift").removeAttr("disabled");
            $("#in_q9_gift").css("background-color","#ffffff");
        }
        else{
            $("#in_q9_gift").attr("disabled", "disabled");
            $("#in_q9_gift").css("background-color","#d2d1c6");
        }
    }
    function ProhibitionText7(){
        if($("#q9_7:checked").val()){
            $("#in_q9_syouhin").removeAttr("disabled");
            $("#in_q9_syouhin").css("background-color","#ffffff");
        }
        else{
            $("#in_q9_syouhin").attr("disabled", "disabled");
            $("#in_q9_syouhin").css("background-color","#d2d1c6");
        }
    }
    function ProhibitionText8(){
        if($("#q9_8:checked").val()){
            $("#in_q9_genbutsu").removeAttr("disabled");
            $("#in_q9_genbutsu").css("background-color","#ffffff");
        }
        else{
            $("#in_q9_genbutsu").attr("disabled", "disabled");
            $("#in_q9_genbutsu").css("background-color","#d2d1c6");
        }
    }
    function ProhibitionText9(){
        if($("#q9_9:checked").val()){
            $("#in_q9_other").removeAttr("disabled");
            $("#in_q9_other").css("background-color","#ffffff");
        }
        else{
            $("#in_q9_other").attr("disabled", "disabled");
            $("#in_q9_other").css("background-color","#d2d1c6");
        }
    }
    
    
});

/*
$(function() {
    $('#in_q8_person_kifu').keyup(checkChange(this, '#in_q8_person_kifu', '#in_q8_person_kifu_cnt'));
    $('#in_q8_person_kifu_cnt').keyup(checkChange(this, '#in_q8_person_kifu_cnt', '#in_q8_person_kifu'));
    $('#in_q8_all_kifu').keyup(checkChange(this, '#in_q8_all_kifu', '#in_q8_person_kifu'));
    $('#in_q8_all_kifu').keyup(checkChange(this, '#in_q8_all_kifu', '#in_q8_person_kifu_cnt'));
    
    function checkChange(e, trigger_id, target_id){
        var old = v=$(e).find(trigger_id).val();
        return function(){
                    v=$(e).find(trigger_id).val();
                    if(old!=v || v=="0"){
                        if(v=="0"){
                            $(target_id).val("0");
                        }

                    }
            }
    }
});
*/

//-->
</script>
{/literal}



</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}


<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e08" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
		<td width="75" align="left" valign="middle">&nbsp;</td>
	</tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar13.gif" width="740" height="30" /></div>

<div>
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10px;">
	<div>
		<div>
			<p><strong>［年間寄附額・寄附回数］</strong></p>
		</div>
			<p>
				<label><strong>問８</strong> 　【<a href="./index.php?action_e06=true&back=1">問７</a>で「1.　寄附をしたことがある」とお答えになった方にお聞きします】　<img src="images/m8.gif" width="128" height="19" /><br />
					<strong><br />
					</strong>あなたが、<strong style="border-bottom:double 3px #000; line-height:2em;">平成25年度（平成25年4月～平成26年3月）</strong>に寄附をした金額（金銭による寄附のみ）と、寄附をした回数（金銭による寄附のみ）を御入力ください。<br />
					<br />
					※<span style="border-bottom:double 3px #000; line-height:2em;">寄附額については</span>、あなた個人の金額と、<span style="border-bottom:double 3px #000; line-height:2em;">世帯全体</span>の金額に分けてお答えください。<br />
					※平成25年度は寄附をしていない場合は、「0」と御入力ください。<br />
				</label>
			</p>
                        {if is_error('in_q8')}<br /><span class="error_s">{message name="in_q8"}</span>{/if}
			<table width="740" border="1" cellspacing="0" cellpadding="4">
				<tr>
					<th scope="col" bgcolor="#33CCFF">&nbsp;</th>
					<th scope="col" bgcolor="#33CCFF">（年間）寄附金額</th>
					<th scope="col" bgcolor="#33CCFF">（年間）寄附回数</th>
				</tr>
				<tr>
					<th scope="row">あなた</th>
					<td style="text-align:center;">
                                            
                                            <input size="20" type="text"  id="in_q8_person_kifu" name="in_q8_person_kifu" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q8_person_kifu}"/>
						円{if is_error('in_q8_person_kifu')}<br /><span class="error_s">{message name="in_q8_person_kifu"}</span>{/if}</td>
                                        
					<td style="text-align:center;">
                                            
                                            <input size="20" type="text"  id="in_q8_person_kifu_cnt" name="in_q8_person_kifu_cnt" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumber(event)"  placeholder="半角数値" value="{$app.data.q8_person_kifu_cnt}"/>
						回{if is_error('in_q8_person_kifu_cnt')}<br /><span class="error_s">{message name="in_q8_person_kifu_cnt"}</span>{/if}</td>
				</tr>
				<tr>
					<th scope="row">世帯全体</th>
					<td style="text-align:center;">
                                            
                                            <input size="20" type="text"  id="in_q8_all_kifu" name="in_q8_all_kifu" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q8_all_kifu}"/>
						円{if is_error('in_q8_all_kifu')}<br /><span class="error_s">{message name="in_q8_all_kifu"}</span>{/if}</td>
					<td  style="background-color:#A3A3A3">&nbsp;</td>
				</tr>
			</table>
		<div> <br />
			<p><strong>［寄附方法］</strong>　</p>
		</div>
                                        
			<p> <strong>問９</strong> 　【<a href="./index.php?action_e06=true&back=1">問７</a>で「1.　寄附をしたことがある」とお答えになった方にお聞きします】　<img src="images/m9.gif" width="154" height="19" /> <br />
				<br />
				あなたが、寄附を行った方法は何ですか。<strong>（複数回答可能）</strong> <br />
				また、各方法の1回のおおよその寄付金額を御入力ください。 </p>
                        {if is_error('in_q9')}<br /><span class="error_s">{message name="in_q9"}</span>{/if}
                        
			<table width="740" border="1" cellspacing="0" cellpadding="4">
				<tr>
					<th scope="col" bgcolor="#33CCFF">寄付方法</th>
					<th scope="col" bgcolor="#33CCFF">行ったことがあるもの</th>
					<th scope="col" bgcolor="#33CCFF">1回の寄付金額</th>
				</tr>
				<tr>
					<td> {$app.master_q9.1}</td>
					<td style="text-align:center;"><input type="checkbox" name="in_q9_1" value="1" id="q9_1" {$app.data.q9_1} /></td>
					<td nowrap>
                                            
                                            <input size="20" type="text" id="in_q9_gaitobokin" name="in_q9_gaitobokin" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q9_gaitobokin}" />
						円{if is_error('in_q9_gaitobokin')}<br /><span class="error_s">{message name="in_q9_gaitobokin"}</span>{/if}</td>
				</tr>
				<tr>
					<td> {$app.master_q9.2}</td>
					<td style="text-align:center;"><input type="checkbox" name="in_q9_2" value="2" id="q9_2" {$app.data.q9_2} /></td>
					<td nowrap>
                                            
                                            <input size="20" type="text" id="in_q9_tewatashi" name="in_q9_tewatashi" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q9_tewatashi}" />
						円{if is_error('in_q9_tewatashi')}<br /><span class="error_s">{message name="in_q9_tewatashi"}</span>{/if}</td>
				</tr>
				<tr>
					<td> {$app.master_q9.3}</td>
					<td style="text-align:center;"><input type="checkbox" name="in_q9_3" value="3" id="q9_3" {$app.data.q9_3} /></td>
					<td nowrap>
                                            
                                            <input size="20" type="text" id="in_q9_bokinbako" name="in_q9_bokinbako" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q9_bokinbako}" />
						円{if is_error('in_q9_bokinbako')}<br /><span class="error_s">{message name="in_q9_bokinbako"}</span>{/if}</td>
				</tr>
				<tr>
					<td> {$app.master_q9.4}（<a href="#column01">※1</a>）</td>
					<td style="text-align:center;"><input type="checkbox" name="in_q9_4" value="4" id="q9_4" {$app.data.q9_4} /></td>
					<td nowrap>
                                            
                                            <input size="20" type="text" id="in_q9_hurikomi" name="in_q9_hurikomi" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q9_hurikomi}" />
						円{if is_error('in_q9_hurikomi')}<br /><span class="error_s">{message name="in_q9_hurikomi"}</span>{/if}</td>
				</tr>
				<tr>
					<td> {$app.master_q9.5}（<a href="#column01">※1</a>）<br />（ポイント・電子マネーの利用含む） </td>
					<td style="text-align:center;"><input type="checkbox" name="in_q9_5" value="5" id="q9_5" {$app.data.q9_5} /></td>
					<td nowrap>
                                            
                                            <input size="20" type="text" id="in_q9_card" name="in_q9_card" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q9_card}" />
						円{if is_error('in_q9_card')}<br /><span class="error_s">{message name="in_q9_card"}</span>{/if}</td>
				</tr>
				<tr>
					<td> {$app.master_q9.6}（<a href="#column02">※2</a>）</td>
					<td style="text-align:center;"><input type="checkbox" name="in_q9_6" value="6" id="q9_6" {$app.data.q9_6} /></td>
					<td nowrap>
                                            
                                            <input size="20" type="text" id="in_q9_gift" name="in_q9_gift" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q9_gift}" />
						円{if is_error('in_q9_gift')}<br /><span class="error_s">{message name="in_q9_gift"}</span>{/if}</td>
				</tr>
				<tr>
					<td> {$app.master_q9.7}（<a href="#column03">※3</a>）</td>
					<td style="text-align:center;"><input type="checkbox" name="in_q9_7" value="7" id="q9_7" {$app.data.q9_7} /></td>
					<td nowrap>
                                            
                                            <input size="20" type="text" id="in_q9_syouhin" name="in_q9_syouhin" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q9_syouhin}" />
						円{if is_error('in_q9_syouhin')}<br /><span class="error_s">{message name="in_q9_syouhin"}</span>{/if}</td>
				</tr>
				<tr>
					<td> {$app.master_q9.8}</td>
					<td style="text-align:center;"><input type="checkbox" name="in_q9_8" value="8" id="q9_8" {$app.data.q9_8} /></td>
					<td nowrap>
                                            
                                            <input size="20" type="text" id="in_q9_genbutsu" name="in_q9_genbutsu" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q9_genbutsu}" />
						円{if is_error('in_q9_genbutsu')}<br /><span class="error_s">{message name="in_q9_genbutsu"}</span>{/if}</td>
				</tr>
				<tr>
					<td> {$app.master_q9.9}<br />
                                                                                    <label for="in_q9_no9_text">
                                                                                        <input size="50" type="text" name="in_q9_no9_text" maxlength="255" id="in_q9_no9_text" value="{$app.data.q9_no9_text}"  onkeyup="CountDownLength('zan_q9_no9_text', value, 255);" />
                                                                                        {if is_error('in_q9_no9_text')}<br /><span class="error_s">{message name="in_q9_no9_text"}</span>{/if}
                                                                                        <span id="zan_q9_no9_text">（残り255文字）</span>
                                                                                    </label>
					</td>
					<td style="text-align:center;"><input type="checkbox" name="in_q9_9" value="9" id="q9_9" {$app.data.q9_9} /></td>
					<td nowrap>
                                            
                                                                                    <input size="20" type="text" id="in_q9_other" name="in_q9_other" maxlength="10" style="text-align:right;ime-mode:disabled;" onkeydown="return OnlyNumberComma(event)"  placeholder="半角数値" value="{$app.data.q9_other}" />
						円{if is_error('in_q9_other')}<br /><span class="error_s">{message name="in_q9_other"}</span>{/if}</td>
				</tr>
			</table>

			<div id="column">
    	<table>
			<tr>
				<td valign="top" id="column01" nowrap>※1&nbsp;&nbsp;</td>
				<td valign="top">口座引落とし、クレジットカードについては１回あたりの額を御入力ください。</td>
			</tr>
            <tr>
				<td valign="top" id="column02" nowrap>※2&nbsp;&nbsp;</td>
				<td valign="top">「マッチングギフト」とは、企業や団体などが社会貢献のために寄附や義捐金を募る際、寄せられた金額に対して企業側が金額の上乗せを行い、寄附金額を増やした上で同じ寄附対象に寄附をするという取り組み。</td>
			</tr>
			<tr>
				<td valign="top" id="column03" nowrap>※3&nbsp;&nbsp;</td>
				<td valign="top">寄附付商品については、よく購入する商品の価格を御入力ください。</td>
			</tr>
		</table>
	</div>
		<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
			</tr>
		</table>
                        
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e06=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->

	</div>
</div>
<div><img src="images/border1.gif" width="740
" height="3" /></div>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
	</tr>
</table>
</form>


{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
