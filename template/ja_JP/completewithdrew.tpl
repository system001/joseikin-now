<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal} 
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.lightbox_me.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	$('label').balloon();
	if ($('#privacy_1').is(':checked')) {
		$('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
	} else {
		$('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
	}
});
</script>
{/literal} 
</head>
<body>

<!-- ここからconteinar -->
<div id="conteinar">
<!-- ここからwrapper -->
<div id="wrapper">

<!-- ここからheader -->

<div id="01"></div>

{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->

<div>

<div id="03"></div>

<br />

{if $app.db_regist_result == "-1" || $app.mail_send_result == "-1"}
<div class="top_header_title" ><span style="border-bottom:1px #562E39 solid;">退会手続きエラー</span></div>
<div style="margin-left: 20px;margin-right: 20px;">
	<span style="font-weight:bold;font-size:100%;">退会手続きができませんでした。</span><br />
	大変お手数ではございますが再度こちらから退会手続きをお願い致します。>> 
	<a href="./index.php?action_mypage=true"> マイページへ</a><br />
	<br />
	<br />
	<span style="color:#666;font-size:80%;">
		【ご登録に失敗する場合、以下の原因が考えられます】<br />
		※通信環境の混雑等の理由によるもの<br />
		<br />
		<br />
	</span>  
</div>
{else}
<div class="top_header_title" ><span style="border-bottom:1px #562E39 solid;">退会手続き完了</span></div>
<div style="margin-left: 20px;margin-right: 20px;">
	<span style="font-weight:bold;font-size:100%;">ご利用いただき、ありがとうございました。</span><br />
	ご登録頂いていたメールアドレスに退会手続き完了のメールを送信致しました。メール受信のご確認をお願い致します。<br />
	ご利用いただいていたログインIDおよびパスワードは退会手続き完了後はご利用いただけません。<br />
	<br />
	  <span style="color:#666;font-size:80%;">
		※通信環境の混雑等の理由によりメールの到着に少々お時間がかかる場合がございます。<br /><br />
		※迷惑メール防止機能により当サイトからのメールが迷惑メールと間違えられ、メール受信画面に表示されない場合が<br />
		&nbsp;&nbsp;&nbsp;ございます。迷惑メールフォルダやゴミ箱に自動的に振り分けられている場合がございますので、一度ご確認頂きま<br />
		&nbsp;&nbsp;&nbsp;すようお願い致します。<br />
	  </span>  
</div>
{/if}
<div style="font-size:0.7em;margin-left: 120px;">
</div>

<div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:400px;">
		<!--<a class="button" href="javascript:void(0)" onclick="javascript:history.back();" id="">戻る</a>-->
		<a class="button2" href="index.php" id="to_top_btn">TOPへ</a>
	</div>
</div>

</div>
		
<input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
<input type="hidden" name="in_company_kana" id="in_company_kana" value="{$app.data.company_kana}">
<input type="hidden" name="in_branch_name" id="in_branch_name" value="{$app.data.branch_name}"> 
<input type="hidden" name="in_zip1" id="in_zip1" value="{$app.data.zip1}">
<input type="hidden" name="in_zip2" id="in_zip2" value="{$app.data.zip2}"> 
<input type="hidden" name="in_address" id="in_address" value="{$app.data.address}"> 
<input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
<input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}"> 
<input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}"> 
<input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}">
<input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}">
<input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}">
<input type="hidden" name="in_fax_no1" id="in_fax_no1" value="{$app.data.fax_no1}">
<input type="hidden" name="in_fax_no2" id="in_fax_no2" value="{$app.data.fax_no2}"> 
<input type="hidden" name="in_fax_no3" id="in_fax_no3" value="{$app.data.fax_no3}"> 
<input type="hidden" name="in_department_name" id="in_department_name" value="{$app.data.department_name}"> 
<input type="hidden" name="in_post_name" id="in_post_name" value="{$app.data.post_name}"> 
<input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="{$app.data.contractor_lname}">
<input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="{$app.data.contractor_fname}">
<input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="{$app.data.contractor_lkana}">
<input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="{$app.data.contractor_fkana}"> 
<input type="hidden" name="in_email" id="in_" value="{$app.data.email}"> 
<input type="hidden" name="in_privacy" id="in_privacy" value="{$app.data.privacy}">

<input type="hidden" name="back" id="back" value="0"> 
		
</form>		   
	<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

</div>

<!-- ここからfooter -->
<br />
<div id="footer">
	
</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>
<!-- スムーズスクロール -->
{/literal}

{include file='common/track.tpl'}

</body>
</html>
