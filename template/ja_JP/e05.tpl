{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!--
var trigger_id1 = "#q5_11";
var trigger_id2 = "#q6_8";
var txtarea_id1 = "#in_q5_no11_text";
var txtarea_id2 = "#in_q6_no8_text";
var zan_text_id1 = "#zan_q5_no11_text";
var zan_text_id2 = "#zan_q6_no8_text";

$(document).ready(function (){
    text_val1 = $(txtarea_id1).val();
    text_val2 = $(txtarea_id2).val();
    CountDownLength('zan_q5_no11_text', text_val1, 255);
    CountDownLength('zan_q6_no8_text', text_val2, 255);
    
    if($(trigger_id1+":checked").val()){
        $(txtarea_id1).removeAttr("disabled");
        $(txtarea_id1).css("background-color","#ffffff");
        $(zan_text_id1).css("color","#000000");
    }
    else{
        $(txtarea_id1).attr("disabled", "disabled");
        $(txtarea_id1).css("background-color","#d2d1c6");
        $(zan_text_id1).css("color","#d2d1c6");
    }

    if($(trigger_id2+":checked").val()){
        $(txtarea_id2).removeAttr("disabled");
        $(txtarea_id2).css("background-color","#ffffff");
        $(zan_text_id2).css("color","#000000");
    }
    else{
        $(txtarea_id2).attr("disabled", "disabled");
        $(txtarea_id2).css("background-color","#d2d1c6");
        $(zan_text_id2).css("color","#d2d1c6");
    }
    
    //For checkbox initialize
    if($("#q5_1:checked").val() ||
           $("#q5_2:checked").val() ||
           $("#q5_3:checked").val() ||
           $("#q5_4:checked").val() ||
           $("#q5_5:checked").val() ||
           $("#q5_6:checked").val() ||
           $("#q5_7:checked").val() ||
           $("#q5_8:checked").val() ||
           $("#q5_9:checked").val() ||
           $("#q5_11:checked").val() ) {
            //1,2,3,4,5,6,7,8,9,11のどれか1つでもチェックされていたら10をチェック不可に
            $("#q5_10").attr("disabled", "disabled");
            $("#q5_10").css("background-color","#d2d1c6");
            $("#q5_10").css("color","#d2d1c6");
    }else{
            //1,2,3,4,5,6,7,8,9,11が1つもチェックされていない場合10をチェック可能に
            $("#q5_10").removeAttr("disabled");
            $("#q5_10").css("background-color","#ffffff");
            $("#q5_10").css("color","#000000");
    }
    if($("#q5_10:checked").val()) {
            ////1,2,3,4,5,6,7,8,9,11をチェック不可に
            $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").attr("disabled", "disabled");
            $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").css("background-color","#d2d1c6");
            $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").css("color","#d2d1c6");
            ////10をチェック可能に
            $("#q5_10").removeAttr("disabled");
            $("#q5_10").css("background-color","#ffffff");
            $("#q5_10").css("color","#000000");
    } else {
        ////1,2,3,4,5,6,7,8,9,11をチェック可能に
        $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").removeAttr("disabled");
        $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").css("background-color","#ffffff");
        $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").css("color","#000000");
    }
    
    if($("#q6_1:checked").val() ||
           $("#q6_2:checked").val() ||
           $("#q6_3:checked").val() ||
           $("#q6_4:checked").val() ||
           $("#q6_5:checked").val() ||
           $("#q6_6:checked").val() ||
           $("#q6_8:checked").val() ) {
                //1,2,3,4,5,6,8のどれか1つでもチェックされていたら7をチェック不可に
                $("#q6_7").attr("disabled", "disabled");
                $("#q6_7").css("background-color","#d2d1c6");
                $("#q6_7").css("color","#d2d1c6");
    }else{
            //1,2,3,4,5,6,8が1つもチェックされていない場合10をチェック可能に
            $("#q6_7").removeAttr("disabled");
            $("#q6_7").css("background-color","#ffffff");
            $("#q6_7").css("color","#000000");
    }
     if($("#q6_7:checked").val()) {
            ////1,2,3,4,5,6,8をチェック不可に
            $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").attr("disabled", "disabled");
            $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").css("background-color","#d2d1c6");
            $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").css("color","#d2d1c6");
            ////7をチェック可能に
            $("#q6_7").removeAttr("disabled");
            $("#q6_7").css("background-color","#ffffff");
            $("#q6_7").css("color","#000000");
    } else {
        ////1,2,3,4,5,6,8をチェック可能に
        $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").removeAttr("disabled");
        $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").css("background-color","#ffffff");
        $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").css("color","#000000");
    }

    
    
});

$(function() {
    $(trigger_id1).on('click', TextAreaEnable1);
    $(trigger_id2).on('click', TextAreaEnable1);
    
    function TextAreaEnable1(){
        if ($(trigger_id1+":checked").val()) {
            $(txtarea_id1).removeAttr("disabled");
            $(txtarea_id1).css("background-color","#ffffff");
            $(zan_text_id1).css("color","#000000");
        } else {
            $(txtarea_id1).attr("disabled", "disabled");
            $(txtarea_id1).css("background-color","#d2d1c6");
            $(zan_text_id1).css("color","#d2d1c6");
        }
        
        if ($(trigger_id2+":checked").val()) {
            $(txtarea_id2).removeAttr("disabled");
            $(txtarea_id2).css("background-color","#ffffff");
            $(zan_text_id2).css("color","#000000");
        } else {
            $(txtarea_id2).attr("disabled", "disabled");
            $(txtarea_id2).css("background-color","#d2d1c6");
            $(zan_text_id2).css("color","#d2d1c6");
        }
    }
});

$(function() {

    $("#q5_1").on('click', ProhibitionItem10);
    $("#q5_2").on('click', ProhibitionItem10);
    $("#q5_3").on('click', ProhibitionItem10);
    $("#q5_4").on('click', ProhibitionItem10);
    $("#q5_5").on('click', ProhibitionItem10);
    $("#q5_6").on('click', ProhibitionItem10);
    $("#q5_7").on('click', ProhibitionItem10);
    $("#q5_8").on('click', ProhibitionItem10);
    $("#q5_9").on('click', ProhibitionItem10);
    $("#q5_10").on('click', ProhibitionItemOther);
    $("#q5_11").on('click', ProhibitionItem10);
    
    function ProhibitionItem10(){
        
        if($("#q5_1:checked").val() ||
           $("#q5_2:checked").val() ||
           $("#q5_3:checked").val() ||
           $("#q5_4:checked").val() ||
           $("#q5_5:checked").val() ||
           $("#q5_6:checked").val() ||
           $("#q5_7:checked").val() ||
           $("#q5_8:checked").val() ||
           $("#q5_9:checked").val() ||
           $("#q5_11:checked").val() ) {
                //1,2,3,4,5,6,7,8,9,11のどれか1つでもチェックされていたら10をチェック不可に
                $("#q5_10").attr("disabled", "disabled");
                $("#q5_10").css("background-color","#d2d1c6");
                $("#q5_10").css("color","#d2d1c6");
        }else{
                //1,2,3,4,5,6,7,8,9,11が1つもチェックされていない場合7をチェック可能に
                $("#q5_10").removeAttr("disabled");
                $("#q5_10").css("background-color","#ffffff");
                $("#q5_10").css("color","#000000");
        } 
        
    }
    
    function ProhibitionItemOther(){
        
        if($("#q5_10:checked").val()) {
            ////1,2,3,4,5,6,7,8,9,11をチェック不可に
            $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").attr("disabled", "disabled");
            $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").css("background-color","#d2d1c6");
            $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").css("color","#d2d1c6");
            ////10をチェック可能に
            $("#q5_10").removeAttr("disabled");
            $("#q5_10").css("background-color","#ffffff");
            $("#q5_10").css("color","#000000");
        } else {
            ////1,2,3,4,5,6,7,8,9,11をチェック可能に
            $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").removeAttr("disabled");
            $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").css("background-color","#ffffff");
            $("#q5_1,#q5_2,#q5_3,#q5_4,#q5_5,#q5_6,#q5_7,#q5_8,#q5_9,#q5_11").css("color","#000000");
        }
    }
    
});

$(function() {

    $("#q6_1").on('click', ProhibitionItem7);
    $("#q6_2").on('click', ProhibitionItem7);
    $("#q6_3").on('click', ProhibitionItem7);
    $("#q6_4").on('click', ProhibitionItem7);
    $("#q6_5").on('click', ProhibitionItem7);
    $("#q6_6").on('click', ProhibitionItem7);
    $("#q6_7").on('click', ProhibitionItemOther);
    $("#q6_8").on('click', ProhibitionItem7);
    
    function ProhibitionItem7(){
        
        if($("#q6_1:checked").val() ||
           $("#q6_2:checked").val() ||
           $("#q6_3:checked").val() ||
           $("#q6_4:checked").val() ||
           $("#q6_5:checked").val() ||
           $("#q6_6:checked").val() ||
           $("#q6_8:checked").val() ) {
                //1,2,3,4,5,6,8のどれか1つでもチェックされていたら7をチェック不可に
                $("#q6_7").attr("disabled", "disabled");
                $("#q6_7").css("background-color","#d2d1c6");
                $("#q6_7").css("color","#d2d1c6");
        }else{
                //1,2,3,4,5,6,8が1つもチェックされていない場合10をチェック可能に
                $("#q6_7").removeAttr("disabled");
                $("#q6_7").css("background-color","#ffffff");
                $("#q6_7").css("color","#000000");
        } 
        
    }
    
    function ProhibitionItemOther(){
        
        if($("#q6_7:checked").val()) {
            ////1,2,3,4,5,6,8をチェック不可に
            $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").attr("disabled", "disabled");
            $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").css("background-color","#d2d1c6");
            $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").css("color","#d2d1c6");
            ////7をチェック可能に
            $("#q6_7").removeAttr("disabled");
            $("#q6_7").css("background-color","#ffffff");
            $("#q6_7").css("color","#000000");
        } else {
            ////1,2,3,4,5,6,8をチェック可能に
            $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").removeAttr("disabled");
            $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").css("background-color","#ffffff");
            $("#q6_1,#q6_2,#q6_3,#q6_4,#q6_5,#q6_6,#q6_8").css("color","#000000");
        }
    }
    
});


//-->
</script>
{/literal}

</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}


<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e06" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
		<td width="75" align="left" valign="middle">&nbsp;</td>
	</tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
	<div><img src="images/bar12.gif" width="740" height="30" /></div>
<div>
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10px;">
	<div>
		<p><strong>
			［参加の妨げとなる要因］</strong></p>
	</div>
		<p> <strong>問５</strong>　【全員の方にお聞きします】　<img src="images/m5.gif" width="128" height="19" /><br />
			<br />
			ボランティア活動に参加することに妨げとなることはありますか。<strong>（複数回答可能）</strong>
                                                <br />
                                                {if is_error('in_q5')}<br /><span class="error">{message name="in_q5"}</span>{/if}
                                                {if is_error('in_q5_no11_text')}<br /><span class="error">{message name="in_q5_no11_text"}</span>{/if}
                                                <br />
                                                <label><input type="checkbox" name="in_q5_1" id="q5_1" value="1" {$app.data.q5_1} />{$app.master_q5.1}</label><br />
                                                <label><input type="checkbox" name="in_q5_2" id="q5_2" value="2" {$app.data.q5_2} />{$app.master_q5.2}</label><br />
                                                <label><input type="checkbox" name="in_q5_3" id="q5_3" value="3" {$app.data.q5_3} />{$app.master_q5.3}</label><br />
                                                <label><input type="checkbox" name="in_q5_4" id="q5_4" value="4" {$app.data.q5_4} />{$app.master_q5.4}</label><br />
                                                <label><input type="checkbox" name="in_q5_5" id="q5_5" value="5" {$app.data.q5_5} />{$app.master_q5.5}</label><br />
                                                <label><input type="checkbox" name="in_q5_6" id="q5_6" value="6" {$app.data.q5_6} />{$app.master_q5.6}</label><br />
                                                <label><input type="checkbox" name="in_q5_7" id="q5_7" value="7" {$app.data.q5_7} />{$app.master_q5.7}</label><br />
                                                <label><input type="checkbox" name="in_q5_8" id="q5_8" value="8" {$app.data.q5_8} />{$app.master_q5.8}</label><br />
                                                <label><input type="checkbox" name="in_q5_9" id="q5_9" value="9" {$app.data.q5_9} />{$app.master_q5.9}</label><br />
                                                <label><input type="checkbox" name="in_q5_10" id="q5_10" value="10" {$app.data.q5_10} />{$app.master_q5.10}</label><br />
                                                <label><input type="checkbox" name="in_q5_11" id="q5_11" value="11" {$app.data.q5_11} />{$app.master_q5.11}</label><br />
                                                <textarea name="in_q5_no11_text"  id="in_q5_no11_text" rows="6" cols="60" onkeyup="CountDownLength('zan_q5_no11_text', value, 255);" maxlength="255">{$app.data.q5_no11_text}</textarea>
                                                <span id="zan_q5_no11_text">（残り255文字）</span>
			<br />
		</p>
	<div id="column">
		<p style="text-indent: 1em;margin-bottom:0;">「NPO」とは「Non Profit Organization」又は「Not for Profit Organization」の略称で、様々な社会貢献活動を行い、団体の構成に対し収益を分配することを目的としない団体の総称です。したがって、収益を目的とする事業を行うこと自体は認められますが、事業で得た利益は、様々な社会貢献活動に充てることになります。</p>
		<p style="text-indent: 1em;margin-top:0;">このうち、特定非営利活動促進法に基づき法人格を取得した法人を、「特定非営利活動法人」といいます。</p>
	</div>
			<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
			</tr>
		</table>
	<div>
		<p><strong><br />
			［国・自治体等への要望］</strong></p>
	</div>
		<p> <strong>問６</strong>　【全員の方にお聞きします】　<img src="images/m6.gif" width="128" height="19" /><br />
			<br />
			ボランティア活動について、国や地方自治体等にどのような事を望みますか。 <strong>（複数回答可能）</strong>
			<br />
                                                {if is_error('in_q6')}<br /><span class="error">{message name="in_q6"}</span>{/if}
                                                {if is_error('in_q6_no8_text')}<br /><span class="error">{message name="in_q6_no8_text"}</span>{/if}
                                                <br />
                                                <label><input type="checkbox" name="in_q6_1" id="q6_1" value="1" {$app.data.q6_1} />{$app.master_q6.1}</label><br />
                                                <label><input type="checkbox" name="in_q6_2" id="q6_2" value="2" {$app.data.q6_2} />{$app.master_q6.2}</label><br />
                                                <label><input type="checkbox" name="in_q6_3" id="q6_3" value="3" {$app.data.q6_3} />{$app.master_q6.3}</label><br />
                                                <label><input type="checkbox" name="in_q6_4" id="q6_4" value="4" {$app.data.q6_4} />{$app.master_q6.4}</label><br />
                                                <label><input type="checkbox" name="in_q6_5" id="q6_5" value="5" {$app.data.q6_5} />{$app.master_q6.5}</label><br />
                                                <label><input type="checkbox" name="in_q6_6" id="q6_6" value="6" {$app.data.q6_6} />{$app.master_q6.6}</label><br />
                                                <label><input type="checkbox" name="in_q6_7" id="q6_7" value="7" {$app.data.q6_7} />{$app.master_q6.7}</label><br />
                                                <label><input type="checkbox" name="in_q6_8" id="q6_8" value="8" {$app.data.q6_8} />{$app.master_q6.8}</label><br />
                                                <textarea name="in_q6_no8_text"  id="in_q6_no8_text" rows="6" cols="60" onkeyup="CountDownLength('zan_q6_no8_text', value, 255);" maxlength="255">{$app.data.q6_no8_text}</textarea>
                                                <span id="zan_q6_no8_text">（残り255文字）</span>
			<br />
		</p>
</div>
<table width="700" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
	</tr>
</table>
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e04=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->   

<div><img src="images/border1.gif" width="740
" height="3" /></div>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
	</tr>
</table>
</form>

{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
