<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」トップページ | 株式会社ナビット| 東京</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="助成金・補助金の情報を簡単に検索！エリアやキーワードを入力するだけで条件に合った自治体、財団から公示される助成金・補助金がすぐに見つけられます。">
<meta name="author" content="株式会社ナビット">
<meta property="og:title" content="助成金・補助金の検索サービス「助成金なう」トップページ | 株式会社ナビット| 東京" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.navit-j.com/" />
<meta property="og:image" content="https://www.navit-j.com/img/tasukekun.png" />
<meta property="fb:app_id" content="584166861742163" />
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="stylesheet" href="css/slick.css">
<link rel="stylesheet" href="css/slick-theme.css">
<link rel="shortcut icon" href="">

<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="js/jquery.jStageAligner.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script src="js/slick.min.js"></script>

{literal}
<script type="text/javascript">
$(window).load(function(){
	$("#gnavi").sticky({ topSpacing: 0, center:true, className:"hey" });
});
$(function(){
	$('label').balloon();
	$("#fixedbox").jStageAligner("CENTER_MIDDLE");
});

function doblur() {
	var element = document.getElementById("name");
	element.blur();
}
$( document ).ready( function() {
	$( ".favorite_button" ).click( function() {
		//var isFavorite = $(this).attr( 'favorite' );
		var conentKind = $(this).attr( 'content' );
		var contentId  = $(this).attr( 'target' );
		var add        = true;
		if ( $( this ).attr( "favorite" ) == "1" ) {
			add = false;
		}

		$.ajax( {
			type: "POST",
			url: "index.php",
			data: "action_favorite=true&in_add=" + add + "&in_content_type=" + conentKind + "&in_contents_id=" + contentId,
			success: function(msg){
				$( "#favorite_button_" + contentId ).attr( 'favorite', msg );
				setFavoriteButtonName();
			}
		} );
	} );
	setFavoriteButtonName();
});

function setFavoriteButtonName()
{
	$( ".favorite_button" ).each( function ( index, element ) {
		element = $(element);
		if ( element.attr( 'favorite' ) == "1" ) {
			element.val( "お気に入りから削除" );
		}
		else {
			element.val( "お気に入りに追加" );
		}
	} );
}
</script>


<script type="text/javascript">
$(window).load(function(){
	$("#gnavi").sticky({ topSpacing: 0, center:true, className:"hey" });
});
$(function(){
	$('label').balloon();
	$("#fixedbox").jStageAligner("CENTER_MIDDLE");
});

function doblur() {
	var element = document.getElementById("name");
	element.blur();
}
</script>

<!-- スライダーオプション設定 -->
<script>
$(function(){
$('.slider').slick({
accessibility: true,
autoplay:false,
autoplaySpeed:3000,
pauseOnHover:true,
dots: false,
arrows: true
});
}); 
</script>
<!-- スライダーオプション設定 -->

{/literal}

<!-- 有料会員バナーランダム表示 -->
<!--<script type="text/javascript">
jQuery(function($) {
$.fn.extend({
	randomdisplay : function(num) {
		return this.each(function() {
			var chn = $(this).children().hide().length;
			for(var i = 0; i < num && i < chn; i++) {
				var r = parseInt(Math.random() * (chn - i)) + i;
			$(this).children().eq(r).show().prependTo($(this));
			}
		});
	}
});
$(function(){
	$("[randomdisplay]").each(function() {
	$(this).randomdisplay($(this).attr("randomdisplay"));
	});
});
});
</script>-->
<!-- 有料会員バナーランダム表示 -->

</head>
<body>

<div id="center_box">
	<input type="hidden" id="show_modal" value="limit" />
	<div id="fixedbox" style="display:none;">
		<div style="position:relative; top:-12px; left:670px; width:24px;">
		<a id="close_fixed" href="javascript:void(0)"><img src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
		</div>

		<table style="margin-left:11%;margin-top: -3%;">

			<tr>
				<td colspan="3">
						<div id="submit_btn">
							<span style="font-size:20px;color:#22a90c;">『助成金なう』のご利用には会員登録が必要となります。<br />まずは無料でお試しください！</span><br />
								<a class="button5" href="./index.php?action_secure_registration=true"  id="registration">今すぐ無料会員登録！</a>
						</div>
				</td>
			</tr>

			<tr>
				<td colspan="3">
						<div id="submit_btn">
							<span style="font-size:20px;color:#22a90c">すでに会員の方はこちらからログインしてください。</span><br />
								<a class="button5" href="./index.php?action_login=true" onclick="" id="login_btn">すでに会員の方はこちら</a>
						</div>
				</td>
			</tr>

			 <tr>
				<td colspan="3">
						<div id="submit_btn">
								<a class="button5" href="./lp.html" onclick="" id="login_btn"><img src='img/wakaba_mark.png' width="17px" height="25px" style="display:inline; margig-top:10px;"/> 助成金なうについて詳しくはこちら</a>
						</div>
				</td>
			</tr>


		</table>
	</div>
</div>

	<!-- ここからconteinar -->
	<div id="conteinar">
		<div id="filter" style="display:none;"></div>

		<!-- ここからwrapper -->
		<div id="wrapper">

			<div id="01"></div>

<!-- ヘッダー読み込みファイル -->
			{include file='common/header.tpl'}
<!-- ヘッダー読み込みファイル -->

<!-- ナビ読み込みファイル -->
			{include file='common/navi.tpl'}
<!-- ナビ読み込みファイル -->



{if $app.rank != 0}
<!-- ここから有料会員向け表示 -->

<!-- DVD販売 img --> 
<!--<div style="margin:20px 0 10px 0;">
	<a href="https://www.navit-j.com/ec/html/products/detail.php?product_id=221" target="_blank" /><img src="img/160818saiyou_dvd.jpg" onmouseover="this.src='img/160818saiyou_dvd_d.jpg'" onmouseout="this.src='img/160818saiyou_dvd.jpg'" alt="DVD販売"></a>
</div>
<!-- DVD販売 img -->

			<div class="reccomented_projects">
			<h2><img src="img/title_reccomented.jpg" alt="おすすめ案件" /></h2>
<div align="center" class="favorite_login"><span style="color:#ad7a27;">{$app.name} 様が会員登録時に選んでいただいた条件を元に、<br />最新の助成金・補助金情報を表示します。</span></div>


	<form class="submit_form" action="?action_secure_updateprofile=true" method="get" target="_blank">
		<input type="hidden" name="action_secure_updateprofile" value= "true"/>
		<input type="submit" class="submit_btn2" value= "登録条件の変更はこちら"/>
	</form>

				<div class="autonomy_projects">
				<h3 class="h3_title">自治体案件 <span style="font-size:70%; font-weight:normal;">(ご登録された都道府県内全域の案件が表示されます)</span></h3>

					<table>
{foreach from=$app_ne.recommended_g_contents item=item key=code}
						<tr>
							<td width="820">
								<a href="?action_display=1&in_callkind=government&in_contents_id={$item.contents_org_id}">{$item.main_title}</a>
							</td>
							<td>
									<input
										type    ="button"
										id      ="favorite_button_{$item.contents_id}"
										class   ="favorite_button"
										content ="1"
										target  ='{$item.contents_id}'
										favorite="{$item.is_favorite}"
										value   ="お気に入りに追加" />
							</td>
						</tr>
{/foreach}
					</table>
				<hr class="hr" />
				<form class="submit_form" action="?action_result=true" method="post" target="_blank">
					<input type="hidden" name="action_index" value= "true"/>
					<input type="hidden" name="government_prefecture" value= "13000"/>
					<input type="hidden" name="government_order" value= "created"/>
					<input type="hidden" name="government_is_in_season" value= "true"/>
					<input type="submit" class="submit_btn" style="width:400px;" value= "通常単価での年間金額目安の自治体おすすめ案件一覧"/>
				</form>
				</div>

				<div class="foundation_projects">
				<h3 class="h3_title2">財団案件 <span style="font-size:70%; font-weight:normal;">(ご登録された都道府県内全域の案件が表示されます)</span></h3>
					<table>
{foreach from=$app_ne.recommended_f_contents item=item key=code}
						<tr>
							<td width="820">
								<a href="?action_display=1&in_callkind=foundation&in_contents_id={$item.contents_org_id}">{$item.main_title}</a>
							</td>
							<td>
								<input
									type    ="button"
									id      ="favorite_button_{$item.contents_id}"
									class   ="favorite_button"
									content ="2"
									target  ='{$item.contents_id}'
									favorite="{$item.is_favorite}"
									value   ="お気に入りに追加" />
							</td>
						</tr>
{/foreach}
					</table>
				<hr class="hr" />
				<form class="submit_form" action="?action_result=true" method="post" target="_blank">
					<input type="hidden" name="action_index" value= "true"/>
					<input type="hidden" name="foundation_prefecture" value= "13000"/>
					<input type="hidden" name="foundation_order" value= "created"/>
					<input type="hidden" name="foundation_is_in_season" value= "true"/>
					<input type="hidden" name="content_mode" value= "foundation"/>
					<input type="submit" class="submit_btn" style="width:400px;" value= "通常単価での年間金額目安の財団おすすめ案件一覧"/>
				</form>
			</div>

				</div>


			<div class="favorite_autonomy_projects">
				<h2><img src="img/title_favorite.jpg" alt="お気に入り案件" /></h2>
					<h3 class="h3_title">自治体案件 <span style="font-size:70%; font-weight:normal;">(表示件数は5件となっています)</span></h3>
						<table>
	{foreach from=$app_ne.favorite_g_items item=item key=code}
							<tr>
								<td width="820">
									<a href="?action_display=1&in_callkind=government&in_contents_id={$item.contents_org_id}">{$item.main_title}</a>
								</td>
							</tr>
	{/foreach}
						</table>
				</div>
				<div class="favorite_foundation_projects">
					<h3 class="h3_title2">財団案件 <span style="font-size:70%; font-weight:normal;">(表示件数は5件となっています)</span></h3>
						<table>
	{foreach from=$app_ne.favorite_f_items item=item key=code}
							<tr>
								<td width="820">
						<a href="?action_display=1&in_callkind=foundation&in_contents_id={$item.contents_org_id}">{$item.main_title}</a>
								</td>
							<td>
								<input
									type    ="button"
									id      ="favorite_button_{$item.contents_id}"
									class   ="favorite_button"
									content ="2"
									target  ='{$item.contents_id}'
									favorite="{$item.is_favorite}"
									value   ="お気に入りに追加" />
							</td>
							</tr>
	{/foreach}
						</table>
					<hr class="hr" />
					<form class="submit_form" action="index.php#favorite_autonomy_projects" method="get" target="_blank">
						<input type="hidden" name="action_mypage" value= "true"/>
						<input type="submit" class="submit_btn" value= "お気に入り案件一覧"/>
					</form>
				</div>
				<div class="reccomented10_projects">
					<h2><img src="img/title_seasonal.jpg" alt="お気に入り案件" /></h2>
						<h3 class="h3_title3">旬の助成金・補助金</h3>
							<table>
		{foreach from=$app_ne.recommended_contents item=item key=code}
								<tr>
									<td width="820">
							<a href="?action_display=1&in_callkind=government&in_contents_id={$item.contents_org_id}">{$item.main_title}</a>
									</td>
								</tr>
		{/foreach}
							</table>
					<hr class="hr" />
					<form class="submit_form" action="index.php" method="get" target="_blank">
						<input type="hidden" name="action_result" value="true"/>
						<input type="hidden" name="content_mode"  value="recommend"/>
						<input type="submit" class="submit_btn" value= "旬の助成金・補助金一覧"/>
					</form>
				</div>

<!-- 今週注目の助成金 ここから-->              
<!-- 今週注目の助成金 ここまで-->              

<!-- ここまで有料会員向け表示 -->
{else}



{if empty($app.name)}
<!-- ここからログインしていないユーザー、無料会員向け表示 -->
			<!-- ここからメインビジュアルslider -->

<div class="slider" style="height:250px;">
    <div>
			<div id="mvis">
				<div style="float:left"><img src="img/mainvis00l.gif" alt="01"></div>

				<div style="float:left"><a href="https://www.navit-j.com/service/joseikin-now/blog/?p=4191" target="_blank"><img src="img/mainvis00r.gif" onmouseover="this.src='img/mainvis00r_d.gif'" onmouseout="this.src='img/mainvis00r.gif'" alt="02"></a></div>
<div style="clear:both;"></div>
				<div style="float:left"><a href="index.php?action_static=service_model_03" target="_blank"><img src="img/mainvis01.gif" onmouseover="this.src='img/mainvis01_d.gif'" onmouseout="this.src='img/mainvis01.gif'" alt="01"></a></div>
                
				<div style="float:left"><a href="./blog/?p=5251" target="_blank"><img src="img/mainvis02.gif" onmouseover="this.src='img/mainvis02_d.gif'" onmouseout="this.src='img/mainvis02.gif'" alt="02"></a></div>
                

				<div style="float:left"><a href="./index.php?action_result=true&content_mode=recommend"><img src="img/mainvis03.gif" onmouseover="this.src='img/mainvis03_d.gif'" onmouseout="this.src='img/mainvis03.gif'" alt="03"></a></div>
                
				<div style="float:left"><a href="index.php?action_static=service_model_05"><img src="img/mainvis04.gif" onmouseover="this.src='img/mainvis04_d.gif'" onmouseout="this.src='img/mainvis04.gif'" alt="04"></a></div>

			</div>
	</div>
    <div><a href="index.php?action_static=service_model_03"><img src="img/mainvis_01.jpg" alt=""></a></div>
    <div><a href="blog/?p=3669" target="_blank"><img src="img/mainvis_05.jpg" alt=""></a></div>
    <div><a href="index.php?action_result=true&content_mode=recommend"><img src="img/mainvis_02.jpg" alt=""></a></div>
    <div><a href="blog/?p=2395" target="_blank"><img src="img/mainvis_03.jpg" alt=""></a></div>
    <div><a href="blog/?p=2675" target="_blank"><img src="img/mainvis_04.jpg" alt=""></a></div>
</div>

			<!-- ここまでメインビジュアルslider -->

<!-- 有料会員価格改定告知 img -->
<!--	<div class="pay_btm" style="margin:10px 0 10px 0;">
		<a href="index.php?action_static=service_model_03" target="_blank"><img src="img/jyo_pay.jpg" onmouseover="this.src='img/jyo_pay_d.jpg'" onmouseout="this.src='img/jyo_pay.jpg'" alt="助成金なう有料会員新価格"></a>
	</div>
--><!-- 有料会員価格改定告知 img -->

<!-- 今週注目の助成金 ここから-->              
<div style=" width:1000px; margin-top:5px; text-align:center;">
<div style="display:inline-block; width:32px; text-align:left; position: relative; top:3px;"><img src="img/tasuke_s.jpg" /></div>
<div style="font-size:20px; color:#64C601; font-weight:bold; display:inline-block; text-align:left;">今週注目の助成金</div>
<div style="font-size:16px; color:#64C601; padding-left:5px; display:inline-block; text-align:left;">2月7日更新&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=4191" target="_blank">←見方はこちら</a>&nbsp;<span style="color:#F00000;">New!</span></div>
</div>
<div style="font-size:18px; border:1px solid #DDDDDD; background-color:#F9F9F9; padding:4px 0;">
<table width="1000" border="0" cellspacing="1" cellpadding="0">
  <tr>
    <td width="17" style="padding:2px 0 2px 20px;"><img src="img/week_arrow.gif" /></td>
    <td width="190">中小企業庁</td>
    <td width="750"><a href="https://www.navit-j.com/service/joseikin-now/index.php?action_display=true&in_callkind=government&in_contents_id=23430" target="_blank">集客力向上支援事業&nbsp;&nbsp;商店街・まちなか集客力向上、外国人観光客の消費需要等</a></td>
  </tr>
  <tr>
    <td style="padding-left:20px;"><img src="img/week_arrow.gif" /></td>
    <td>埼玉県羽生市</td>
    <td><a href="https://www.navit-j.com/service/joseikin-now/index.php?action_display=true&in_callkind=government&in_contents_id=23471" target="_blank">空き店舗対策モデル事業費補助金&nbsp;&nbsp;商店街づくりに資することを目的</a></td>
  </tr>
  <tr>
    <td style="padding-left:20px;"><img src="img/week_arrow.gif" /></td>
    <td>山形県高畠町</td>
    <td><a href="https://www.navit-j.com/service/joseikin-now/index.php?action_display=true&in_callkind=government&in_contents_id=17148" target="_blank">設備投資等補助金&nbsp;&nbsp;安定的な雇用の創出を図ることを目的</a></td>
  </tr>
  <tr>
    <td style="padding-left:20px;"><img src="img/week_arrow.gif" /></td>
    <td>研究振興財団</td>
    <td><a href="https://www.navit-j.com/service/joseikin-now/index.php?action_display=true&in_callkind=foundation&in_contents_id=f-00174_1" target="_blank">研究助成&nbsp;&nbsp;コスメトロジー（化粧品学）の進歩・発展に寄与する独創的研究の援助</a></td>
  </tr>
  <tr>
    <td style="padding-left:20px;"><img src="img/week_arrow.gif" /></td>
    <td>公益財団法人住友財団</td>
    <td><a href="https://www.navit-j.com/service/joseikin-now/index.php?action_display=true&in_callkind=foundation&in_contents_id=f-00188_5" target="_blank">外国人等講演会等補助&nbsp;&nbsp;若手研究者による萌芽的な研究に対する支援</a></td>
  </tr>
</table>
</div>
<!-- 今週注目の助成金 ここまで-->              

<!-- ここまでログインしていないユーザー、無料会員向け表示 -->
{else}


<!-- ここから無料会員向け表示（ログイン後） -->
			<!-- ここからメインビジュアルslider -->

<div class="slider" style="height:250px;">
    <div>
			<div id="mvis">
				<div style="float:left"><img src="img/mainvis00l.gif" alt="01"></div>

				<div style="float:left"><a href="https://www.navit-j.com/service/joseikin-now/blog/?p=4191" target="_blank"><img src="img/mainvis00r.gif" onmouseover="this.src='img/mainvis00r_d.gif'" onmouseout="this.src='img/mainvis00r.gif'" alt="02"></a></div>
<div style="clear:both;"></div>
				<div style="float:left"><a href="index.php?action_static=service_model_03" target="_blank"><img src="img/mainvis01.gif" onmouseover="this.src='img/mainvis01_d.gif'" onmouseout="this.src='img/mainvis01.gif'" alt="01"></a></div>

				<div style="float:left"><a href="./index.php?action_result=true&content_mode=recommend"><img src="img/mainvis02.gif" onmouseover="this.src='img/mainvis02_d.gif'" onmouseout="this.src='img/mainvis02.gif'" alt="02"></a></div>

				<div style="float:left"><a href="./blog/?p=5251" target="_blank"><img src="img/mainvis03.gif" onmouseover="this.src='img/mainvis03_d.gif'" onmouseout="this.src='img/mainvis03.gif'" alt="03"></a></div>

				<div style="float:left"><a href="index.php?action_static=service_model_05"><img src="img/mainvis04.gif" onmouseover="this.src='img/mainvis04_d.gif'" onmouseout="this.src='img/mainvis04.gif'" alt="04"></a></div>

			</div>
	</div>
    <div><a href="index.php?action_static=service_model_03"><img src="img/mainvis_01.jpg" alt=""></a></div>
    <div><a href="blog/?p=3669" target="_blank"><img src="img/mainvis_05.jpg" alt=""></a></div>
    <div><a href="index.php?action_result=true&content_mode=recommend"><img src="img/mainvis_02.jpg" alt=""></a></div>
    <div><a href="blog/?p=2395" target="_blank"><img src="img/mainvis_03.jpg" alt=""></a></div>
    <div><a href="blog/?p=2675" target="_blank"><img src="img/mainvis_04.jpg" alt=""></a></div>
</div>

			<!-- ここまでメインビジュアルslider -->

<!-- DVD販売 img  
<div style="margin:20px 0 10px 0;">
	<a href="https://www.navit-j.com/ec/html/products/detail.php?product_id=221" target="_blank" /><img src="img/160818saiyou_dvd.jpg" onmouseover="this.src='img/160818saiyou_dvd_d.jpg'" onmouseout="this.src='img/160818saiyou_dvd.jpg'" alt="DVD販売"></a>
</div>
 DVD販売 img 
-->
<!-- 有料会員価格改定告知 img -->
<!--	<div class="pay_btm" style="margin:10px 0 10px 0;">
		<a href="index.php?action_static=service_model_03" target="_blank" /><img src="img/jyo_pay.jpg" onmouseover="this.src='img/jyo_pay_d.jpg'" onmouseout="this.src='img/jyo_pay.jpg'" alt="助成金なう有料会員新価格" /></a>
	</div>
<!-- 有料会員価格改定告知 img -->

<!-- キャンペーン img -->
<!--<div align="center" style="margin:20px 0 10px 0;">
	<a href="https://www.navit-j.com/press/sem160818.html" target="_blank" /><img src="img/jyo_sem160818.jpg" onmouseover="this.src='img/jyo_sem160818_d.jpg'" onmouseout="this.src='img/jyo_sem160818.jpg'" alt="セミナー"></a>
</div>-->
<!-- キャンペーン img -->

<div class="reccomented_projects">
	<h2><img src="img/title_reccomented.jpg" alt="おすすめ案件" /></h2>
	<div align="center" class="favorite_login"><span style="color:#ad7a27;">{$app.name} 様が会員登録時に選んでいただいた条件を元に、<br />最新の助成金・補助金情報を表示します。</span></div>
</div>

<img src="img/sampleimg.gif" />

<div align="center" style="margin:5px 0 40px 0;">
<a href="https://www.navit-j.com/service/joseikin-now/index.php?action_static=service_model_03" / target="_blank"><img src="img/jyo_paybtn.jpg" onmouseover="this.src='img/jyo_paybtn_d.jpg'" onmouseout="this.src='img/jyo_paybtn.jpg'" alt="有料会員に申し込む"></a>
</div>

<!--<div align="center" style="margin:5px 0 40px 0;">
<a href="?action_payment=true" / target="_blank"><img src="img/jyo_paybtn.jpg" onmouseover="this.src='img/jyo_paybtn_d.jpg'" onmouseout="this.src='img/jyo_paybtn.jpg'" alt="有料会員に申し込む"></a>
</div>-->

<!--ここまで無料会員向け表示（ログイン後） -->

{/if}

{/if}




{if empty($app.name)}
<!-- ここからログインしていないユーザー -->

<!-- 160719現在告知無し -->




<!-- ここまでログインしていないユーザー -->
{/if}


<!-- ここからメインコンテンツ -->

<!-- ここから自治体フォーム -->


<div id="03"></div>
<form action="{$script}" name="f2" method="post" id="f2">
	<input type="hidden" name="action_index" value="true" />
	<input type="hidden" name="selected_government_prefecture" id="selected_government_prefecture" value="{$app_ne.params.government_prefecture}" />
	<input type="hidden" name="selected_government_city" id="selected_government_city" value="{$app_ne.params.government_city}" />
	<input type="hidden" name="selected_foundation_prefecture" id="selected_foundation_prefecture" value="{$app_ne.params.foundation_prefecture}" />
	<input type="hidden" name="selected_foundation_city" id="selected_foundation_city" value="{$app_ne.params.foundation_city}" />

<!--<img src="img/title_a_search_2.jpg" alt="自治体案件検索" />-->

			<div class="table" style="float:left;">

<!--▼▼▼ 2カラム囲み-->
<div style="width:1000px; overflow:hidden; margin-top:15px;">
<!--<div style="width:1000px; background-color:#00f; overflow:hidden;">
-->
<!--▼▼ カラム 左-->
<div class="column_left" style="float:left; width:680px; margin-right:20px;">

				<table border="0" cellpadding="0" cellspacing="1">

					<tr>
						<td colspan="2" height="56" width="680px" style="padding-bottom:0;">
							<img src="img/title_a_search_2.jpg" alt="自治体案件検索" />
						</td></tr>

                        <table border="0" style="text-align:left;">
                        <tr>
						<td rowspan="2" style="height:90px; width:200px;">
                        <img src="img/midashi_syubetu.jpg" alt="種別/エリア" /></td>
                        
									<td style="width:185px;">
										<label><input class="CheckList4 government_kind" type="checkbox" name="government_kind_1000" value="1"{if $app_ne.params.government_kind_values.1000 == 1} checked{/if} />{$app_ne.government_kind_master.1000}</label>
									</td>
									<td style="width:140px;">
										<label><input class="CheckList4 government_kind" type="checkbox" name="government_kind_2000" value="1"{if $app_ne.params.government_kind_values.2000 == 1} checked{/if} />{$app_ne.government_kind_master.2000}</label>
									</td>
									<td style="padding-bottom:0;">
										<label><input class="CheckList4 government_kind" type="checkbox" name="government_kind_3000" value="1"{if $app_ne.params.government_kind_values.3000 == 1} checked{/if} />{$app_ne.government_kind_master.3000}</label>
									</td>
								</tr>
<!--								<tr>
									<td colspan="2" style="padding-bottom:0;">
										<label><input class="CheckList4 government_kind" type="checkbox" name="government_kind_3000" value="1"{if $app_ne.params.government_kind_values.3000 == 1} checked{/if} />{$app_ne.government_kind_master.3000}</label>
									</td>
								</tr>-->
<!--							</table>
-->							{if $app_ne.errors.government_kind}<br /><br /><span class="error">{$app_ne.errors.government_kind|escape}</span>{/if}

<!--                     <table border="0" style="text-align:left;">
-->                     <tr>
						<td colspan="3" style="padding-top:3px;"><!-- style=" padding-left:205px;"-->
							<select name="government_prefecture" id="government_prefecture" class="select_font area_prefecture" style="width:190px;">
							<option value="">全国</option>
							{foreach from=$app_ne.prefectures item=name key=code}
								<option value="{$code}"{if $code == $app_ne.params.government_prefecture} selected{/if}>{$name}</option>
							{/foreach}
							</select>
<!--                            </td>
                            <td>-->
							<select name="government_city" id="government_city" class="select_font">
							<option value="">市区町村を選択</option>
							</select>
						</td>
					</tr>
                    </table>

					<tr>
						<td colspan="2">
							<hr width="660" align="left">
						</td>
					</tr>

					<tr>
						<td>
                            <div style="height:68px; width:210px; float:left;"><img src="img/midashi_bunya.jpg" alt="分野" /></div>
							<div style=" padding:18px 0 0 0; float:left;"><a href="javascript:void(0)" class="check_all AllorClearButton" item="government_field" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
							<a href="javascript:void(0)" class="clear_all AllorClearButton" item="government_field" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a></div>
                        </td>
                        </tr>
                        
                        <tr>
						<td class="r_Cel_2">
							<table border="0" style="text-align:left;width:640px;">
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9000" value="1"{if $app_ne.params.government_field_values.9000 == 1} checked{/if} />{$app_ne.government_field_master.9000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9100" value="1"{if $app_ne.params.government_field_values.9100 == 1} checked{/if} />{$app_ne.government_field_master.9100}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9200" value="1"{if $app_ne.params.government_field_values.9200 == 1} checked{/if} />{$app_ne.government_field_master.9200}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9300" value="1"{if $app_ne.params.government_field_values.9300 == 1} checked{/if} />{$app_ne.government_field_master.9300}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9400" value="1"{if $app_ne.params.government_field_values.9400 == 1} checked{/if} />{$app_ne.government_field_master.9400}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9500" value="1"{if $app_ne.params.government_field_values.9500 == 1} checked{/if} />{$app_ne.government_field_master.9500}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9600" value="1"{if $app_ne.params.government_field_values.9600 == 1} checked{/if} />{$app_ne.government_field_master.9600}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9700" value="1"{if $app_ne.params.government_field_values.9700 == 1} checked{/if} />{$app_ne.government_field_master.9700}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9800" value="1"{if $app_ne.params.government_field_values.9800 == 1} checked{/if} />{$app_ne.government_field_master.9800}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_9900" value="1"{if $app_ne.params.government_field_values.9900 == 1} checked{/if} />{$app_ne.government_field_master.9900}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_10000" value="1"{if $app_ne.params.government_field_values.10000 == 1} checked{/if} />{$app_ne.government_field_master.10000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_field" name="government_field_10100" value="1"{if $app_ne.params.government_field_values.10100 == 1} checked{/if} />{$app_ne.government_field_master.10100}
										</label>
									</td>
								</tr>
							</table>
							{if $app_ne.errors.government_field}<br /><br /><span class="error">{$app_ne.errors.government_field|escape}</span>{/if}
						</td>
					</tr>
                    
					<tr>
						<td colspan="2">
							<hr width="660" align="left">
						</td>
					</tr>
					<tr>
						<td>
                        <div style="height:73px; width:210px; float:left;"><img src="img/midashi_taishousha.jpg" alt="対象者" /></div>
							<div style=" padding:20px 0 0 0; float:left;"><a href="javascript:void(0)" class="check_all AllorClearButton" item="government_target" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
							<a href="javascript:void(0)" class="clear_all AllorClearButton" item="government_target" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a></div>
						</td>
                        </tr>
                        
                        <tr>
						<td class="r_Cel_2">
							<table border="0" style="text-align:left;width:640px;">
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_target" name="government_target_1020" value="1"{if $app_ne.params.government_target_values.1020 == 1} checked{/if} />{$app_ne.government_target_master.1020}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_target" name="government_target_1030" value="1"{if $app_ne.params.government_target_values.1030 == 1} checked{/if} />{$app_ne.government_target_master.1030}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_target" name="government_target_1040" value="1"{if $app_ne.params.government_target_values.1040 == 1} checked{/if} />{$app_ne.government_target_master.1040}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_target" name="government_target_1050" value="1"{if $app_ne.params.government_target_values.1050 == 1} checked{/if} />{$app_ne.government_target_master.1050}
										</label>
									</td>
								</tr>
							</table>
						</td>
					</tr>
                    
					<tr>
						<td colspan="2">
							<hr width="660" align="left">
						</td>
					</tr>
                    
					<tr>
						<td>
							<div style="height:73px; width:210px; float:left;">
<img src="img/midashi_gyoshu.jpg" alt="業種" /></div>
							<div style=" padding:20px 0 0 0; float:left;"><a href="javascript:void(0)" class="check_all AllorClearButton" item="government_category" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
							<a href="javascript:void(0)" class="clear_all AllorClearButton" item="government_category" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a></div>
						</td>
                        </tr>
                        
                        <tr>
						<td class="r_Cel_2">
							<table border="0" style="text-align:left;width:640px;">
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_category" name="government_category_1060" value="1"{if $app_ne.params.government_category_values.1060 == 1} checked{/if} />{$app_ne.government_category_master.1060}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_category" name="government_category_1070" value="1"{if $app_ne.params.government_category_values.1070 == 1} checked{/if} />{$app_ne.government_category_master.1070}
										</label>
									</td>
								</tr>
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_category" name="government_category_1080" value="1"{if $app_ne.params.government_category_values.1080 == 1} checked{/if} />{$app_ne.government_category_master.1080}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_category" name="government_category_1090" value="1"{if $app_ne.params.government_category_values.1090 == 1} checked{/if} />{$app_ne.government_category_master.1090}
										</label>
									</td>
								</tr>
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_category" name="government_category_1100" value="1"{if $app_ne.params.government_category_values.1100 == 1} checked{/if} />{$app_ne.government_category_master.1100}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_category" name="government_category_1110" value="1"{if $app_ne.params.government_category_values.1110 == 1} checked{/if} />{$app_ne.government_category_master.1110}
										</label>
									</td>
								</tr>
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_category" name="government_category_1120" value="1"{if $app_ne.params.government_category_values.1120 == 1} checked{/if} />{$app_ne.government_category_master.1120}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_category" name="government_category_1130" value="1"{if $app_ne.params.government_category_values.1130 == 1} checked{/if} />{$app_ne.government_category_master.1130}
										</label>
									</td>
								</tr>
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_category" name="government_category_1140" value="1"{if $app_ne.params.government_category_values.1140 == 1} checked{/if} />{$app_ne.government_category_master.1140}
										</label>
									</td>
									<td>
									</td>
								</tr>
							</table>
						</td>
					</tr>
                    
					<tr>
						<td colspan="2">
							<hr width="660" align="left">
						</td>
					</tr>
                    
					<tr>
						<td>
							<div style="height:73px; width:210px; float:left;">
<img src="img/midashi_kibo.jpg" alt="支援規模" /></div>
							<div style=" padding:20px 0 0 0; float:left;"><a href="javascript:void(0)" class="check_all AllorClearButton" item="government_scale" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
							<a href="javascript:void(0)" class="clear_all AllorClearButton" item="government_scale" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て解除</a></div>
						</td>
                        </tr>
                        
                        <tr>
						<td class="r_Cel_2">
							<table border="0" style="text-align:left;width:640px;">
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_scale" name="government_scale_1150" value="1"{if $app_ne.params.government_scale_values.1150 == 1} checked{/if} />{$app_ne.government_scale_master.1150}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_scale" name="government_scale_1160" value="1"{if $app_ne.params.government_scale_values.1160 == 1} checked{/if} />{$app_ne.government_scale_master.1160}
										</label>
									</td>
								</tr>
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_scale" name="government_scale_1170" value="1"{if $app_ne.params.government_scale_values.1170 == 1} checked{/if} />{$app_ne.government_scale_master.1170}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_scale" name="government_scale_1180" value="1"{if $app_ne.params.government_scale_values.1180 == 1} checked{/if} />{$app_ne.government_scale_master.1180}
										</label>
									</td>
								</tr>
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList government_scale" name="government_scale_1190" value="1"{if $app_ne.params.government_scale_values.1190 == 1} checked{/if} />{$app_ne.government_scale_master.1190}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList government_scale" name="government_scale_1200" value="1"{if $app_ne.params.government_scale_values.1200 == 1} checked{/if} />{$app_ne.government_scale_master.1200}
										</label>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<hr width="660" align="left">
						</td>
					</tr>
					<tr>
						<td>
							<div style="height:73px; width:210px; float:left;">
<img src="img/midashi_jiki.jpg" alt="募集時期" /></div>
							<div style=" padding:20px 0 0 0; float:left;"><a href="javascript:void(0)" class="check_all AllorClearButton" item="government_season" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
							<a href="javascript:void(0)" class="clear_all AllorClearButton" item="government_season" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a></div>
						</td>
                        </tr>
                        
                        <tr>
						<td class="r_Cel_2">
							<table border="0" style="text-align:left;width:640px;">
								<tr>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1210" value="1"{if $app_ne.params.government_season_values.1210 == 1} checked{/if} />{$app_ne.government_season_master.1210}
										</label>
									</td>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1220" value="1"{if $app_ne.params.government_season_values.1220 == 1} checked{/if} />{$app_ne.government_season_master.1220}
										</label>
									</td>
									<td style="width:214px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1230" value="1"{if $app_ne.params.government_season_values.1230 == 1} checked{/if} />{$app_ne.government_season_master.1230}
										</label>
									</td>
								</tr>


								<tr>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1240" value="1"{if $app_ne.params.government_season_values.1240 == 1} checked{/if} />{$app_ne.government_season_master.1240}
										</label>
									</td>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1250" value="1"{if $app_ne.params.government_season_values.1250 == 1} checked{/if} />{$app_ne.government_season_master.1250}
										</label>
									</td>
									<td style="width:214px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1260" value="1"{if $app_ne.params.government_season_values.1260 == 1} checked{/if} />{$app_ne.government_season_master.1260}
										</label>
									</td>
								</tr>
                                
                                
								<tr>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1270" value="1"{if $app_ne.params.government_season_values.1270 == 1} checked{/if} />{$app_ne.government_season_master.1270}
										</label>
									</td>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1280" value="1"{if $app_ne.params.government_season_values.1280 == 1} checked{/if} />{$app_ne.government_season_master.1280}
										</label>
									</td>
									<td style="width:214px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1290" value="1"{if $app_ne.params.government_season_values.1290 == 1} checked{/if} />{$app_ne.government_season_master.1290}
										</label>
									</td>
								</tr>
                                
                                
								<tr>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1300" value="1"{if $app_ne.params.government_season_values.1300 == 1} checked{/if} />{$app_ne.government_season_master.1300}
										</label>
									</td>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1310" value="1"{if $app_ne.params.government_season_values.1310 == 1} checked{/if} />{$app_ne.government_season_master.1310}
										</label>
									</td>
									<td style="width:214px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1320" value="1"{if $app_ne.params.government_season_values.1320 == 1} checked{/if} />{$app_ne.government_season_master.1320}
										</label>
									</td>
								</tr>
                                
                                
								<tr>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1330" value="1"{if $app_ne.params.government_season_values.1330 == 1} checked{/if} />{$app_ne.government_season_master.1330}
										</label>
									</td>
									<td style="width:213px;">
										<label>
											<input type="checkbox" class="CheckList government_season" name="government_season_1340" value="1"{if $app_ne.params.government_season_values.1340 == 1} checked{/if} />{$app_ne.government_season_master.1340}
										</label>
									</td>
                                    <td style="width:214px;">
                                    &emsp;
                                    </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<hr width="660" align="left">
						</td>
					</tr>
                    
					<tr>
						<td>
							<div style="height:70px; width:210px; float:left;">
<img src="img/midashi_keyword.jpg" alt="キーワード" /></div>
                            <div style=" padding:20px 0 0 0; float:left;">
<a style="padding:5px 30px 0 0" href="https://www.navit-j.com/service/joseikin-now/blog/?p=910" target="_blank">使い方はこちら</a></div>
						</td>
                        </tr>
                        
                        <tr>
						<td colspan="2" class="r_Cel_2">
										<input type="text" id="government_keyword" name="government_keyword" value="{$app_ne.params.government_keyword|escape}" size="40" maxlength="20" style="font-size:20px;" />
										<label for="keyword1"></label>
										{if is_error('in_keyword1')}<br /><span class="error">{message name="in_keyword1"}</span>{/if}
						</td>
					  </tr>

					<tr>
						<td colspan="2">
							<hr width="660" align="left" style="margin-top:30px;">
						</td>
					</tr>

					<tr>
						<td colspan="3">
							<div id="submit_btn">
								<a class="button" href="javascript:void(0)" id="reset_government">リセット</a>
								<a class="button2" href="javascript:void(0)" id="search_government">一覧表示</a>
							</div>
						</td>
					 </tr>
				</table>
			</div>
<!--▲▲ カラム 左-->

<!-- ここまで自治体フォーム -->

<!--▼▼ カラム 右-->
<div class="column_right" style="float:left; width:300px;">

<!--<div style="margin-bottom:20px;">
<script type="text/javascript" src="https://cdn-fluct.sh.adingo.jp/f.js?G=1000055666"></script>-->
<!--<script type="text/javascript">
//<![CDATA[
if(typeof(adingoFluct)!="undefined") adingoFluct.showAd('1000084280');
//]]>
</script>
</div>-->

<div style="margin-bottom:22px;"><a href="index.php?action_static=service_model_03" target="_blank"><img src="img/jyo_r_pay_member.jpg" onmouseover="this.src='img/jyo_r_pay_member_d.jpg'" onmouseout="this.src='img/jyo_r_pay_member.jpg'" alt="助成金なう有料会員登録" /></a></div>

<div style="margin-bottom:22px;"><a href="https://www.navit-j.com/press/info.html#info170210" target="_blank"><img src="img/jyo_r_gentou2.jpg" onmouseover="this.src='img/jyo_r_gentou2_d.jpg'" onmouseout="this.src='img/jyo_r_gentou2.jpg'" alt="幻冬舎 助成金・補助金Q＆A" /></a></div>

<div style="margin-bottom:22px;"><a href="https://www.navit-j.com/service/joseikin-now/blog/?p=5238" target="_blank"><img src="img/jyo_r_sem170302.jpg" onmouseover="this.src='img/jyo_r_sem170302_d.jpg'" onmouseout="this.src='img/jyo_r_sem170302.jpg'" alt="セミナー開催告知" /></a></div>

<!--<div style="margin-bottom:20px;"><a href="https://www.navit-j.com/service/joseikin-now/blog/?p=4519" target="_blank"><img src="img/jyo_r_notice.jpg" onmouseover="this.src='img/jyo_r_notice_d.jpg'" onmouseout="this.src='img/jyo_r_notice.jpg'" alt="お知らせ" /></a></div>-->

<!--<div style="margin-bottom:22px;"><a href="https://www.navit-j.com/service/joseikin-now/blog/?p=4752" target="_blank"><img src="img/jyo_r_gentou.jpg" onmouseover="this.src='img/jyo_r_gentou_d.jpg'" onmouseout="this.src='img/jyo_r_gentou.jpg'" alt="幻冬舎 経営者のための「助成金・補助金」の基礎知識" /></a></div>-->

<div style="margin-bottom:22px;">
<!--  ad tags Size: 300x250 ZoneId:1094132-->
<script type="text/javascript" src="https://js.gsspcln.jp/t/094/132/a1094132.js"></script>
</div>

<!--<div>
<a href="https://www.navit-j.com/ec/html/products/list.php?category_id=134" target="_blank"><img src="img/jyo_r_dvdsale.jpg" onmouseover="this.src='img/jyo_r_dvdsale_d.jpg'" onmouseout="this.src='img/jyo_r_dvdsale.jpg'" alt="助成金DVD販売" /></a></div>
</div>
-->

<div style="margin-bottom:22px;"><a href="https://www.navit-j.com/service/joseikin-now/blog/?p=4353" target="_blank"><img src="img/jyo_r_payvideo.jpg" onmouseover="this.src='img/jyo_r_payvideo_d.jpg'" onmouseout="this.src='img/jyo_r_payvideo.jpg'" alt="有料会員限定動画" /></a></div>

<div style="margin-bottom:22px;"><a href="https://www.navit-j.com/service/joseikin-now/blog/?p=4857" target="_blank"><img src="img/jyo_r_btrip.gif" onmouseover="this.src='img/jyo_r_btrip_d.gif'" onmouseout="this.src='img/jyo_r_btrip.gif'" alt="出張セミナー" /></a></div>

<!--<div style="margin-bottom:20px;">
  ad tags Size: 300x250 ZoneId:1094133
<script type="text/javascript" src="https://js.gsspcln.jp/t/094/133/a1094133.js"></script>
</div>-->

</div>
<!--▲▲ カラム 右-->

</div>
<!--▲▲▲ 2カラム囲み-->

			<!-- ここからトップへ戻る -->

			<div class="re_top2" style="padding-right:280px; float:right; margin:0 5px 0 0;">
				<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
			</div>
			<div class="clear">
			</div>

			<!-- ここまでトップへ戻る -->
</div>
            

<!--  財団ここから  -->



<!-- ここから財団フォーム --><div id="04"></div>

			<div class="table" style="float:left; margin-top:0px;">

<!--▼▼▼ 2カラム囲み-->
<div style="width:1000px; overflow:hidden; margin-top:0px;">
<!--<div style="width:1000px; background-color:#00f; overflow:hidden;">
-->

<!--▼▼ カラム 左-->
<div class="column_left" style="float:left; width:680px; margin:40px 20px 0 0;">

				<table border="0" cellpadding="0" cellspacing="1">

					<tr>
						<td colspan="2" height="56" width="680px">
							<img src="img/title_f_search_2.jpg" alt="財団案件検索" />
						</td></tr>
				<table border="0" cellpadding="0" cellspacing="1">
					<tr>
						<td>
<!--						<td style="height:68px; width:200px;">
-->                        <img src="img/midashi_area.jpg" alt="エリア" /></td>
						<td style="padding-left:20px;">
							<select name="foundation_prefecture" id="foundation_prefecture" class="select_font area_prefecture" style="width:190px;">
							<option value="">全国</option>
							{foreach from=$app_ne.prefectures item=name key=code}
								<option value="{$code}"{if $code == $app_ne.params.foundation_prefecture} selected{/if}>{$name}</option>
							{/foreach}
							</select>
                            </td>
                            <td>
							<select name="foundation_city" id="foundation_city" class="select_font">
							<option value="">市区町村を選択</option>
							</select>

							{if is_error('in_area3')}<br /><br /><span class="error">{message name="in_area3"}</span>{/if}
							{if is_error('in_area4')}<br /><br /><span class="error">{message name="in_area4"}</span>{/if}
						</td>
					</tr>
</table>
					<tr>
						<td colspan="2">
							<hr width="660" align="left">
						</td>
					</tr>

					<tr>
						<td>
                            <div style="height:73px; width:210px; float:left;"><img src="img/midashi_bunya.jpg" alt="分野" /></div>
							<div style=" padding:20px 0 0 0; float:left;"><a href="javascript:void(0)" class="check_all AllorClearButton" item="foundation_field" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
							<a href="javascript:void(0)" class="clear_all AllorClearButton" item="foundation_field" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a></div>
                        </td>
                        </tr>
                        
                        <tr>
						<td class="r_Cel_2">
							<table border="0" style="text-align:left;width:640px;">
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_1000" value="1"{if $app_ne.params.foundation_field_values.1000 == 1} checked{/if} />{$app_ne.foundation_field_master.1000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_2000" value="1"{if $app_ne.params.foundation_field_values.2000 == 1} checked{/if} />{$app_ne.foundation_field_master.2000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_3000" value="1"{if $app_ne.params.foundation_field_values.3000 == 1} checked{/if} />{$app_ne.foundation_field_master.3000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_4000" value="1"{if $app_ne.params.foundation_field_values.4000 == 1} checked{/if} />{$app_ne.foundation_field_master.4000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_5000" value="1"{if $app_ne.params.foundation_field_values.5000 == 1} checked{/if} />{$app_ne.foundation_field_master.5000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_6000" value="1"{if $app_ne.params.foundation_field_values.6000 == 1} checked{/if} />{$app_ne.foundation_field_master.6000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_7000" value="1"{if $app_ne.params.foundation_field_values.7000 == 1} checked{/if} />{$app_ne.foundation_field_master.7000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_8000" value="1"{if $app_ne.params.foundation_field_values.8000 == 1} checked{/if} />{$app_ne.foundation_field_master.8000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_9000" value="1"{if $app_ne.params.foundation_field_values.9000 == 1} checked{/if} />{$app_ne.foundation_field_master.9000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_10000" value="1"{if $app_ne.params.foundation_field_values.10000 == 1} checked{/if} />{$app_ne.foundation_field_master.10000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_field" name="foundation_field_11000" value="1"{if $app_ne.params.foundation_field_values.11000 == 1} checked{/if} />{$app_ne.foundation_field_master.11000}
										</label>
									</td>
									<td>
									</td>
								</tr>
							</table>
							{if $app_ne.errors.foundation_field}<br /><br /><span class="error">{$app_ne.errors.foundation_field|escape}</span>{/if}
						</td>
					</tr>
                    
					<tr>
						<td colspan="2">
							<hr width="660" align="left">
						</td>
					</tr>

					<tr>
						<td>
                        <div style="height:73px; width:210px; float:left;"><img src="img/midashi_keitai.jpg" alt="形態" /></div>
							<div style=" padding:20px 0 0 0; float:left;"><a href="javascript:void(0)" class="check_all AllorClearButton" item="foundation_business" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
							<a href="javascript:void(0)" class="clear_all AllorClearButton" item="foundation_business" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a></div>
						</td>
                        </tr>
                        
                        <tr>
						<td class="r_Cel_2">
							<table border="0" style="text-align:left;width:640px;">
								<tr>
									<td style="width:320px;">
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_1000" value="1"{if $app_ne.params.foundation_business_values.1000 == 1} checked{/if} />{$app_ne.foundation_business_master.1000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_2000" value="1"{if $app_ne.params.foundation_business_values.2000 == 1} checked{/if} />{$app_ne.foundation_business_master.2000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_3000" value="1"{if $app_ne.params.foundation_business_values.3000 == 1} checked{/if} />{$app_ne.foundation_business_master.3000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_4000" value="1"{if $app_ne.params.foundation_business_values.4000 == 1} checked{/if} />{$app_ne.foundation_business_master.4000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_5000" value="1"{if $app_ne.params.foundation_business_values.5000 == 1} checked{/if} />{$app_ne.foundation_business_master.5000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_6000" value="1"{if $app_ne.params.foundation_business_values.6000 == 1} checked{/if} />{$app_ne.foundation_business_master.6000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_7000" value="1"{if $app_ne.params.foundation_business_values.7000 == 1} checked{/if} />{$app_ne.foundation_business_master.7000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_8000" value="1"{if $app_ne.params.foundation_business_values.8000 == 1} checked{/if} />{$app_ne.foundation_business_master.8000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_9000" value="1"{if $app_ne.params.foundation_business_values.9000 == 1} checked{/if} />{$app_ne.foundation_business_master.9000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_10000" value="1"{if $app_ne.params.foundation_business_values.10000 == 1} checked{/if} />{$app_ne.foundation_business_master.10000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_11000" value="1"{if $app_ne.params.foundation_business_values.11000 == 1} checked{/if} />{$app_ne.foundation_business_master.11000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_12000" value="1"{if $app_ne.params.foundation_business_values.12000 == 1} checked{/if} />{$app_ne.foundation_business_master.12000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_13000" value="1"{if $app_ne.params.foundation_business_values.13000 == 1} checked{/if} />{$app_ne.foundation_business_master.13000}
										</label>
									</td>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_14000" value="1"{if $app_ne.params.foundation_business_values.14000 == 1} checked{/if} />{$app_ne.foundation_business_master.14000}
										</label>
									</td>
								</tr>
								<tr>
									<td>
										<label>
											<input type="checkbox" class="CheckList foundation_business" name="foundation_business_15000" value="1"{if $app_ne.params.foundation_business_values.15000 == 1} checked{/if} />{$app_ne.foundation_business_master.15000}
										</label>
									</td>
								</tr>
							</table>
							{if $app_ne.errors.foundation_business}<br /><br /><span class="error">{$app_ne.errors.foundation_business|escape}</span>{/if}
						</td>
					</tr>
                    
					<tr>
						<td colspan="2">
							<hr width="660" align="left">
						</td>
					</tr>

					<tr>
						<td>
							<div style="height:73px; width:210px; float:left;">
<img src="img/midashi_keyword.jpg" alt="キーワード" /></div>
                            <div style=" padding:20px 0 0 0; float:left;"><a style="padding:5px 30px 0 0" href="https://www.navit-j.com/service/joseikin-now/blog/?p=910" target="_blank">使い方はこちら</a></div>
						</td>
                        </tr>

                        <tr>
						<td colspan="2" class="r_Cel_2">
<input type="text" id="foundation_keyword" name="foundation_keyword" value="{$app_ne.params.foundation_keyword|escape}" size="40" maxlength="20" value="案件名などのワードを入力してください" style="font-size:20px;" />
							<label for="keyword2"></label>
							{if is_error('in_keyword2')}<br /><span class="error">{message name="in_keyword2"}</span>{/if}						</td>
					  </tr>

					<tr>
						<td colspan="2">
							<hr width="660" align="left" style="margin-top:30px;">
						</td>
					</tr>
                    
					<tr>
						<td colspan="3">
							<div id="submit_btn">
								<a class="button" href="javascript:void(0)" id="reset_foundation">リセット</a>
								<a class="button2" href="javascript:void(0)" id="search_foundation">一覧表示</a>
							</div>
						</td>
					 </tr>

				</table>
	<input type="hidden" id = "content_mode" name="content_mode" value="" />
</form>
			</div>
<!--▲▲ カラム 左-->

			<!-- ここまで自治体フォーム -->

<!--▼▼ カラム 右-->
<div class="column_right" style="float:left; width:300px;">

<!--<div style="margin-bottom:20px;"><a href="https://www.navit-j.com/ec/html/products/list.php?category_id=134" target="_blank"><img src="img/jyo_r_dvdsale.jpg" onmouseover="this.src='img/jyo_r_dvdsale_d.jpg'" onmouseout="this.src='img/jyo_r_dvdsale.jpg'" alt="助成金DVD販売" /></a></div>
</div>-->

<!--<div style="margin-bottom:22px;"><a href="https://www.navit-j.com/service/joseikin-now/blog/?p=4857" target="_blank"><img src="img/jyo_r_btrip.gif" onmouseover="this.src='img/jyo_r_btrip_d.gif'" onmouseout="this.src='img/jyo_r_btrip.gif'" alt="出張セミナー" /></a></div>

<!--<div style="margin-bottom:20px;">
  ad tags Size: 300x250 ZoneId:1094387
<script type="text/javascript" src="https://js.gsspcln.jp/t/094/387/a1094387.js"></script>
</div>-->

&emsp;
<!--<a href="#" target="_blank"><img src="img/ssp_sample.jpg" alt="広告テスト" /></a>-->
</div>
<!--▲▲ カラム 右-->

</div>
<!--▲▲▲ 2カラム囲み-->

			<!-- ここからトップへ戻る -->

			<div class="re_top" style="padding-right:280px;">
				<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
			</div>
			<div class="clear">

			<!-- ここまでトップへ戻る -->
            </div>

<!--  財団ここまで  -->



<!-- ここからお知らせ -->
<div id="02"></div>

<img src="img/title_news_line.jpg" alt="" />

<!-- 一覧リンクあり <a href="id/index.html" target="_blank"><img src="img/title_news.jpg" onmouseover="this.src='img/title_news_d.jpg'" onmouseout="this.src='img/title_news.jpg'" alt="お知らせ" /></a>-->

<img src="img/title_news2.jpg" alt="お知らせ" />

<iframe class="test_news" src="admin/announce/list.php" type="text/html" name="notice_test" width="95%" height="270px" style="text-algin:center;margin:10px 0 20px 50px; padding:3px 2px; border:solid 1px #ccc;">
</iframe>

<!-- ここまでお知らせ -->


<!-- DVD販売コンテンツ -->
<div style="font-size:16px; margin:10px 0 30px 0; font-weight:bold;">
<table width="1000" border="0" align="center">
  <tr>
    <td colspan="4" align="center" style="font-size:22px; color:#FFFFFF; padding:6px 0 3px 0; background-color:#64C601;">助成金・補助金セミナービデオ（DVD）販売</td>
  </tr>
<!-- <td align="center" width="273" style="padding:15px 0 0 0;"><a href="https://www.navit-j.com/ec/html/products/detail.php?product_id=239" target="_blank"><img src="img/dvd_0107.gif" onmouseover="this.src='img/dvd_0107_d.gif'" onmouseout="this.src='img/dvd_0107.gif'" alt="平成28年度補正ものづくり補助金セミナー"></a></td>-->    <td align="center" width="273" style="padding:15px 0 0 0;"><img src="img/dvd_0107.gif" alt="平成28年度補正ものづくり補助金セミナー"></td>

    <td align="center" width="273" style="padding:15px 0 0 0;"><img src="img/dvd_0105.gif" alt="「採用&研修に使える全国の助成金、まとめセミナー」"></td>

    <td align="center" width="273"  style="padding:15px 0 0 0;"><img src="img/dvd_0104.gif" alt="徹底解説!!入札保証金セミナー"></td>

    <td align="center" width="273"  style="padding:15px 0 0 0;"><img src="img/dvd_0103.gif" alt="徹底解説!!入札保証金セミナー"></td>
</table>
</div>
<!-- DVD販売コンテンツ -->


<!-- キャンペーン img-->
<!--	<div style="margin:20px 0 10px 0;">
		<a href="#" target="_blank"><img src="img/.jpg" onmouseover="this.src='img/.jpg'" onmouseout="this.src='img/.jpg'" alt="セミナー"></a>
	</div>
--><!--ここまでキャンペーン img -->

<!-- キャンペーン img -->
<!--<div align="center" style="margin:20px 0 10px 0;">
	<a href="https://joseikin-now.com/blog/?p=3828" target="_blank" /><img src="img/40000hit_top.jpg" onmouseover="this.src='img/40000hit_top_d.jpg'" onmouseout="this.src='img/40000hit_top.jpg'" alt="40000いいね"></a>
</div>
<!-- キャンペーン img -->

<!-- DVD販売 img --> 
<!--<div style="margin:20px 0 10px 0;">
	<a href="https://www.navit-j.com/ec/html/products/detail.php?product_id=221" target="_blank" /><img src="img/160818saiyou_dvd.jpg" onmouseover="this.src='img/160818saiyou_dvd_d.jpg'" onmouseout="this.src='img/160818saiyou_dvd.jpg'" alt="DVD販売"></a>
</div>
<!-- DVD販売 img -->

<!-- 有料会員価格改定告知 img -->
	<div class="pay_btm" style="margin:10px 0 10px 0;">
		<a href="index.php?action_static=service_model_03" target="_blank"><img src="img/jyo_pay.jpg" onmouseover="this.src='img/jyo_pay_d.jpg'" onmouseout="this.src='img/jyo_pay.jpg'" alt="助成金なう有料会員新価格" /></a>
	</div>
<!-- 有料会員価格改定告知 img -->

<!-- 小冊子 img -->
<div style="margin:0 0 10px 0;">
	<a href="https://www.navit-j.com/service/joseikin-now/inquiry/jyo_fp_new.html" target="_blank"><img src="img/jyo_sassi2top_off.jpg" onmouseover="this.src='img/jyo_sassi2top_on.jpg'" onmouseout="this.src='img/jyo_sassi2top_off.jpg'" alt="助成金小冊子プレゼント" /></a>

	<a href="https://www.navit-j.com/service/joseikin-now/blog/?p=387" target="_blank"><img src="img/jyo_sassi2bottm_off.jpg" onmouseover="this.src='img/jyo_sassi2bottm_on.jpg'" onmouseout="this.src='img/jyo_sassi2bottm_off.jpg'" alt="助成金小冊子紹介" /></a>
</div>
<!-- 小冊子 img -->


<div align="center">
	<a href="https://www.navit-j.com/contactus/" target="_blank">
		<img src="img/joseikin_submit.png" onMouseOver="this.src='img/joseikin_submit_d.png'" onMouseOut="this.src='img/joseikin_submit.png'" />
	</a>
</div>

<div id="pan" align="center" style="margin:5px 0 0px 0;font-size:11px;">
<a href="https://www.navit-j.com">データ・リストの販売、調査代行ならナビット</a> > 助成金なう
</div>

<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer" style="width:1000px;">
	<a href="https://navit-j.com/">運営会社</a>　｜　
	<a href="index.php?action_static=terms_index">利用規約</a>　｜　
	<a href="https://www.navit-j.com/privacy/index.html">Pマークについて</a>　｜　
	<a href="index.php?action_static=law_index">特定商取引法に基づく表記</a>　｜　
	<a href="https://www.navit-j.com/contactus/">問い合わせフォーム</a>
</div>

<!-- ここまでfooter -->

<div style="display: none;">
{foreach from=$app_ne.cities item=name key=code}
	<span class="city_item" code="{$code}" name="{$name}">{$name}</span>
{/foreach}
</div>

</div>
</div><!-- ここまでwrapper -->
</div><!-- ここまでconteinar -->


<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>




<script type="text/javascript">
function set_options(name, value)
{
	$('.' + name).each(function(){
		$(this).prop('checked', value);
	});
}

$('#reset_government').click(function(){
	$('#government_city').val('');
	$('#government_city').html('');
	$('#government_city').append('<option value="">市区町村を選択</option>');
	$('#government_prefecture').val('');
	$('#government_keyword').val('');
	set_options('government_kind', false);
	set_options('government_field', false);
	set_options('government_category', false);
	set_options('government_target', false);
	set_options('government_scale', false);
	set_options('government_season', false);
});

$('#reset_foundation').click(function(){
	$('#foundation_city').val('');
	$('#foundation_city').html('');
	$('#foundation_city').append('<option value="">市区町村を選択</option>');
	$('#foundation_prefecture').val('');
	$('#foundation_keyword').val('');
	set_options('foundation_field', false);
	set_options('foundation_business', false);
});

function changeCity( name, pref_code )
{
	var matches = name.match(/^(.+_)prefecture$/);
	if(matches){
		var target = '#' + matches[1] + 'city';
		$(target).selectedIndex = 0;
		$(target).html('');
		$(target).append('<option value="">市区町村を選択</option>');
		$('.city_item').each(function(){
			var code = $(this).attr('code');
			var prefix = code.substr(0,2);
			var name = $(this).attr('name');
			if(prefix == pref_code){
				$(target).append('<option value="' + code + '">' + name + '</option>');
			}
		});
	}
}
$('.area_prefecture').change( function() {
	var name = $(this).attr('name');
	var pref_code = $(this).val().substr(0,2);
	changeCity( name, pref_code );
} );

$('.check_all').click(function(){
	set_options($(this).attr('item'), true);
});

$('.clear_all').click(function(){
	set_options($(this).attr('item'), false);
});

$('#close_fixed').click(function(){
	$('#filter').hide();
	$('#fixedbox').hide();
});

$('#search_government').click(function(){
	do_submit_with_param('government');
});
$('#search_foundation').click(function(){
	do_submit_with_param('foundation');
});

$(document).ready(function(){
	var selected_government_city = $('#selected_government_city').val();
	if(selected_government_city){
		var selected_government_prefecture= $('#selected_government_prefecture').val();
		var pref_code = selected_government_prefecture.substr(0,2);
		var target = '#government_city';
		$(target).selectedIndex = 0;
		$(target).html('');
		$(target).append('<option value="">市区町村を選択</option>');
		$('.city_item').each(function(){
			var code = $(this).attr('code');
			var prefix = code.substr(0,2);
			var name = $(this).attr('name');
			if(prefix == pref_code){
				if(code == selected_government_city){
					$(target).append('<option value="' + code + '" selected>' + name + '</option>');
				} else {
					$(target).append('<option value="' + code + '">' + name + '</option>');
				}
			}
		});
	}
	var selected_foundation_city = $('#selected_foundation_city').val();
	if(selected_foundation_city){
		var selected_foundation_prefecture= $('#selected_foundation_prefecture').val();
		var pref_code = selected_foundation_prefecture.substr(0,2);
		var target = '#foundation_city';
		$(target).selectedIndex = 0;
		$(target).html('');
		$(target).append('<option value="">市区町村を選択</option>');
		$('.city_item').each(function(){
			var code = $(this).attr('code');
			var prefix = code.substr(0,2);
			var name = $(this).attr('name');
			if(prefix == pref_code){
				if(code == selected_foundation_city){
					$(target).append('<option value="' + code + '" selected>' + name + '</option>');
				} else {
					$(target).append('<option value="' + code + '">' + name + '</option>');
				}
			}
		});
	}

	$.each( $(".area_prefecture"), function( i, v ) {
		var name = $(this).attr('name');
		var pref_code = $(this).val().substr(0,2);
		changeCity( name, pref_code );
	} );

});
{/literal}

{if $app.name}
{literal}
function do_submit_with_param(param){
	document.f2.content_mode.value = param;
	document.f2.submit();
}
{/literal}
{else}
{literal}
function do_submit_with_param(param){
	$('#filter').show();
	$('#fixedbox').show();
}
{/literal}
{/if}
</script>

{include file='common/track.tpl'}

</body>
</html>
