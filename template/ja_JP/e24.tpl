{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>

</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="index.php" name="f2" method="{$form_action}" target="_blank">
<input type="hidden" name="action_e25" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar_tks.gif" width="740" height="30" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr align="center">
    <td width="740" height="125" valign="middle"><p>調査に御協力頂きまして、ありがとうございました。こちらで調査は終了になります。</p>
    <p>ブラウザの閉じるボタンで画面を閉じて終了してください。</p></td>
  </tr>
  <tr align="center">
    <td height="30" valign="middle"><p><a href="javascript:void(0)" onClick="javascript:do_submit();">お問い合わせはこちら</a> ｜ <a href="https://www.npo-homepage.go.jp/">内閣府NPOホームページ</a></p></td>
  </tr>
</table>
<br />

<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>
</form>


{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
