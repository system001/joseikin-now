{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="index.php" name="f2" method="{$form_action}">
<input type="hidden" name="action_e24" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>

<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar15.gif" width="740" height="30" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="740" height="50" align="left" valign="middle"><p><font size="3">御回答いただき誠にありがとうございました。下記はお答えいただいた内容の一覧になります。御回答内容を変更、修正する場合は、戻ってあらためて回答することが出来ます。その際は、「回答ページに戻る」ボタンを押してください。</font></p></td>
  </tr>
</table>
<div><img src="images/border1.gif" width="740" height="3" /></div>
<br />

  <table width="740" border="1" cellpadding="1" cellspacing="0">
     <tr>
       <td colspan="4" align="center" valign="middle" nowrap="nowrap" bgcolor="#66CCFF"><p align="center"><strong>市民の社会貢献に関する実態調査　設問項目一覧</strong></p></td>
     </tr>
     <tr>
       <td width="23" nowrap="nowrap" valign="top"><p align="center">№ </p></td>
       <td width="256" valign="top"><p align="center">項　目 </p></td>
       <td width="381" align="left" valign="middle"><p align="center">回答内容</td>
       <td width="62" valign="top">&nbsp;</td>
     </tr>
     <tr>
       <td nowrap="nowrap" colspan="4" valign="top"><p>●　属性 </p></td>
     </tr>
     <tr>
       <td colspan="2" align="left" valign="middle" nowrap="nowrap"><p>・性 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.sex}</p></td>
       <td width="62" rowspan="6" align="center" valign="middle">{if $app.data.sex != ""}<a href="./index.php?action_e02=true&back=1"><img src="images/btn6.gif" width="102" height="26" /></a>{/if}</td>
     </tr>
     <tr>
       <td colspan="2" align="left" valign="middle" nowrap="nowrap"><p>・年齢 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.age}</p></td>
     </tr>
     <tr>
       <td colspan="2" align="left" valign="middle" nowrap="nowrap"><p>・結婚 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.marriage_status}</p></td>
     </tr>
     <tr>
       <td colspan="2" align="left" valign="middle" nowrap="nowrap"><p>・住所（都道府県） </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.area}</p></td>
     </tr>
     <tr>
       <td colspan="2" align="left" valign="middle" nowrap="nowrap"><p>・職種 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.job_kind}</p></td>
     </tr>
     <tr>
       <td colspan="2" align="left" valign="middle" nowrap="nowrap"><p>・世帯全体の年間収入 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.yearly_income}</p></td>
     </tr>
     <tr>
       <td nowrap="nowrap" colspan="4" valign="top"><p>●　ボランティア活動について（最大６問）　 </p></td>
     </tr>
     <tr>
       <td width="23" align="left" valign="middle" nowrap="nowrap"><p align="right">1</p></td>
       <td width="256" align="left" valign="middle"><p>ボランティア活動に対する関心の有無 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.q1}</p></td>
       <td width="62" rowspan="2" align="center" valign="middle">{if $app.data.q2 != ""}<a href="./index.php?action_e03=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
     <tr>
       <td width="23" align="left" valign="middle" nowrap="nowrap"><p align="right">2</p></td>
       <td width="256" align="left" valign="middle"><p>ボランティア活動経験の有無 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.q2}</p></td>
     </tr>
     <tr>
       <td width="23" align="left" valign="middle" nowrap="nowrap"><p align="right">3</p></td>
       <td width="256" align="left" valign="middle"><p>ボランティア活動に参加した分野 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app_ne.data2.q3}</p></td>
       <td width="62" rowspan="2" align="center" valign="middle">{if $app_ne.data2.q3 != "" || $app_ne.data2.q4 != ""}<a href="./index.php?action_e04=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" />{/if}</a></td>
     </tr>
     <tr>
       <td width="23" align="left" valign="middle" nowrap="nowrap"><p align="right">4</p></td>
       <td width="256" align="left" valign="middle"><p>参加理由 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app_ne.data2.q4}</p></td>
     </tr>
     <tr>
       <td width="23" align="left" valign="middle" nowrap="nowrap"><p align="right">5</p></td>
       <td width="256" align="left" valign="middle"><p>参加の妨げとなる要因 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app_ne.data2.q5}</p></td>
       <td width="62" rowspan="2" align="center" valign="middle">{if $app_ne.data2.q5 != "" || $app_ne.data2.q6 != ""}<a href="./index.php?action_e05=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
     <tr>
       <td width="23" align="left" valign="middle" nowrap="nowrap"><p align="right">6</p></td>
       <td width="256" align="left" valign="middle"><p>国・地方自治体等への要望 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app_ne.data2.q6}</p></td>
     </tr>
     <tr>
       <td nowrap="nowrap" colspan="4" valign="top"><p>●　寄附について（最大７問） </p></td>
     </tr>
     <tr>
       <td width="23" align="center" valign="middle" nowrap="nowrap"><p align="right">7</p></td>
       <td width="256" align="left" valign="middle"><p>寄附経験の有無 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.q7}</p></td>
       <td width="62" align="center" valign="middle">{if $app.data.q7 != ""}<a href="./index.php?action_e06=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
     <tr>
       <td width="23" align="center" valign="middle" nowrap="nowrap"><p align="right">8</p></td>
       <td width="256" align="left" valign="middle"><p>年間寄附額・寄附回数 </p></td>
       <td width="381" align="left" valign="middle"><table border="1" cellspacing="0" style="font-size:0.8em;">
				<tr>
					<th>&nbsp;</th>
					<th>（年間）寄附金額</th>
					<th>（年間）寄附回数</th>
				</tr>
				<tr>
					<th>あなた</th>
					<td style="text-align:center;">{$app.data.q8_person_kifu}円</td>
					<td style="text-align:center;">{$app.data.q8_person_kifu_cnt}回</td>
				</tr>
				<tr>
					<th>世帯全体</th>
					<td style="text-align:center;">{$app.data.q8_all_kifu}円</td>
					<td  style="background-color:#A3A3A3">&nbsp;</td>
				</tr>
			</table></td>
       <td width="62" rowspan="2" align="center" valign="middle">{if $app.data.q8_person_kifu != "" || 
                                                                                                        $app.data.q8_person_kifu_cnt != "" || 
                                                                                                        $app.data.q8_all_kifu != "" || 
                                                                                                        $app.data.q9_gaitobokin != "" ||
                                                                                                        $app.data.q9_tewatashi != "" || 
                                                                                                        $app.data.q9_bokinbako != "" || 
                                                                                                        $app.data.q9_hurikomi != "" || 
                                                                                                        $app.data.q9_card != "" || 
                                                                                                        $app.data.q9_gift != "" || 
                                                                                                        $app.data.q9_syouhin != "" || 
                                                                                                        $app.data.q9_genbutsu != "" || 
                                                                                                        $app.data.q9_other != ""}<a href="./index.php?action_e07=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
     <tr>
       <td width="23" align="center" valign="middle" nowrap="nowrap"><p align="right">9</p></td>
       <td width="256" align="left" valign="middle"><p>寄附方法 </p></td>
       <td width="381" align="left" valign="middle">
                                                                <table border="1" cellspacing="0" style="font-size:0.8em;">
                                                                    <tr>
                                                                            <th>寄付方法</th>
                                                                            <th>1回の寄付金額</th>
                                                                    </tr>
                                                                {if $app.data.q9_gaitobokin != ""}
                                                                    <tr>
                                                                            <td>1.　街頭募金</td>
                                                                                                                                    <td style="text-align:center;">{$app.data.q9_gaitobokin}円</td>
                                                                    </tr>
                                                                {/if}
                                                                {if $app.data.q9_tewatashi != ""}
                                                                    <tr>
                                                                            <td>2.　直接手渡し</td>
                                                                                                                                    <td style="text-align:center;">{$app.data.q9_tewatashi}円</td>
                                                                    </tr>
                                                                {/if}
				{if $app.data.q9_bokinbako != ""}
                                                                    <tr>
                                                                            <td>3.　設置されている募金箱</td>
                                                                                                                                    <td style="text-align:center;">{$app.data.q9_bokinbako}円</td>
                                                                    </tr>
                                                                {/if}
                                                                {if $app.data.q9_hurikomi != ""}
                                                                    <tr>
                                                                            <td>4.　銀行・コンビニ等での振込み・口座引き落とし</td>
                                                                                                                                    <td style="text-align:center;">{$app.data.q9_hurikomi}円</td>
                                                                    </tr>
                                                                {/if}
                                                                {if $app.data.q9_card != ""}
                                                                    <tr>
                                                                            <td>5.　クレジットカード等の利用</td>
                                                                                                                                    <td style="text-align:center;">{$app.data.q9_card}円</td>
                                                                    </tr>
                                                                {/if}
                                                                {if $app.data.q9_gift != ""}
                                                                    <tr>
                                                                            <td>6.　マッチングギフト</td>
                                                                                                                                    <td style="text-align:center;">{$app.data.q9_gift}円</td>
                                                                    </tr>
                                                                {/if}
                                                                {if $app.data.q9_syouhin != ""}
                                                                    <tr>
                                                                            <td>7.　寄附付商品の購入</td>
                                                                                                                                    <td style="text-align:center;">{$app.data.q9_syouhin}円</td>
                                                                    </tr>
                                                                {/if}
                                                                {if $app.data.q9_genbutsu != ""}
                                                                    <tr>
                                                                            <td>8.　現物寄附（品物の郵送等）</td>
                                                                                                                                    <td style="text-align:center;">{$app.data.q9_genbutsu}円</td>
                                                                    </tr>
                                                                {/if}
                                                                {if $app.data.q9_other != ""}
                                                                    <tr>
                                                                            <td>9.　その他</td>
                                                                                                                                    <td style="text-align:center;">{$app.data.q9_other}円</td>
                                                                    </tr>
                                                                {/if}
                                                                </table>
                                                                    
       </td>
                        
                        
                        
                        
                                       
     </tr>
     <tr>
       <td width="23" align="center" valign="middle" nowrap="nowrap"><p align="right">10</p></td>
       <td width="256" align="left" valign="middle"><p>寄附をした相手 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app_ne.data2.q10}</p></td>
       <td width="62" rowspan="3" align="center" valign="middle">{if $app_ne.data2.q10 != "" || $app_ne.data2.q11 != "" || $app_ne.data2.q12 != ""}<a href="./index.php?action_e08=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
     
     <tr>
       <td width="23" align="center" valign="middle" nowrap="nowrap"><p align="right">11</p></td>
       <td width="256" align="left" valign="middle"><p>寄附をした分野 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app_ne.data2.q11}</p></td>
     </tr>
     <tr>
       <td width="23" align="center" valign="middle" nowrap="nowrap"><p align="right">12</p></td>
       <td width="256" align="left" valign="middle"><p>寄附理由 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app_ne.data2.q12}</p></td>
     </tr>
     <tr>
       <td width="23" align="center" valign="middle" nowrap="nowrap"><p align="right">13</p></td>
       <td width="256" align="left" valign="middle"><p>寄附の妨げとなる要因 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app_ne.data2.q13}</p></td>
       <td width="62" align="center" valign="middle">{if $app_ne.data2.q13 != ""}<a href="./index.php?action_e09=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
     <tr>
       <td nowrap="nowrap" colspan="4" valign="top"><p>●　NPO全般について（最大５問）　 </p></td>
     </tr>
     <tr>
       <td width="23" nowrap="nowrap" valign="top"><p align="right">14</p></td>
       <td width="256" align="left" valign="middle"><p>NPO法人に対する関心 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.q14}</p></td>
       <td width="62" rowspan="2" align="center" valign="middle">{if $app.data.q14 != "" || $app.data.q15 != ""}<a href="./index.php?action_e10=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
     <tr>
       <td width="23" nowrap="nowrap" valign="top"><p align="right">15</p></td>
       <td width="256" align="left" valign="middle"><p>認定・仮認定NPO法人に対する寄附の税制優遇措置の認識</p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.q15} </p></td>
     </tr>
     <tr>
       <td width="23" nowrap="nowrap" valign="top"><p align="right">16</p></td>
       <td width="256" align="left" valign="middle"><p>認定・仮認定NPO法人への寄附 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app.data.q16}</p></td>
       <td width="62" align="center" valign="middle">{if $app.data.q16 != ""}<a href="./index.php?action_e11=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
     <tr>
       <td width="23" nowrap="nowrap" valign="top"><p align="right">17</p></td>
       <td width="256" align="left" valign="middle"><p>認定・仮認定NPO法人へ寄附をしたいと思わない理由 </p></td>
       <td width="381" align="left" valign="middle"><p>{$app_ne.data2.q17}</p></td>
       <td width="62" align="center" valign="middle">{if $app_ne.data2.q17 != ""}<a href="./index.php?action_e12=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
     <tr>
       <td width="23" nowrap="nowrap"><p align="right">18</p></td>
       <td width="256" align="left" valign="middle">寄附を行う場合に必要と考えるNPO法人の情報</td>
       <td width="381" align="left" valign="middle">{$app_ne.data2.q18}</td>
       <td width="62" align="center" valign="middle">{if $app_ne.data2.q18 != ""}<a href="./index.php?action_e13=true&back=1"><img src="images/btn6.gif" alt="" width="102" height="26" /></a>{/if}</td>
     </tr>
   </table>
</div>
<table width="700" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
	</tr>
</table>
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;">
    <br />一度終了した回答は修正・閲覧ができません。
    <br />回答内容を保存したい場合は画面を印刷して
    <br />保存してください。(<a href="javascript:window.print();">印刷はこちら</a>)
    <br />
    <br />
    <p><strong>　</strong><strong></a>御回答結果に問題なければ、「回答を終了する」ボタンを押してください。</a></strong></p>
</div>

<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><img onClick="javascript:do_submit();" src="images/btn4.gif" alt="進む" width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->
<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>
</form>




















{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
