{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!--
var trigger_id1 = "#q10_10";
var trigger_id2 = "#q11_13";
var trigger_id3 = "#q12_8";
var txtarea_id1 = "#in_q10_no10_text";
var txtarea_id2 = "#in_q11_no13_text";
var txtarea_id3 = "#in_q12_no8_text";
var zan_text_id1 = "#zan_q10_no10_text";
var zan_text_id2 = "#zan_q11_no13_text";
var zan_text_id3 = "#zan_q12_no8_text";

$(document).ready(function (){
    //For TextArea initialize
    text_val1 = $(txtarea_id1).val();
    text_val2 = $(txtarea_id2).val();
    text_val3 = $(txtarea_id3).val();
    CountDownLength('zan_q10_no10_text', text_val1, 255);
    CountDownLength('zan_q11_no13_text', text_val2, 255);
    CountDownLength('zan_q12_no8_text', text_val3, 255);
    if($(trigger_id1+":checked").val()){
        $(txtarea_id1).removeAttr("disabled");
        $(txtarea_id1).css("background-color","#ffffff");
        $(zan_text_id1).css("color","#000000");
    }
    else{
        $(txtarea_id1).attr("disabled", "disabled");
        $(txtarea_id1).css("background-color","#d2d1c6");
        $(zan_text_id1).css("color","#d2d1c6");
    }

    if($(trigger_id2+":checked").val()){
        $(txtarea_id2).removeAttr("disabled");
        $(txtarea_id2).css("background-color","#ffffff");
        $(zan_text_id2).css("color","#000000");
    }
    else{
        $(txtarea_id2).attr("disabled", "disabled");
        $(txtarea_id2).css("background-color","#d2d1c6");
        $(zan_text_id2).css("color","#d2d1c6");
    }
    
    if($(trigger_id3+":checked").val()){
        $(txtarea_id3).removeAttr("disabled");
        $(txtarea_id3).css("background-color","#ffffff");
        $(zan_text_id3).css("color","#000000");
    }
    else{
        $(txtarea_id3).attr("disabled", "disabled");
        $(txtarea_id3).css("background-color","#d2d1c6");
        $(zan_text_id3).css("color","#d2d1c6");
    }
    
    //For checkbox initialize
    if($("#q10_1:checked").val() ||
       $("#q10_2:checked").val() ||
       $("#q10_3:checked").val() ||
       $("#q10_4:checked").val() ||
       $("#q10_5:checked").val() ||
       $("#q10_6:checked").val() ||
       $("#q10_7:checked").val() ||
       $("#q10_8:checked").val() ||
       $("#q10_10:checked").val() ) {
            //1,2,3,4,5,6,7,8,10のどれか1つでもチェックされていたら9をチェック不可に
            $("#q10_9").attr("disabled", "disabled");
            $("#q10_9").css("background-color","#d2d1c6");
            $("#q10_9").css("color","#d2d1c6");
            $("#q10_9").attr("checked", false); //チェックを外す
            //Q11の12をチェック不可に
            $("#q11_12").attr("disabled", "disabled");
            $("#q11_12").css("background-color","#d2d1c6");
            $("#q11_12").css("color","#d2d1c6");
            $("#q11_12").attr("checked", false); //チェックを外す
    }else{
            //1,2,3,4,5,6,7,8,10が1つもチェックされていない場合9をチェック可能に
            $("#q10_9").removeAttr("disabled");
            $("#q10_9").css("background-color","#ffffff");
            $("#q10_9").css("color","#000000");
            //Q11の12をチェック可能に
            $("#q11_12").removeAttr("disabled");
            $("#q11_12").css("background-color","#ffffff");
            $("#q11_12").css("color","#000000");
    } 
    if($("#q10_9:checked").val()) {
        //1,2,3,4,5,6,7,8,10をチェック不可に
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").attr("disabled", "disabled");
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").css("background-color","#d2d1c6");
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").css("color","#d2d1c6");
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").attr("checked", false); //チェックを外す
        //9をチェック可能に
        $("#q10_9").removeAttr("disabled");
        $("#q10_9").css("background-color","#ffffff");
        $("#q10_9").css("color","#000000");
        //Q11の1,2,3,4,5,6,7,8,9,10,11,13をチェック不可に
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").attr("disabled", "disabled");
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#d2d1c6");
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#d2d1c6");
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").attr("checked", false); //チェックを外す
        //Q11の12をチェック可能に
        $("#q11_12").removeAttr("disabled");
        $("#q11_12").css("background-color","#ffffff");
        $("#q11_12").css("color","#000000");
    } else {
        //1,2,3,4,5,6,7,8,10をチェック可能に
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").removeAttr("disabled");
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").css("background-color","#ffffff");
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").css("color","#000000");
        //Q11の1,2,3,4,5,6,7,8,9,10,11,13をチェック可能に
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").removeAttr("disabled");
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#ffffff");
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#000000");
    }

    if($("#q10_1:checked").val() ||
       $("#q10_2:checked").val() ||
       $("#q10_3:checked").val() ||
       $("#q10_4:checked").val() ||
       $("#q10_5:checked").val() ||
       $("#q10_6:checked").val() ||
       $("#q10_7:checked").val() ||
       $("#q10_9:checked").val() ||
       $("#q10_10:checked").val() ) {
            //1,2,3,4,5,6,7,9,10のどれか1つでもチェックされていたら8をチェック不可に
            $("#q10_8").attr("disabled", "disabled");
            $("#q10_8").css("background-color","#d2d1c6");
            $("#q10_8").css("color","#d2d1c6");
    }else{
            //1,2,3,4,5,6,7,9,10が1つもチェックされていない場合8をチェック可能に
            $("#q10_8").removeAttr("disabled");
            $("#q10_8").css("background-color","#ffffff");
            $("#q10_8").css("color","#000000");
    }
    if($("#q10_8:checked").val()) {
        //1,2,3,4,5,6,7,9,10をチェック不可に
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_9,#q10_10").attr("disabled", "disabled");
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_9,#q10_10").css("background-color","#d2d1c6");
        $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_9,#q10_10").css("color","#d2d1c6");
        //8をチェック可能に
        $("#q10_8").removeAttr("disabled");
        $("#q10_8").css("background-color","#ffffff");
        $("#q10_8").css("color","#000000");
    } else {
        
        if($("#q10_1:checked").val() ||
           $("#q10_2:checked").val() ||
           $("#q10_3:checked").val() ||
           $("#q10_4:checked").val() ||
           $("#q10_5:checked").val() ||
           $("#q10_6:checked").val() ||
           $("#q10_7:checked").val() ||
           $("#q10_10:checked").val() ) {
                //1,2,3,4,5,6,7,10のどれか1つでもチェックされていたら9をチェック不可に
                $("#q10_9").attr("disabled", "disabled");
                $("#q10_9").css("background-color","#d2d1c6");
                $("#q10_9").css("color","#d2d1c6");
                //Q11の12をチェック不可に
                $("#q11_12").attr("disabled", "disabled");
                $("#q11_12").css("background-color","#d2d1c6");
                $("#q11_12").css("color","#d2d1c6");
           }
        
        
    }
    
    if($("#q11_1:checked").val() ||
       $("#q11_2:checked").val() ||
       $("#q11_3:checked").val() ||
       $("#q11_4:checked").val() ||
       $("#q11_5:checked").val() ||
       $("#q11_7:checked").val() ||
       $("#q11_8:checked").val() ||
       $("#q11_9:checked").val() ||
       $("#q11_10:checked").val() ||
       $("#q11_11:checked").val() ||
       $("#q11_13:checked").val() ) {
            //1,2,3,4,5,6,7,8,9,10,11,13のどれか1つでもチェックされていたら12をチェック不可に
            $("#q11_12").attr("disabled", "disabled");
            $("#q11_12").css("background-color","#d2d1c6");
            $("#q11_12").css("color","#d2d1c6");
    }else{
            //1,2,3,4,5,6,7,8,9,10,11,13が1つもチェックされていない場合12をチェック可能に
            $("#q11_12").removeAttr("disabled");
            $("#q11_12").css("background-color","#ffffff");
            $("#q11_12").css("color","#000000");
    }
    if($("#q11_12:checked").val()) {
        ////1,2,3,4,5,6,7,8,9,10,11,13をチェック不可に
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").attr("disabled", "disabled");
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#d2d1c6");
        $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#d2d1c6");
        ////12をチェック可能に
        $("#q11_12").removeAttr("disabled");
        $("#q11_12").css("background-color","#ffffff");
        $("#q11_12").css("color","#000000");
    } else {
        if($("#q10_9:checked").val()) {
             ////1,2,3,4,5,6,7,8,9,10,11,13をチェック不可に(Q10の9がチェックされている場合は12以外は選べない)
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").attr("disabled", "disabled");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#d2d1c6");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#d2d1c6");
        }else{
            ////1,2,3,4,5,6,7,8,9,10,11,13をチェック可能に(Q10の9がチェックされていなければ)
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").removeAttr("disabled");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#ffffff");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#000000");
        }
        
        
        
        
    }
     
});

$(function() {
    $(trigger_id1).on('click', TextAreaEnable1);
    $(trigger_id2).on('click', TextAreaEnable1);
    $(trigger_id3).on('click', TextAreaEnable1);
    
    function TextAreaEnable1(){
        if ($(trigger_id1+":checked").val()) {
            $(txtarea_id1).removeAttr("disabled");
            $(txtarea_id1).css("background-color","#ffffff");
            $(zan_text_id1).css("color","#000000");
        } else {
            $(txtarea_id1).attr("disabled", "disabled");
            $(txtarea_id1).css("background-color","#d2d1c6");
            $(zan_text_id1).css("color","#d2d1c6");
        }
        
        if ($(trigger_id2+":checked").val()) {
            $(txtarea_id2).removeAttr("disabled");
            $(txtarea_id2).css("background-color","#ffffff");
            $(zan_text_id2).css("color","#000000");
        } else {
            $(txtarea_id2).attr("disabled", "disabled");
            $(txtarea_id2).css("background-color","#d2d1c6");
            $(zan_text_id2).css("color","#d2d1c6");
        }
        
        if ($(trigger_id3+":checked").val()) {
            $(txtarea_id3).removeAttr("disabled");
            $(txtarea_id3).css("background-color","#ffffff");
            $(zan_text_id3).css("color","#000000");
        } else {
            $(txtarea_id3).attr("disabled", "disabled");
            $(txtarea_id3).css("background-color","#d2d1c6");
            $(zan_text_id3).css("color","#d2d1c6");
        }
    }
});


$(function() {

    $("#q10_1").on('click', ProhibitionItem9);
    $("#q10_2").on('click', ProhibitionItem9);
    $("#q10_3").on('click', ProhibitionItem9);
    $("#q10_4").on('click', ProhibitionItem9);
    $("#q10_5").on('click', ProhibitionItem9);
    $("#q10_6").on('click', ProhibitionItem9);
    $("#q10_7").on('click', ProhibitionItem9);
    $("#q10_8").on('click', ProhibitionItem9);
    $("#q10_9").on('click', ProhibitionItemOther);
    $("#q10_10").on('click', ProhibitionItem9);
    
    function ProhibitionItem9(){
        
        if($("#q10_1:checked").val() ||
           $("#q10_2:checked").val() ||
           $("#q10_3:checked").val() ||
           $("#q10_4:checked").val() ||
           $("#q10_5:checked").val() ||
           $("#q10_6:checked").val() ||
           $("#q10_7:checked").val() ||
           $("#q10_8:checked").val() ||
           $("#q10_10:checked").val() ) {
                //1,2,3,4,5,6,7,8,10のどれか1つでもチェックされていたら9をチェック不可に
                $("#q10_9").attr("disabled", "disabled");
                $("#q10_9").css("background-color","#d2d1c6");
                $("#q10_9").css("color","#d2d1c6");
                $("#q10_9").attr("checked", false); //チェックを外す
                //Q11の12をチェック不可に
                $("#q11_12").attr("disabled", "disabled");
                $("#q11_12").css("background-color","#d2d1c6");
                $("#q11_12").css("color","#d2d1c6");
                $("#q11_12").attr("checked", false); //チェックを外す
                //Q11の1,2,3,4,5,6,7,8,9,10,11,13をチェック可能に
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").removeAttr("disabled");
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#ffffff");
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#000000");
                
        }else{
                //1,2,3,4,5,6,7,8,10が1つもチェックされていない場合9をチェック可能に
                $("#q10_9").removeAttr("disabled");
                $("#q10_9").css("background-color","#ffffff");
                $("#q10_9").css("color","#000000");
                //Q11の12をチェック可能に
                $("#q11_12").removeAttr("disabled");
                $("#q11_12").css("background-color","#ffffff");
                $("#q11_12").css("color","#000000");
        } 
        
    }
    
    function ProhibitionItemOther(){
        
        if($("#q10_9:checked").val()) {//Q11の1,2,3,4,5,6,7,8,9,10,11,13をチェック可能に
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").removeAttr("disabled");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#ffffff");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#000000");
            //1,2,3,4,5,6,7,8,10をチェック不可に
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").attr("disabled", "disabled");
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").css("background-color","#d2d1c6");
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").css("color","#d2d1c6");
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").attr("checked", false); //チェックを外す
            //9をチェック可能に
            $("#q10_9").removeAttr("disabled");
            $("#q10_9").css("background-color","#ffffff");
            $("#q10_9").css("color","#000000");
             //Q11の1,2,3,4,5,6,7,8,9,10,11,13をチェック不可に
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").attr("disabled", "disabled");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#d2d1c6");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#d2d1c6");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").attr("checked", false); //チェックを外す
            //Q11の12をチェック可能に
            $("#q11_12").removeAttr("disabled");
            $("#q11_12").css("background-color","#ffffff");
            $("#q11_12").css("color","#000000");
        } else {
            //1,2,3,4,5,6,7,8,10をチェック可能に
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").removeAttr("disabled");
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").css("background-color","#ffffff");
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_8,#q10_10").css("color","#000000");
            
            if($("#q11_12:checked").val()) {
                
            }else{
                //Q11の1,2,3,4,5,6,7,8,9,10,11,13をチェック可能に(Q11の12がチェックされていなければ)
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").removeAttr("disabled");
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#ffffff");
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#000000");
            }
    
            
        }
    }
    
});

$(function() {

    $("#q10_1").on('click', ProhibitionItem8);
    $("#q10_2").on('click', ProhibitionItem8);
    $("#q10_3").on('click', ProhibitionItem8);
    $("#q10_4").on('click', ProhibitionItem8);
    $("#q10_5").on('click', ProhibitionItem8);
    $("#q10_6").on('click', ProhibitionItem8);
    $("#q10_7").on('click', ProhibitionItem8);
    $("#q10_8").on('click', ProhibitionItemOther);
    $("#q10_9").on('click', ProhibitionItem8);
    $("#q10_10").on('click', ProhibitionItem8);
    
    function ProhibitionItem8(){
        
        if($("#q10_1:checked").val() ||
           $("#q10_2:checked").val() ||
           $("#q10_3:checked").val() ||
           $("#q10_4:checked").val() ||
           $("#q10_5:checked").val() ||
           $("#q10_6:checked").val() ||
           $("#q10_7:checked").val() ||
           $("#q10_9:checked").val() ||
           $("#q10_10:checked").val() ) {
                //1,2,3,4,5,6,7,9,10のどれか1つでもチェックされていたら8をチェック不可に
                $("#q10_8").attr("disabled", "disabled");
                $("#q10_8").css("background-color","#d2d1c6");
                $("#q10_8").css("color","#d2d1c6");
        }else{
                //1,2,3,4,5,6,7,9,10が1つもチェックされていない場合8をチェック可能に
                $("#q10_8").removeAttr("disabled");
                $("#q10_8").css("background-color","#ffffff");
                $("#q10_8").css("color","#000000");
        } 
    }
    
    function ProhibitionItemOther(){
        
        if($("#q10_8:checked").val()) {
            //1,2,3,4,5,6,7,9,10をチェック不可に
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_9,#q10_10").attr("disabled", "disabled");
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_9,#q10_10").css("background-color","#d2d1c6");
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_9,#q10_10").css("color","#d2d1c6");
            //8をチェック可能に
            $("#q10_8").removeAttr("disabled");
            $("#q10_8").css("background-color","#ffffff");
            $("#q10_8").css("color","#000000");
        } else {
            //1,2,3,4,5,6,7,9,10をチェック可能に
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_9,#q10_10").removeAttr("disabled");
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_9,#q10_10").css("background-color","#ffffff");
            $("#q10_1,#q10_2,#q10_3,#q10_4,#q10_5,#q10_6,#q10_7,#q10_9,#q10_10").css("color","#000000");
            
            if($("#q11_1:checked").val() ||
               $("#q11_2:checked").val() ||
               $("#q11_3:checked").val() ||
               $("#q11_4:checked").val() ||
               $("#q11_5:checked").val() ||
               $("#q11_7:checked").val() ||
               $("#q11_8:checked").val() ||
               $("#q11_9:checked").val() ||
               $("#q11_10:checked").val() ||
               $("#q11_11:checked").val() ||
               $("#q11_13:checked").val() ) {
                    //1,2,3,4,5,6,7,8,9,10,11,13のどれか1つでもチェックされていたら12をチェック不可に
                    $("#q11_12").attr("disabled", "disabled");
                    $("#q11_12").css("background-color","#d2d1c6");
                    $("#q11_12").css("color","#d2d1c6");
                    
                }else{
                    //1,2,3,4,5,6,7,8,9,10,11,13が1つもチェックされていない場合12をチェック可能に
                    $("#q11_12").removeAttr("disabled");
                    $("#q11_12").css("background-color","#ffffff");
                    $("#q11_12").css("color","#000000");
                } 
            
            
            
            
            
            
        }
    }
    
});

$(function() {

    $("#q11_1").on('click', ProhibitionItem12);
    $("#q11_2").on('click', ProhibitionItem12);
    $("#q11_3").on('click', ProhibitionItem12);
    $("#q11_4").on('click', ProhibitionItem12);
    $("#q11_5").on('click', ProhibitionItem12);
    $("#q11_6").on('click', ProhibitionItem12);
    $("#q11_7").on('click', ProhibitionItem12);
    $("#q11_8").on('click', ProhibitionItem12);
    $("#q11_9").on('click', ProhibitionItem12);
    $("#q11_10").on('click', ProhibitionItem12);
    $("#q11_11").on('click', ProhibitionItem12);
    $("#q11_12").on('click', ProhibitionItemOther);
    $("#q11_13").on('click', ProhibitionItem12);
    
    
    function ProhibitionItem12(){
        
        if($("#q11_1:checked").val() ||
           $("#q11_2:checked").val() ||
           $("#q11_3:checked").val() ||
           $("#q11_4:checked").val() ||
           $("#q11_5:checked").val() ||
           $("#q11_7:checked").val() ||
           $("#q11_8:checked").val() ||
           $("#q11_9:checked").val() ||
           $("#q11_10:checked").val() ||
           $("#q11_11:checked").val() ||
           $("#q11_13:checked").val() ) {
                //1,2,3,4,5,6,7,8,9,10,11,13のどれか1つでもチェックされていたら12をチェック不可に
                $("#q11_12").attr("disabled", "disabled");
                $("#q11_12").css("background-color","#d2d1c6");
                $("#q11_12").css("color","#d2d1c6");
        }else{
                //1,2,3,4,5,6,7,8,9,10,11,13が1つもチェックされていない場合12をチェック可能に
                $("#q11_12").removeAttr("disabled");
                $("#q11_12").css("background-color","#ffffff");
                $("#q11_12").css("color","#000000");
        } 
        
    }
    
    function ProhibitionItemOther(){
        
        if($("#q11_12:checked").val()) {
            ////1,2,3,4,5,6,7,8,9,10,11,13をチェック不可に
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").attr("disabled", "disabled");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#d2d1c6");
            $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#d2d1c6");
            ////12をチェック可能に
            $("#q11_12").removeAttr("disabled");
            $("#q11_12").css("background-color","#ffffff");
            $("#q11_12").css("color","#000000");
        } else {
            if($("#q10_9:checked").val()) {
                 ////1,2,3,4,5,6,7,8,9,10,11,13をチェック不可に(Q10の9がチェックされている場合は12以外は選べない)
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").attr("disabled", "disabled");
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#d2d1c6");
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#d2d1c6");
            }else{
                ////1,2,3,4,5,6,7,8,9,10,11,13をチェック可能に(Q10の9がチェックされていなければ)
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").removeAttr("disabled");
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("background-color","#ffffff");
                $("#q11_1,#q11_2,#q11_3,#q11_4,#q11_5,#q11_6,#q11_7,#q11_8,#q11_9,#q11_10,#q11_11,#q11_13").css("color","#000000");
            }
        }
    }
    
});

//-->
</script>
{/literal}
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}


<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e09" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　Web調査</h3></td>
		<td width="75" align="left" valign="middle">&nbsp;</td>
	</tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar13.gif" width="740" height="30" /></div>
<div class="設問内容">
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10px;">
	<div>
		<div>
			<p><strong>［寄附をした相手］</strong>　</p>
		</div>
			<p><strong>問１０</strong> 　【<a href="./index.php?action_e06=true&back=1">問７</a>で「1.　寄附をしたことがある」とお答えになった方にお聞きします】<font color="red"><strong>【必須】</strong></font><img src="images/m10.gif" width="128" height="19" /><br />
				<br />
				あなたが、<strong style="border-bottom:double 3px #000; line-height:2em;">平成25年度（平成25年4月～平成26年3月）</strong>に寄附をした団体・NPO法人等はどこですか。<strong>（複数回答可能）</strong>
				<br />
                                                                {if is_error('in_q10')}<br /><span class="error">{message name="in_q10"}</span>{/if}
                                                                {if is_error('in_q10_no10_text')}<br /><span class="error">{message name="in_q10_no10_text"}</span>{/if}
                                                                <br />
                                                                <label><input type="checkbox" name="in_q10_1" id="q10_1" value="1" {$app.data.q10_1} />{$app.master_q10.1}</label><br />
                                                                <label><input type="checkbox" name="in_q10_2" id="q10_2" value="2" {$app.data.q10_2} />{$app.master_q10.2}</label><br />
                                                                <label><input type="checkbox" name="in_q10_3" id="q10_3" value="3" {$app.data.q10_3} />{$app.master_q10.3}</label><br />
                                                                <label><input type="checkbox" name="in_q10_4" id="q10_4" value="4" {$app.data.q10_4} />{$app.master_q10.4}</label><br />
                                                                <label><input type="checkbox" name="in_q10_5" id="q10_5" value="5" {$app.data.q10_5} />{$app.master_q10.5}</label><br />
                                                                <label><input type="checkbox" name="in_q10_6" id="q10_6" value="6" {$app.data.q10_6} />{$app.master_q10.6}</label><br />
                                                                <label><input type="checkbox" name="in_q10_7" id="q10_7" value="7" {$app.data.q10_7} />{$app.master_q10.7}</label><br />
                                                                <label><input type="checkbox" name="in_q10_8" id="q10_8" value="8" {$app.data.q10_8} />{$app.master_q10.8}</label><br />
                                                                <label><input type="checkbox" name="in_q10_9" id="q10_9" value="9" {$app.data.q10_9} />{$app.master_q10.9}</label><br />
                                                                <label><input type="checkbox" name="in_q10_10" id="q10_10" value="10" {$app.data.q10_10} />{$app.master_q10.10}</label><br />
                                                                <textarea name="in_q10_no10_text"  id="in_q10_no10_text" rows="6" cols="60" onkeyup="CountDownLength('zan_q10_no10_text', value, 255);" maxlength="255">{$app.data.q10_no10_text}</textarea>
                                                                <span id="zan_q10_no10_text">（残り255文字）</span>
                                                <br />
                                
			</p>
				<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
			</tr>
		</table>
		<div><br />
			<p><strong>［寄附をした分野］</strong>　</p>
		</div>
			<p> <strong>問１１</strong> 　【<a href="./index.php?action_e06=true&back=1">問７</a>で「1.　寄附をしたことがある」とお答えになった方にお聞きします】　<img src="images/m11.gif" width="128" height="19" /><br />
				<br />
				あなたが、<strong style="border-bottom:double 3px #000; line-height:2em;">平成25年度（平成25年4月～平成26年3月）</strong>に寄附をしたのはどのような分野の団体・NPO法人ですか。 <strong>（複数回答可能）</strong>
                                                                <br />
                                                {if is_error('in_q11')}<br /><span class="error">{message name="in_q11"}</span>{/if}
                                                {if is_error('in_q11_no13_text')}<br /><span class="error">{message name="in_q11_no13_text"}</span>{/if}
                                                <table width="740" border="1" bordercolor="#777" cellspacing="0" >
                                                  <tr>
                                                    <td >{$app.master_q11.1}</td>
                                                    <td align="center" valign="middle" width="100"><input type="checkbox" name="in_q11_1" value="1" id="q11_1" {$app.data.q11_1} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.2}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_2" value="2" id="q11_2" {$app.data.q11_2} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.3}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_3" value="3" id="q11_3" {$app.data.q11_3} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.4}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_4" value="4" id="q11_4" {$app.data.q11_4} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.5}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_5" value="5" id="q11_5" {$app.data.q11_5} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.6}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_6" value="6" id="q11_6" {$app.data.q11_6} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.7}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_7" value="7" id="q11_7" {$app.data.q11_7} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.8}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_8" value="8" id="q11_8" {$app.data.q11_8} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.9}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_9" value="9" id="q11_9" {$app.data.q11_9} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.10}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_10" value="10" id="q11_10" {$app.data.q11_10} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.11}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_11" value="11" id="q11_11" {$app.data.q11_11} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.12}</td>
                                                    <td align="center" valign="middle"><input type="checkbox" name="in_q11_12" value="12" id="q11_12" {$app.data.q11_12} /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>{$app.master_q11.13}<br />
                                                        <label for="in_q11_no13_text">
                                                            <input size="100" type="text" name="in_q11_no13_text" maxlength="255" id="in_q11_no13_text" value="{$app.data.q11_no13_text}"  onkeyup="CountDownLength('zan_q11_no13_text', value, 255);" /><span id="zan_q11_no13_text">（残り255文字）</span>
                                                        </label>
                                                    </td>
                                                    <td align="center" valign="middle">
                                                        <input type="checkbox" name="in_q11_13" value="13" id="q11_13" {$app.data.q11_13} />
                                                    </td>
                                                  </tr>
                                                </table>
                                               </p>      
                                                    
				<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
			</tr>
		</table>
		<div><br />
			<p><strong>［寄附理由］</strong></p>
		</div>
			<p><strong>問１２</strong> 　【<a href="./index.php?action_e06=true&back=1">問７</a>で「1.　寄附をしたことがある」とお答えになった方にお聞きします】　<img src="images/m12.gif" width="128" height="19" /><br />
				<br />
				あなたが、寄附をした理由はどのようなものですか。<strong>（複数回答可能）</strong>
				<br />
                                                                {if is_error('in_q12')}<br /><span class="error">{message name="in_q12"}</span>{/if}
                                                                {if is_error('in_q12_no8_text')}<br /><span class="error">{message name="in_q12_no8_text"}</span>{/if}
                                                                <br />
                                                                <label><input type="checkbox" name="in_q12_1" id="q12_1" value="1" {$app.data.q12_1} />{$app.master_q12.1}</label><br />
                                                                <label><input type="checkbox" name="in_q12_2" id="q12_2" value="2" {$app.data.q12_2} />{$app.master_q12.2}</label><br />
                                                                <label><input type="checkbox" name="in_q12_3" id="q12_3" value="3" {$app.data.q12_3} />{$app.master_q12.3}</label><br />
                                                                <label><input type="checkbox" name="in_q12_4" id="q12_4" value="4" {$app.data.q12_4} />{$app.master_q12.4}</label><br />
                                                                <label><input type="checkbox" name="in_q12_5" id="q12_5" value="5" {$app.data.q12_5} />{$app.master_q12.5}</label><br />
                                                                <label><input type="checkbox" name="in_q12_6" id="q12_6" value="6" {$app.data.q12_6} />{$app.master_q12.6}</label><br />
                                                                <label><input type="checkbox" name="in_q12_7" id="q12_7" value="7" {$app.data.q12_7} />{$app.master_q12.7}</label><br />
                                                                <label><input type="checkbox" name="in_q12_8" id="q12_8" value="8" {$app.data.q12_8} />{$app.master_q12.8}</label><br />
                                                                <textarea name="in_q12_no8_text"  id="in_q12_no8_text" rows="6" cols="60" onkeyup="CountDownLength('zan_q12_no8_text', value, 255);" maxlength="255">{$app.data.q12_no8_text}</textarea>
                                                                <span id="zan_q12_no8_text">（残り255文字）</span>
                                
		<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
			</tr>
		</table>
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e07=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->
	</div>
</div>
<div><img src="images/border1.gif" width="740
" height="3" /></div>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
	</tr>
</table>
</form>


{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
