<div id="header">
	<div id="logo">
		<a href="index.php" ><img src="img/logo.jpg" alt="助成金なう" /></a>
	</div>

	<div id="h1">
		<h1>助成金・補助金の検索サービス「助成金なう」</h1>
	</div>

	<div id="mini_contact">
		<img src="img/m_contact.gif" alt="お問合せ" />
	</div>

	<div id="mini_contact_txt">
		<a href="https://www.navit-j.com/contactus/" target="_blank">お問合せ</a>
	</div>

	<div id="mini_sitemap">
		<img src="img/m_sitemap.gif" alt="運営会社" />
	</div>

	<div id="mini_sitemap_txt">
		<a href="http://navit-j.com/" target="_blank">運営会社</a>
	</div>

	<div id="freedial">
		<img src="img/freedial.jpg" alt="0120-937-781" />
	</div>

	{if empty($app.name)}
		<div id="newaccount">
			<a class="button3_a" href="./index.php?action_secure_registration=true"	id="registration">新規会員登録</a>
		</div>
		<div id="login">
			<a class="button3_a" href="./index.php?action_login=true" onclick="" id="login_btn">ログイン</a>
		</div>
		<div id="mypage_btn">
			<a class="button3_a" href="javascript:void(0);" onclick="" id="mypage_btn_off">マイページ</a>
		</div>
	{else}
		<div style="display:inline-block;width:360px;">
			<table	border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#f00">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td colspan="2" align="left">
									<span id="logininfo">{$app.name} 様　ログイン中</span>	 
								</td>
							</tr>
							<tr>
								<td align="right">
									<a href="./index.php?action_logout_do=true" id="logout_btn">ログアウト</a>
								</td>
								<td align="right">
									<a href="./index.php?action_mypage=true" id="mypage_btn_on">マイページ</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	
		</div>
	{/if}
</div>

<div class="clear"></div>
