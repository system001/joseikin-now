<!-- ▼************ Google Anarytics トラッキングコード ************ ▼-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- ▲ *********** Google Anarytics トラッキングコード ************ ▲-->

<!-- googleanalytics -->
{literal}
<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-45128510-12', 'auto');
ga('send', 'pageview');
</script>
{/literal}
<!-- googleanalytics -->

<!-- LF innov -->
{literal}
<script type="text/javascript">
var _trackingid = 'LFT-10394-1';
(function() {
  var lft = document.createElement('script'); lft.type = 'text/javascript'; lft.async = true;
  lft.src = document.location.protocol + '//track.list-finder.jp/js/ja/track.js';
  var snode = document.getElementsByTagName('script')[0]; snode.parentNode.insertBefore(lft, snode);
})();
</script>
{/literal}
