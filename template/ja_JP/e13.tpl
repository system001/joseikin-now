{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!--
var trigger_id1 = "#q18_7";
var txtarea_id1 = "#in_q18_no7_text";
var zan_text_id1 = "#zan_q18_no7_text";

$(document).ready(function (){
     text_val1 = $(txtarea_id1).val();
    CountDownLength('zan_q18_no7_text', text_val1, 255);
    if($(trigger_id1+":checked").val()){
        $(txtarea_id1).removeAttr("disabled");
        $(txtarea_id1).css("background-color","#ffffff");
        $(zan_text_id1).css("color","#000000");
    }else{
        $(txtarea_id1).attr("disabled", "disabled");
        $(txtarea_id1).css("background-color","#d2d1c6");
        $(zan_text_id1).css("color","#d2d1c6");
    }
    
    
});

$(function() {
    $(trigger_id1).on('click', TextAreaEnable1);
    
    function TextAreaEnable1(){
        if ($(trigger_id1+":checked").val()) {
            $(txtarea_id1).removeAttr("disabled");
            $(txtarea_id1).css("background-color","#ffffff");
            $(zan_text_id1).css("color","#000000");
        } else {
            $(txtarea_id1).attr("disabled", "disabled");
            $(txtarea_id1).css("background-color","#d2d1c6");
            $(zan_text_id1).css("color","#d2d1c6");
        }
        
    }
});

//-->
</script>
{/literal}
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e23" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
		<td width="75" align="left" valign="middle">&nbsp;</td>
	</tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar14.gif" width="740" height="30" /></div>
<div>
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10px;">
	<div>
		<div>
			<p><strong>［寄附を行う場合に必要と考えるNPO法人の情報］</strong>　</p>
		</div>
			<p> <strong>問１８</strong> 　【全員の方にお聞きします】　<img src="images/m18.gif" alt="" width="161" height="19" /> <br />
				<br />
				NPO法人（認定・仮認定含む）へ寄附を行う場合、必要と考える情報は何ですか。<strong>（複数回答可能）</strong></p>
			<p>
                                                                {if is_error('in_q18')}<br /><span class="error">{message name="in_q18"}</span>{/if}
                                                                {if is_error('in_q18_no7_text')}<br /><span class="error">{message name="in_q18_no7_text"}</span>{/if}
                                                                <br />
                                                                <label><input type="checkbox" name="in_q18_1" id="q18_1" value="1" {$app.data.q18_1} />{$app.master_q18.1}</label><br />
                                                                <label><input type="checkbox" name="in_q18_2" id="q18_2" value="2" {$app.data.q18_2} />{$app.master_q18.2}</label><br />
                                                                <label><input type="checkbox" name="in_q18_3" id="q18_3" value="3" {$app.data.q18_3} />{$app.master_q18.3}</label><br />
                                                                <label><input type="checkbox" name="in_q18_4" id="q18_4" value="4" {$app.data.q18_4} />{$app.master_q18.4}</label><br />
                                                                <label><input type="checkbox" name="in_q18_5" id="q18_5" value="5" {$app.data.q18_5} />{$app.master_q18.5}</label><br />
                                                                <label><input type="checkbox" name="in_q18_6" id="q18_6" value="6" {$app.data.q18_6} />{$app.master_q18.6}</label><br />
                                                                <label><input type="checkbox" name="in_q18_7" id="q18_7" value="7" {$app.data.q18_7} />{$app.master_q18.7}</label><br />
                                                                <textarea name="in_q18_no7_text"  id="in_q18_no7_text" rows="6" cols="60" onkeyup="CountDownLength('zan_q18_no7_text', value, 255);" maxlength="255">{$app.data.q18_no7_text}</textarea>
                                                                <span id="zan_q18_no7_text">（残り255文字）</span>
                                                                <br />
			</p>
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e12=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->
	</div>
</div>
<div><img src="images/border1.gif" width="740
" height="3" /></div>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
	</tr>
</table>
</form>



{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
