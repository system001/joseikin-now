<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.lightbox_me.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	$('label').balloon();
});

function do_delete(){
	$("#drew_dialog").lightbox_me({centered: true,closeSelector:'#drew_dialog_close',overlayCSS:{background:'#D3C9BA',opacity: .8}});
}

</script>
{/literal}
</head>
<body>

<!-- ここからconteinar -->
<div id="conteinar">
<!-- ここからwrapper -->
<div id="wrapper">

<!-- ここからheader -->

<div id="01"></div>

{include file='common/header.tpl'}

{include file='common/navi.tpl'}

<!-- ここからメインコンテンツ -->

{if $app.rank == 0}

<div align="center" style="margin:30px 0 0 0;">
<a href="./index.php?action_payment=true" target="_blank" /><img src="img/jyo_paybtn.jpg" onmouseover="this.src='img/jyo_paybtn_d.jpg'" onmouseout="this.src='img/jyo_paybtn.jpg'" alt="有料会員に申し込む"></a>
</div>

{/if}

<div>

{if $app.rank != 0}

	<form class="submit_form" action="#favorite_autonomy_projects" method="get" target="_blank">
		<input type="hidden" name="action_mypage" value= "true"/>
		<input type="submit" class="submit_btn2" value= "お気に入り案件一覧はこちら"/>
	</form>

{/if}

<div id="03"></div>
<div class="top_header_title" ><h2><img src="img/title_mypage.jpg" alt="マイページ" /></h2></div>
<div class="">

<table width="950" border="0" cellpadding="5" cellspacing="1" style="margin-left:30px;">
	<tbody>
		<tr>
			<td colspan="2" style="border:0;background-color:#E6FFE1;">&nbsp;お客様情報</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:right;"><a class="change_btn" href="./index.php?action_secure_updateprofile=true" id="delete_button">会員情報編集</a></td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">ログインID</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.login_id}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">パスワード</td>
			<td class="s_Cel" style="width:550px;">***********</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">会社名</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.company_name}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">会社名かな</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.company_kana}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">支店名</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.branch_name}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">郵便番号</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.zip1}-{$app.user_data.zip2}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">住所</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.address}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">電話番号</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.phone_no1}-{$app.user_data.phone_no2}-{$app.user_data.phone_no3}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">携帯電話番号</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.k_phone_no1}-{$app.user_data.k_phone_no2}-{$app.user_data.k_phone_no3}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">FAX番号</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.fax_no1}-{$app.user_data.fax_no2}-{$app.user_data.fax_no3}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">部署名</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.department_name}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">役職名</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.post_name}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">担当者名</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.contractor_lname}&nbsp;&nbsp;{$app.user_data.contractor_fname}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">担当者名かな</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.contractor_lkana}&nbsp;&nbsp;{$app.user_data.contractor_fkana}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">メールアドレス</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.email}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">サイトを知ったきっかけ</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.come_from}</td>
		</tr>
		 <tr>
			<td class="l_Cel_01_01">メールマガジンの<br />送付を希望する</td>
			<td class="s_Cel" style="width:550px;">{if $app.user_data.mailmaga eq '1'}希望する{else}希望しない{/if}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">支援を受けたい分野1<br />(自治体案件カテゴリ)</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.target_field1_str|replace:",":" "}</td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">支援を受けたい分野2<br />(財団法人案件カテゴリ)</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.target_field2_str|replace:",":" "}</td>
		</tr>
		 <tr>
			<td class="l_Cel_01_01">支援を受けたい事業の形態</td>
			<td class="s_Cel" style="width:550px;">{$app.user_data.target_businessform_str|replace:",":" "}</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:right;"><a class="withdrew_btn" href="javascript:void(0)" onclick="do_delete();" id="delete_button">退会</a></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border:0;background-color:#E6FFE1;" colspan="2">&nbsp;興味通知履歴（32時間以内）</td>
		</tr>
		<tr>
			<td colspan="2" style="border:0;padding-top:10px;"></td>
		</tr>
		<tr>
			<td class="l_Cel_01_01">興味通知日</td>
			<td class="l_Cel_01_01">案件名</td>
		</tr>
	{if $app.interest_offer_history|@count == 0}
		<tr>
			<td class="s_Cel_01" colspan="2" align="center">対象履歴が見つかりません</td>
		</tr>
	{else}
		{foreach from=$app.interest_offer_history key=k item=v name=l}
		<tr>
			<td class="s_Cel">{$v.created}</td>
			<td class="s_Cel">{$v.main_title}</td>
		</tr>
		{/foreach}
	{/if}
	</tbody>
</table>

<!--無料会員向け告知-->
{if empty($app.name)}
<div align="center" style="margin:20px 0 0 0;">
<a href="./index.php?action_payment=true" target="_blank" /><img src="img/jyo_paybtn.jpg" onmouseover="this.src='img/jyo_paybtn_d.jpg'" onmouseout="this.src='img/jyo_paybtn.jpg'" alt="有料会員に申し込む"></a>
</div>
{/if}

</div>
<br />

{if $app.rank != 0}
<a name="favorite_autonomy_projects"></a>
<div class="favorite_autonomy_projects">
	<h2><img src="img/title_favorite.jpg" alt="お気に入り案件" /></h2>
		<h3 class="h3_title">自治体案件</h3>
			<table class="mypage_table">
				<tr>
					<th>ID</th>
					<th>名称</th>
					<th>分野</th>
					<th>登録日</th>
					<th>削除</th>
				</tr>
			{foreach from=$app_ne.favorite_g_items item=item key=code}
				<tr>
					<td align="center" width="6%">{$item.contents_id}</td>
					<td width="40%"><a href="?action_display=1&in_callkind=government&in_contents_id={$item.contents_org_id}">{$item.main_title}</a></td>
					<td width="27%">
				{foreach from=$item.field_names item=field_name name=fg}
						{if $smarty.foreach.fg.first}{else}/{/if}
						{$field_name}
				{/foreach}
					</td>
					<td align="center" width="20%">{$item.created}</td>
					<td align="center" width="7%"><a class="erase_button" href="?action_mypage=true&in_type=delete&in_kind=government&in_contents_id={$item.contents_id}">削除</a></td>
				</tr>
				<tr>
					<td colspan="5">
						<hr class="hr2" />
					</td>
				</tr>
			{/foreach}
			</table>

		<h3 class="h3_title2">財団案件</h3>
			<table class="mypage_table">
				<tr>
					<th>ID</th>
					<th>名称</th>
					<th>分野</th>
					<th>登録日</th>
					<th>削除</th>
				</tr>
			{foreach from=$app_ne.favorite_f_items item=item key=code}
				<tr>
					<td align="center" width="6%">{$item.contents_id}</td>
					<td width="40%"><a href="?action_display=1&in_callkind=foundation&in_contents_id={$item.contents_org_id}">{$item.main_title}</a></td>
					<td width="27%">
				{foreach from=$item.field_names item=field_name name=ff}
						{if $smarty.foreach.ff.first}{else}/{/if}
						{$field_name}
				{/foreach}
					</td>
					<td align="center" width="20%">{$item.created}</td>
					<td align="center" width="7%"><a class="erase_button" href="?action_mypage=true&in_type=delete&in_kind=foundation&in_contents_id={$item.contents_id}">削除</a></td>
				</tr>
				<tr>
					<td colspan="5">
						<hr class="hr2" />
					</td>
				</tr>
			{/foreach}
			</table>
	<form class="submit_form" action="index.php" method="get">
		<input type="hidden" value= "true"/>
		<input type="submit" class="submit_btn2" value= "検索画面へ戻る"/>
	</form>

</div>
{/if}

	<div class="re_top_first" style="float:right;">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear"></div>


{include file=drew_dialog.tpl}

</div>
<!-- ここまでメインコンテンツ -->
</div>

<!-- ここからfooter -->
<br />
<div id="footer">
	<a href="http://navit-j.com/">運営会社</a>　｜　
	<a href="/terms/">利用規約</a>　｜　
	<a href="http://www.navit-j.com/privacy/index.html">Pマークについて</a>　｜　
	<a href="/law/">特定商取引法に基づく表記</a>　｜　
	<a href="https://www.navit-j.com/contactus/">問い合わせフォーム</a>
</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
	// #で始まるアンカーをクリックした場合に処理
	$('a[href^=#]').click(function() {
		// スクロールの速度
		var speed = 700; // ミリ秒
		// アンカーの値取得
		var href= $(this).attr("href");
		// 移動先を取得
		var target = $(href == "#" || href == "" ? 'html' : href);
		// 移動先を数値で取得
		var position = target.offset().top;
		// スムーススクロール
		$('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	});
});
</script>
<!-- スムーズスクロール -->
{/literal}

{include file='common/track.tpl'}

</body>
</html>
