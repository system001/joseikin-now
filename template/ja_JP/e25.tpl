{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>

</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="index.php" name="f2" method="{$form_action}">
<input type="hidden" name="action_e25chk" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar19.gif" width="740" height="30" /></div>

<br />
<div class="設問内容">
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10px;">
  <div>

      <p><strong>［御問い合わせ項目を御選択ください。]</strong>　</p>

      <p>
        <input type="checkbox" name="in_19_1" id="in_19_1" value="1" {$app.data.19_1}/>1.　調査（全般・内容）について <br />
        <input type="checkbox" name="in_19_2" id="in_19_2" value="2" {$app.data.19_2}/>2.　回答（方法・修正）について <br />
        <input type="checkbox" name="in_19_3" id="in_19_3" value="3" {$app.data.19_3}/>3.　回答内容の確認<br />
        <input type="checkbox" name="in_19_4" id="in_19_4" value="4" {$app.data.19_4}/>4.　その他<br />
      </p>
    <div>
      <br />
      <p><strong>［御問い合わせ内容を御入力ください。］</strong>　</p>
        <p>
        <textarea name="in_19_text" id="in_19_text" rows="6" cols="60"  onkeyup="CountDownLength('zan19mail', value, 255);">{$app.data.19_text}</textarea>{if is_error('in_19_text')}<br />
        <span class="error">{message name="in_19_text"}</span>{/if}
        <span id="zan19mail">（残り255文字）</span></label<br />
        </p>
      </div>
      
      メールアドレスを御入力ください。<font color="red"><strong>【必須】</strong></font><br />
        <input type="text" name="in_19_mail" value="{$app.data.19_mail}" style="height: 22px;" id="19_mail" size="65" maxlength="255" />
        {if is_error('in_19_mail')}<br /><span class="error">{message name="in_19_mail"}</span>{/if}<br />
        <br />
        <br />
        確認のため、再度メールアドレスを御入力ください。<font color="red"><strong>【必須】</strong></font><br />
        <input type="text" name="in_19_mail2" value="" id="19_mail2"  style="height: 22px;" size="65"  maxlength="255" />
        {if is_error('in_19_mail2')}<br /><span class="error">{message name="in_19_mail2"}</span>{/if}<br />

      <br />
      <br />
      <br />
      <img src="images/bar_caution5.gif" width="328" height="25" style="margin-bottom:5px;" alt="個人情報にお取り扱いについて" /><br />
      業務請負先である株式会社ナビットでは、あなた様の氏名、住所、年齢、性別が記載された「調査対象一覧表」を厳重に管理しています。一方、調査票には、氏名等の個人情報の記載を求めない事としており、誰がどのように回答したかがわからないようになっています。なお、「調査対象一覧表」と「回収した調査票」は調査終了から1年後に廃棄します。
      <br />
      
<!--ここから確認ボタン挿入-->
<div id="submit_1" style="text-align:center;">

        <br />
        <br />
                        <img onClick="javascript:do_submit();" border="0" src="images/btn_kakunin.gif" alt="確認する" width="228" height="47" style="cursor:pointer;" />
                        <br />
                        <br />
	
<!--ここまで確認ボタン挿入-->
		<p class="center_text">御問い合わせ頂いた内容については3営業日以内にメールで回答致します。<br />
      3営業日過ぎても回答が来ない場合は、お手数ですが電話にてお問い合わせください。<br />
      <br />
      <strong> 株式会社ナビット　</strong>（担当）堀田、野村、立川 <br />
      電話（フリーダイヤル）：0120-964-603　（受付時間：平日10時～18時）<br />
			※携帯電話からも御利用頂けます。
</div>

    
    <table width="700" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
      </tr>
    </table>
  </div>
</div>
<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>
</form>


{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
