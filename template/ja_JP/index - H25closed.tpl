{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<style type="text/css">
table.error{
    padding: 2px;
    width: 60%;
    font-weight: bold;
    text-align: center;
    border: 1px #ff0000 solid;
    border-collapse: collapse;
}
tr.error{
    background-color:#ffaaaa;
}
.pass {
	text-align: center;
	margin-top: 0px;
}
</style>
{/literal}
</head>
<body>



<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<h3>
<table width="740" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td><p class="pass">
    </p>
    </td>
  </tr>
</table>
</h3>



<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar_login.gif" width="740" height="30" /></div>

    <form action="index.php" name="f2" method="post">
    <input type="hidden" name="action_login_do" value="true">
    <table width="740" >
        <tr>
            <td width="739">
              <table border="0" cellpadding="2" cellspacing="0" id="table3" style="text-align:center; margin:40px auto; font-size:18px;">
                <tr>
                    <td>こちらは、内閣府実施「平成25年度　市民の社会貢献に関する実態調査」の回答画面です。<br />
本調査は平成25年10月22日（火）をもって終了致しました。<br />
御 協力いただき、誠にありがとうございました。<br />
なお、集計結果は平成25年度中に、内閣府ＮＰＯホームページ上で公表させていただく予定です。<br />
<br />
<a href="https://www.npo-homepage.go.jp/">内閣府ＮＰＯホームページ</a>
</td>
                </tr>
              </table>
<!--エラーメッセージ-->
{if count($errors)}
<table class="error" align="center">
{foreach from=$errors item=error}
<tr class="error"><td>{$error}</td></tr>
{/foreach}
</table>
{/if}
<!--エラーメッセージ-->
                <div class="pass">
                    <br />
              </div>
            </td>
        </tr>
    </table>
    </form>
</td>
</tr></table>


{$footer}

<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
