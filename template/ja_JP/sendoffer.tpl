<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal} 
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	  $('label').balloon();
});
</script>
{/literal} 
</head>
<body>
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_secure_complete" value="true">
<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
<div id="wrapper">

<div id="01"></div>
{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->

<!-- ここから入力フォーム --><div id="03"></div>

<br />
{if $app.mail_send_result == "-1"}
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">送信エラー</span></div>
<div style="margin-left: 64px;">
	申請サポートお申し込みの送信ができませんでした。<br />
	大変お手数ではございますが再度こちら送信をお願い致します。>> 
	<a href="javascript:void(0);" onclick="javascript:history.back();">案件詳細に戻る</a><br />
	  <br />
	  <br />
	  <span style="color:#666;font-size:80%;">
		【送信に失敗する場合、以下の原因が考えられます】<br />
		※通信環境の混雑等の理由によるもの<br />
		※サーバ高負荷のため<br />
		<br />
		<br />
	  </span>
</div>
{else}
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">申請サポート申し込み完了</span></div>
<div style="margin-left: 64px;">
<center>
申請サポートにお申し込み頂きありがとうございます。<br />送信が完了しましたのでお知らせ致します。<br />
こちらの案件の申請サポートをご希望の型は、下記よりお申し込みください。お申し込み頂いた会員様には、折り返し『助成金なう』事務局よりご連絡させて頂きます。<br /><br />

<b>【案件へのご相談】</b><br />
個別にこの助成金についてご相談したい方は、以下の番号まで、まずはご連絡ください。<br />
0120-937-781（フリーダイヤル）<br />
担当：谷口、前田、千葉<br /><br />

<b>【申請サポート費用】</b><br />
基本費用 10万円～20万円<br />
成果報酬 採択金額の10～15%<br />
※採択金額が100万円以下の助成金・補助金は申請サポートできな場合がございます。
</center>
</div>
{/if}
<div style="font-size:0.7em;margin-left: 120px;">
</div>
<div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:300px;">
		<div style="float:left;">
		<form method="post" action="index.php" style="float:left;">
			<input type="hidden" name="action_display" value="1" />
			<input type="hidden" name="in_callkind"    value="{$app_ne.in_callkind}" />
			<input type="hidden" name="in_contents_id" value="{$app_ne.in_contents_id}" />
			<input type="submit" class="button" value=" 戻る" />
		</form>
		</div>
		<div style="float:left;">
		<form method="POST" action="index.php" style="float:left;margin-left:10px;">
			<input type="hidden" name="action_index" value="1" />
			<input type="submit" class="button2" value=" TOP" />
		</form>
		</div>
		<div class="clear"></div>
	</div>
</div>
		
</form>		   
<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">
	
</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>

{include file='common/track.tpl'}

</body>
</html>
