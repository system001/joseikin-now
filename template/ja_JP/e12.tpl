{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!--
var trigger_id1 = "#q17_4";
var txtarea_id1 = "#in_q17_no4_text";
var zan_text_id1 = "#zan_q17_no4_text";

$(document).ready(function (){
    text_val1 = $(txtarea_id1).val();
    CountDownLength('zan_q17_no4_text', text_val1, 255);
    if($(trigger_id1+":checked").val()){
        $(txtarea_id1).removeAttr("disabled");
        $(txtarea_id1).css("background-color","#ffffff");
        $(zan_text_id1).css("color","#000000");
    }else{
        $(txtarea_id1).attr("disabled", "disabled");
        $(txtarea_id1).css("background-color","#d2d1c6");
        $(zan_text_id1).css("color","#d2d1c6");
    }

    
});

$(function() {
    $(trigger_id1).on('click', TextAreaEnable1);
    
    function TextAreaEnable1(){
        if ($(trigger_id1+":checked").val()) {
            $(txtarea_id1).removeAttr("disabled");
            $(txtarea_id1).css("background-color","#ffffff");
            $(zan_text_id1).css("color","#000000");
        } else {
            $(txtarea_id1).attr("disabled", "disabled");
            $(txtarea_id1).css("background-color","#d2d1c6");
            $(zan_text_id1).css("color","#d2d1c6");
        }
        
    }
});




//-->
</script>
{/literal}
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e13" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar14.gif" width="740" height="30" /></div>
<div>
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10
 px;">
  <div>
    <div>
      <p><strong>［認定・仮認定NPO法人へ寄附をしたいと思わない理由］</strong>　</p>
    </div>
      <p> <strong>
        問１７</strong> 　【<a href="./index.php?action_e11=true&back=1">問１６</a>で「２．寄附をしたいとは思わない」とお答えになった方にお聞きします。】　<img src="images/m17.gif" width="128" height="19" /><br />
        <br />
        寄附をしたいと思わない理由は何ですか。<strong>（複数回答可能）</strong></p>
      <p>
        {if is_error('in_q17')}<br /><span class="error">{message name="in_q17"}</span>{/if}
        {if is_error('in_q17_no4_text')}<br /><span class="error">{message name="in_q17_no4_text"}</span>{/if}
        
        <label><input type="checkbox" name="in_q17_1" id="q17_1" value="1" {$app.data.q17_1} />{$app.master_q17.1}</label><br />
        <label><input type="checkbox" name="in_q17_2" id="q17_2" value="2" {$app.data.q17_2} />{$app.master_q17.2}</label><br />
        <label><input type="checkbox" name="in_q17_3" id="q17_3" value="3" {$app.data.q17_3} />{$app.master_q17.3}</label><br />
        <label><input type="checkbox" name="in_q17_4" id="q17_4" value="4" {$app.data.q17_4} />{$app.master_q17.4}</label><br />
        <textarea name="in_q17_no4_text"  id="in_q17_no4_text" rows="6" cols="60" onkeyup="CountDownLength('zan_q17_no4_text', value, 255);" maxlength="255">{$app.data.q17_no4_text}</textarea>
        <span id="zan_q17_no4_text">（残り255文字）</span>
        <br />
        <br />
      </p>
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e11=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->
  </div>
</div>
<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>
</form>

{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
