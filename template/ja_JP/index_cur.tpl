{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/style.css" rel="stylesheet" />
<style type="text/css">
.pass {
	text-align: center;
	margin-top: 0px;
}
.toptitle {
	text-align: left;
	margin-top: 0px;
}
.title {
	color: #32CD32; 
        font-size: xx-large;
}
.backcolor_text {
    background-color:#191970;
    /*font-weight: bold;*/
    font-size: medium;
    color: #ffffff; 
}



</style>
{/literal}
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<h3>

<table width="740" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td><p class="toptitle">助成金・補助金情報配信サービス「助成金なう」<br /><span class="title">助成金なう</span></p></td>
  </tr>
</table>
</h3>
<div></div>
<div class="image">
<div class="backcolor_text">&nbsp;&nbsp;&nbsp;ログイン</div>

    <table width="740" >
        <tr>
            <form action="{$script}" name="f2" method="{$form_action}">
            <input type="hidden" name="action_login_do" value="true">

            <td width="739">
              <table border="0" cellpadding="2" cellspacing="0" id="table3" style="text-align:center; margin:40px auto; font-size:18px;">
                <tr>
                      <th></th>
                        <td width="300" height="5"><img src="images/login_kikan_bar.gif" width="400" height="2" /></td>
                </tr>
                <tr>
                    　<th></th>
                        <td>ログインIDとパスワードを入力してください</td>
                </tr>
                    <tr>
                      <th></th>
                        <td width="300" height="5"><img src="images/login_kikan_bar.gif" width="400" height="2" /></td>
                    </tr>
              </table>
              <table border="0" cellpadding="2" cellspacing="0" id="table3" style="text-align:center; margin:40px auto; font-size:18px;">
                    <tr>
                        <th>ログインID</th>
                        <td>
                            <label for="ID"></label>
                            <input type="text" name="in_id" value="{$form.in_id}"  maxlength="64" style="height: 22px; width: 230px;">
                        </td>
                    </tr>
                    <tr><td height="20"></td></tr>
                    <tr>
                        <th>パスワード</th>
                        <td>
                            <input type="password" name="in_pw"  maxlength="64" style="height: 22px; width: 230px;">
                        </td>
                    </tr>
              </table>
<!--エラーメッセージ-->
{if count($errors)}
<table class="error" align="center">
{foreach from=$errors item=error}
<tr class="error"><td>{$error}</td></tr>
{/foreach}
</table>
{/if}
<!--エラーメッセージ-->
                        
                <div class="pass">
                    <table border="0" cellpadding="2" cellspacing="0" id="table3" style="text-align:center; margin:40px auto; font-size:18px;">
                    <tr>
                        <td>
                            <div>
                                <a href="#" class="css_button" onClick="javascript:do_submit();" style="padding:10px 100px 10px 100px;">ログイン</a>
                            </div>
                        </td>
                    </tr>
                    </table>
                    <br />
                    <br />
              </div>
            </td>
            </form>
        </tr>
    </table>
</td>
</tr></table>



<br />
<br />
<br />
<br />
<br />
<br />
<br />





<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>

<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="110" align="left" valign="middle"><font size="2"><p class="navit"><a href="http://www.navit-j.com/"><img src="images/navit_logo.gif" width="109" height="40" /></a></p></font></td>
    <td width="130" align="left" valign="middle"><font size="4">株式会社ナビット</font></td>
    <td width="420" align="right" valign="middle"><p>〒101-0051 東京都千代田区神田神保町3-10-2共立ビル3F</p></td>
  </tr>
</table>
<div align="right" valign="top">
        <p class="tel" valign="top"><img src="images/freedial.gif" width="100" height="20" alt="フリーダイアル"/> <font size="5">0120-937-781</font><br />受付時間10：00～19：00<br />（土日祝祭日休み）</p>
        <p></p>
</div>
<br />
<br />
<p align="center">Copyright (C) NAVIT CO.,LTD. All Rights Reserved. </p>
 

{$footer}
<!--フッター-->
{$app_ne.footer}
<!--フッター-->
</body>
</html>