<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	$('label').balloon();
});

function do_submit(kind){
	if(kind=="back"){
		document.f2.back.value = "1";
		document.f2.action = "index.php?action_secure_registration=true";
		document.f2.submit();
	}else{
		document.f2.submit();
	}
}
</script>
{/literal}
</head>
<body>
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_secure_complete" value="true">
<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->

<br />
<!-- <form action="{$script}" name="f3" method="POST" id="f3"> -->
<input type="hidden" name="action_password_complete" value="true">
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">新規ID・パスワード発行画面</span></div>
<div style="margin-left: 120px;">パスワードをお忘れの方は、下記フォームに必要事項を入力後、送信ボタンを押してください。<br /></div>
<div style="margin-left: 120px;">新しく発行したID・パスワードを記載したメールをお届けいたします。<br /></div>
<div style="font-size:0.7em;margin-left: 120px;">
</div>
<br />

<div style="margin-left: 120px;"><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><span style="font-size:80%;"> は必須入力項目です</span></div>
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
	<tr>
		<td class="l_Cel_01_01">会社名　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td class="s_Cel">例：株式会社ナビット<!--　※法人のお客様のみご登録いただけます。--><br />
			<input type="text" id="in_company_name" name="in_company_name" value="{$app.data.company_name}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_company_name')}<br /><span class="error" style="color:red;">{message name="in_company_name"}</span>{/if}
		</td>

	<tr>
		<td class="l_Cel_01_01">電話番号　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）</td>
		<td width="550" class="s_Cel">例：03-5215-5701<br />
			<input type="text" id="in_phone_no1" name="in_phone_no1" value="{$app.data.phone_no1}" size="10" maxlength="5"	style="font-size:16px;width:100px;" class="tel-number" />
			－
			<input type="text" id="in_phone_no2" name="in_phone_no2" value="{$app.data.phone_no2}" size="10" maxlength="4"	style="font-size:16px;width:100px;" class="tel-number" />
			－
			<input type="text" id="in_phone_no3" name="in_phone_no3" value="{$app.data.phone_no3}" size="10" maxlength="4"	style="font-size:16px;width:100px;" class="tel-number" />
			{if is_error('in_phone_no1')}<div class="error" style="color:red;">{message name="in_phone_no1"}</div>{/if}
			{if is_error('in_phone_no2')}<div class="error" style="color:red;">{message name="in_phone_no2"}</div>{/if}
			{if is_error('in_phone_no3')}<div class="error" style="color:red;">{message name="in_phone_no3"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">メールアドレス　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<input type="text" id="in_email" name="in_email" value="{$app.data.email}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_email')}<div class="error" style="color:red;">{message name="in_email"}</div>{/if}
			{if is_error('in_user_data')}<br /><span class="error" style="color:red;">{message name="in_user_data"}</span>{/if}
		</td>
	</tr>
	<tr>
		<td colspan="3">
				<!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
				<!-- ここまでトップへ戻る -->
		</td>
	</tr>
</table>
</div>
<div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:280px;">
		<a class="button" href="index.php" id="to_top_btn">TOPへ</a>
	</div>
	<div style="margin-top:-46px;margin-left:520px;">
		<a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();" id="send_btn">送信</a>
	</div>
</div>


</div>
</form>
<!-- ここまで入力フォーム -->
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->


</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>
<!-- スムーズスクロール -->
{/literal}

{include file='common/track.tpl'}

</body>
</html>
