<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	$('label').balloon();
});

function do_submit(kind){
	if(kind=="back"){
		document.f2.back.value = "1";
		document.f2.action = "index.php?action_secure_registration=true";
		document.f2.submit();
	}else{
		document.f2.submit();
	}
}
</script>
{/literal}
</head>
<body>
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_secure_complete" value="true">
<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->

<br />
<form action="{$script}" name="f3" method="POST" id="f3">
<input type="hidden" name="action_secure_complete" value="true">
{if $app.db_regist_result == "-1" || $app.mail_send_result == "-1"}
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">新規ID・パスワード発行送信エラー</span></div>
<div style="text-align:center;">
	<span style="font-weight:bold;font-size:100%;">ご入力頂いた内容で送信ができませんでした。</span><br />
	<br />
	大変お手数ではございますが再度こちらから再送信をお願い致します。>>
	<a href="index.php?action_password_registration=true"> 新規ID・パスワード発行フォームへ</a><br />
</div>
	  <br />
	  <br />
<div style="margin-left: 180px;">
	  <span style="color:#666;font-size:80%;">
		【ご登録に失敗する場合、以下の原因が考えられます】<br />
		※通信環境の混雑等の理由によるもの<br /><br />
		※ご登録頂いたメールアドレスへの送信失敗によるもの(ご登録頂いたメールアドレスをご確認ください)<br />
		<br />
		<br />
	  </span>
</div>
{else}
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">新規ID・パスワード発行送信完了</span></div>
<div style="text-align:center;">
	<span style="font-weight:bold;font-size:100%;">新規ID・パスワード発行を受け付けました。</span><br />
	<br />
</div>
<div style="margin-left: 200px;">
	ご入力頂いたメールアドレスに、<br />新しく発行したID・パスワードを記載したメールを送信致しました。<br />受信のご確認をお願い致します。<br />
</div>
	  <br />
	  <br />
<div style="margin-left: 150px;">
	  <span style="color:#666;font-size:80%;">
		※通信環境の混雑等の理由によりメールの到着に少々お時間がかかる場合がございます。<br /><br />
		※迷惑メール防止機能により当サイトからのメールが迷惑メールと間違えられ、<br />
		&nbsp;&nbsp;&nbsp;メール受信画面に表示されない場合がございます。<br />
		&nbsp;&nbsp;&nbsp;迷惑メールフォルダやゴミ箱に自動的に振り分けられている場合がございますので、<br />
		&nbsp;&nbsp;&nbsp;一度ご確認頂きますようお願い致します。<br />

	  </span>
</div>
{/if}
<div style="font-size:0.7em;margin-left: 120px;">
</div>

<div class="mod_form_btn">
<div style="margin-top:20px;margin-left:400px;">
<a class="button2" href="index.php" id="to_top_btn">TOPへ</a>
</div>
</div>

</div>

<input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
<input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
<input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}">
<input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}">
<input type="hidden" name="in_email" id="in_email" value="{$app.data.email}">

<input type="hidden" name="back" id="back" value="0">

</form>
<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>
<!-- スムーズスクロール -->

{/literal}

{include file='common/track.tpl'}

</body>
</html>
