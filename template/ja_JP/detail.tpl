<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>助成金・補助金検索結果一覧｜助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」の助成金・補助金検索結果一覧">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="stylesheet" href="css/first.css" type="text/css">
<link rel="stylesheet" href="css/ditail.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<style type="text/css">
<!--
@media print {

#header,
#result_title_img,
#gnavi,
#back_result1,
#back_result2,
#interest_offer,
.re_top_first,
.link_text_box,
#footer
{
display: none;
}


#proposal_ditail {
    /*
width:933px;
height:1019px;
background-image: url("../img/ditail_back.png");
background-repeat: no-repeat;
margin:30px 0 0 35px;
    */

border: #000000 solid 1px;
background-color: #ffffff;
width: 933px;
/*height: 1019px;*/
padding: 4px;
margin-left: 18px;
float: left;
box-shadow: 0px 0px 00px #ffffff;
}
.link_text {
        text-align: center;
        width:130px;
	font-size:12px;
	font-family:"メイリオ", Meiryo;
	font-weight:normal;
	border:1px solid #000000;
	padding:2px 76px;
	text-decoration:none;
	color:#000000;
	display:inline-block;
}
.ditail_box {
font-size: 11px;
width:933px;
margin:30px 0 0 15px;
}
.ditail_title_head_text {
width:850px;
font-size: 10px;
font-family:"メイリオ", Meiryo;
padding:50px 0 0 30px;
margin-bottom:-18px;
margin-top:-36px;
}
.ditail_title_text {
/*width:200px;*/
font-size: 20px;
font-family:"メイリオ", Meiryo;
font-weight:bold;
padding:20px 0 0 30px;
margin-top: -20px;
}
.ditail_midashitext {
font-size: 20px;
font-family:"メイリオ", Meiryo;
font-weight:bold;
margin:0 0 10px 0px;
}
.ditail_midashiback {
width:400px;
margin:30px 0 0 30px;
background-image: url("");
}
.ditail_midashiback_left {
width:0px;
margin:30px 0 0 30px;
background-image: url("");
}
.ditail_midashiback_right {
width:0px;
margin:30px 67px 0 30px;
background-image: url("");
}


}
-->
</style>

<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.lightbox_me.js"></script>

<script>
    $(window).load(function(){
//      $("#gnavi").sticky({ topSpacing: 0, center:true, className:"hey" });
    });
</script>

<script type="text/javascript">
function do_submit(){
    document.f2.submit();
}
</script>

<script type="text/javascript">

//$('#send_offer').click(function() {

//    alert("click");

    //$('#send_interest_confirm').lightbox_me({centered: true, closeSelector:"#close_x"});
    //e.preventDefault();
//});


function displaySendForm(){
    $("#send_interest_confirm").lightbox_me({centered: true,closeSelector:'#send_confirm_close',overlayCSS:{background:'#C2D6CD',opacity: .8}});
    //$("#send_interest_confirm").lightbox_me({centered: true, preventScroll: true, onLoad: function() {
    //    $("#send_interest_confirm").find("input:first").focus();
    //}});
}

</script>


{/literal}
</head>

<body>

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
	<div id="wrapper">

<!-- ここからheader --><div id="01"></div>
		<div id="header">

			<div id="logo">
				<a href="index.php"><img src="img/logo.jpg" alt="助成金なう" /></a>
			</div>

			<div id="h1">
				<h1>助成金・補助金の検索サービス「助成金なう」</h1>
			</div>

			<div id="mini_contact">
				<img src="img/m_contact.gif" alt="お問合せ" />
			</div>

			<div id="mini_contact_txt">
				<a href="https://www.navit-j.com/contactus/" target="_blank">お問合せ</a>
			</div>

			<div id="mini_sitemap">
				<img src="img/m_sitemap.gif" alt="運営会社" />
			</div>

			<div id="mini_sitemap_txt">
				<a href="http://www.navit-j.com/" target="_blank">運営会社</a>
			</div>

			<div id="freedial">
				<img src="img/freedial.jpg" alt="0120-937-781" />
			</div>

			{if $app.name !=""}
                        <div style="display:inline-block;width:360px;">
                            <table  border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#f00">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <span id="logininfo">{$app.name} 様　ログイン中</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <a href="./index.php?action_logout_do=true" id="logout_btn">ログアウト</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    {else}
                        <div id="newaccount">
                            <a class="button3" href="./index.php?action_secure_registration=true"  id="registration">新規会員登録</a>
                        </div>
                        <div id="login">
                            <a class="button3" href="./index.php?action_login=true" onclick="" id="login_btn">ログイン</a>
                        </div>
                    {/if}

		</div>

		<div class="clear">
		</div>

<!-- ここまでheader -->

<!-- ここからG NAVI -->


<div id="gnavi">
	<ul>
		<li style="height:93px;">
			<a href="index.php?#01"><img src="img/gnavi_home.jpg" alt="ホーム" width="143px" height="94px"></a>
		</li>
		<li style="height:93px;">
			<a href="lp.html" target="_blank"><img src="img/gnavi_news.jpg" onmouseover="this.src='img/gnavi_news_d.jpg'" onmouseout="this.src='img/gnavi_news.jpg'" /></a>
		</li>
		<li style="height:93px;">
			<a href="https://www.navit-j.com/service/joseikin-now/blog/" target="_blank" /><img src="img/gnavi_a_search.jpg" onmouseover="this.src='img/gnavi_a_search_d.jpg'" onmouseout="this.src='img/gnavi_a_search.jpg'" alt="助成金ブログ" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="https://www.facebook.com/joseikinnow" target="_blank /"><img src="img/gnavi_f_search.jpg" onmouseover="this.src='img/gnavi_f_search_d.jpg'" onmouseout="this.src='img/gnavi_f_search.jpg'" alt="助成金FB" width="142px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="seminar/index.html" target="_blank"><img src="img/gnavi_sem.jpg" onmouseover="this.src='img/gnavi_sem_d.jpg'" onmouseout="this.src='img/gnavi_sem.jpg'" alt="セミナー・イベント" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
                    {if $app.name !=""}
			<a href="./index.php?action_mypage=true"><img src="img/main_company.jpg" onmouseover="this.src='img/main_company_d.jpg'" onmouseout="this.src='img/main_company.jpg'" /></a>
                    {else}
			<a href="./index.php?action_login=true"><img src="img/main_company.jpg" onmouseover="this.src='img/main_company_d.jpg'" onmouseout="this.src='img/main_company.jpg'" /></a>
                    {/if}
		</li>
		<li style="height:93px;">
			<a href="http://nyusatsu-now.com/" target="_blank"><img src="img/main_mypage.jpg" onmouseover="this.src='img/main_mypage_d.jpg'" onmouseout="this.src='img/main_mypage.jpg'" /></a>
		</li>
	</ul>


</div>
		<div class="clear">
		</div>

<!-- ここまでG NAVI -->


<!-- ここからメインコンテンツ -->
<!-- ここから検索結果詳細 -->

<img id="result_title_img" src="img/title_proposal_ditail.jpg" alt="案件検索結果詳細" />
<div id="back_result1">
    <a class="button4" href="javascript:history.back()" onclick="" id="a_search">戻る</a>
</div>

<div id="proposal_ditail">

	<div class="ditail_box">

            {$app_ne.contents_mod_html}

	</div>
        <div id="interest_offer">

    		<form action="{$script}" name="f2" method="POST" id="f2">
		    	<input type="hidden" name="action_sendoffer" value="true" />
		    	<input type="hidden" id = "in_callkind" name="in_callkind" value="{$app.data.callkind}" />
		    	<input type="hidden" id = "in_contents_id" name="in_contents_id" value="{$app.data.contents_id}" />
			    <input type="hidden" id = "in_contents_mod_html" name="in_contents_mod_html" value="{$app.contents_mod_html_escape}" />
		    </form>
            <a id="send_offer" class="button6" href="javascript:void(0);" onclick="do_submit();">申請サポート申込み</a>
            <div>
こちらの案件の申請サポートをご希望の型は、こちらよりお申し込みください。お申し込み頂いた会員様には、折り返し『助成金なう』事務局（谷口、前田、千葉）よりご連絡させて頂きます。<br />
<br />
【申請サポート費用】<br />
基本費用 10万円～20万円<br />成果報酬 採択金額の10～15%（応相談）<br />
※採択金額が100万円以下の助成金・補助金は申請サポートできない場合がございます。<br />
<br />
【案件へのご相談】<br />
個別にこの助成金についてご相談したい方は、以下の番号まで、まずはご連絡ください。<br />
0120-937-781（フリーダイヤル）<br />担当：谷口、前田、千葉<br />

            </div>
        </div>


</div>


<br />
<div id="back_result2">

    <a class="button4" href="javascript:history.back()" onclick="" id="a_search">戻る</a>
</div>

<!-- ここから送信確認 -->
{literal}
<!--
<div id="send_interest_confirm" style="display:none;">

    <div style="width:100%;">
        <div style="width:24px;">
            <a href="javascript:void(0);" style="position:relative; top:-10px; left:592px;"><img id="send_confirm_close" src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
        </div>
    </div>
    <div style="font-size:20px; color:#22a90c; text-align:center;">
こちらの案件の詳細とサポートをお受けになりたい方は<br />


    <div style="text-align:center; margin-top:20px;">
        <a class="button5" href="https://www.navit-j.com/service/joseikin-now/service/model_03.html">有料会員サービスに申し込む</a>
    </div>

<br />
無料にてサポート情報を受け取りたい場合は<br />


    <div style="text-align:center; margin-top:20px;">
        <a class="button5" href="javascript:void(0)" onclick="do_submit();" id="send_interest">無料サポート依頼</a>
    </div>

    </div>

</div>
-->
{/literal}
<!-- ここまで送信確認 -->


<!--
<div id="back_result2">
    <a class="button4" href="javascript:history.back()" onclick="" id="a_search">戻る</a>
</div>
-->
<!-- ここまで検索結果詳細 -->

<!-- ここからトップへ戻る -->

	<div class="re_top_first">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>

<!-- ここまでトップへ戻る -->

<!-- ここから会社概要 -->
<!--
	<div id="com_info">
	<hr />
		<h3 style="padding:0 0 0 5px; font-size:19px; font-weight:bold; float:left;">
			株式会社ナビット
		</h3>

		<div style="padding:5px 150px 0 0; font-size:15px; line-height:15px; float:right;">
			本　　社：〒101-0051 東京都千代田区神田神保町3-10-2共立ビル3Ｆ<br />
	　　　　　TEL：03-5215-5701　FAX：03-5215-5702　URL：<a href="http://www.navit-j.com/" target="_blank" />http://www.navit-j.com/</a><br />
		</div>
		<div style="padding:5px 121px 0 0; font-size:15px; line-height:15px; float:right;">
			大阪支社：〒530-0001 大阪府大阪市北区梅田1丁目11番4-1100　大阪駅前第四ビル11階10号室<br />
	　　　　　TEL：06-4799-9201　FAX：06-4799-9011
		</div>
	</div>
	</div>
-->
<!-- ここまで会社概要 -->


</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->


</div>





{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 700; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
</script>

<!-- ▼************ Google Anarytics トラッキングコード ************ ▼-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- ▲ *********** Google Anarytics トラッキングコード ************ ▲-->

<!-- googleanalytics TAG -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-12', 'auto');
  ga('send', 'pageview');

</script>
<!-- googleanalytics TAG -->
{/literal}



<!-- LF innov TAG 20150127 -->

<script type="text/javascript">
var _trackingid = 'LFT-10394-1';

(function() {
  var lft = document.createElement('script'); lft.type = 'text/javascript'; lft.async = true;
  lft.src = document.location.protocol + '//track.list-finder.jp/js/ja/track.js';
  var snode = document.getElementsByTagName('script')[0]; snode.parentNode.insertBefore(lft, snode);
})();
</script>

<!-- LF innov TAG 20150127 -->

</body>
</html>

