<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal} 
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	$('label').balloon();
	if ($('#privacy_1').is(':checked')) {
		$('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
	} else {
		$('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
	}
});

function do_submit(){
    document.f2.submit();
}

</script>
{/literal}
</head>
<body>
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_login_do" value="true">
<!-- ここからconteinar -->
<div id="conteinar">
<!-- ここからwrapper -->
<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->

<!-- ここから入力フォーム --><div id="03"></div>

<!--<img src="img/title_a_search_line.jpg" alt="" />-->
<br />
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">ログイン</span></div>
<div style="margin-left: 296px;">ログインIDとパスワードを入力してください。<br /></div>
<div style="font-size:0.7em;margin-left: 120px;">
<span>
</span>
</div>
<br />

<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 208px;">
	<tr>
		<td style="text-align:right;">ログインID</td>
		<td>
			<input type="text" name="in_id" value="{$app.data.id}"	size="30" maxlength="64" style="font-size:20px;width:300px;">
			
		</td>
	</tr>
	<tr>
		<td style="text-align:right;">パスワード</td>
		<td>
			<input type="password" name="in_pw"  size="30" maxlength="64" style="font-size:20px;width:300px;">
			
		</td>
	</tr>
	<tr>
		<td style="text-align:right;vertical-align top;">文字認証</td>
		<td>
			<img src="index.php?action_captcha=true" style="margin-bottom:5px" id ='captcha'>
			<input type="text" name="in_captcha_code" size="15" maxlength="8" style="font-size:20px;width:300px;" /><br />
			<a href="#" onclick="document.getElementById('captcha').src = 'index.php?action_captcha=true' + Math.random(); return false">[画像変更]</a>
		</td>
	</tr>
	<tr>
		<td colspan=2>
			<div class="mod_form_btn">
				<div style="margin-top:20px;margin-left:190px;"><a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();" id="login_btn">ログイン</a></div>
			</div>
		</td>
	</tr>
</table>
<div class="request" style="font-size:16px; margin-top:20px; text-align:center;">
	<a href="./index.php?action_password_registration=true" target="_blank">ログインIDやパスワードを忘れた場合はこちら</a>
</div>
<!--エラーメッセージ-->
{if count($errors)}
<table class="error" align="center">
{foreach from=$errors item=error}
<tr class="error" style="color:red;"><td>{$error}</td></tr>
{/foreach}
</table>
{/if}
<!--エラーメッセージ-->
</div>

</div>
</form>				   
<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">
	
</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>
<!-- スムーズスクロール -->
{/literal}

{include file='common/track.tpl'}

</body>
</html>
