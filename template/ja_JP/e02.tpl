{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>

{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="js/jquery.ui.datepicker-ja.js"></script>
<script src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!--

$(function(){
    $('.datepicker').datepicker(
        {
            changeMonth: true,
            changeYear: true,
            showMonthAfterYear: true,
            wareki: true,
            dateFormat: 'yy-mm-dd',
            yearRange: '1989:2015'

        }
    );
    
    // 日付の和暦表示
    $("#in_preyear_span_start").change(function(){
        var d_arr  = parseDate($("#in_preyear_span_start").val());
        var wareki = "";
        if(d_arr){
            wareki = toWareki(d_arr["year"], d_arr["month"], d_arr["day"]);
        }
        $("#in_preyear_span_start").val(wareki);
    });
    if($("#in_preyear_span_start").val()){
        var d_arr  = parseDate($("#in_preyear_span_start").val());
        var wareki = "";
        if(d_arr){
            wareki = toWareki(d_arr["year"], d_arr["month"], d_arr["day"]);
        }
        $("#in_preyear_span_start").html(wareki);
    }

    $("#in_preyear_span_end").change(function(){
        var d_arr  = parseDate($("#in_preyear_span_end").val());
        var wareki = "";
        if(d_arr){
            wareki = toWareki(d_arr["year"], d_arr["month"], d_arr["day"]);
        }
        $("#in_preyear_span_end").val(wareki);
    });
    if($("#in_preyear_span_end").val()){
        var d_arr  = parseDate($("#in_preyear_span_end").val());
        var wareki = "";
        if(d_arr){
            wareki = toWareki(d_arr["year"], d_arr["month"], d_arr["day"]);
        }
        $("#in_preyear_span_end").html(wareki);
    }
    
    
    
    
});

function nextFocus(elm, max) {
    /* 次のエレメント番号 */
    var next_elm = eval(elm+1);
    if (document.f2.elements[elm].value.length >= max) {
        document.f2.elements[next_elm].focus();
    }
}


//-->
</script>
{/literal}
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}


<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e03" value="true">
<a name="top"></a>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar11.gif" width="740" height="30" /></div>
<div>
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10
 px;">
  <div>
    <p><strong>［性］</strong>　【全員の方にお聞きします】<font color="red"><strong>【必須】</strong></font></p>
  </div>
    <p> ●あなたの性を選択してください。 <br />
        {$app_ne.radio.sex}
        {if is_error('in_sex')}<br /><span class="error">{message name="in_sex"}</span>{/if}
    </p>
  <div><br />
    <p><strong>［年齢］</strong>　【全員の方にお聞きします】<font color="red"><strong>【必須】</strong></font></p>
  </div>
    <p> ●あなたの年齢を選択してください。<br />
        {$app_ne.radio.age}
        {if is_error('in_age')}<br /><span class="error">{message name="in_age"}</span>{/if}
    </p>
  <div> <br />
    <p><strong>［結婚］</strong>　【 全員の方にお聞きします】<font color="red"><strong>【必須】</strong></font></p>
  </div>
    <p> ●あなたの婚姻状況を選択してください。<br />
        {$app_ne.radio.marriage_status}
        {if is_error('in_marriage_status')}<br /><span class="error">{message name="in_marriage_status"}</span>{/if}
    </p>
  <div> <br />
    <p><strong>［住所（都道府県）］</strong>　【全員の方にお聞きします】<font color="red"><strong>【必須】</strong></font></p>
  </div>
    <p>
      <label for="in_area"></label>
      ●あなたが住んでいる都道府県を選択してください。<br />
      <br />
        <select name="in_area" id="in_area">
            <option value="">---都道府県---</option>
            {$app_ne.pulldown.area}
        </select>
        {if is_error('in_area')}<br /><span class="error">{message name="in_area"}</span>{/if}
    </p>
  <div> <br />
    <p><strong>［職種］</strong>　【全員の方にお聞きします】<font color="red"><strong>【必須】</strong></font></p>
  </div>
    <p> ●あなたの職業は次のうちのどれにあたりますか。選択してください。<br />
        {$app_ne.radio.job_kind}
        {if is_error('in_job_kind')}<br /><span class="error">{message name="in_job_kind"}</span>{/if}
    </p>
  
  <div><br />
    <p><strong>［世帯全体の年間収入］</strong>　【全員の方にお聞きします】<font color="red"><strong>【必須】</strong></font></p>
  </div>
    <p> ●あなたの<span style="border-bottom:double 3px #000; line-height:2em;">世帯全体</span>の年間収入（税込み額）は、およそどのくらいですか。選択してください。<br />
        {$app_ne.radio.yearly_income}
        {if is_error('in_yearly_income')}<br /><span class="error">{message name="in_yearly_income"}</span>{/if}
    </p>

</div>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
  </tr>
</table>
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e01=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();" width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入--> 

<div><img src="images/border1.gif" width="740
" height="3" /></div>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>
</form>

{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
