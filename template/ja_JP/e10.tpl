{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/common.js"></script>

{/literal}
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e11" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
		<td width="75" align="left" valign="middle">&nbsp;</td>
	</tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
	<div><img src="images/bar14.gif" width="740" height="30" /></div>
	<div>
		<div id="column">
			<div>
				<p><strong> ◆<font size="3">『特定非営利活動法人（以下「NPO法人」という。）』等に関する説明</font></strong></p>
			</div>
			<table width="710" border="0" cellspacing="3" cellpadding="3">
				<tr>
					<td width="10"><strong>1.</strong></td>
					<td width="708"><strong>「NPO法人」について</strong></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><p>　平成10年12月から特定非営利活動促進法（以下「NPO法」という。）が施行され、営利を目的としないボランティア団体などがこの法律に基づいて法人格（いわゆる「NPO法人」）を取得できるようになりました。<br />
						</p></td>
				</tr>
				<tr>
					<td><strong>2.</strong></td>
					<td><strong>「改正特定非営利活動促進法（以下「改正NPO法」という。）」の施行について</strong></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><p>平成24年4月1日施行の「改正NPO法」の主な改正点は以下の通りです。</p></td>
				</tr>
				<tr>
					<td><strong>①</strong></td>
					<td><strong>NPO法人に関する事務を地方自治体で一元的に実施</strong></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>・所轄庁の変更</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>　　2以上の都道府県に事務所を設置するNPO法人の所轄庁事務は、その主たる事務所の所在する都道府県（従来の内閣府から変更）が、その事務所が1の指定都市区域内にのみ所在するNPO法人にあってはその指定都市が行うようになりました。</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>・認定事務も地方自治体で実施</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>　　NPO法人のうち、その運営組織及び事業活動が適正であって公益の増進に資するものは、所轄庁（都道府県知事又は指定都市の長）の認定を受けることができるようになりました。</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td valign="bottom"><strong>②</strong></td>
					<td valign="bottom"><strong>制度の使いやすさと信頼性の向上のための見直し </strong></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>・申請手続きの簡素化・柔軟化</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>　所轄庁への届出のみで定款の変更を行うことができる事項(役員の定数等)が追加されました。また、社員総会の決議について、書面等による社員全員の同意の意思表示に替えることができるようになりました。</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>・会計の明確化</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>　　NPO法人が作成すべき計算書類のうち、｢収支計算書」が「活動計算書」(活動に係る事業の実績を表示するもの）に変更されました。</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><strong>③</strong></td>
					<td><strong>認定制度の見直し </strong></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>・認定基準の緩和</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>　　認定を受けるための基準が緩和されました。また、設立初期のNPO法人には財政基盤が弱い法人が多いことから、1回に限りスタートアップ支援としてPST基準を免除した仮認定（3年間有効）制度が導入されました。</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>

		<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
			</tr>
		</table>


		<div><img src="images/border1.gif" width="740" height="3" /></div>

		<p><strong>［NPO法人に対する関心］</strong>　</p>
	</div>
		<p>
			<strong>問１４</strong> 　【全員の方にお聞きします】　<img src="images/m14.gif" width="128" height="19" /> <br />
			<br />
			非営利活動を行うNPO法人に対し、関心はありますか。ひとつお選びください。 <br />
			{if is_error('in_q14')}<br /><span class="error">{message name="in_q14"}</span>{/if}
                                                <br />
                                                {$app_ne.radio.q14}
		</p>

	<div> <br />
		<p><strong>［認定・仮認定NPO法人に対する寄附の税制優遇措置の認識］</strong></p>
	</div>
		<p>
			<strong>問１５</strong> 　【全員の方にお聞きします】　<img src="images/m15.gif" width="128" height="19" /> <br />
			<br />
			NPO法人において、一定の基準を満たすことができた法人については、認定・仮認定NPO法人となり、認定・仮認定NPO法人に寄附を行った場合は、税制の優遇措置を受けることを御存知ですか。 ひとつお選びください。</p>
		<p>
			{if is_error('in_q15')}<br /><span class="error">{message name="in_q15"}</span>{/if}
                                                
                                                {$app_ne.radio.q15}
		</p>
	<table width="700" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
		</tr>
	</table>
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e09=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->
</div>
<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
	</tr>
</table>
</form>



{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
