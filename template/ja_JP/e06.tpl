{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!--
var trigger_id2 = "#q4_1_7";
var txtarea_id2 = "#in_q4_1_no7_text";
var zan_text_id2 = "#zan_q4_1_no7_text";

$(document).ready(function (){
    text_val2 = $(txtarea_id2).val();
    CountDownLength('zan_q4_1_no7_text', text_val2, 255);
    
    if($(trigger_id2+":checked").val()){
        $(txtarea_id2).removeAttr("disabled");
        $(txtarea_id2).css("background-color","#ffffff");
        $(zan_text_id2).css("color","#000000");
    }
    else{
        $(txtarea_id2).attr("disabled", "disabled");
        $(txtarea_id2).css("background-color","#d2d1c6");
        $(zan_text_id2).css("color","#d2d1c6");
    }
});

$(function() {
    $(trigger_id2).on('click', TextAreaEnable1);
    
    function TextAreaEnable1(){
        if ($(trigger_id2+":checked").val()) {
            $(txtarea_id2).removeAttr("disabled");
            $(txtarea_id2).css("background-color","#ffffff");
            $(zan_text_id2).css("color","#000000");
        } else {
            $(txtarea_id2).attr("disabled", "disabled");
            $(txtarea_id2).css("background-color","#d2d1c6");
            $(zan_text_id2).css("color","#d2d1c6");
        }
    }
});

//-->
</script>
{/literal}

</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e07" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
		<td width="75" align="left" valign="middle">&nbsp;</td>
	</tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
	<div><img src="images/bar13.gif" width="740" height="30" /></div>
</div>
<div id="column">
	<div>
		<p><strong>◆「寄附金」に関する説明</strong></p>
	</div>
	<table width="710" border="0" cellspacing="3" cellpadding="3">
		<tr>
			<td width="8">&nbsp;</td>
			<td width="710">ここでは、以下の2つの要件を満たすものを「寄附金」といいます。</td>
		</tr>
		<tr>
		<tr>
			<td width="8">&nbsp;</td>
			<td width="710"><strong>●支出する側に任意性があること</strong></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>金銭寄附金（時には物品も含む。）を出す人自身が、その金銭寄附金（時には物品）を出すか出さないかを自由に決定でき、かつその金額も自由に決めることができること。</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><strong>●直接の反対給付がないこと</strong></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>金銭寄附金（時には物品も含む。）を出した人が、その支出した金銭寄附金（時には物品も含む。）の代わりに、一般に流通するような商業的価値を持つ商品やサービスなどを受け取らないこと。</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
<div>
	<p><strong>［寄附経験の有無］</strong>　</p>
</div>
	<p> <strong>問７</strong> 　【全員の方にお聞きします】<font color="red"><strong>【必須】</strong></font><img src="images/m7.gif" width="128" height="19" /> <br />
		<br />
		あなたは、過去3年間に寄附をしたことがありますか。ひとつお選びください。<br />
		{if is_error('in_q7')}<br /><span class="error">{message name="in_q7"}</span>{/if}
                                <br />
                                {$app_ne.radio.q7}
	</p>
<table width="700" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right" valign="middle">&nbsp;</td>
	</tr>
</table>
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e05=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->


<div><img src="images/border1.gif" width="740
" height="3" /></div>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
		<td width="20" align="left" valign="middle">&nbsp;</td>
		<td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
	</tr>
</table>
</form>




{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
