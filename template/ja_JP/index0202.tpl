<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal} 
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>

<script>
    $(window).load(function(){
      $("#gnavi").sticky({ topSpacing: 0, center:true, className:"hey" });
    });
</script>
<script type="text/javascript">
//吹き出し
$('label').balloon();


$(function(){
      changeCity(document.f2.in_area1);
});  
  	
//全選択・全解除
$(function() {
    $('#all_check').on("click",function(){
        $('.CheckList').prop("checked", true);
    });
});
$(function() {
    $('#all_clear').on("click",function(){
        $('.CheckList').prop("checked", false);
    });
});

$(function() {
    $('#all_check2').on("click",function(){
        $('.CheckList2').prop("checked", true);
    });
});
$(function() {
    $('#all_clear2').on("click",function(){
        $('.CheckList2').prop("checked", false);
    });
});

$(function() {
    $('#all_check3').on("click",function(){
        $('.CheckList3').prop("checked", true);
    });
});
$(function() {
    $('#all_clear3').on("click",function(){
        $('.CheckList3').prop("checked", false);
    });
});

//リセット(自治体)
$(function() {
    $('#a_reset').on("click",function(){
        //ラジオボタン初期値セット
        $('#in_kind_1').prop("checked", true);
        $('#in_kind_2').prop("checked", false);
        //プルダウン初期値セット
        $('select[name="in_area1"]').val("");
        $('#in_area2').html('');//一度select内を空に
        $('#in_area2').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');
        
        //チェックボックスクリア
        $('.CheckList').prop("checked", false);
        
        $("#a_reset").blur();
    });
});
//リセット(財団)
$(function() {
    $('#f_reset').on("click",function(){
        //チェックボックスクリア
        $('.CheckList2').prop("checked", false);
        $('.CheckList3').prop("checked", false);
        $('#in_keyword').val("");
        $("#f_reset").blur();
    });
});

function changeCity(sel){

    var pref_code = sel.options[sel.selectedIndex].value;
    if(pref_code==""){
        $('#in_area2').html('');//一度select内を空に
        $('#in_area2').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');
    }
    
    
    $.ajax({
        type: "GET",
        url: "lib/citydata.php",
        dataType : 'json',
        data: "in_prefcode="+pref_code,
        success: function( res )
        {
            if(pref_code!=""){
                $('#in_area2').html('');//一度select内を空に
                $('#in_area2').append('<option id="city00000" value="'+''+'">'+'選択しない'+'</option>');
                for(var i=0; i<res.length; ++i){
                    $('#in_area2').append('<option id="city'+res[i].city_code+'" value="'+res[i].city_code+'">'+res[i].city_name+'</option>');
                }
            }
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
            alert('Error : '+ errorThrown);
        }
        
    });
}

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit_with_param(param){
    document.f2.in_callkind.value = param;
    document.f2.submit();
}



</script>

{/literal} 
</head>
<body>
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_searchresult" value="true">
<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
	<div id="wrapper">

<!-- ここからheader --><div id="01"></div>
		<div id="header">

			<div id="logo">
				<a href="index.php" ><img src="img/logo.jpg" alt="助成金なう" /></a>
			</div>

			<div id="h1">
				<h1>助成金・補助金の検索サービス「助成金なう」</h1>
			</div>

			<div id="mini_contact">
				<img src="img/m_contact.gif" alt="お問合せ" />
			</div>

			<div id="mini_contact_txt">
				<a href="https://www.navit-j.com/contactus/" target="_blank">お問合せ</a>
			</div>

			<div id="mini_sitemap">
				<img src="img/m_sitemap.gif" alt="サイトマップ" />
			</div>

			<div id="mini_sitemap_txt">
				<a href="#">サイトマップ</a>
			</div>

			<div id="freedial">
				<img src="img/freedial.jpg" alt="0120-937-781" />
			</div>

                    {if $app.name !=""}
                        <div style="display:inline-block;width:360px;">
                            <table  border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#f00">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <span id="logininfo">{$app.name} 様　ログイン中</span>   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <a href="./index.php?action_logout_do=true" id="logout_btn">ログアウト</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>    
                        </div>
                    {else}
                        <div id="newaccount">
                            <a class="button3" href="./index.php?action_secure_registration=true"  id="registration">新規会員登録</a>
                        </div>
                        <div id="login">
                            <a class="button3" href="./index.php?action_login=true" onclick="" id="login_btn">ログイン</a>
                        </div>
                    {/if}

		</div>

		<div class="clear">
		</div>

<!-- ここまでheader -->

<!-- ここからG NAVI -->


<div id="gnavi">
	<ul>
		<li style="height:93px;">
			<a href="#01"><img src="img/gnavi_home.jpg" alt="ホーム" width="143px" height="94px"></a>
		</li>
		<li style="height:93px;">
			<a href="lp.html" target="_blank"><img src="img/gnavi_news.jpg" onmouseover="this.src='img/gnavi_news_d.jpg'" onmouseout="this.src='img/gnavi_news.jpg'" /></a>
		</li>
		<li style="height:93px;">
			<a href="#03"><img src="img/gnavi_a_search.jpg" onmouseover="this.src='img/gnavi_a_search_d.jpg'" onmouseout="this.src='img/gnavi_a_search.jpg'" alt="自治体案件検索" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="#04"><img src="img/gnavi_f_search.jpg" onmouseover="this.src='img/gnavi_f_search_d.jpg'" onmouseout="this.src='img/gnavi_f_search.jpg'" alt="財団案件検索" width="142px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="http://www.navit-j.com/press/sem150203.html" target="_blank"><img src="img/gnavi_sem.jpg" onmouseover="this.src='img/gnavi_sem_d.jpg'" onmouseout="this.src='img/gnavi_sem.jpg'" alt="セミナー・イベント" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="http://www.navit-j.com/" target="_blank"><img src="img/gnavi_com.jpg" onmouseover="this.src='img/gnavi_com_d.jpg'" onmouseout="this.src='img/gnavi_com.jpg'" /></a>
		</li>
		<li style="height:93px;">
			<a href="http://www.navit-j.com/press/info.html" target="_blank"><img src="img/gnavi_ccontact.jpg" onmouseover="this.src='img/gnavi_ccontact_d.jpg'" onmouseout="this.src='img/gnavi_ccontact.jpg'" /></a>
		</li>
	</ul>

</div>
		<div class="clear">
		</div>

<!-- ここまでG NAVI -->

<!-- ここからメインビジュアル -->

	<img src="img/main_vis01.jpg" alt="助成金なうの特徴" usemap="#Map" border="0" />
  <map name="Map" id="Map">
      <area shape="rect" coords="91,12,302,227" href="#03" />
      <area shape="rect" coords="394,12,615,229" href="http://www.navit-j.com/press/sem150203.html" target="_blank" />
    <area shape="rect" coords="706,10,921,226" href="https://www.navit-j.com/service/joseikin-now/lp.html#7" target="_blank" />
    </map>

<!-- ここまでメインビジュアル -->

<!-- ここからメインコンテンツ -->


<!-- ここから自治体フォーム --><div id="03"></div>

	<img src="img/title_a_search_line.jpg" alt="" />
	<img src="img/title_a_search.jpg" alt="自治体案件検索" />

<div class="table">

		<table border="0" cellpadding="5" cellspacing="1">

		<tr>
			<td class="l_Cel"><img src="img/midashi_syubetu.jpg" alt="種別" /></td>
			<td colspan="2" class="r_Cel">
                            <table border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label><input class="CheckList4" type="checkbox" name="in_kind_1000" id="n_kind_1000" value="1000" {$app.data.kind_1000} />{$app.master_kind.1000}</label></td>
                                    <td><label><input  class="CheckList4" type="checkbox" name="in_kind_2000" id="in_kind_2000" value="2000" {$app.data.kind_2000} />{$app.master_kind.2000}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList4" type="checkbox" name="in_kind_3000" id="in_kind_3000" value="3000" {$app.data.kind_3000} />{$app.master_kind.3000}</label></td>
                                </tr>
                            </table>
                            {*$app_ne.radio.kind*}
                            {if is_error('in_kind')}<br /><br /><span class="error">{message name="in_kind"}</span>{/if}
                        </td>
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_area.jpg" alt="エリア" /></td>
			<td colspan="2" class="r_Cel">
                            
                            <select id='in_area1' name="in_area1" class="select_font" onchange="changeCity(this);">{$app_ne.pulldown.area1}</select>
                            <select id='in_area2' name="in_area2" class="select_font"><option value="">市区町村を選択</option></select>
                            {if is_error('in_area1')}<br /><br /><span class="error">{message name="in_area1"}</span>{/if}
                            {if is_error('in_area2')}<br /><br /><span class="error">{message name="in_area2"}</span>{/if}
                            
                        </td>
                            
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_bunya.jpg" alt="分野" />
                        <a href="javascript:void(0)" id="all_check" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
                        <a href="javascript:void(0)" id="all_clear" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a>
                        </td>
			<td class="r_Cel">
                            <table border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label><input class="CheckList" type="checkbox" name="in_field_90" id="in_field_90" value="9000" {$app.data.field_90} />{$app.master_field.9000}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_91" id="in_field_91" value="9100" {$app.data.field_91} />{$app.master_field.9100}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_92" id="in_field_92" value="9200" {$app.data.field_92} />{$app.master_field.9200}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_93" id="in_field_93" value="9300" {$app.data.field_93} />{$app.master_field.9300}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_94" id="in_field_94" value="9400" {$app.data.field_94} />{$app.master_field.9400}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_95" id="in_field_95" value="9500" {$app.data.field_95} />{$app.master_field.9500}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_96" id="in_field_96" value="9600" {$app.data.field_96} />{$app.master_field.9600}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_97" id="in_field_97" value="9700" {$app.data.field_97} />{$app.master_field.9700}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_98" id="in_field_98" value="9800" {$app.data.field_98} />{$app.master_field.9800}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_99" id="in_field_99" value="9900" {$app.data.field_99} />{$app.master_field.9900}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_100" id="in_field_100" value="10000" {$app.data.field_100} />{$app.master_field.10000}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_101" id="in_field_101" value="10100" {$app.data.field_101} />{$app.master_field.10100}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_field')}<br /><span class="error">{message name="in_field"}</span>{/if}
                            
                            
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td colspan="4">
				<div id="submit_btn">
					<!--<input type="image" onclick="return false;" id="a_reset" src="img/subbtn_reset.png" onMouseOver="this.src='img/subbtn_reset_d.png'" onMouseOut="this.src='img/subbtn_reset.png'" alt="リセット">-->
                                        <a class="button" href="javascript:void(0)" onclick="return false;" id="a_reset">リセット</a>
					<!--<input type="image" id="a_search" onClick="javascript:do_submit_with_param('autonomy');" src="img/subbtn_submit.png" onMouseOver="this.src='img/subbtn_submit_d.png'" onMouseOut="this.src='img/subbtn_submit.png'" alt="一覧表示">-->
                                        <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit_with_param('autonomy');" id="a_search">一覧表示</a>     
				</div>
			</td>
		  </tr>

    </table>
</div>
	<!-- ここまで自治体フォーム -->

<!-- ここからトップへ戻る -->

	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>

<!-- ここまでトップへ戻る -->

<!-- ここから財団フォーム --><div id="04"></div>

	<img src="img/title_f_search_line.jpg" alt="" />
	<img src="img/title_f_search.jpg" alt="財団案件検索" />

<div class="table">
		<table border="0" cellpadding="5" cellspacing="1">

		<tr>
			<td class="l_Cel"><img src="img/midashi_bunya.jpg" alt="分野" />
                        <a href="javascript:void(0)" id="all_check2" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
                        <a href="javascript:void(0)" id="all_clear2" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a>
                        </td>
			<td class="r_Cel">
                            <table  border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label title="{$app.master_field2_desc.1000}"><input class="CheckList2" type="checkbox" name="in_field2_1" id="in_field2_1" value="1000" {$app.data.field2_1} />{$app.master_field2.1000}</label></td>
                                    <td><label title="{$app.master_field2_desc.2000}"><input  class="CheckList2" type="checkbox" name="in_field2_2" id="in_field2_2" value="2000" {$app.data.field2_2} />{$app.master_field2.2000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.3000}"><input  class="CheckList2" type="checkbox" name="in_field2_3" id="in_field2_3" value="3000" {$app.data.field2_3} />{$app.master_field2.3000}</label></td>
                                    <td><label title="{$app.master_field2_desc.4000}"><input  class="CheckList2" type="checkbox" name="in_field2_4" id="in_field2_4" value="4000" {$app.data.field2_4} />{$app.master_field2.4000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.5000}"><input  class="CheckList2" type="checkbox" name="in_field2_5" id="in_field2_5" value="5000" {$app.data.field2_5} />{$app.master_field2.5000}</label></td>
                                    <td><label title="{$app.master_field2_desc.6000}"><input  class="CheckList2" type="checkbox" name="in_field2_6" id="in_field2_6" value="6000" {$app.data.field2_6} />{$app.master_field2.6000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.7000}"><input  class="CheckList2" type="checkbox" name="in_field2_7" id="in_field2_7" value="7000" {$app.data.field2_7} />{$app.master_field2.7000}</label></td>
                                    <td><label title="{$app.master_field2_desc.8000}"><input  class="CheckList2" type="checkbox" name="in_field2_8" id="in_field2_8" value="8000" {$app.data.field2_8} />{$app.master_field2.8000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.9000}"><input  class="CheckList2" type="checkbox" name="in_field2_9" id="in_field2_9" value="9000" {$app.data.field2_9} />{$app.master_field2.9000}</label></td>
                                    <td><label title="{$app.master_field2_desc.10000}"><input class="CheckList2" type="checkbox" name="in_field2_10" id="in_field2_10" value="10000" {$app.data.field2_10} />{$app.master_field2.10000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.11000}"><input class="CheckList2" type="checkbox" name="in_field2_11" id="in_field2_11" value="11000" {$app.data.field2_11} />{$app.master_field2.11000}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_field2')}<br /><span class="error">{message name="in_field2"}</span>{/if}
                            
                            
			</td>

			
		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_keitai.jpg" alt="形態" />
                        <a href="javascript:void(0)" id="all_check3" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
                        <a href="javascript:void(0)" id="all_clear3" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a>
                        </td>
			<td class="r_Cel">
                            <table  border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label title="{$app.master_field3_desc.1000}"><input class="CheckList3" type="checkbox" name="in_field3_1" id="in_field3_1" value="1000" {$app.data.field3_1} />{$app.master_field3.1000}</label></td>
                                    <td><label title="{$app.master_field3_desc.2000}"><input  class="CheckList3" type="checkbox" name="in_field3_2" id="in_field3_2" value="2000" {$app.data.field3_2} />{$app.master_field3.2000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.3000}"><input  class="CheckList3" type="checkbox" name="in_field3_3" id="in_field3_3" value="3000" {$app.data.field3_3} />{$app.master_field3.3000}</label></td>
                                    <td><label title="{$app.master_field3_desc.4000}"><input  class="CheckList3" type="checkbox" name="in_field3_4" id="in_field3_4" value="4000" {$app.data.field3_4} />{$app.master_field3.4000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.5000}"><input  class="CheckList3" type="checkbox" name="in_field3_5" id="in_field3_5" value="5000" {$app.data.field3_5} />{$app.master_field3.5000}</label></td>
                                    <td><label title="{$app.master_field3_desc.6000}"><input  class="CheckList3" type="checkbox" name="in_field3_6" id="in_field3_6" value="6000" {$app.data.field3_6} />{$app.master_field3.6000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.7000}"><input  class="CheckList3" type="checkbox" name="in_field3_7" id="in_field3_7" value="7000" {$app.data.field3_7} />{$app.master_field3.7000}</label></td>
                                    <td><label title="{$app.master_field3_desc.8000}"><input  class="CheckList3" type="checkbox" name="in_field3_8" id="in_field3_8" value="8000" {$app.data.field3_8} />{$app.master_field3.8000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.9000}"><input  class="CheckList3" type="checkbox" name="in_field3_9" id="in_field3_9" value="9000" {$app.data.field3_9} />{$app.master_field3.9000}</label></td>
                                    <td><label title="{$app.master_field3_desc.10000}"><input class="CheckList3" type="checkbox" name="in_field3_10" id="in_field3_10" value="10000" {$app.data.field3_10} />{$app.master_field3.10000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.11000}"><input class="CheckList3" type="checkbox" name="in_field3_11" id="in_field3_11" value="11000" {$app.data.field3_11} />{$app.master_field3.11000}</label></td>
                                    <td><label title="{$app.master_field3_desc.12000}"><input class="CheckList3" type="checkbox" name="in_field3_12" id="in_field3_12" value="12000" {$app.data.field3_12} />{$app.master_field3.12000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.13000}"><input class="CheckList3" type="checkbox" name="in_field3_13" id="in_field3_13" value="13000" {$app.data.field3_13} />{$app.master_field3.13000}</label></td>
                                    <td><label title="{$app.master_field3_desc.14000}"><input class="CheckList3" type="checkbox" name="in_field3_14" id="in_field3_14" value="14000" {$app.data.field3_14} />{$app.master_field3.14000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.15000}"><input class="CheckList3" type="checkbox" name="in_field3_15" id="in_field3_15" value="15000" {$app.data.field3_15} />{$app.master_field3.15000}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_field3')}<br /><span class="error">{message name="in_field3"}</span>{/if}
                            
                            
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_keyword.jpg" alt="キーワード" /></td>
			<td colspan="2" class="r_Cel">
                            <input type="text" id="in_keyword" name="in_keyword" value="{$app.data.keyword}" size="40" maxlength="20" value="案件名などのワードを入力してください" style="font-size:20px;" />
                            <label for="keyword"></label>
                            {if is_error('in_keyword')}<br /><span class="error">{message name="in_keyword"}</span>{/if}
			</td>
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<div id="submit_btn">
					<!--<input type="image" onClick="return false;" id="f_reset" src="img/subbtn_reset.png" onMouseOver="this.src='img/subbtn_reset_d.png'" onMouseOut="this.src='img/subbtn_reset.png'" alt="リセット">-->
                                        <a class="button" href="javascript:void(0)" onclick="return false;" id="f_reset">リセット</a> 
					<!--<input type="image" onClick="javascript:do_submit_with_param('foundation');" src="img/subbtn_submit.png" onMouseOver="this.src='img/subbtn_submit_d.png'" onMouseOut="this.src='img/subbtn_submit.png'" alt="リセット">-->
                                        <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit_with_param('foundation');" id="f_search">一覧表示</a>     
				</div>
			</td>
		  </tr>

	</table>
<input type="hidden" id = "in_callkind" name="in_callkind" value="">                        
</form>
</div>

<!-- ここまで財団フォーム -->

<!-- ここからトップへ戻る -->

	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>

<!-- ここまでトップへ戻る -->

<!-- ここからお知らせ -->

<div id="02"></div>

	<img src="img/title_news_line.jpg" alt="" />
	<img src="img/title_news.jpg" alt="お知らせ" />



<div class="topics_maincont_news">

<!-- 一行 -->
		<div class="space">
			<div class="matter">
				2015.01.30
			</div>
			<div class="text">
				[中小企業活路開拓調査・実現化事業]が公示されました！
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="matter">
				2015.01.29
			</div>
			<div class="text">
				[地域商業自立促進事業]が公示されました！
			</div>
		</div>
		<div class="clear">
		</div>


<!-- 一行 -->
		<div class="space">
			<div class="news">
				2015.01.19
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150119" target="_blank">「助成金なう」サービスがスタートいたしました。全国の助成金・補助金対応 日本初！官公庁、自治体、財団11187機関の情報を網羅しております。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="media">
				2015.01.07
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150107" target="_blank">雑誌「地図ジャーナル」2015年 新春号 No.176 業界消息にて、CEATEC JAPANでの弊社出展と新サービス「地番変換サービス」「雷発生予想データ」が紹介されております。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="media">
				2015.01.05
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info150105" target="_blank">ランコム社内報2014年第2期号にて、当社代表福井が登壇しました9/6（土）開催の復興支援セミナー「アイズ・フォー・フィーチャーbyランコム～女性が輝く。石巻が輝く。～」が紹介されました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space">
			<div class="news">
				2014.12.26
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info141226-2" target="_blank">「2015年1月版 価格表」のダウンロードを開始いたしました。</a>
			</div>
		</div>
		<div class="clear">
		</div>

<!-- 一行 -->
		<div class="space_height">
			<div class="news">
				2014.12.05
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info141205" target="_blank">12月6日（土）テレビ朝日「タメになる同世代自慢バラエティ！　学年ゲーム～TAME～」（24:45～25:15）において、当社代表福井と当社の業務が紹介される予定です。 関東ローカルのみの放送となります。</a>
			</div>
		</div>
		<div class="clear">
		</div>


</div>

<!-- ここまでお知らせ -->



<!-- ここからセミナー -->
<!--
<div id="05"></div>

	<img src="img/title_sem_line.jpg" alt="" />
	<img src="img/title_sem.jpg" alt="セミナー・イベント" />
	<img src="img/con_sem.jpg" alt="セミナー・イベント" />
-->
<!-- ここまでセミナー -->

<!-- ここからトップへ戻る -->
<!-- 
	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>
-->
<!-- ここまでトップへ戻る -->

<!-- ここから会社概要 -->
<!--

<div id="06"></div>

	<img src="img/title_com_line.jpg" alt="" />
	<img src="img/title_com.jpg" alt="会社概要" />

<div id="company">

<div id="company_left">

	<div id="com_msg">

		<div id="navit_logo">
			<a href="http://www.navit-j.com/" target="_blank" /><img src="img/navit_logo.jpg" alt="株式会社ナビット"></a>
		</div>

		<div id="msg">
			入札なうを運営する「株式会社ナビット」は、様々なサービスを<br />提供しております。その実績をご評価いただき、テレビ他、<br />多様なメディアに取り上げていただいております。
		</div>

		<div class="clear">
		</div>
		<hr />
	</div>

	<div id="pre_msg">

		<div id="pre_photo">
			<img src="img/com_fukui.jpg" alt="福井泰代">
		</div>

		<div id="pmsg"><span style="font-size:1.5em; font-weight:bold; color:#FF4000;">のりかえ便利マップをご存じですか？</span><br />
ひとりの主婦が、小さな子供を抱えて駅で困った経験、階段やエスカレータ<br />ーがどこにあって、出口はどこなのか？重いベビーカーを押しながら思いつ<br />いた答えがのりかえ便利マップのきっかけでした。<br />その主婦がナビット代表の福井です。
		</div>

		<div class="clear">
		</div>

	</div>

</div>

	<div id="company_right">
		<img src="img/com_noriben.jpg" alt="のりかえ便利マップ">
	</div>

	<div class="clear">
	</div>

	<div id="company_bottom">
	<hr />
		<h3 style="padding:0 0 0 5px; font-size:19px; font-weight:bold; float:left;">
			株式会社ナビット
		</h3>

		<div style="padding:5px 150px 0 0; font-size:15px; line-height:15px; float:right;">
			本　　社：〒101-0051 東京都千代田区神田神保町3-10-2共立ビル3Ｆ<br />
	　　　　　TEL：03-5215-5701　FAX：03-5215-5702　URL：<a href="http://www.navit-j.com/" target="_blank" />http://www.navit-j.com/</a><br />
		</div>
		<div style="padding:5px 121px 0 0; font-size:15px; line-height:15px; float:right;">
			大阪支社：〒530-0001 大阪府大阪市北区梅田1丁目11番4-1100　大阪駅前第四ビル11階10号室<br />
	　　　　　TEL：06-4799-9201　FAX：06-4799-9011
		</div>
	</div>
	</div>
-->
<!-- ここまで会社概要 -->

<!-- ここからトップへ戻る -->
<!--
	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>
-->
<!-- ここまでトップへ戻る -->
</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">
	
</div>

<!-- ここまでfooter -->


</div>


<!-- スムーズスクロール -->
{literal}
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 700; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
</script>
<!-- スムーズスクロール -->

<!-- グローバルナビ -->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->

<!-- グローバルナビ -->

<!-- ▼************ Google Anarytics トラッキングコード ************ ▼-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- ▲ *********** Google Anarytics トラッキングコード ************ ▲-->

<!-- googleanalytics TAG -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-12', 'auto');
  ga('send', 'pageview');

</script>
<!-- googleanalytics TAG -->


<!-- LF innov TAG 20150127 -->

<script type="text/javascript">
var _trackingid = 'LFT-10394-1';

(function() {
  var lft = document.createElement('script'); lft.type = 'text/javascript'; lft.async = true;
  lft.src = document.location.protocol + '//track.list-finder.jp/js/ja/track.js';
  var snode = document.getElementsByTagName('script')[0]; snode.parentNode.insertBefore(lft, snode);
})();
</script>

<!-- LF innov TAG 20150127 -->


{/literal}


</body>
</html>