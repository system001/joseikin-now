<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	$('label').balloon();
});

function do_submit(kind){
	if(kind=='back'){
		document.f2.submit();
	}else{
		document.f3.submit();
	}
}

//確認ボタンの有効無効

jQuery(function(){
	$('#privacy_1').change(function(){
			if ($(this).is(':checked')) {
					$('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
			} else {
					$('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
			}
	});
});

</script>
{/literal}
</head>
<body>

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->

<!-- ここから入力フォーム --><div id="03"></div>

<br />
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">ご入力内容の確認</span></div>
<div style="margin-left: 120px;">以下の内容でよろしければ、『送信』ボタンを押してください。<br />修正する場合は、『戻る』ボタンを押して該当箇所を修正してください。</div>
<div style="font-size:0.7em;margin-left: 120px;">
</div>

<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
	<tr>
		<td class="l_Cel_01_02" width="120px;">会社名</td>
		<td class="s_Cel_01" style="text-align:left;">{$app.data.company_name}</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">会社名かな</td>
		<td class="s_Cel_01" style="text-align:left;">{$app.data.company_kana}</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">支店名</td>
		<td class="s_Cel_01" style="text-align:left;">{$app.data.branch_name}</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">郵便番号</td>
		<td class="s_Cel_01" style="text-align:left;">{$app.data.zip1}－{$app.data.zip2}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">住所</td>
		<td class="s_Cel_01" style="text-align:left;">{$app.data.address}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">電話番号</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.phone_no1}{$app.data.phone_no2}{$app.data.phone_no3}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">携帯電話番号</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.k_phone_no1}{$app.data.k_phone_no2}{$app.data.k_phone_no3}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">FAX番号</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.fax_no1}{$app.data.fax_no2}{$app.data.fax_no3}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">部署名</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.department_name}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">役職名</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.post_name}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">担当者名</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.contractor_lname}&nbsp;&nbsp;{$app.data.contractor_fname}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">担当者名かな</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.contractor_lkana}&nbsp;&nbsp;{$app.data.contractor_fkana}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">メールアドレス</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.email}
		</td>
	</tr>

{if $app.data.email_2}
	<tr>
		<td class="l_Cel_01_02">メールアドレス2人目</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.email_2}
		</td>
	</tr>
{/if}
{if $app.data.email_3}
	<tr>
		<td class="l_Cel_01_02">メールアドレス3人目</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.email_3}
		</td>
	</tr>
{/if}
{if $app.data.email_4}
	<tr>
		<td class="l_Cel_01_02">メールアドレス4人目</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.email_4}
		</td>
	</tr>
{/if}
{if $app.data.email_5}
	<tr>
		<td class="l_Cel_01_02">メールアドレス5人目</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.email_5}
		</td>
	</tr>
{/if}
	<tr>
		<td class="l_Cel_01_02">サイトを知ったきっかけ</td>
		<td class="s_Cel_01" style="text-align:left;">
			{$app.data.from}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">支援を受けたい分野1<br />(自治体案件カテゴリ)</td>
		<td width="550" class="s_Cel">
			<table border="0" style="text-align:left;">
				<tr>
					<td style="width:220px;">{if $app.data.field_90 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9000}</td>
					<td>{if $app.data.field_91 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9100}</td>
				</tr>
				<tr>
					<td>{if $app.data.field_92 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9200}</td>
					<td>{if $app.data.field_93 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9300}</td>
				</tr>
				<tr>
					<td>{if $app.data.field_94 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9400}</td>
					<td>{if $app.data.field_95 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9500}</td>
				</tr>
				<tr>
					<td>{if $app.data.field_96 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9600}</td>
					<td>{if $app.data.field_97 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9700}</td>
				</tr>
				<tr>
					<td>{if $app.data.field_98 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9800}</td>
					<td>{if $app.data.field_99 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.9900}</td>
				</tr>
				<tr>
					<td>{if $app.data.field_100 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.10000}</td>
					<td>{if $app.data.field_101 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field.10100}</td>
				</tr>
			</table>
				{if is_error('in_field')}<br /><span class="error">{message name="in_field"}</span>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">支援を受けたい分野2<br />(財団法人案件カテゴリ)</td>
		<td width="550" class="s_Cel">
			<table	border="0" style="text-align:left;">
				<tr>
					<td style="width:220px;">{if $app.data.field2_1 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.1000}</td>
					<td>{if $app.data.field2_2 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.2000}</td>
				</tr>
				<tr>
					<td>{if $app.data.field2_3 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.3000}</td>
					<td>{if $app.data.field2_4 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.4000}</td>
				</tr>
				<tr>
					<td>{if $app.data.field2_5 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.5000}</td>
					<td>{if $app.data.field2_6 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.6000}</td>
				</tr>
				<tr>
					<td>{if $app.data.field2_7 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.7000}</td>
					<td>{if $app.data.field2_8 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.8000}</td>
				</tr>
				<tr>
					<td>{if $app.data.field2_9 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.9000}</td>
					<td>{if $app.data.field2_10 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.10000}</td>
				</tr>
				<tr>
					<td>{if $app.data.field2_11 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field2.11000}</td>
				</tr>
			</table>
				{if is_error('in_field2')}<br /><span class="error">{message name="in_field2"}</span>{/if}
		</td>
	</tr>
	 <tr>
		<td class="l_Cel_01_02">支援を受けたい事業の<br />形態</td>
		<td width="550" class="s_Cel">
			<table	border="0" style="text-align:left;">
				<tr>
					<td style="width:220px;">{if $app.data.field3_1 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.1000}</td>
					<td>{if $app.data.field3_2 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.2000}</label></td>
				</tr>
				<tr>
					<td>{if $app.data.field3_3 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.3000}</label></td>
					<td>{if $app.data.field3_4 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.4000}</label></td>
				</tr>
				<tr>
					<td>{if $app.data.field3_5 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.5000}</label></td>
					<td>{if $app.data.field3_6 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.6000}</label></td>
				</tr>
				<tr>
					<td>{if $app.data.field3_7 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.7000}</label></td>
					<td>{if $app.data.field3_8 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.8000}</label></td>
				</tr>
				<tr>
					<td>{if $app.data.field3_9 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.9000}</label></td>
					<td>{if $app.data.field3_10 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.10000}</label></td>
				</tr>
				<tr>
					<td>{if $app.data.field3_11 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.11000}</label></td>
					<td>{if $app.data.field3_12 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.12000}</label></td>
				</tr>
				<tr>
					<td>{if $app.data.field3_13 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.13000}</label></td>
					<td>{if $app.data.field3_14 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.14000}</label></td>
				</tr>
				<tr>
					<td>{if $app.data.field3_15 != ""}<img class="checkbox_img" src = "img/checked.png"/>{else}<img class="checkbox_img" src = "img/non-checked.png" />{/if}{$app.master_field3.15000}</label></td>
				</tr>
			</table>
				{if is_error('in_field3')}<br /><span class="error">{message name="in_field3"}</span>{/if}
		</td>
	</tr>


	{if $app.data.check_payment == 1}
		<tr>
			<td class="l_Cel_01_02">有料会員</td>
			<td class="s_Cel_01" style="text-align:left;">
				有料会員に申し込む
			</td>
		</tr>
		<tr>
			<td class="l_Cel_01_02">支払方法</td>
			<td class="s_Cel_01" style="text-align:left;">
				{if $app.data.payment_method == 1}
					クレジットカード決済
				{else}
					銀行振込
				{/if}
			</td>
		</tr>
		<tr>
			<td class="l_Cel_01_02">ご利用期間/料金</td>
			<td class="s_Cel_01" style="text-align:left;">
				12ヶ月 \{$app.data.account_count*12960|number_format}(税込)
			</td>
		</tr>
		<tr>
			<td class="l_Cel_01_02">入金予定日</td>
			<td class="s_Cel_01" style="text-align:left;">
				{if $app.data.payment_method == 1}
					即日
				{else}
					{$app.data.pay_date}
				{/if}
			</td>
		</tr>
		<tr>
			<td class="l_Cel_01_02">開始希望日</td>
			<td class="s_Cel_01" style="text-align:left;">
				{$app.data.begin_date}
			</td>
		</tr>
	{/if}

	<tr>
		<td class="l_Cel_01_02">利用規約への同意</td>
		<td class="s_Cel_01" style="text-align:left;">
			同意します
		</td>
	</tr>
	<tr>
		<td colspan="3">
				<!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
				<!-- ここまでトップへ戻る -->
		</td>
	</tr>
</table>
</div>
<div class="mod_form_btn">
		<div style="margin-top:20px;margin-left:280px;">
			<form action="{$script}" name="f2" method="post" id="f2">
				<input type="hidden" name="action_secure_registration" value="true">
				<a class="button" href="javascript:void(0)" onclick="javascript:do_submit('back');" id="back_btn">戻る</a>
					<input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
					<input type="hidden" name="in_company_kana" id="in_company_kana" value="{$app.data.company_kana}">
					<input type="hidden" name="in_branch_name" id="in_branch_name" value="{$app.data.branch_name}">
					<input type="hidden" name="in_zip1" id="in_zip1" value="{$app.data.zip1}">
					<input type="hidden" name="in_zip2" id="in_zip2" value="{$app.data.zip2}">
					<input type="hidden" name="in_address" id="in_address" value="{$app.data.address}">
					<input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
					<input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}">
					<input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}">
					<input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}">
					<input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}">
					<input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}">
					<input type="hidden" name="in_fax_no1" id="in_fax_no1" value="{$app.data.fax_no1}">
					<input type="hidden" name="in_fax_no2" id="in_fax_no2" value="{$app.data.fax_no2}">
					<input type="hidden" name="in_fax_no3" id="in_fax_no3" value="{$app.data.fax_no3}">
					<input type="hidden" name="in_department_name" id="in_department_name" value="{$app.data.department_name}">
					<input type="hidden" name="in_post_name" id="in_post_name" value="{$app.data.post_name}">
					<input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="{$app.data.contractor_lname}">
					<input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="{$app.data.contractor_fname}">
					<input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="{$app.data.contractor_lkana}">
					<input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="{$app.data.contractor_fkana}">
					<input type="hidden" name="in_email" id="in_email" value="{$app.data.email}">
					<input type="hidden" name="in_email_2" id="in_email_2" value="{$app.data.email_2}">
					<input type="hidden" name="in_email_3" id="in_email_3" value="{$app.data.email_3}">
					<input type="hidden" name="in_email_4" id="in_email_4" value="{$app.data.email_4}">
					<input type="hidden" name="in_email_5" id="in_email_5" value="{$app.data.email_5}">
					<input type="hidden" name="in_account_count" id="in_account_count" value="{$app.data.account_count}">
					<input type="hidden" name="in_from" id="in_from" value="{$app.data.from}">
					<input type="hidden" name="in_mailmaga" id="in_mailmaga" value="{$app.data.mailmaga}">
					<input type="hidden" name="in_privacy" id="in_privacy" value="{$app.data.privacy}">

					<input type="hidden" name="in_check_payment" id="in_check_payment" value="{$app.data.check_payment}">
					<input type="hidden" name="in_payment_method" id="in_payment_method" value="{$app.data.payment_method}">
					<input type="hidden" name="in_begin_date" id="in_begin_date" value="{$app.data.begin_date}">
					<input type="hidden" name="in_pay_date" id="in_begin_date" value="{$app.data.pay_date}">

					<input type="hidden" name="in_field_90" id="in_field_90" value="{$app.data.field_90}">
					<input type="hidden" name="in_field_91" id="in_field_91" value="{$app.data.field_91}">
					<input type="hidden" name="in_field_92" id="in_field_92" value="{$app.data.field_92}">
					<input type="hidden" name="in_field_93" id="in_field_93" value="{$app.data.field_93}">
					<input type="hidden" name="in_field_94" id="in_field_94" value="{$app.data.field_94}">
					<input type="hidden" name="in_field_95" id="in_field_95" value="{$app.data.field_95}">
					<input type="hidden" name="in_field_96" id="in_field_96" value="{$app.data.field_96}">
					<input type="hidden" name="in_field_97" id="in_field_97" value="{$app.data.field_97}">
					<input type="hidden" name="in_field_98" id="in_field_98" value="{$app.data.field_98}">
					<input type="hidden" name="in_field_99" id="in_field_99" value="{$app.data.field_99}">
					<input type="hidden" name="in_field_100" id="in_field_100" value="{$app.data.field_100}">
					<input type="hidden" name="in_field_101" id="in_field_101" value="{$app.data.field_101}">

					<input type="hidden" name="in_field2_1" id="in_field2_1" value="{$app.data.field2_1}">
					<input type="hidden" name="in_field2_2" id="in_field2_2" value="{$app.data.field2_2}">
					<input type="hidden" name="in_field2_3" id="in_field2_3" value="{$app.data.field2_3}">
					<input type="hidden" name="in_field2_4" id="in_field2_4" value="{$app.data.field2_4}">
					<input type="hidden" name="in_field2_5" id="in_field2_5" value="{$app.data.field2_5}">
					<input type="hidden" name="in_field2_6" id="in_field2_6" value="{$app.data.field2_6}">
					<input type="hidden" name="in_field2_7" id="in_field2_7" value="{$app.data.field2_7}">
					<input type="hidden" name="in_field2_8" id="in_field2_8" value="{$app.data.field2_8}">
					<input type="hidden" name="in_field2_9" id="in_field2_9" value="{$app.data.field2_9}">
					<input type="hidden" name="in_field2_10" id="in_field2_10" value="{$app.data.field2_10}">
					<input type="hidden" name="in_field2_11" id="in_field2_11" value="{$app.data.field2_11}">

					<input type="hidden" name="in_field3_1" id="in_field3_1" value="{$app.data.field3_1}">
					<input type="hidden" name="in_field3_2" id="in_field3_2" value="{$app.data.field3_2}">
					<input type="hidden" name="in_field3_3" id="in_field3_3" value="{$app.data.field3_3}">
					<input type="hidden" name="in_field3_4" id="in_field3_4" value="{$app.data.field3_4}">
					<input type="hidden" name="in_field3_5" id="in_field3_5" value="{$app.data.field3_5}">
					<input type="hidden" name="in_field3_6" id="in_field3_6" value="{$app.data.field3_6}">
					<input type="hidden" name="in_field3_7" id="in_field3_7" value="{$app.data.field3_7}">
					<input type="hidden" name="in_field3_8" id="in_field3_8" value="{$app.data.field3_8}">
					<input type="hidden" name="in_field3_9" id="in_field3_9" value="{$app.data.field3_9}">
					<input type="hidden" name="in_field3_10" id="in_field3_10" value="{$app.data.field3_10}">
					<input type="hidden" name="in_field3_11" id="in_field3_11" value="{$app.data.field3_11}">
					<input type="hidden" name="in_field3_12" id="in_field3_12" value="{$app.data.field3_12}">
					<input type="hidden" name="in_field3_13" id="in_field3_13" value="{$app.data.field3_13}">
					<input type="hidden" name="in_field3_14" id="in_field3_14" value="{$app.data.field3_14}">
					<input type="hidden" name="in_field3_15" id="in_field3_15" value="{$app.data.field3_15}">

					<input type="hidden" name="back" id="back" value="0">
			</form>
			</div>
		<div style="margin-top:-46px;margin-left:500px;">
			<form action="{$script}" name="f3" method="post" id="f3">
				<input type="hidden" name="action_secure_complete" value="true">
				<a class="button2" href="javascript:void(0)" onclick="javascript:do_submit('send');" id="send_btn">送信</a>
					<input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
					<input type="hidden" name="in_company_kana" id="in_company_kana" value="{$app.data.company_kana}">
					<input type="hidden" name="in_branch_name" id="in_branch_name" value="{$app.data.branch_name}">
					<input type="hidden" name="in_zip1" id="in_zip1" value="{$app.data.zip1}">
					<input type="hidden" name="in_zip2" id="in_zip2" value="{$app.data.zip2}">
					<input type="hidden" name="in_address" id="in_address" value="{$app.data.address}">
					<input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
					<input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}">
					<input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}">
					<input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}">
					<input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}">
					<input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}">
					<input type="hidden" name="in_fax_no1" id="in_fax_no1" value="{$app.data.fax_no1}">
					<input type="hidden" name="in_fax_no2" id="in_fax_no2" value="{$app.data.fax_no2}">
					<input type="hidden" name="in_fax_no3" id="in_fax_no3" value="{$app.data.fax_no3}">
					<input type="hidden" name="in_department_name" id="in_department_name" value="{$app.data.department_name}">
					<input type="hidden" name="in_post_name" id="in_post_name" value="{$app.data.post_name}">
					<input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="{$app.data.contractor_lname}">
					<input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="{$app.data.contractor_fname}">
					<input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="{$app.data.contractor_lkana}">
					<input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="{$app.data.contractor_fkana}">
					<input type="hidden" name="in_email" id="in_email" value="{$app.data.email}">
					<input type="hidden" name="in_email_2" id="in_email_2" value="{$app.data.email_2}">
					<input type="hidden" name="in_email_3" id="in_email_3" value="{$app.data.email_3}">
					<input type="hidden" name="in_email_4" id="in_email_4" value="{$app.data.email_4}">
					<input type="hidden" name="in_email_5" id="in_email_5" value="{$app.data.email_5}">
					<input type="hidden" name="in_account_count" id="in_account_count" value="{$app.data.account_count}">
					<input type="hidden" name="in_from" id="in_from" value="{$app.data.from}">
					<input type="hidden" name="in_mailmaga" id="in_mailmaga" value="{$app.data.mailmaga}">
					<input type="hidden" name="in_privacy" id="in_privacy" value="{$app.data.privacy}">

					<input type="hidden" name="in_check_payment" id="in_check_payment" value="{$app.data.check_payment}">
					<input type="hidden" name="in_payment_method" id="in_payment_method" value="{$app.data.payment_method}">
					<input type="hidden" name="in_begin_date" id="in_begin_date" value="{$app.data.begin_date}">
					<input type="hidden" name="in_pay_date" id="in_begin_date" value="{$app.data.pay_date}">

					<input type="hidden" name="in_field_90" id="in_field_90" value="{$app.data.field_90}">
					<input type="hidden" name="in_field_91" id="in_field_91" value="{$app.data.field_91}">
					<input type="hidden" name="in_field_92" id="in_field_92" value="{$app.data.field_92}">
					<input type="hidden" name="in_field_93" id="in_field_93" value="{$app.data.field_93}">
					<input type="hidden" name="in_field_94" id="in_field_94" value="{$app.data.field_94}">
					<input type="hidden" name="in_field_95" id="in_field_95" value="{$app.data.field_95}">
					<input type="hidden" name="in_field_96" id="in_field_96" value="{$app.data.field_96}">
					<input type="hidden" name="in_field_97" id="in_field_97" value="{$app.data.field_97}">
					<input type="hidden" name="in_field_98" id="in_field_98" value="{$app.data.field_98}">
					<input type="hidden" name="in_field_99" id="in_field_99" value="{$app.data.field_99}">
					<input type="hidden" name="in_field_100" id="in_field_100" value="{$app.data.field_100}">
					<input type="hidden" name="in_field_101" id="in_field_101" value="{$app.data.field_101}">

					<input type="hidden" name="in_field2_1" id="in_field2_1" value="{$app.data.field2_1}">
					<input type="hidden" name="in_field2_2" id="in_field2_2" value="{$app.data.field2_2}">
					<input type="hidden" name="in_field2_3" id="in_field2_3" value="{$app.data.field2_3}">
					<input type="hidden" name="in_field2_4" id="in_field2_4" value="{$app.data.field2_4}">
					<input type="hidden" name="in_field2_5" id="in_field2_5" value="{$app.data.field2_5}">
					<input type="hidden" name="in_field2_6" id="in_field2_6" value="{$app.data.field2_6}">
					<input type="hidden" name="in_field2_7" id="in_field2_7" value="{$app.data.field2_7}">
					<input type="hidden" name="in_field2_8" id="in_field2_8" value="{$app.data.field2_8}">
					<input type="hidden" name="in_field2_9" id="in_field2_9" value="{$app.data.field2_9}">
					<input type="hidden" name="in_field2_10" id="in_field2_10" value="{$app.data.field2_10}">
					<input type="hidden" name="in_field2_11" id="in_field2_11" value="{$app.data.field2_11}">

					<input type="hidden" name="in_field3_1" id="in_field3_1" value="{$app.data.field3_1}">
					<input type="hidden" name="in_field3_2" id="in_field3_2" value="{$app.data.field3_2}">
					<input type="hidden" name="in_field3_3" id="in_field3_3" value="{$app.data.field3_3}">
					<input type="hidden" name="in_field3_4" id="in_field3_4" value="{$app.data.field3_4}">
					<input type="hidden" name="in_field3_5" id="in_field3_5" value="{$app.data.field3_5}">
					<input type="hidden" name="in_field3_6" id="in_field3_6" value="{$app.data.field3_6}">
					<input type="hidden" name="in_field3_7" id="in_field3_7" value="{$app.data.field3_7}">
					<input type="hidden" name="in_field3_8" id="in_field3_8" value="{$app.data.field3_8}">
					<input type="hidden" name="in_field3_9" id="in_field3_9" value="{$app.data.field3_9}">
					<input type="hidden" name="in_field3_10" id="in_field3_10" value="{$app.data.field3_10}">
					<input type="hidden" name="in_field3_11" id="in_field3_11" value="{$app.data.field3_11}">
					<input type="hidden" name="in_field3_12" id="in_field3_12" value="{$app.data.field3_12}">
					<input type="hidden" name="in_field3_13" id="in_field3_13" value="{$app.data.field3_13}">
					<input type="hidden" name="in_field3_14" id="in_field3_14" value="{$app.data.field3_14}">
					<input type="hidden" name="in_field3_15" id="in_field3_15" value="{$app.data.field3_15}">

					<input type="hidden" name="back" id="back" value="0">
			</form>
		</div>
	</div>
</div>

<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>
<!-- スムーズスクロール -->

{/literal}

{include file='common/track.tpl'}

</body>
</html>
