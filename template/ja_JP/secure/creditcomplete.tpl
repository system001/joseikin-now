<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Content-Style-Type" content="text/css"/>
<meta http-equiv="Content-Script-Type" content="text/javascript"/>
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ"/>
<meta name="author" content="株式会社ナビット"/>
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件"/>
<link rel="stylesheet" href="css/default.css" type="text/css"/>
<link rel="shortcut icon" href=""/>
</head>
<body>

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
	<div id="wrapper">

	{include file='common/header.tpl'}
	{include file='common/navi.tpl'}

<!-- ここからメインコンテンツ -->

	{if $app.result === '0' }
		<div class="credit_cont">
			<img style="margin:30px auto 0;" src="img/creditcomp_title.jpg" alt="クレジットカード決済完了" />
			<p class="credit_title">クレジットカード決済が完了致しました。</p>

			<div class="credit_deta">
				決済金額:{$app.amount}<br />
				伝票番号:{$app.charge}<br />
				メールアドレス:{$app.mail}<br /><br />
			</div>

			ご登録頂きましたメールアドレスへ手続完了のお知らせメールをお送り致しましたので、ご確認下さい。
			お送りしたメールにログインIDとパスワードの記載がございますが、有料会員機能につきましては、ご入金確認後、お申込み頂きました「開始希望日」からご利用頂けます。<br /><br />
			※無料会員機能は、ご利用可能です。

		</div>

		<div class="btn_top">
			<a href="https://www.navit-j.com/service/joseikin-now/">
				<img style="margin:30px auto 0;" src="img/btn_top.png" alt="TOPへ戻る" />
			</a>
		</div>

	{else}

		<div class="credit_cont">
			<img style="margin:30px auto 0;" src="img/crediterror_title.jpg" alt="クレジット決済失敗" />

			<p class="credit_title">クレジットカード決済に失敗しました。<br />
			<span style="font-size:20px; color:#000;">クレジットカード番号などをお確かめの上、再度、処理を行なってください。</span></p>


			<div class="credit_deta">
				決済金額:{$app.amount}<br />
				伝票番号:{$app.charge}<br />
				メールアドレス:{$app.mail}<br /><br />
			</div>

			※<span style="font-weight:bold; color:#F00;">無料会員として登録済</span>になりますので、メールでお知らせしたID・PWでログイン後、マイページに表示されている「有料会員に申込む」ボタンより再度ご登録をお願い致します。

			また、ネット接続や決済システムの一時的なエラーも可能性も考えられます。<br />
			その際には、お手数をおかけいたしますが、しばらくお時間をおいてから再度ご注文をお願いいたします。

			<p class="credit_text">※決済失敗に関する例</p>
			・カード番号入力に誤りがある<br />
			・カードの有効期限が過ぎている<br />
			・カード名義入力に誤りがある（ローマ字ではなく漢字で入力されているなどを含む）<br />
			・有効期限の年と月を逆に入力している<br />
			・セキュリティコードの入力に誤りがある<br />
			・「助成金なう」で利用できないブランドのカードをご利用されている<br />
			・利用限度額を超えている（総額ではなく個別注文でも限度額がある場合がございます）

			<p class="credit_text">※その他注意事項</p>
			エラーの詳細について細かい表示はされませんのでご了承ください。<br />
			クレジットカードのご利用状況に関しては、秘匿性が高いため、当方で詳細をお調べすることが出来ませんのでご了承ください。<br />
			クレジットカードのご利用状況に関しては、ご利用の各クレジットカード会社にお問合せいただけますようお願いいたします。
		</div>

	<div class="btn_top">
		<a href="https://www.navit-j.com/service/joseikin-now/">
			<img style="margin:30px auto 0;" src="img/btn_top.png" alt="TOPへ戻る" />
		</a>
	</div>

	{/if}

<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">
	<a href="http://navit-j.com/">運営会社</a>　｜　
	<a href="/terms/">利用規約</a>　｜　
	<a href="http://www.navit-j.com/privacy/index.html">Pマークについて</a>　｜　
	<a href="/law/">特定商取引法に基づく表記</a>　｜　
	<a href="https://www.navit-j.com/contactus/">問い合わせフォーム</a>
</div>

<!-- ここまでfooter -->


{include file='common/track.tpl'}


</div>
<!-- ここまでwrapper -->
</div>
<!-- ここまでconteinar -->

<!-- googleanalytics -->

<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-45128510-12', 'auto');
ga('send', 'pageview');
</script>


<!-- LF innov -->

<script type="text/javascript">
var _trackingid = 'LFT-10394-1';
(function() {
  var lft = document.createElement('script'); lft.type = 'text/javascript'; lft.async = true;
  lft.src = document.location.protocol + '//track.list-finder.jp/js/ja/track.js';
  var snode = document.getElementsByTagName('script')[0]; snode.parentNode.insertBefore(lft, snode);
})();
</script>

</body>
</html>
