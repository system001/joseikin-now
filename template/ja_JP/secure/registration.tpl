<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<style>
.state_payment_open {
    background: url("img/jyo_paybtn_d.jpg") no-repeat;
}

.state_payment_open a {
    height: 60px;
    background: url("img/jyo_paybtn_d.jpg") no-repeat;
    display: block;
    text-indent: -9999px;
}

.state_payment_open a:hover {
    background-image: url("img/jyo_paybtn.jpg");
}

.state_payment_closed {
    background: url("img/jyo_paybtn.jpg") no-repeat;
}

.state_payment_closed a {
    height: 60px;
    background: url("img/jyo_paybtn.jpg") no-repeat;
    display: block;
    text-indent: -9999px;
}

.state_payment_closed a:hover {
    background-image: url("img/jyo_paybtn_d.jpg");
}
input::-webkit-input-placeholder {
	color : #88f;
}
input:-ms-input-placeholder {
	color : #88f;
}
input::-moz-placeholder {
	color : #88f;
}
div.ui-datepicker {
	font-size: 70%;
}


/*
.img_checkbox li {
    position: relative;
    display: inline-block;
    margin: 0 32px 0 0;
    padding: 0;
}

.img_checkbox input {
    position: absolute;
    top: 0;
    opacity: 0;
    width: 100%;
    height: 100%;
}

.img_checkbox input[type="checkbox"] + label {
    display: block;
    background-image: url(img/jyo_paybtn_d.jpg);
    background-size: 600px;
    background-position: left center;
    background-repeat: no-repeat;
    padding: 4px 0 0 28px;
    width:600px;
    height:50px;
    margin-left:60px;
}

.img_checkbox input[type="checkbox"]:checked + label {
    background-image: url(img/jyo_paybtn.jpg);
}
*/


</style>
<link rel="stylesheet" href="js/jquery-ui/jquery-ui.css" type="text/css">
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript">

//吹き出し
$(window).load(function(){
	$('label').balloon();


	if ($('#privacy_1').is(':checked')) {
		$('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
	} else {
		$('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
	}

	showEMailFields();

	$( "#in_account_count" ).change( showEMailFields );

	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#in_pay_date" ).datepicker({
		beforeShow: function(input, inst) {
			var calendar = inst.dpDiv;
			setTimeout(function() {
				calendar.position({
					my: 'left bottom',
					at: 'left top',
					of: input
				});
			}, 1);
		}
	});
	$( "#in_begin_date" ).datepicker({
		beforeShow: function(input, inst) {
			var calendar = inst.dpDiv;
			setTimeout(function() {
				calendar.position({
					my: 'left bottom',
					at: 'left top',
					of: input
				});
			}, 1);
		}
	});
});



function doblur() {
	var element = document.getElementById("name");
	element.blur();
}

function do_submit(){

	if ($('#privacy_1').is(':checked')) {
		document.f2.submit();
	}else{
		return false;
	}
}

/*
 　　全角->半角変換
 */
jQuery(function(){

	// 郵便番号の処理
	$('.zip-number').change( function(){
		var data = $(this).val();
		var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});

		// 半角数字のみ残す
		var zenkakuDel = new String( hankaku ).match(/\d/g);
		if(zenkakuDel){
			zenkakuDel = zenkakuDel.join("");
		}else {
			zenkakuDel = "";
		}

		$(this).val(zenkakuDel);
	});
	// 電話番号の処理
	$('.tel-number').change( function(){
		var data = $(this).val();
		var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});

		// 半角数字と+-のみ残す
		var zenkakuDel = new String( hankaku ).match(/\d|\-|\+/g);
		if(zenkakuDel){
			zenkakuDel = zenkakuDel.join("");
		}else {
			zenkakuDel = "";
		}

		$(this).val(zenkakuDel);
	});

	// メールアドレスの処理
	$('.mail-address').change( function(){
		var zenkigou = "＠－ー＋＿．，、";
		var hankigou = "@--+_...";
		var data = $(this).val();
		var str = "";

		// 指定された全角記号のみを半角に変換
		for (i=0; i<data.length; i++)
		{
			var dataChar = data.charAt(i);
			var dataNum = zenkigou.indexOf(dataChar,0);
			if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
			str += dataChar;
		}
		// 定番の、アルファベットと数字の変換処理
		var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
		$(this).val(hankaku);
	});

});

function showEMailFields()
{
	var count = $( "#in_account_count" ).val();

	for ( var i = 2 ; i <= 5 ; ++ i )
	{
		if ( i <= count )
		{
			$( "#email_" + i + "_area" ).css( 'display', '' );
		}
		else
		{
			$( "#email_" + i + "_area" ).css( 'display', 'none' );
		}
	}
	$( "#price" ).text( number_format( 12960 * count ) );
}
function number_format( num )
{
	if ( num )
	{
		num += "";
		num  = num.toString().replace(/([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,');
	}
	return num;
}


//確認ボタンの有効無効

jQuery(function(){
	$('#privacy_1').change(function(){
		if ($(this).is(':checked')) {
			$('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
		} else {
			$('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
		}
	});
});


</script>
{/literal}
</head>
<body>
<form action="{$script}" name="f2" method="post" id="f2">
<input type="hidden" name="action_secure_confirm" value="true">
<!-- ここからconteinar -->
<div id="conteinar">
<!-- ここからwrapper -->
	<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->


<!-- ここから入力フォーム --><div id="03"></div>

<br />
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">新規会員登録</span></div>
<div style="margin-left: 120px;">下記フォームに必要事項を入力後、確認ボタンを押してください。<br /></div>
<div style="margin-left: 120px;">ご登録頂いたメールアドレスにはナビットからのお役立ち情報を送付させて頂きます。<br /></div>
<div style="font-size:0.7em;margin-left: 120px;">
	<span>
	【注意事項】<br />
	<span style="color:#64c601;font-size:16px;">■</span>ログインID・パスワードはご登録頂いたメールアドレスにご連絡致します。<br />
	<span style="color:#64c601;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
	<span style="color:red;font-size:16px;">[重要]</span> 無料検索サービスにご登録いただくとナビット発行のメールマガジンにも自動登録されます。<br />
	メールマガジンの配信停止処理を行うと、助成金なうの無料検索がご利用できなくなりますので予めご了承ください。
	</span>
</div>
<br />


<div style="margin-left: 120px;"><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><span style="font-size:80%;"> は必須入力項目です</span></div>
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
	<tr>
		<td class="l_Cel_01_01">会員種別　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<label><input
				type ="radio"
				name ="in_check_payment"
				value="1"
				id   ="cmd_check_payment_open"
				{if $app.data.check_payment == 1} checked{/if}/>有料会員</label>
			<label><input
				type ="radio"
				name ="in_check_payment"
				value="2"
				id   ="cmd_check_payment_close"
				{if $app.data.check_payment != 1} checked{/if}/>無料会員</label><br />


			<a href="index.php?action_static=service_model_03" target="_blank">有料会員とは？</a>
		</td>
	</tr>

	<tr>
		<td class="l_Cel_01_01">会社名　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td class="s_Cel">
			<input
				type ="text"
				id   ="in_company_name"
				name ="in_company_name"
				value="{$app.data.company_name}"
				style="font-size:16px;"
				size ="50"
				maxlength  ="50"
				placeholder="株式会社ナビット"/>
			{if is_error('in_company_name')}<br /><span class="error" style="color:red;">{message name="in_company_name"}</span>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">会社名かな　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<input
				type="text"
				id   ="in_company_kana"
				name ="in_company_kana"
				value="{$app.data.company_kana}"
				style="font-size:16px;"
				size ="50"
				maxlength  ="50"
				placeholder="かぶしきがいしゃなびっと"/>
			{if is_error('in_company_kana')}<div class="error" style="color:red;">{message name="in_company_kana"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">支店名</td>
		<td width="550" class="s_Cel">
			<input
				type ="text"
				id   ="in_branch_name"
				name ="in_branch_name"
				value="{$app.data.branch_name}"
				style="font-size:16px;"
				size ="50"
				maxlength  ="50"
				placeholder="東京支店"/>
			{if is_error('in_branch_name')}<div class="error" style="color:red;">{message name="in_branch_name"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">郵便番号　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）</td>
		<td width="550" class="s_Cel">
			<input
				type ="text"
				id   ="in_zip1"
				name ="in_zip1"
				value="{$app.data.zip1}"
				class="zip-number"
				style="font-size:16px;width:100px;"
				size ="10"
				maxlength  ="3"
				onKeyUp    ="AjaxZip3.zip2addr('in_zip1','in_zip2','in_address','in_address','in_address');"
				placeholder="102"
			/>－<input
				type ="text"
				id   ="in_zip2"
				name ="in_zip2"
				value="{$app.data.zip2}"
				size ="10"
				class="zip-number"
				style="font-size:16px;width:100px;"
				maxlength  ="4"
				onKeyUp    ="AjaxZip3.zip2addr('in_zip1','in_zip2','in_address','in_address','in_address');"
				placeholder="0074"/>
			{if is_error('in_zip1')}<div class="error" style="color:red;">{message name="in_zip1"}</div>{/if}
			{if is_error('in_zip2')}<div class="error" style="color:red;">{message name="in_zip2"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">住所　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<input
				type ="text"
				id   ="in_address"
				name ="in_address"
				value="{$app.data.address}"
				style="font-size:16px;width:560px;"
				size ="100"
				maxlength  ="100"
				placeholder="東京都千代田区九段南1-5-5　Daiwa九段ビル8F"/>
			{if is_error('in_address')}<div class="error" style="color:red;">{message name="in_address"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">電話番号　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）</td>
		<td width="550" class="s_Cel">
			<input
				type ="text"
				id   ="in_phone_no1"
				name ="in_phone_no1"
				value="{$app.data.phone_no1}"
				class="tel-number"
				style="font-size:16px;width:100px;"
				size ="10"
				maxlength  ="5"
				placeholder="03"/>
			－
			<input
				type ="text"
				id   ="in_phone_no2"
				name ="in_phone_no2"
				value="{$app.data.phone_no2}"
				class="tel-number"
				style="font-size:16px;width:100px;"
				size ="10"
				maxlength  ="4"
				placeholder="5215"/>
			－
			<input
				type ="text"
				id   ="in_phone_no3"
				name ="in_phone_no3"
				value="{$app.data.phone_no3}"
				class="tel-number"
				style="font-size:16px;width:100px;"
				size ="10"
				maxlength  ="4"
				placeholder="5701"/>
			{if is_error('in_phone_no1')}<div class="error" style="color:red;">{message name="in_phone_no1"}</div>{/if}
			{if is_error('in_phone_no2')}<div class="error" style="color:red;">{message name="in_phone_no2"}</div>{/if}
			{if is_error('in_phone_no3')}<div class="error" style="color:red;">{message name="in_phone_no3"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">携帯電話番号<br />（半角数字）</td>
		<td width="550" class="s_Cel">
			<input
				type ="text"
				id   ="in_k_phone_no1"
				name ="in_k_phone_no1"
				value="{$app.data.k_phone_no1}"
				size ="10"
				class="tel-number"
				style="font-size:16px;width:100px;"
				maxlength="5"
				placeholder="090"/>
			－
			<input
				type ="text"
				id   ="in_k_phone_no2"
				name ="in_k_phone_no2"
				value="{$app.data.k_phone_no2}"
				size ="10"
				class="tel-number"
				style="font-size:16px;width:100px;"
				maxlength  ="5"
				placeholder="1234" />
			－
			<input
				type ="text"
				id   ="in_k_phone_no3"
				name ="in_k_phone_no3"
				value="{$app.data.k_phone_no3}"
				class="tel-number"
				style="font-size:16px;width:100px;"
				size ="10"
				maxlength  ="5"
				placeholder="5789" />
			{if is_error('in_k_phone_no1')}<div class="error" style="color:red;">{message name="in_k_phone_no1"}</div>{/if}
			{if is_error('in_k_phone_no2')}<div class="error" style="color:red;">{message name="in_k_phone_no2"}</div>{/if}
			{if is_error('in_k_phone_no3')}<div class="error" style="color:red;">{message name="in_k_phone_no3"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">FAX番号<br />（半角数字）</td>
		<td width="550" class="s_Cel">
			<input
				type ="text"
				id   ="in_fax_no1"
				name ="in_fax_no1"
				value="{$app.data.fax_no1}"
				class="tel-number"
				style="font-size:16px;width:100px;"
				size ="10"
				maxlength="5"
				placeholder="03"/>
			－
			<input
				type ="text"
				id   ="in_fax_no2"
				name ="in_fax_no2"
				value="{$app.data.fax_no2}"
				class="tel-number"
				style="font-size:16px;width:100px;"
				size ="10"
				maxlength ="5"
				placeholder="5215"/>
			－
			<input
				type ="text"
				id   ="in_fax_no3"
				name ="in_fax_no3"
				value="{$app.data.fax_no3}"
				class="tel-number"
				style="font-size:16px;width:100px;"
				size ="10"
				maxlength  ="5"
				placeholder="3020"/>
			{if is_error('in_fax_no1')}<div class="error" style="color:red;">{message name="in_fax_no1"}</div>{/if}
			{if is_error('in_fax_no2')}<div class="error" style="color:red;">{message name="in_fax_no2"}</div>{/if}
			{if is_error('in_fax_no3')}<div class="error" style="color:red;">{message name="in_fax_no3"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">部署名</td>
		<td width="550" class="s_Cel">
			<input
				type ="text"
				id   ="in_department_name"
				name ="in_department_name"
				value="{$app.data.department_name}"
				style="font-size:16px;"
				size ="50"
				maxlength  ="50"
				placeholder="マーケティング事業部"/>
			{if is_error('in_department_name')}<div class="error" style="color:red;">{message name="in_department_name"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">役職名</td>
		<td width="550" class="s_Cel">
			<input
				type ="text"
				id   ="in_post_name"
				name ="in_post_name"
				value="{$app.data.post_name}"
				style="font-size:16px;"
				size ="50"
				maxlength="50"
				placeholder="事業部長"/>
			{if is_error('in_post_name')}<div class="error" style="color:red;">{message name="in_post_name"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">担当者名　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<span>姓&nbsp;&nbsp;&nbsp;&nbsp;<input
				type ="text"
				id   ="in_contractor_lname"
				name ="in_contractor_lname"
				value="{$app.data.contractor_lname}"
				style="font-size:16px;width:200px;"
				size ="25"
				maxlength="50"
				/></span>&nbsp;&nbsp;
			<span>名&nbsp;&nbsp;&nbsp;&nbsp;<input
				type ="text"
				id   ="in_contractor_fname"
				name ="in_contractor_fname"
				value="{$app.data.contractor_fname}"
				style="font-size:16px;width:200px;"
				size ="25"
				maxlength="50"
				/></span>
			{if is_error('in_contractor_lname')}<div class="error" style="color:red;">{message name="in_contractor_lname"}</div>{/if}
			{if is_error('in_contractor_fname')}<div class="error" style="color:red;">{message name="in_contractor_fname"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">担当者名かな　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<span>せい&nbsp;<input
				type ="text"
				id   ="in_contractor_lkana"
				name ="in_contractor_lkana"
				value="{$app.data.contractor_lkana}"
				style="font-size:16px;width:200px;"
				size ="25"
				maxlength="50"
				/></span>&nbsp;&nbsp;
			<span>めい&nbsp;<input
				type ="text"
				id   ="in_contractor_fkana"
				name ="in_contractor_fkana"
				value="{$app.data.contractor_fkana}"
				style="font-size:16px;width:200px;"
				size ="25"
				maxlength="50"
				/></span>
			{if is_error('in_contractor_lkana')}<div class="error" style="color:red;">{message name="in_contractor_lkana"}</div>{/if}
			{if is_error('in_contractor_fkana')}<div class="error" style="color:red;">{message name="in_contractor_fkana"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">メールアドレス　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<input
				type ="text"
				id   ="in_email"
				name ="in_email"
				value="{$app.data.email}"
				style="font-size:16px;"
				size ="50"
				maxlength="50"
				/>
			{if is_error('in_email')}<div class="error" style="color:red;">{message name="in_email"}</div>{/if}
		</td>
	</tr>


	<tr>
		<td class="l_Cel_01_01">サイトを知ったきっかけ　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<select id='in_from' name="in_from" class="select_font_s">{$app_ne.pulldown.from}</select>
			{if is_error('in_from')}<div class="error" style="color:red;">{message name="in_from"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">支援を受けたい分野1<br />(自治体案件カテゴリ)<br /><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<table border="0" style="text-align:left;">
				<tr>
					<td style="width:240px;"><label><input class="CheckList" type="checkbox" name="in_field_90" id="in_field_90" value="9000" {$app.data.field_90} />{$app.master_field.9000}</label></td>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_91" id="in_field_91" value="9100" {$app.data.field_91} />{$app.master_field.9100}</label></td>
				</tr>
				<tr>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_92" id="in_field_92" value="9200" {$app.data.field_92} />{$app.master_field.9200}</label></td>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_93" id="in_field_93" value="9300" {$app.data.field_93} />{$app.master_field.9300}</label></td>
				</tr>
				<tr>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_94" id="in_field_94" value="9400" {$app.data.field_94} />{$app.master_field.9400}</label></td>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_95" id="in_field_95" value="9500" {$app.data.field_95} />{$app.master_field.9500}</label></td>
				</tr>
				<tr>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_96" id="in_field_96" value="9600" {$app.data.field_96} />{$app.master_field.9600}</label></td>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_97" id="in_field_97" value="9700" {$app.data.field_97} />{$app.master_field.9700}</label></td>
				</tr>
				<tr>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_98" id="in_field_98" value="9800" {$app.data.field_98} />{$app.master_field.9800}</label></td>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_99" id="in_field_99" value="9900" {$app.data.field_99} />{$app.master_field.9900}</label></td>
				</tr>
				<tr>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_100" id="in_field_100" value="10000" {$app.data.field_100} />{$app.master_field.10000}</label></td>
					<td><label><input  class="CheckList" type="checkbox" name="in_field_101" id="in_field_101" value="10100" {$app.data.field_101} />{$app.master_field.10100}</label></td>
				</tr>
			</table>
				{if is_error('in_field')}<br /><span class="error">{message name="in_field"}</span>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">支援を受けたい分野2<br />(財団法人案件カテゴリ)<br /><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<table	border="0" style="text-align:left;">
				<tr>
					<td style="width:240px;"><label title="{$app.master_field2_desc.1000}"><input class="CheckList2" type="checkbox" name="in_field2_1" id="in_field2_1" value="1000" {$app.data.field2_1} />{$app.master_field2.1000}</label></td>
					<td><label title="{$app.master_field2_desc.2000}"><input  class="CheckList2" type="checkbox" name="in_field2_2" id="in_field2_2" value="2000" {$app.data.field2_2} />{$app.master_field2.2000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field2_desc.3000}"><input  class="CheckList2" type="checkbox" name="in_field2_3" id="in_field2_3" value="3000" {$app.data.field2_3} />{$app.master_field2.3000}</label></td>
					<td><label title="{$app.master_field2_desc.4000}"><input  class="CheckList2" type="checkbox" name="in_field2_4" id="in_field2_4" value="4000" {$app.data.field2_4} />{$app.master_field2.4000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field2_desc.5000}"><input  class="CheckList2" type="checkbox" name="in_field2_5" id="in_field2_5" value="5000" {$app.data.field2_5} />{$app.master_field2.5000}</label></td>
					<td><label title="{$app.master_field2_desc.6000}"><input  class="CheckList2" type="checkbox" name="in_field2_6" id="in_field2_6" value="6000" {$app.data.field2_6} />{$app.master_field2.6000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field2_desc.7000}"><input  class="CheckList2" type="checkbox" name="in_field2_7" id="in_field2_7" value="7000" {$app.data.field2_7} />{$app.master_field2.7000}</label></td>
					<td><label title="{$app.master_field2_desc.8000}"><input  class="CheckList2" type="checkbox" name="in_field2_8" id="in_field2_8" value="8000" {$app.data.field2_8} />{$app.master_field2.8000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field2_desc.9000}"><input  class="CheckList2" type="checkbox" name="in_field2_9" id="in_field2_9" value="9000" {$app.data.field2_9} />{$app.master_field2.9000}</label></td>
					<td><label title="{$app.master_field2_desc.10000}"><input class="CheckList2" type="checkbox" name="in_field2_10" id="in_field2_10" value="10000" {$app.data.field2_10} />{$app.master_field2.10000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field2_desc.11000}"><input class="CheckList2" type="checkbox" name="in_field2_11" id="in_field2_11" value="11000" {$app.data.field2_11} />{$app.master_field2.11000}</label></td>
				</tr>
			</table>
				{if is_error('in_field2')}<br /><span class="error">{message name="in_field2"}</span>{/if}
		</td>
	</tr>
	 <tr>
		<td class="l_Cel_01_01">支援を受けたい事業の<br />形態<br /><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<table	border="0" style="text-align:left;">
				<tr>
					<td style="width:240px;"><label title="{$app.master_field3_desc.1000}"><input class="CheckList3" type="checkbox" name="in_field3_1" id="in_field3_1" value="1000" {$app.data.field3_1} />{$app.master_field3.1000}</label></td>
					<td><label title="{$app.master_field3_desc.2000}"><input  class="CheckList3" type="checkbox" name="in_field3_2" id="in_field3_2" value="2000" {$app.data.field3_2} />{$app.master_field3.2000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field3_desc.3000}"><input  class="CheckList3" type="checkbox" name="in_field3_3" id="in_field3_3" value="3000" {$app.data.field3_3} />{$app.master_field3.3000}</label></td>
					<td><label title="{$app.master_field3_desc.4000}"><input  class="CheckList3" type="checkbox" name="in_field3_4" id="in_field3_4" value="4000" {$app.data.field3_4} />{$app.master_field3.4000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field3_desc.5000}"><input  class="CheckList3" type="checkbox" name="in_field3_5" id="in_field3_5" value="5000" {$app.data.field3_5} />{$app.master_field3.5000}</label></td>
					<td><label title="{$app.master_field3_desc.6000}"><input  class="CheckList3" type="checkbox" name="in_field3_6" id="in_field3_6" value="6000" {$app.data.field3_6} />{$app.master_field3.6000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field3_desc.7000}"><input  class="CheckList3" type="checkbox" name="in_field3_7" id="in_field3_7" value="7000" {$app.data.field3_7} />{$app.master_field3.7000}</label></td>
					<td><label title="{$app.master_field3_desc.8000}"><input  class="CheckList3" type="checkbox" name="in_field3_8" id="in_field3_8" value="8000" {$app.data.field3_8} />{$app.master_field3.8000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field3_desc.9000}"><input  class="CheckList3" type="checkbox" name="in_field3_9" id="in_field3_9" value="9000" {$app.data.field3_9} />{$app.master_field3.9000}</label></td>
					<td><label title="{$app.master_field3_desc.10000}"><input class="CheckList3" type="checkbox" name="in_field3_10" id="in_field3_10" value="10000" {$app.data.field3_10} />{$app.master_field3.10000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field3_desc.11000}"><input class="CheckList3" type="checkbox" name="in_field3_11" id="in_field3_11" value="11000" {$app.data.field3_11} />{$app.master_field3.11000}</label></td>
					<td><label title="{$app.master_field3_desc.12000}"><input class="CheckList3" type="checkbox" name="in_field3_12" id="in_field3_12" value="12000" {$app.data.field3_12} />{$app.master_field3.12000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field3_desc.13000}"><input class="CheckList3" type="checkbox" name="in_field3_13" id="in_field3_13" value="13000" {$app.data.field3_13} />{$app.master_field3.13000}</label></td>
					<td><label title="{$app.master_field3_desc.14000}"><input class="CheckList3" type="checkbox" name="in_field3_14" id="in_field3_14" value="14000" {$app.data.field3_14} />{$app.master_field3.14000}</label></td>
				</tr>
				<tr>
					<td><label title="{$app.master_field3_desc.15000}"><input class="CheckList3" type="checkbox" name="in_field3_15" id="in_field3_15" value="15000" {$app.data.field3_15} />{$app.master_field3.15000}</label></td>
				</tr>
			</table>
				{if is_error('in_field3')}<br /><span class="error">{message name="in_field3"}</span>{/if}
		</td>
	</tr>

	<tr class="payment_params"{if $app.data.check_payment != 1} style="display:none;"{/if}>
		<td class="l_Cel_01_01">お申込み人数</td>
		<td width="550" class="s_Cel">
			<select id="in_account_count" name="in_account_count">
				<option value="1" {if $app.data.account_count==1}selected{/if}>1名</option>
				<option value="2" {if $app.data.account_count==2}selected{/if}>2名</option>
				<option value="3" {if $app.data.account_count==3}selected{/if}>3名</option>
				<option value="4" {if $app.data.account_count==4}selected{/if}>4名</option>
				<option value="5" {if $app.data.account_count==5}selected{/if}>5名</option>
			</select>
			<br />
			※複数人でご利用の場合には、お申込者様以外の人数分メールアドレスの入力をお願い致します。<br />
			※IDとパスワードはそれぞれのメールアドレスに送信されます。<br />

		</td>
	</tr>
	<tr id="email_2_area" class="payment_params"{if $app.data.check_payment != 1} style="display:none;"{/if}>
		<td class="l_Cel_01_01">メールアドレス2人目　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<input type="text" id="in_email_2" name="in_email_2" value="{$app.data.email_2}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_email_2')}<div class="error" style="color:red;">{message name="in_email_2"}</div>{/if}
		</td>
	</tr>
	<tr id="email_3_area" class="payment_params"{if $app.data.check_payment != 1} style="display:none;"{/if}>
		<td class="l_Cel_01_01">メールアドレス3人目　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<input type="text" id="in_email_3" name="in_email_3" value="{$app.data.email_3}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_email_3')}<div class="error" style="color:red;">{message name="in_email_3"}</div>{/if}
		</td>
	</tr>
	<tr id="email_4_area" class="payment_params"{if $app.data.check_payment != 1} style="display:none;"{/if}>
		<td class="l_Cel_01_01">メールアドレス4人目　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<input type="text" id="in_email_4" name="in_email_4" value="{$app.data.email_4}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_email_4')}<div class="error" style="color:red;">{message name="in_email_4"}</div>{/if}
		</td>
	</tr>
	<tr id="email_5_area" class="payment_params"{if $app.data.check_payment != 1} style="display:none;"{/if}>
		<td class="l_Cel_01_01">メールアドレス5人目　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<input type="text" id="in_email_5" name="in_email_5" value="{$app.data.email_5}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_email_5')}<div class="error" style="color:red;">{message name="in_email_5"}</div>{/if}
		</td>
	</tr>

	<tr class="payment_params"{if $app.data.check_payment != 1} style="display:none;"{/if}>
		<td class="l_Cel_01_01">お支払い方法<br /><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<label><input type="radio" name="in_payment_method" value="1" {if $app.data.payment_method == 1} checked{/if}/>クレジットカード決済</label>
			<label><input type="radio" name="in_payment_method" value="0" {if $app.data.payment_method != 1} checked{/if}/>銀行振込</label>
		</td>
	</tr>

	<tr class="payment_params"{if $app.data.check_payment != 1} style="display:none;"{/if}>
		<td class="l_Cel_01_01">ご利用期間/料金<br /></td>
		<td width="550" class="s_Cel">
		12ヶ月 \<span id="price">12,960</span>(税込)
		</td>
	</tr>

	<tr class="payment_params"{if $app.data.check_payment != 1} style="display:none;"{/if}>
		<td class="l_Cel_01_01">入金予定日<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">例：2015年04月01日<span style="font-size:0.8em;">(クレジットカード決済の場合は即日となります)</span><br />
			<input style="width:20em;"  type="text" id="in_pay_date" name="in_pay_date" value="{$app.data.pay_date}" size="20" maxlength="20"  style="font-size:16px;" />
			{if is_error('in_pay_date')}<div class="error" style="color:red;">{message name="in_pay_date"}</div>{/if}
		</td>
	</tr>

	<tr class="payment_params"{if $app.data.check_payment != 1} style="display:none;"{/if}>
		<td class="l_Cel_01_01">開始希望日<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">例：2015年04月03日<span style="font-size:0.8em;">(入金予定日から２営業日以降を指定してください)</span><br />
			<input style="width:20em;"  type="text" id="in_begin_date" name="in_begin_date" value="{$app.data.begin_date}" size="20" maxlength="20"  style="font-size:16px;" />
			{if is_error('in_begin_date')}<div class="error" style="color:red;">{message name="in_begin_date"}</div>{/if}
		</td>
	</tr>

	<tr>
		<td colspan="3">
			<div class="kiyaku">
				<div class="title">助成金なう利用規約</div>
				<pre style="font-family:Meiryo;">
					{$app_ne.kiyaku_txt}
				</pre>
			</div>

			<div class="mod_form_importance_btn">
			<label for="privacy_1" style="font-size:80%;" id="privacy_label"><input value="{$app.data.privacy}" type="checkbox" name="in_privacy" id="privacy_1">上記「助成金なう利用規約」に同意します。 </label>
			&nbsp;<span style="font-size:1em"></span>
			{if is_error('in_privacy')}<div class="error" style="color:red;">{message name="in_privacy"}</div>{/if}
			<br />
			</div>
			<!-- ここからトップへ戻る -->
			<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
			<!-- ここまでトップへ戻る -->
		</td>
	</tr>
</table>
</div>

<div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:380px;"><a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();" id="confirm_btn">確認</a></div>
	</div>
</div>
</form>
<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$('#in_check_payment').change(function(){
	if($(this).prop('checked')){
		$('.payment_params').each(function(){
			$(this).css('display','');
		});
	} else {
		$('.payment_params').each(function(){
			$(this).css('display','none');
		});
	}
});

$('#cmd_check_payment_open').click(function(){
	$('#in_check_payment').prop('checked', true);
	$('.payment_params').each(function(){
		$(this).css('display','');
	});
	$('#tr_check_payment_open').hide();
	$('#tr_check_payment_closed').show();
	showEMailFields();
});

$('#cmd_check_payment_close').click(function(){

	$('#in_check_payment').prop('checked', false);
	$('.payment_params').each(function(){
		$(this).css('display','none');
	});
	$('#tr_check_payment_closed').hide();
	$('#tr_check_payment_open').show();
});


$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>
<!-- スムーズスクロール -->

{/literal}

{include file='common/track.tpl'}

</body>
</html>
