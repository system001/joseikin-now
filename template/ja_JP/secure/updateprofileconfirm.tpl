<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	$('label').balloon();
});
function do_submit(kind){
	if(kind=='back'){
		document.f2.submit();
	}else{
		document.f3.submit();
	}

}
</script>
{/literal}
</head>
<body>

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
<div id="wrapper">

<!-- ここからheader -->

<div id="01"></div>

{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->

<!-- ここから入力フォーム --><div id="03"></div>

<br />
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">ご入力内容の確認</span></div>
<div style="margin-left: 120px;">以下の内容でよろしければ、『送信』ボタンを押してください。<br />修正する場合は、『キャンセル』ボタンを押して該当箇所を修正してください。</div>
<div style="font-size:0.7em;margin-left: 120px;">
</div>

<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
	<tr>
		<td class="l_Cel_01_02" width="80px;">ログインID</td>
		<td class="s_Cel_01">{$app.data.login_id}</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">パスワード</td>
		<td class="s_Cel_01">***********</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02" width="80px;">会社名</td>
		<td class="s_Cel_01">{$app.data.company_name}</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">会社名かな</td>
		<td class="s_Cel_01">{$app.data.company_kana}</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">支店名</td>
		<td class="s_Cel_01">{$app.data.branch_name}</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">郵便番号</td>
		<td class="s_Cel_01">{$app.data.zip1}－{$app.data.zip2}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">住所</td>
		<td class="s_Cel_01">{$app.data.address}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">電話番号</td>
		<td class="s_Cel_01">
			{$app.data.phone_no1}{$app.data.phone_no2}{$app.data.phone_no3}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">携帯電話番号</td>
		<td class="s_Cel_01">
			{$app.data.k_phone_no1}{$app.data.k_phone_no2}{$app.data.k_phone_no3}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">FAX番号</td>
		<td class="s_Cel_01">
			{$app.data.fax_no1}{$app.data.fax_no2}{$app.data.fax_no3}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">部署名</td>
		<td class="s_Cel_01">
			{$app.data.department_name}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">役職名</td>
		<td class="s_Cel_01">
			{$app.data.post_name}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">担当者名</td>
		<td class="s_Cel_01">
			{$app.data.contractor_lname}&nbsp;&nbsp;{$app.data.contractor_fname}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">担当者名かな</td>
		<td class="s_Cel_01">
			{$app.data.contractor_lkana}&nbsp;&nbsp;{$app.data.contractor_fkana}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">メールアドレス</td>
		<td class="s_Cel_01">
			{$app.data.email}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">サイトを知ったきっかけ</td>
		<td class="s_Cel_01">
			{$app.data.come_from}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">メールマガジンの送付希望</td>
		<td class="s_Cel_01">
			{if $app.data.mailmaga == 1}希望する{else}希望しない{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">支援を受けたい分野1<br />(自治体案件カテゴリ)</td>
		<td width="550" class="s_Cel">
			<table border="0" style="text-align:left;">
{assign var="td_count" value=0}
{foreach from=$app.data.target_field1 key=k item=v name=l}
{if $td_count%2 == 0}
	<tr>
		<td style="width:220px;">
{else}
		<td>
{/if}
{if $v.checked == 'checked'}<img class="checkbox_img" src = "img/checked.png"/>{$v.name}{else}<img class="checkbox_img" src = "img/non-checked.png" />{$v.name}{/if}
{assign var="td_count" value=$td_count+1}
{if $td_count%2 == 0}
</td>
	</tr>
	{else}
</td>
{/if}
{/foreach}
</table>{if is_error('in_target_field1')}<br /><span class="error">{message name="in_target_field1"}</span>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_02">支援を受けたい分野2<br />(財団法人案件カテゴリ)</td>
		<td width="550" class="s_Cel">
			<table	border="0" style="text-align:left;">
{assign var="td_count" value=0}
{foreach from=$app.data.target_field2 key=k item=v name=l}
{if $td_count%2 == 0}
	<tr>
		<td style="width:220px;">
{else}
		<td>
{/if}
{if $v.checked == 'checked'}<img class="checkbox_img" src = "img/checked.png"/>{$v.name}{else}<img class="checkbox_img" src = "img/non-checked.png" />{$v.name}{/if}
{assign var="td_count" value=$td_count+1}
{if $td_count%2 == 0}
</td>
	</tr>
	{else}
</td>
{/if}
{/foreach}
</table>{if is_error('in_target_field2')}<br /><span class="error">{message name="in_target_field2"}</span>{/if}
		</td>
	</tr>
	 <tr>
		<td class="l_Cel_01_02">支援を受けたい事業の<br />形態</td>
		<td width="550" class="s_Cel">
			<table	border="0" style="text-align:left;">
{assign var="td_count" value=0}
{foreach from=$app.data.target_businessform key=k item=v name=l}
{if $td_count%2 == 0}
	<tr>
		<td style="width:220px;">
{else}
		<td>
{/if}
{if $v.checked == 'checked'}<img class="checkbox_img" src = "img/checked.png"/>{$v.name}{else}<img class="checkbox_img" src = "img/non-checked.png" />{$v.name}{/if}
{assign var="td_count" value=$td_count+1}
{if $td_count%2 == 0}
</td>
	</tr>
	{else}
</td>
{/if}
{/foreach}
</table>{if is_error('in_target_businessform')}<br /><span class="error">{message name="in_target_businessform"}</span>{/if}
		</td>
	</tr>

	<tr>
		<td colspan="3">
				<!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
				<!-- ここまでトップへ戻る -->
		</td>
	</tr>
</table>
</div>
<div class="mod_form_btn">
		<div style="margin-top:20px;margin-left:280px;">
			<form action="{$script}" name="f2" id="f2" method="post">
				<input type="hidden" name="action_secure_updateprofile" value="true">
				<input type="hidden" name="mode" value="confirm">
				<a class="button" href="javascript:void(0)" onclick="javascript:do_submit('back');" id="back_btn">キャンセル</a>
					<input type="hidden" name="in_login_id" id="in_login_id" value="{$app.data.login_id}">
					<input type="hidden" name="in_pw" id="in_pw" value="{$app.data.pw}">
					<input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
					<input type="hidden" name="in_company_kana" id="in_company_kana" value="{$app.data.company_kana}">
					<input type="hidden" name="in_branch_name" id="in_branch_name" value="{$app.data.branch_name}">
					<input type="hidden" name="in_zip1" id="in_zip1" value="{$app.data.zip1}">
					<input type="hidden" name="in_zip2" id="in_zip2" value="{$app.data.zip2}">
					<input type="hidden" name="in_address" id="in_address" value="{$app.data.address}">
					<input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
					<input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}">
					<input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}">
					<input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}">
					<input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}">
					<input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}">
					<input type="hidden" name="in_fax_no1" id="in_fax_no1" value="{$app.data.fax_no1}">
					<input type="hidden" name="in_fax_no2" id="in_fax_no2" value="{$app.data.fax_no2}">
					<input type="hidden" name="in_fax_no3" id="in_fax_no3" value="{$app.data.fax_no3}">
					<input type="hidden" name="in_department_name" id="in_department_name" value="{$app.data.department_name}">
					<input type="hidden" name="in_post_name" id="in_post_name" value="{$app.data.post_name}">
					<input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="{$app.data.contractor_lname}">
					<input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="{$app.data.contractor_fname}">
					<input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="{$app.data.contractor_lkana}">
					<input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="{$app.data.contractor_fkana}">
					<input type="hidden" name="in_email" id="in_email" value="{$app.data.email}">
					<input type="hidden" name="in_mailmaga" id="in_mailmaga" value="{$app.data.mailmaga}">
					<input type="hidden" name="in_come_from" id="in_come_from" value="{$app.data.come_from}">
					{foreach from=$app.data.target_field1_save key=k item=v name=l}
						<input type="hidden" name="in_target_field1[]" value="{$v}">
					{/foreach}
					{foreach from=$app.data.target_field2_save key=k item=v name=l}
						<input type="hidden" name="in_target_field2[]" value="{$v}">
					{/foreach}
					{foreach from=$app.data.target_businessform_save key=k item=v name=l}
						<input type="hidden" name="in_target_businessform[]" value="{$v}">
					{/foreach}
					<input type="hidden" name="back" id="back" value="0">
			</form>
			</div>
		<div style="margin-top:-46px;margin-left:500px;">
			<form action="{$script}" name="f3" id="f3" method="post">
				<input type="hidden" name="action_secure_updateprofilecomplete" value="true">
				<a class="button2" href="javascript:void(0)" onclick="javascript:do_submit('send');" id="send_btn">送信</a>
					<input type="hidden" name="mode" value="complete">
					<input type="hidden" name="in_login_id" id="in_login_id" value="{$app.data.login_id}">
					<input type="hidden" name="in_pw" id="in_pw" value="{$app.data.pw}">
					<input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
					<input type="hidden" name="in_company_kana" id="in_company_kana" value="{$app.data.company_kana}">
					<input type="hidden" name="in_branch_name" id="in_branch_name" value="{$app.data.branch_name}">
					<input type="hidden" name="in_zip1" id="in_zip1" value="{$app.data.zip1}">
					<input type="hidden" name="in_zip2" id="in_zip2" value="{$app.data.zip2}">
					<input type="hidden" name="in_address" id="in_address" value="{$app.data.address}">
					<input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
					<input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}">
					<input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}">
					<input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}">
					<input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}">
					<input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}">
					<input type="hidden" name="in_fax_no1" id="in_fax_no1" value="{$app.data.fax_no1}">
					<input type="hidden" name="in_fax_no2" id="in_fax_no2" value="{$app.data.fax_no2}">
					<input type="hidden" name="in_fax_no3" id="in_fax_no3" value="{$app.data.fax_no3}">
					<input type="hidden" name="in_department_name" id="in_department_name" value="{$app.data.department_name}">
					<input type="hidden" name="in_post_name" id="in_post_name" value="{$app.data.post_name}">
					<input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="{$app.data.contractor_lname}">
					<input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="{$app.data.contractor_fname}">
					<input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="{$app.data.contractor_lkana}">
					<input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="{$app.data.contractor_fkana}">
					<input type="hidden" name="in_email" id="in_email" value="{$app.data.email}">
					<input type="hidden" name="in_mailmaga" id="in_mailmaga" value="{$app.data.mailmaga}">
					<input type="hidden" name="in_come_from" id="in_come_from" value="{$app.data.come_from}">
					{foreach from=$app.data.target_field1_save key=k item=v name=l}
						<input type="hidden" name="in_target_field1[]" value="{$v}">
					{/foreach}
					{foreach from=$app.data.target_field2_save key=k item=v name=l}
						<input type="hidden" name="in_target_field2[]" value="{$v}">
					{/foreach}
					{foreach from=$app.data.target_businessform_save key=k item=v name=l}
						<input type="hidden" name="in_target_businessform[]" value="{$v}">
					{/foreach}
					<input type="hidden" name="back" id="back" value="0">
			</form>
		</div>
	</div>
</div>

<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>
<!-- スムーズスクロール -->

{/literal}

{include file='common/track.tpl'}

</body>
</html>
