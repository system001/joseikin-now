<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Content-Style-Type" content="text/css"/>
<meta http-equiv="Content-Script-Type" content="text/javascript"/>
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ"/>
<meta name="author" content="株式会社ナビット"/>
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件"/>
<link rel="stylesheet" href="css/default.css" type="text/css"/>
<link rel="shortcut icon" href=""/>
</head>
<body>

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
<div id="wrapper">

	{include file='common/header.tpl'}
	{include file='common/navi.tpl'}

<!-- ここからメインコンテンツ -->

	<div class="credit_cont">
		<img style="margin:30px auto 0;" src="img/creditcancel_title.jpg" alt="クレジット決済キャンセル" />
		<p class="credit_title">クレジットカード決済のキャンセルを承りました。<br />

		<div class="btn_top">
			<a href="https://www.navit-j.com/service/joseikin-now/">
				<img style="margin:30px auto 0;" src="img/btn_top.png" alt="TOPへ戻る" />
			</a>
		</div>
	</div>

<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->

	<div id="footer">
		<a href="http://navit-j.com/">運営会社</a>　｜　
		<a href="/terms/">利用規約</a>　｜　
		<a href="http://www.navit-j.com/privacy/index.html">Pマークについて</a>　｜　
		<a href="/law/">特定商取引法に基づく表記</a>　｜　
		<a href="https://www.navit-j.com/contactus/">問い合わせフォーム</a>
	</div>

<!-- ここまでfooter -->


	{include file='common/track.tpl'}

</div>
<!-- ここまでwrapper -->
</div>
<!-- ここまでconteinar -->

<!-- googleanalytics -->

<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-45128510-12', 'auto');
ga('send', 'pageview');
</script>


<!-- LF innov -->

<script type="text/javascript">
var _trackingid = 'LFT-10394-1';
(function() {
  var lft = document.createElement('script'); lft.type = 'text/javascript'; lft.async = true;
  lft.src = document.location.protocol + '//track.list-finder.jp/js/ja/track.js';
  var snode = document.getElementsByTagName('script')[0]; snode.parentNode.insertBefore(lft, snode);
})();
</script>

</body>
</html>
