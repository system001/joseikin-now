<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」のホームページ">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="http://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3.js" charset="UTF-8"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript">
//吹き出し
$(window).load(function(){
	$('label').balloon();
});

function do_submit(){
	document.f2.submit();
}
/*
 　　全角->半角変換
 */
jQuery(function(){

	// 郵便番号の処理
	$('.zip-number').change( function(){
		var data = $(this).val();
		var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});

		// 半角数字のみ残す
		var zenkakuDel = new String( hankaku ).match(/\d/g);
		if(zenkakuDel){
			zenkakuDel = zenkakuDel.join("");
		}else {
			zenkakuDel = "";
		}

		$(this).val(zenkakuDel);
	});
	// 電話番号の処理
	$('.tel-number').change( function(){
		var data = $(this).val();
		var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});

		// 半角数字と+-のみ残す
		var zenkakuDel = new String( hankaku ).match(/\d|\-|\+/g);
		if(zenkakuDel){
			zenkakuDel = zenkakuDel.join("");
		}else {
			zenkakuDel = "";
		}

		$(this).val(zenkakuDel);
	});

	// メールアドレスの処理
	$('.mail-address').change( function(){
		var zenkigou = "＠－ー＋＿．，、";
		var hankigou = "@--+_...";
		var data = $(this).val();
		var str = "";

		// 指定された全角記号のみを半角に変換
		for (i=0; i<data.length; i++)
		{
			var dataChar = data.charAt(i);
			var dataNum = zenkigou.indexOf(dataChar,0);
			if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
			str += dataChar;
		}
		// 定番の、アルファベットと数字の変換処理
		var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
		$(this).val(hankaku);
	});

});
</script>
{/literal}
</head>
<body>
<form action="{$script}" name="f2" id="f2" method="post">
<input type="hidden" name="action_secure_updateprofileconfirm" value="true">
<input type="hidden" name="mode" value="confirm">
<!-- ここからconteinar -->
<div id="conteinar">
<!-- ここからwrapper -->
<div id="wrapper">

<div id="01"></div>

{include file='common/header.tpl'}

<!-- ここからメインコンテンツ -->

<!-- ここから入力フォーム --><div id="03"></div>

	<!--<img src="img/title_a_search_line.jpg" alt="" />-->
		<br />
<div class="top_header_title" ><span style="border-bottom:1px #64c601 solid;">会員情報編集ページ</span></div>

<div style="font-size:0.7em;margin-left: 120px;">
	<span>
	【注意事項】<br />
	<span style="color:#64c601;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
	<span style="color:red;font-size:16px;">[重要]</span> メールマガジンの配信停止処理を行うと、助成金なうの無料検索がご利用できまくなりますので予めご了承ください。
	</span>
</div>
<br />

<div style="margin-left: 120px;"><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><span style="font-size:80%;"> は必須入力項目です</span></div>
<div class="table">

<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
	<tr>
		<td class="l_Cel_01_01">ログインID　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td class="s_Cel">例：navit001<br />
			ログインIDは4文字以上16文字以内の半角英数字でご入力下さい<br />
			すでに登録されているIDは登録できません<br />
			<input type="text" id="in_account" name="in_login_id" value="{$app.data.login_id}" size="50" maxlength="16"  style="font-size:16px;" />
			{if is_error('in_login_id')}<br /><span class="error" style="color:red;">{message name="in_login_id"}</span>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">パスワード</td>
		<td class="s_Cel">例：xcd54dsx6<br />
			パスワードは変更がある場合のみご入力下さい<br />
			パスワードは4文字以上16文字以内の半角英数字でご入力下さい<br />
			<input type="text" id="in_pw" name="in_pw" value="{$app.data.pw}" size="50" maxlength="16"	style="font-size:16px;" />
			{if is_error('in_pw')}<br /><span class="error" style="color:red;">{message name="in_pw"}</span>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">会社名　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td class="s_Cel">例：株式会社ナビット<br />
			<input type="text" id="in_company_name" name="in_company_name" value="{$app.data.company_name}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_company_name')}<br /><span class="error" style="color:red;">{message name="in_company_name"}</span>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">会社名かな　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">例：かぶしきがいしゃなびっと<br />
			<input type="text" id="in_company_kana" name="in_company_kana" value="{$app.data.company_kana}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_company_kana')}<div class="error" style="color:red;">{message name="in_company_kana"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">支店名</td>
		<td width="550" class="s_Cel">例：東京支店<br />
			<input type="text" id="in_branch_name" name="in_branch_name" value="{$app.data.branch_name}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_branch_name')}<div class="error" style="color:red;">{message name="in_branch_name"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">郵便番号　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）</td>
		<td width="550" class="s_Cel">例：101-0051<br />
			<input type="text" id="in_zip1" name="in_zip1" value="{$app.data.zip1}" size="10" maxlength="3"  style="font-size:16px;width:100px;" class="zip-number" onKeyUp="AjaxZip3.zip2addr('in_zip1','in_zip2','in_address','in_address','in_address');"/>
			－<input type="text" id="in_zip2" name="in_zip2" value="{$app.data.zip2}" size="10" maxlength="4"  style="font-size:16px;width:100px;" class="zip-number" onKeyUp="AjaxZip3.zip2addr('in_zip1','in_zip2','in_address','in_address','in_address');"/>
			{if is_error('in_zip1')}<div class="error" style="color:red;">{message name="in_zip1"}</div>{/if}
			{if is_error('in_zip2')}<div class="error" style="color:red;">{message name="in_zip2"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">住所　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">例：東京都千代田区神田神保町3-10-2 共立ビル3Ｆ<br />
			<input type="text" id="in_address" name="in_address" value="{$app.data.address}" size="100" maxlength="100"  style="font-size:16px;width:560px;" />
			{if is_error('in_address')}<div class="error" style="color:red;">{message name="in_address"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">電話番号　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）</td>
		<td width="550" class="s_Cel">例：03-5215-5701<br />
			<input type="text" id="in_phone_no1" name="in_phone_no1" value="{$app.data.phone_no1}" size="10" maxlength="5"	style="font-size:16px;width:100px;" class="tel-number" />
			－
			<input type="text" id="in_phone_no2" name="in_phone_no2" value="{$app.data.phone_no2}" size="10" maxlength="4"	style="font-size:16px;width:100px;" class="tel-number" />
			－
			<input type="text" id="in_phone_no3" name="in_phone_no3" value="{$app.data.phone_no3}" size="10" maxlength="4"	style="font-size:16px;width:100px;" class="tel-number" />
			{if is_error('in_phone_no1')}<div class="error" style="color:red;">{message name="in_phone_no1"}</div>{/if}
			{if is_error('in_phone_no2')}<div class="error" style="color:red;">{message name="in_phone_no2"}</div>{/if}
			{if is_error('in_phone_no3')}<div class="error" style="color:red;">{message name="in_phone_no3"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">携帯電話番号<br />（半角数字）</td>
		<td width="550" class="s_Cel">例：090-1234-5789<br />
			<input type="text" id="in_k_phone_no1" name="in_k_phone_no1" value="{$app.data.k_phone_no1}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
			－
			<input type="text" id="in_k_phone_no2" name="in_k_phone_no2" value="{$app.data.k_phone_no2}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
			－
			<input type="text" id="in_k_phone_no3" name="in_k_phone_no3" value="{$app.data.k_phone_no3}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
			{if is_error('in_k_phone_no1')}<div class="error" style="color:red;">{message name="in_k_phone_no1"}</div>{/if}
			{if is_error('in_k_phone_no2')}<div class="error" style="color:red;">{message name="in_k_phone_no2"}</div>{/if}
			{if is_error('in_k_phone_no3')}<div class="error" style="color:red;">{message name="in_k_phone_no3"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">FAX番号<br />（半角数字）</td>
		<td width="550" class="s_Cel">例：03-5215-3020<br />
			<input type="text" id="in_fax_no1" name="in_fax_no1" value="{$app.data.fax_no1}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
			－
			<input type="text" id="in_fax_no2" name="in_fax_no2" value="{$app.data.fax_no2}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
			－
			<input type="text" id="in_fax_no3" name="in_fax_no3" value="{$app.data.fax_no3}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
			{if is_error('in_fax_no1')}<div class="error" style="color:red;">{message name="in_fax_no1"}</div>{/if}
			{if is_error('in_fax_no2')}<div class="error" style="color:red;">{message name="in_fax_no2"}</div>{/if}
			{if is_error('in_fax_no3')}<div class="error" style="color:red;">{message name="in_fax_no3"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">部署名</td>
		<td width="550" class="s_Cel">例：マーケティング事業部 <span style="font-size:0.8em;">(部署名がない方は 「なし」 とご記入ください)</span><br />
			<input type="text" id="in_department_name" name="in_department_name" value="{$app.data.department_name}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_department_name')}<div class="error" style="color:red;">{message name="in_department_name"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">役職名</td>
		<td width="550" class="s_Cel">例：事業部長 <span style="font-size:0.8em;">(役職名がない方は 「なし」 とご記入ください)</span><br />
			<input type="text" id="in_post_name" name="in_post_name" value="{$app.data.post_name}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_post_name')}<div class="error" style="color:red;">{message name="in_post_name"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">担当者名　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<span>姓&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="in_contractor_lname" name="in_contractor_lname" value="{$app.data.contractor_lname}" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>&nbsp;&nbsp;
			<span>名&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="in_contractor_fname" name="in_contractor_fname" value="{$app.data.contractor_fname}" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>
			{if is_error('in_contractor_lname')}<div class="error" style="color:red;">{message name="in_contractor_lname"}</div>{/if}
			{if is_error('in_contractor_fname')}<div class="error" style="color:red;">{message name="in_contractor_fname"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">担当者名かな　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<span>せい&nbsp;<input type="text" id="in_contractor_lkana" name="in_contractor_lkana" value="{$app.data.contractor_lkana}" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>&nbsp;&nbsp;
			<span>めい&nbsp;<input type="text" id="in_contractor_fkana" name="in_contractor_fkana" value="{$app.data.contractor_fkana}" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>
			{if is_error('in_contractor_lkana')}<div class="error" style="color:red;">{message name="in_contractor_lkana"}</div>{/if}
			{if is_error('in_contractor_fkana')}<div class="error" style="color:red;">{message name="in_contractor_fkana"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">メールアドレス　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<input type="text" id="in_email" name="in_email" value="{$app.data.email}" size="50" maxlength="50"  style="font-size:16px;" />
			{if is_error('in_email')}<div class="error" style="color:red;">{message name="in_email"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">サイトを知ったきっかけ　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<select id='in_come_from' name="in_come_from" class="select_font_s">{$app_ne.pulldown.come_from}</select>
			{if is_error('in_come_from')}<div class="error" style="color:red;">{message name="in_come_from"}</div>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">メールマガジンの送付を希望する　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			{$app_ne.radio.mailmaga}
			{if is_error('in_mailmaga')}<br /><br /><span class="error">{message name="in_mailmaga"}</span>{/if}
			<br />「いいえ」を選択した場合、助成金なうの無料検索がご利用できまくなります。
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">支援を受けたい分野1<br />(自治体案件カテゴリ)<br /><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<table border="0" style="text-align:left;">
{assign var="td_count" value=0}
{foreach from=$app.data.target_field1 key=k item=v name=l}
{if $td_count%2 == 0}
	<tr>
		<td style="width:240px;">
{else}
		<td>
{/if}
<label><input  class="CheckList" type="checkbox" name="in_target_field1[]" value="{$k}" {$v.checked} />{$v.name}</label>{assign var="td_count" value=$td_count+1}
{if $td_count%2 == 0}
</td>
	</tr>
	{else}
</td>
{/if}
{/foreach}
</table>{if is_error('in_target_field1')}<br /><span class="error">{message name="in_target_field1"}</span>{/if}
		</td>
	</tr>
	<tr>
		<td class="l_Cel_01_01">支援を受けたい分野2<br />(財団法人案件カテゴリ)<br /><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
			<table border="0" style="text-align:left;">
{assign var="td_count" value=0}
{foreach from=$app.data.target_field2 key=k item=v name=l}
{if $td_count%2 == 0}
	<tr>
		<td style="width:240px;">
{else}
		<td>
{/if}
<label><input  class="CheckList" type="checkbox" name="in_target_field2[]" value="{$k}" {$v.checked} />{$v.name}</label>{assign var="td_count" value=$td_count+1}
{if $td_count%2 == 0}
</td>
	</tr>
	{else}
</td>
{/if}
{/foreach}
</table>{if is_error('in_target_field2')}<br /><span class="error">{message name="in_target_field2"}</span>{/if}
		</td>
	</tr>
	 <tr>
		<td class="l_Cel_01_01">支援を受けたい事業の形態<br /><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
		<td width="550" class="s_Cel">
<table border="0" style="text-align:left;">
{assign var="td_count" value=0}
{foreach from=$app.data.target_businessform key=k item=v name=l}
{if $td_count%2 == 0}
	<tr>
		<td style="width:240px;">
{else}
		<td>
{/if}
<label><input  class="CheckList" type="checkbox" name="in_target_businessform[]" value="{$k}" {$v.checked} />{$v.name}</label>{assign var="td_count" value=$td_count+1}
{if $td_count%2 == 0}
</td>
	</tr>
	{else}
</td>
{/if}
{/foreach}
</table>{if is_error('in_target_businessform')}<br /><span class="error">{message name="in_target_businessform"}</span>{/if}
		</td>
	</tr>

	<tr>
		<td colspan="3">
				<!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
				<!-- ここまでトップへ戻る -->
		</td>
	</tr>
</table>

</div>
	<div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:280px;"><a class="button" href="./index.php?action_mypage=true" id="back_btn">キャンセル</a></div>
	<div style="margin-top:-46px;margin-left:500px;"><a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();" id="confirm_btn">確認</a></div>
	</div>
</div>
</form>
	<!-- ここまで入力フォーム -->

</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">

</div>

<!-- ここまでfooter -->

</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
	  // スクロールの速度
	  var speed = 700; // ミリ秒
	  // アンカーの値取得
	  var href= $(this).attr("href");
	  // 移動先を取得
	  var target = $(href == "#" || href == "" ? 'html' : href);
	  // 移動先を数値で取得
	  var position = target.offset().top;
	  // スムーススクロール
	  $('body,html').animate({scrollTop:position}, speed, 'swing');
	  return false;
   });
});
</script>
<!-- スムーズスクロール -->

{/literal}

{include file='common/track.tpl'}

</body>
</html>
