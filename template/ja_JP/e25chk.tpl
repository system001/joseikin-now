{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}


<form action="index.php" name="f2" method="{$form_action}">
<input type="hidden" name="action_e25fin" value="true">


<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar19.gif" width="740" height="30" /></div>




	<div class="box">
		<div>
			<dl id="kakunin" style="margin-top: 20px;">
				<dt><strong>お問い合わせ項目</strong></dt>
				<dd>{$app_ne.data2.19_view}{$app_ne.data2.19_fa}</dd>
                                                                <br />
				<dt><strong>お問い合わせ内容</strong></dt>
				<dd><br />{$app_ne.data2.19_text|nl2br}</dd>
                                                                <br />
				<dt><strong>メールアドレス</strong></dt>
				<dd><br />{$app_ne.data2.19_mail}</dd>
			</dl>
                                                <br />
			<p class="center_text" style="text-align:center;"><strong>上記を確認していただき、「送信する」ボタンを押してください。</strong></p>
		</div>
	</div>
	<table width="700" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
                  </tr>
               </table>
        
        
        
        
        <!--ここから確認ボタン挿入-->
<div id="submit_1" style="text-align:center;">
            
        
        <br />
                        <div id="submit_1"><img  onClick="javascript:history.back()" src="images/btn2.gif" alt="戻る" width="135" height="47"  style="cursor:pointer;"/>
            <img onClick="javascript:do_submit();" src="images/btn_sousin.gif" alt="進む" width="228" height="47"  style="cursor:pointer;" /></div>
                        <br />
                        <br />
	
<!--ここまで確認ボタン挿入-->
		<p class="center_text">御問い合わせ頂いた内容については3営業日以内にメールで回答致します。<br />
      3営業日過ぎても回答が来ない場合は、お手数ですが電話にてお問い合わせください。<br />
      <br />
      <strong> 株式会社ナビット　</strong>（担当）堀田、野村、立川 <br />
      電話（フリーダイヤル）：0120-964-603　（受付時間：平日10時～18時）<br />
			※携帯電話からも御利用頂けます。
</div>
        
      <br />
      <br />
<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>

<input type="hidden" name="in_19_1" value="{$app.data.19_1}" />
<input type="hidden" name="in_19_2" value="{$app.data.19_2}" />
<input type="hidden" name="in_19_3" value="{$app.data.19_3}" />
<input type="hidden" name="in_19_4" value="{$app.data.19_4}" />
<input type="hidden" name="in_19_text" value="{$app.data.19_text_hidden}" />
<input type="hidden" name="in_19_mail" value="{$app.data.19_mail}" />



</form>



{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<br />
<br />
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
