{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!--
var trigger_id1 = "#q3_13";
var trigger_id2 = "#q4_8";
var txtarea_id1 = "#in_q3_no13_text";
var txtarea_id2 = "#in_q4_no8_text";
var zan_text_id1 = "#zan_q3_no13_text";
var zan_text_id2 = "#zan_q4_no8_text";

$(document).ready(function (){
    text_val1 = $(txtarea_id1).val();
    text_val2 = $(txtarea_id2).val();
    CountDownLength('zan_q3_no13_text', text_val1, 255);
    CountDownLength('zan_q4_no8_text', text_val2, 255);
    
    if($(trigger_id1+":checked").val()){
        $(txtarea_id1).removeAttr("disabled");
        $(txtarea_id1).css("background-color","#ffffff");
        $(zan_text_id1).css("color","#000000");
    }
    else{
        $(txtarea_id1).attr("disabled", "disabled");
        $(txtarea_id1).css("background-color","#d2d1c6");
        $(zan_text_id1).css("color","#d2d1c6");
    }

    if($(trigger_id2+":checked").val()){
        $(txtarea_id2).removeAttr("disabled");
        $(txtarea_id2).css("background-color","#ffffff");
        $(zan_text_id2).css("color","#000000");
    }
    else{
        $(txtarea_id2).attr("disabled", "disabled");
        $(txtarea_id2).css("background-color","#d2d1c6");
        $(zan_text_id2).css("color","#d2d1c6");
    }
    
    //For checkbox initialize
    if($("#q3_1:checked").val() ||
           $("#q3_2:checked").val() ||
           $("#q3_3:checked").val() ||
           $("#q3_4:checked").val() ||
           $("#q3_5:checked").val() ||
           $("#q3_7:checked").val() ||
           $("#q3_8:checked").val() ||
           $("#q3_9:checked").val() ||
           $("#q3_10:checked").val() ||
           $("#q3_11:checked").val() ||
           $("#q3_13:checked").val() ) {
            //1,2,3,4,5,6,7,8,9,10,11,13のどれか1つでもチェックされていたら12をチェック不可に
            $("#q3_12").attr("disabled", "disabled");
            $("#q3_12").css("background-color","#d2d1c6");
            $("#q3_12").css("color","#d2d1c6");
    }else{
            //1,2,3,4,5,6,7,8,9,10,11,13が1つもチェックされていない場合12をチェック可能に
            $("#q3_12").removeAttr("disabled");
            $("#q3_12").css("background-color","#ffffff");
            $("#q3_12").css("color","#000000");
    } 
    if($("#q3_12:checked").val()) {
            ////1,2,3,4,5,6,7,8,9,10,11,13をチェック不可に
            $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").attr("disabled", "disabled");
            $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").css("background-color","#d2d1c6");
            $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").css("color","#d2d1c6");
            ////12をチェック可能に
            $("#q3_12").removeAttr("disabled");
            $("#q3_12").css("background-color","#ffffff");
            $("#q3_12").css("color","#000000");
    } else {
        ////1,2,3,4,5,6,7,8,9,10,11,13をチェック可能に
        $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").removeAttr("disabled");
        $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").css("background-color","#ffffff");
        $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").css("color","#000000");
    }
    
    
});

$(function() {
    $(trigger_id1).on('click', TextAreaEnable1);
    $(trigger_id2).on('click', TextAreaEnable1);
    
    function TextAreaEnable1(){
        if ($(trigger_id1+":checked").val()) {
            $(txtarea_id1).removeAttr("disabled");
            $(txtarea_id1).css("background-color","#ffffff");
            $(zan_text_id1).css("color","#000000");
        } else {
            $(txtarea_id1).attr("disabled", "disabled");
            $(txtarea_id1).css("background-color","#d2d1c6");
            $(zan_text_id1).css("color","#d2d1c6");
        }
        
        if ($(trigger_id2+":checked").val()) {
            $(txtarea_id2).removeAttr("disabled");
            $(txtarea_id2).css("background-color","#ffffff");
            $(zan_text_id2).css("color","#000000");
        } else {
            $(txtarea_id2).attr("disabled", "disabled");
            $(txtarea_id2).css("background-color","#d2d1c6");
            $(zan_text_id2).css("color","#d2d1c6");
        }
    }
});

$(function() {

    $("#q3_1").on('click', ProhibitionItem12);
    $("#q3_2").on('click', ProhibitionItem12);
    $("#q3_3").on('click', ProhibitionItem12);
    $("#q3_4").on('click', ProhibitionItem12);
    $("#q3_5").on('click', ProhibitionItem12);
    $("#q3_6").on('click', ProhibitionItem12);
    $("#q3_7").on('click', ProhibitionItem12);
    $("#q3_8").on('click', ProhibitionItem12);
    $("#q3_9").on('click', ProhibitionItem12);
    $("#q3_10").on('click', ProhibitionItem12);
    $("#q3_11").on('click', ProhibitionItem12);
    $("#q3_12").on('click', ProhibitionItemOther);
    $("#q3_13").on('click', ProhibitionItem12);
    
    
    function ProhibitionItem12(){
        
        if($("#q3_1:checked").val() ||
           $("#q3_2:checked").val() ||
           $("#q3_3:checked").val() ||
           $("#q3_4:checked").val() ||
           $("#q3_5:checked").val() ||
           $("#q3_7:checked").val() ||
           $("#q3_8:checked").val() ||
           $("#q3_9:checked").val() ||
           $("#q3_10:checked").val() ||
           $("#q3_11:checked").val() ||
           $("#q3_13:checked").val() ) {
                //1,2,3,4,5,6,7,8,9,10,11,13のどれか1つでもチェックされていたら12をチェック不可に
                $("#q3_12").attr("disabled", "disabled");
                $("#q3_12").css("background-color","#d2d1c6");
                $("#q3_12").css("color","#d2d1c6");
        }else{
                //1,2,3,4,5,6,7,8,9,10,11,13が1つもチェックされていない場合12をチェック可能に
                $("#q3_12").removeAttr("disabled");
                $("#q3_12").css("background-color","#ffffff");
                $("#q3_12").css("color","#000000");
        } 
        
    }
    
    function ProhibitionItemOther(){
        
        if($("#q3_12:checked").val()) {
            ////1,2,3,4,5,6,7,8,9,10,11,13をチェック不可に
            $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").attr("disabled", "disabled");
            $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").css("background-color","#d2d1c6");
            $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").css("color","#d2d1c6");
            ////12をチェック可能に
            $("#q3_12").removeAttr("disabled");
            $("#q3_12").css("background-color","#ffffff");
            $("#q3_12").css("color","#000000");
        } else {
            ////1,2,3,4,5,6,7,8,9,10,11,13をチェック可能に
            $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").removeAttr("disabled");
            $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").css("background-color","#ffffff");
            $("#q3_1,#q3_2,#q3_3,#q3_4,#q3_5,#q3_6,#q3_7,#q3_8,#q3_9,#q3_10,#q3_11,#q3_13").css("color","#000000");
        }
    }
    
});


//-->
</script>
{/literal}

</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<!--メニュー-->
{$app_ne.menu}
<!--メニュー-->

<div id="main">

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->

{$header}

<form action="{$script}" name="f2" method="{$form_action}" id="f2">
<input type="hidden" name="action_e05" value="true">
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="665" height="40" align="left" valign="middle"><h3>■平成26年度　市民の社会貢献に関する実態調査　<span class="pass">Web回答ページ</span></h3></td>
    <td width="75" align="left" valign="middle">&nbsp;</td>
  </tr>
</table>
<div><img src="images/bar.gif" width="740" height="5" /></div>
<div class="image">
<div><img src="images/bar12.gif" width="740" height="30" /></div>
<div>
<div style="width : 740px; height : px; border : 0px; margin-bottom: 10px; font-size: 16px; margin-top: 10px;">
  <div>
    <p><strong>［ボランティア活動に参加した分野］</strong></p>
  </div>
    <p>
      <label> <strong>問３</strong>　【<a href="./index.php?action_e03=true&back=1#q2">問２</a>で「1.&nbsp;したことがある」とお答えになった方にお聞きします】　<img src="images/m3.gif" width="128" height="19" /><br />
        <br />
        あなたは、平成25年度（平成25年4月～平成26年3月）にどのような分野のボランティア活動に参加したことがありますか。<strong>（複数回答可能）</strong>
        <br />
      </label>
    </p>
    {if is_error('in_q3')}<br /><span class="error">{message name="in_q3"}</span>{/if}
    {if is_error('in_q3_no13_text')}<br /><span class="error">{message name="in_q3_no13_text"}</span>{/if}
    <table width="740" border="1" bordercolor="#777" cellspacing="0" >
      <tr>
        <td >{$app.master_q3.1}</td>
        <td align="center" valign="middle" width="100"><input type="checkbox" name="in_q3_1" value="1" id="q3_1" {$app.data.q3_1} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.2}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_2" value="2" id="q3_2" {$app.data.q3_2} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.3}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_3" value="3" id="q3_3" {$app.data.q3_3} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.4}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_4" value="4" id="q3_4" {$app.data.q3_4} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.5}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_5" value="5" id="q3_5" {$app.data.q3_5} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.6}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_6" value="6" id="q3_6" {$app.data.q3_6} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.7}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_7" value="7" id="q3_7" {$app.data.q3_7} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.8}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_8" value="8" id="q3_8" {$app.data.q3_8} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.9}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_9" value="9" id="q3_9" {$app.data.q3_9} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.10}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_10" value="10" id="q3_10" {$app.data.q3_10} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.11}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_11" value="11" id="q3_11" {$app.data.q3_11} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.12}</td>
        <td align="center" valign="middle"><input type="checkbox" name="in_q3_12" value="12" id="q3_12" {$app.data.q3_12} /></td>
      </tr>
      <tr>
        <td>{$app.master_q3.13}<br />
            <label for="in_q3_no13_text">
                <input size="100" type="text" name="in_q3_no13_text" maxlength="255" id="in_q3_no13_text" value="{$app.data.q3_no13_text}"  onkeyup="CountDownLength('zan_q3_no13_text', value, 255);" /><span id="zan_q3_no13_text">（残り255文字）</span>
            </label>
        </td>
        <td align="center" valign="middle">
            <input type="checkbox" name="in_q3_13" value="13" id="q3_13" {$app.data.q3_13} />
        </td>
      </tr>
    </table>
			<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
			</tr>
		</table>
  <div> <br />
    <p><strong>［参加理由］</strong>　</p>
  </div>

    <p> <strong>問４</strong>　【<a href="./index.php?action_e03=true&back=1#q2">問２</a>で「1.　したことがある」とお答えになった方にお聞きします】　<img src="images/m4.gif" width="128" height="19" /><br />
      <br />
      あなたにとって、ボランティア活動に参加する理由は何ですか。<strong>（複数回答可能）</strong>
      <br />
      {if is_error('in_q4')}<br /><span class="error">{message name="in_q4"}</span>{/if}
      {if is_error('in_q4_no8_text')}<br /><span class="error">{message name="in_q4_no8_text"}</span>{/if}
      <br />
        <label><input type="checkbox" name="in_q4_1" id="q4_1" value="1" {$app.data.q4_1} />{$app.master_q4.1}</label><br />
        <label><input type="checkbox" name="in_q4_2" id="q4_2" value="2" {$app.data.q4_2} />{$app.master_q4.2}</label><br />
        <label><input type="checkbox" name="in_q4_3" id="q4_3" value="3" {$app.data.q4_3} />{$app.master_q4.3}</label><br />
        <label><input type="checkbox" name="in_q4_4" id="q4_4" value="4" {$app.data.q4_4} />{$app.master_q4.4}</label><br />
        <label><input type="checkbox" name="in_q4_5" id="q4_5" value="5" {$app.data.q4_5} />{$app.master_q4.5}</label><br />
        <label><input type="checkbox" name="in_q4_6" id="q4_6" value="6" {$app.data.q4_6} />{$app.master_q4.6}</label><br />
        <label><input type="checkbox" name="in_q4_7" id="q4_7" value="7" {$app.data.q4_7} />{$app.master_q4.7}</label><br />
        <label><input type="checkbox" name="in_q4_8" id="q4_8" value="8" {$app.data.q4_8} />{$app.master_q4.8}</label><br />
        <textarea name="in_q4_no8_text"  id="in_q4_no8_text" rows="6" cols="60" onkeyup="CountDownLength('zan_q4_no8_text', value, 255);" maxlength="255">{$app.data.q4_no8_text}</textarea>
        <span id="zan_q4_no8_text">（残り255文字）</span>
      <br />
    </p>
</div>
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="right" valign="middle"><h5><a href="#top">ページ上に戻る</a></h5></td>
  </tr>
</table>
<!--ここから確認ボタン挿入-->
<div style="text-align:center; margin:0px 0 20px 0; width: 740px;"><a href="./index.php?action_e03=true&back=1"><img src="images/btn2.gif" alt="戻る" width="135" height="47" style="cursor:pointer;" /></a><img src="images/btn3.gif" alt="進む"  onClick="javascript:do_submit();"  width="228" height="47" style="cursor:pointer;" /></div>
<!--ここまで確認ボタン挿入-->        
<div><img src="images/border1.gif" width="740
" height="3" /></div>
<div><img src="images/bar.gif" width="740
" height="5" /></div>
<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="191" height="50" align="right" valign="middle"><img src="images/toukei_logo.gif" width="40" height="42" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="109" align="left" valign="middle"><img src="images/header.jpg" width="109" height="40" /></td>
    <td width="20" align="left" valign="middle">&nbsp;</td>
    <td width="400" align="left" valign="middle"><font size="2">平成26年度　市民の社会貢献に関する実態調査　</font></td>
  </tr>
</table>
</form>

{$footer}

<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</div>
<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
