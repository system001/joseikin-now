<?php
/**
 * USENさん向け自治体助成金案件TSVリスト出力処理
 * 
 * 週一でファイル生成→FTPサーバーの所定のディレクトリに配置（詳細については、野村さんからのメールにて設定）
 * 削除のタイミングは？
 * 
 */
$dir = dirname(dirname(__FILE__));
$path = $dir . '/app/Subsidy_Controller.php';
require_once $path;

// app/action_cli/Batch/Generatelist.php
Subsidy_Controller::main_CLI('Subsidy_Controller', 'Batch_Generatelist');
