#!/usr/local/bin/php -q
<?php

mb_language("japanese");
mb_internal_encoding("UTF-8");

$listfile   = dirname(__FILE__).'/joseikin_check_list.tsv';
$lockfile   = dirname(__FILE__).'/joseikin_checker.lock';
$loadfile   = dirname(__FILE__).'/joseikin_checker_status.bin';
$savefile   = dirname(__FILE__).'/joseikin_checker_status.bin';

try {
	$lock_fp = fopen($lockfile, 'w');
	$result =  flock($lock_fp, LOCK_EX | LOCK_NB);
	if($result){
		$process_datetime = Date('Y-m-d H:i:s');
		$count_updated = 0;
		$count_error   = 0;

		// 更新監視対象リスト読み込み
		$check_list   = array();
		$list_fp = @fopen($listfile, 'r');
		if(!$list_fp) throw new Exception("cannot read {$listfile}");
		while(!feof($list_fp)){
			$line = trim(fgets($list_fp, 4096));
			$columns = explode("\t", $line);
			if(count($columns) < 4) continue;
			$serial = $columns[0];
			$check_list[$serial] = array(
				'is_updated'    => false,
				'serial'        => $serial,
				'jiscode'       => $columns[1],
				'name'          => $columns[2],
				'url'           => $columns[3],
				'updated'       => null,
				'content_hash'  => null,
				'last_modified' => null,
				'content_length'  => null,
			);
		}
		fclose($list_fp);

		// 前回までの監視結果の読み込み
		$load_fp = @fopen($loadfile, 'r');
		if($load_fp){
			while(!feof($load_fp)){
				$line = trim(fgets($load_fp, 4096));
				$columns = explode("\t", $line);
				if(count($columns) < 8) continue;
				$serial = $columns[0];
				if(array_key_exists($serial, $check_list)){
					// serial, jiscode, name, url は更新しない
					$check_list[$serial]['updated']        = $columns[4];
					$check_list[$serial]['content_hash']   = $columns[5];
					$check_list[$serial]['last_modified']  = $columns[6];
					$check_list[$serial]['content_length'] = $columns[7];
				}
			}
			fclose($load_fp);
		}

		$updated_list = array();
		$error_list   = array();
		// URL監視
		foreach($check_list as $serial=>&$data){
			$url = $data['url'];
			$http_status    = null;
			$last_modified  = null;
			$content_length = null;
			$content_hash   = null;
			$http_response_header = null;
			$start_time = time();

			$context = stream_context_create(array('http' => array('timeout'=>30,'ignore_errors'=>true)));
			$response = @file_get_contents($url, false, $context);
			if(is_array($http_response_header)){
				foreach($http_response_header as $header_line){
					if(preg_match('/^HTTP\/[0-9\.]+\s([0-9]+)/i', $header_line, $matches)){
						$http_status = $matches[1];
					} elseif(preg_match('/^last-modified: (.+)$/i', $header_line, $matches)){
						$last_modified = Date('Y-m-d H:i:s', strtotime($matches[1]));
					} elseif(preg_match('/^content-length: (.+)$/i', $header_line, $matches)) {
						$content_length = $matches[1];
					}
				}
			}
			$end_time  = time();
			$process_time = $end_time - $start_time;
			$data['process_time'] = $process_time;

			if($http_status == 200){
				$content_hash = hash('sha1', $response);
				$is_updated = false;

				if(empty($last_modified)){
					// last-modifiedが取得できないページはhashで比較する
					if(!empty($content_hash)&&($content_hash != $data['content_hash'])){
						$is_updated = true;
					}
				} else {
					// 1. 冗長化され、複数のlast_modifiedを返すページがあるため更新時刻が１時間以内のものは同一ページとみなす
					// 2. 正しいlast-modifiedを返すがページ内に同的コンテンツを含むページに対応するため、last-modifedを取得できたページはhashでの比較は行わない
					// 3. 保存しているlast-modifiedが空の場合は更新
					if(empty($data['last_modified'])){
						$is_updated = true;
					} elseif($last_modified != $data['last_modified']){
						$modified_1 = strtotime($last_modified);
						$modified_2 = strtotime($data['last_modified']);
						if(abs($modified_1 - $modified_2) >= 3600){
							$is_updated = true;
						}
					}
				}

				if($is_updated){
					// 更新のあったページの各項目を更新する
					$data['is_updated']     = $is_updated = true;
					$data['updated']        = $process_datetime;
					$data['content_hash']   = $content_hash;
					$data['last_modified']  = $last_modified;
					$data['content_length'] = $content_length;
					$updated_list[$serial] = true;
					$count_updated++;
				}
			} else {
				// エラーのあったページを保持
				$error_list[$serial] = array(
					'serial'  => $serial,
					'jiscode' => $data['jiscode'],
					'name'    => $data['name'],
					'url'     => $data['url'],
				);
				$count_error++;
			}
		}

		// 更新のあったときのみ保存ファイルを更新する
		if($count_updated > 0){
			$save_content = '';
			foreach($check_list as $serial=>&$data){
				$save_content .= "{$data['serial']}\t{$data['jiscode']}\t{$data['name']}\t{$data['url']}\t{$data['updated']}\t{$data['content_hash']}\t{$data['last_modified']}\t{$data['content_length']}\t{$data['process_time']}\r\n";
			}
			$save_fp = fopen($savefile, 'w');
			if(!$save_fp) throw new Exception("cannot open {$savefile}");
			fwrite($save_fp, $save_content);
			fclose($save_fp);
		}

		// 結果をjiscode毎にまとめる
		$report_list  = array();
		foreach($check_list as $serial=>&$data){
			$jiscode = $data['jiscode'];
			if(!array_key_exists($jiscode, $report_list)){
				$report_list[$jiscode] = array(
					'serial'         => $serial,
					'count_updated'  => 0,
					'count_error'    => 0,
					'jiscode'        => $data['jiscode'],
					'name'           => $data['name'],
					'list'           => array(),
				);
			}
			$report_list[$jiscode]['list'][$serial] = array(
				'serial'     => $serial,
				'url'        => $data['url'],
				'is_updated' => $data['is_updated'],
				'is_error'   => false,
			);
			if($data['is_updated']){
				$report_list[$jiscode]['count_updated']++;
			}
			if(array_key_exists($serial, $error_list)){
				$report_list[$jiscode]['list'][$serial]['is_error'] = true;
				$report_list[$jiscode]['count_error']++;
			}
		}

		// 通知メールの本文生成
		$report  = "開始日時: {$process_datetime}\n";
		$report .= "終了日時: " . Date('Y-m-d H:i:s') . "\n";
		$report .= "\n";
		$report .= "監視対象: " . count($check_list) . "件\n";
		$report .= "更新:     {$count_updated}件\n";
		$report .= "エラー:   {$count_error}件\n";

		// 更新ページがある場合は更新情報詳細を出力する
		if($count_updated > 0){
			$report .= "\n";
			$report .= "[更新]\n";
			foreach($report_list as $jiscode=>$report_data){
				if($report_data['count_updated'] < 1) continue;
				$report .= "{$report_data['jiscode']} {$report_data['name']}\n";
				foreach($report_data['list'] as $item){
					if($item['is_updated']){
						$report .= "#{$item['serial']} {$item['url']}\n";
					}
				}
			}
		}

		// エラーページがある場合はエラー情報詳細を出力する
		if($count_error > 0){
			$report .= "\n";
			$report .= "[エラー]\n";
			foreach($report_list as $jiscode=>$report_data){
				if($report_data['count_error'] < 1) continue;
				$report .= "{$report_data['jiscode']} {$report_data['name']}\n";
				foreach($report_data['list'] as $item){
					if($item['is_error']){
						$report .= "#{$item['serial']} {$item['url']}\n";
					}
				}
			}
		}

		$to = 't.chiba@navit-j.com';
		$subject = '助成金なう 定期監視結果';
		$from = 'info@joseikin-now.com';
		mb_send_mail($to, $subject, $report, "From: ".$from);

	}
	flock($lock_fp, LOCK_UN);
	fclose($lock_fp);

}catch(Exception $e){
	// エラー時の処理は未定
}
