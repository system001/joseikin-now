<?php
function edit_recommend_index( $serial,$lines ) {
	/*
	$sql = 'select a.callkind as callkind, a.contents_org_id as contents_org_id, a.main_title as main_title, a.flg_hidden as flg_hidden';
	$sql .= ', b.contents_id as contents_id';
	$sql .= ', c.contents_id as contents_id';
	$sql .= ' from recommend_contents as a';
	$sql .= ' left join government_contents as b';
	$sql .= ' on (a.contents_org_id = b.contents_org_id';
	$sql .= ' and a.main_title = b.main_title';
	$sql .= ' and a.callkind = \'autonomy\'';
	$sql .= ' and b.flg_delete = 0)';
	$sql .= ' left join foundation_contents as c';
	$sql .= ' on (a.contents_org_id = c.contents_org_id';
	$sql .= ' and a.main_title = c.main_title';
	$sql .= ' and a.callkind = \'foundation\'';
	$sql .= ' and c.flg_delete = 0)';
	$sql .= ' where a.flg_delete = 0';
	$sql .= ' and b.flg_delete = 0';
	$sql .= ' and c.flg_delete = 0';
	$sql .= " order by a.flg_hidden asc, a.updated desc";
	*/
	if( $_REQUEST['page'] == "" ) {
			$offset = 0;
	} else {
			$offset = ($_REQUEST['page']-1) * $lines;
	}
	if( $lines != "" ){
			$page = " limit " . $lines . " offset " . $offset;
	}

	$gov_sql = 'select a.callkind as callkind, a.contents_org_id as contents_org_id, a.main_title as main_title, a.flg_hidden as flg_hidden';
	$gov_sql .= ', b.contents_id as contents_id';
	$gov_sql .= ' from recommend_contents as a';
	$gov_sql .= ' left join government_contents as b';
	$gov_sql .= ' on (a.contents_org_id = b.contents_org_id';
	$gov_sql .= ' and a.main_title = b.main_title';
	$gov_sql .= ' and a.callkind = \'autonomy\'';
	$gov_sql .= ' and b.flg_delete = 0)';
	$gov_sql .= ' where a.flg_delete = 0';
	$gov_sql .= ' and b.flg_delete = 0';
	$gov_sql .= " order by a.flg_hidden asc, a.updated desc";
	$gov_sql .= $page;
	$gov = mysql_query( $gov_sql );

	$fou_sql = 'select a.callkind as callkind, a.contents_org_id as contents_org_id, a.main_title as main_title, a.flg_hidden as flg_hidden';
	$fou_sql .= ', b.contents_id as contents_id';
	$fou_sql .= ' from recommend_contents as a';
	$fou_sql .= ' left join foundation_contents as b';
	$fou_sql .= ' on (a.contents_org_id = b.contents_org_id';
	$fou_sql .= ' and a.main_title = b.main_title';
	$fou_sql .= ' and a.callkind = \'foundation\'';
	$fou_sql .= ' and b.flg_delete = 0)';
	$fou_sql .= ' where a.flg_delete = 0';
	$fou_sql .= ' and b.flg_delete = 0';
	$fou_sql .= " order by a.flg_hidden asc, a.updated desc";
	$fou_sql .= $page;
	$fou = mysql_query( $fou_sql );

	return array($gov, $fou);
}

function edit_recommend_hidden($hidden, $kind, $title, $org_id){
	$hidden_st = ($hidden == 0) ? '1' : '0' ;
	$sql = "update recommend_contents";
	$sql .= " set flg_hidden = " . $hidden_st;
	$sql .= " , updated = now()";
	$sql .= " where callkind = \"" . $kind . "\"";
	$sql .= " and main_title = \"" . $title . "\"";
	$sql .= " and contents_org_id = \"" . $org_id . "\"";
	common_exec_sql( $sql );
	return;
}

function edit_recommend_delete($kind, $title, $org_id){
	$sql = "update recommend_contents";
	$sql .= " set flg_delete = 1";
	$sql .= " , updated = now()";
	$sql .= " where callkind = \"" . $kind . "\"";
	$sql .= " and main_title = \"" . $title . "\"";
	$sql .= " and contents_org_id = \"" . $org_id . "\"";
	common_exec_sql( $sql );
	return;
}
?>
