<?php
function common_login_check(){
	if (!isset($_SESSION['user_id'])) {
		$_SESSION['admin_page'] = $_SERVER['REQUEST_URI'];
		header("Location: /testing/public_html/admin/login/");
		exit;
	}
	return;
}
function common_get_now_date(){
    return date( "Y/m/d", time() );
}
function common_get_now_datetime(){
    return date( "Y/m/d H:i:s", time() );
}
function common_get_mysql_num_rows( $sql ){
    $rand = mt_rand();
    $result{$rand} = mysql_query( $sql );
    return mysql_num_rows($result{$rand});
}
function common_get_password(){
        //パスワード用乱数発生
        $rtn = random_string();
        //パスワードを生成
        $password = sha1( $rtn . 'SecretKey');
    return $password;
}
function common_exec_sql( $sql ){
    if( !mysql_query( $sql ) ){
        common_mysql_error( $sql, $_SERVER['REQUEST_URI']);
    }else{
        return "ok";
    }
}
function common_mysql_error($sql,$uri){

    $subject = "【助成金なう】SQLエラーが発生しました";
    $comment = "ファイル名：" . $uri . "\n";
    $comment .= "実行クエリー：" . $sql . "\n";
    $comment .= "実行ユーザー：" . $_POST['id'] . "\n";
    $comment .= "氏名：" . $_POST['contractor_lname'] . " " . $_POST['contractor_fname'] . "\n";
    $comment .= "メールアドレス：" . $_SESSION['email'] . "\n";
    common_send_mail( "info@joseikin-now.com",$subject,$comment );

    header('Location:/admin/error.php');
    exit;
}
function date_exist_check($year,$month,$day)
{
    if( $month == "02")
    {
        if( $day == "29" )
        {
            if( $year != "2016" && $year != "2020" )
            {
                $err_msg = "存在しない日付が指定されています。";
            }

        }
        else if( $day == "30" || $day == "31" )
        {
            $err_msg = "存在しない日付が指定されています。";
        }
    }
    else if( $month == "04" || $month == "06" || $month == "09" || $month == "11")
    {
        if( $day == "31" ){ $err_msg = "存在しない日付が指定されています。"; }
    }
    return $err_msg;
}
function random_string($length) {
    if($length == ""){$length = 8;}
    $str = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z"'));
    for ($i = 0; $i < $length; $i++) {
        $r_str .= $str[rand(0, count($str)-1)];
    }
    return $r_str;
}
function common_send_mail( $to,$subject,$comment,$from ) {
    $headers = "From: " . $from . "\r\n" . "Reply-To: " . $from;
    mb_language(Japanese);
    mb_internal_encoding("utf-8");
    mb_send_mail( $to,$subject,$comment,$headers );
    return;
}
function common_get_max( $table ) {
    $sql = "select max(id) as max from $table";
    $result = mysql_query($sql);
    $arr = mysql_fetch_array($result);
    return $arr['max'];
}
function common_get_value( $table,$column,$id,$where ) {
    if( $where == "" ) {
        $where = "id";
    }
    $sql = "select $column from $table where $where = '$id'";
    $result = mysql_query($sql);
    $arr = mysql_fetch_array($result);
    return( $arr[$column] );
}
function common_get_value_all( $table,$id,$where ) {
    if( $where == "" ) {
        $where = "id";
    }
    $sql = "select * from $table where $where = '$id'";
    $result = mysql_query($sql);
    return  mysql_fetch_array($result);
}
function change_date_to_jdate($date,$weekday) {
	$year = substr( $date,0,4);
	$month = substr( $date,5,2);
	$day = substr( $date,8,2);
	setlocale(LC_TIME, "ja_JP.utf8");
	$wday = strftime('%a', strtotime( $date ) );
	$jdate = $year . "年" . $month . "月" . $day . "日";
	if( $weekday == "on" ){
		$jdate .= "（" . $wday . "）";
	}
	return $jdate;
}
function change_datetime_to_jdate($datetime) {
    $year = substr( $datetime,0,4);
    $month = substr( $datetime,5,2);
    $day = substr( $datetime,8,2);
    $hour = substr( $datetime,11,2);
    $minute = substr( $datetime,14,2);
    setlocale(LC_TIME, "ja_JP.utf8");
    $wday = strftime('%a', strtotime( $datetime ) );
    $jdate = $year . "年" . $month . "月" . $day . "日（" . $wday . "）" . $hour . "時" . $minute . "分";
    return $jdate;
}
function get_contents_tag_name( $arr,$table,$delimiter){
    //$arrは配列の場合とカンマ区切りの文字列の場合があるので、一旦分岐
    if( is_array($arr) ){
        //$arrが配列の場合はそのまま変数セット
        $tmp_arr = $arr;
    }else{
        //$arrがカンマ区切りの文字列の場合は一旦配列に変換
        $tmp_arr = split( "," , $arr );
    }
    //共通の処理
    if( $delimiter == "" ){
        $delimiter = "<br>";
    }
        foreach ( $tmp_arr as $key) {
                if( $name == "" ){
                        $name = get_value($table,name,$key);
                }else{
                        $name .= $delimiter . get_value($table,name,$key);
                }
        }
    return $name;
}
//配列をカンマ区切りの文字列に変換する
function change_array_to_comma( $arr ){
    foreach ( $arr as $key){
        if( $tmp == "" ){
            $tmp = $key;
        }else{
            $tmp .= "," . $key;
        }
    }
    return $tmp;
}
function set_session_publish_status(){
        $_SESSION['publish_status'] = "";
        $tmp = "";
        $tmp = $_POST['publish_status'];
        foreach( $tmp as $key ) {
                if( $_SESSION['publish_status'] == "" ) {
                        $_SESSION['publish_status'] = $key;
                } else {
                        $_SESSION['publish_status'] .= "," . $key;
                }
        }
    return;
}
function set_session_reception(){
        $_SESSION['reception'] = "";
        $tmp = "";
        $tmp = $_POST['reception'];
        foreach( $tmp as $key ) {
                if( $_SESSION['reception'] == "" ) {
                        $_SESSION['reception'] = $key;
                } else {
                        $_SESSION['reception'] .= "," . $key;
                }
        }
    return;
}
function set_session_charge_type(){
        $_SESSION['charge_type'] = "";
        $tmp = "";
        $tmp = $_POST['charge_type'];
        foreach( $tmp as $key ) {
                if( $_SESSION['charge_type'] == "" ) {
                        $_SESSION['charge_type'] = $key;
                } else {
                        $_SESSION['charge_type'] .= "," . $key;
                }
        }
    return;
}
function set_session_check_status(){
        $_SESSION['check_status'] = "";
        $tmp = "";
        $tmp = $_POST['check_status'];
        foreach( $tmp as $key ) {
                if( $_SESSION['check_status'] == "" ) {
                        $_SESSION['check_status'] = $key;
                } else {
                        $_SESSION['check_status'] .= "," . $key;
                }
        }
    return;
}
function common_get_url(){
	$url = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
	return $url;
}
function common_get_weekday($date){
	$weekday = array( '日', '月', '火', '水', '木', '金', '土' );
	return $weekday[date('w',strtotime($date))];
}
?>
