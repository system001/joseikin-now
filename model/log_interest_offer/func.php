<?php
function log_interest_offer_index( $serial,$lines ) {
        $sql = "(";
        $sql .= "select";
        $sql .= " t1.*";
        $sql .= ",t2.contents_id";
        $sql .= ",t2.main_title";
        $sql .= " from";
        $sql .= " log_interest_offer t1";
        $sql .= " left join";
        $sql .= " government_contents t2";
        $sql .= " on";
        $sql .= " (t1.contents_org_id = t2.contents_org_id)";
        $sql .= " where";
        $sql .= " t1.flg_delete = 0";
        $sql .= " and";
        $sql .= " t2.flg_delete = 0";
	if(isset($serial)){
		$sql .= " and";
		$sql .= " t1.user_serial = '$serial'";
	}
	if( isset($_SESSION['log_interest_offer']['company_name'])){
		$sql .= " and";
		$sql .= " t1.company_name like '%" . $_SESSION['log_interest_offer']['company_name'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['address'] != "" ){
		$sql .= " and";
		$sql .= " t1.address like '%" . $_SESSION['log_interest_offer']['address'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['tel'] != "" ){
		$sql .= " and";
		$sql .= " concat(t1.phone_no1,t1.phone_no2,t1.phone_no3) like '%" . $_SESSION['log_interest_offer']['tel'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['contractor'] != "" ){
		$sql .= " and";
		$sql .= " concat(t1.contractor_lname,t1.contractor_fname) like '%" . $_SESSION['log_interest_offer']['contractor'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['email'] != "" ){
		$sql .= " and";
		$sql .= " t1.email like '%" . $_SESSION['log_interest_offer']['email'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['kind'] != "" ){
		$sql .= " and";
		$sql .= " t1.kind = '" . $_SESSION['log_interest_offer']['kind'] . "'";
	}
	if( $_SESSION['log_interest_offer']['main_title'] != "" ){
		$sql .= " and";
		$sql .= " t2.main_title like '%" . $_SESSION['log_interest_offer']['main_title'] . "%'";
	}
        $sql .= ")";
        $sql .= " UNION ";
        $sql .= "(";
        $sql .= "select";
        $sql .= " t1.*";
        $sql .= ",t2.contents_id";
        $sql .= ",t2.main_title";
        $sql .= " from";
        $sql .= " log_interest_offer t1";
        $sql .= " left join";
        $sql .= " foundation_contents t2";
        $sql .= " on";
        $sql .= " (t1.contents_org_id = t2.contents_org_id)";
        $sql .= " where";
        $sql .= " t1.flg_delete = 0";
        $sql .= " and";
        $sql .= " t2.flg_delete = 0";
	if(isset($serial)){
		$sql .= " and";
		$sql .= " t1.user_serial = '$serial'";
	}
	if( $_SESSION['log_interest_offer']['company_name'] != "" ){
		$sql .= " and";
		$sql .= " t1.company_name like '%" . $_SESSION['log_interest_offer']['company_name'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['address'] != "" ){
		$sql .= " and";
		$sql .= " t1.address like '%" . $_SESSION['log_interest_offer']['address'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['tel'] != "" ){
		$sql .= " and";
		$sql .= " concat(t1.phone_no1,t1.phone_no2,t1.phone_no3) like '%" . $_SESSION['log_interest_offer']['tel'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['contractor'] != "" ){
		$sql .= " and";
		$sql .= " concat(t1.contractor_lname,t1.contractor_fname) like '%" . $_SESSION['log_interest_offer']['contractor'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['email'] != "" ){
		$sql .= " and";
		$sql .= " t1.email like '%" . $_SESSION['log_interest_offer']['email'] . "%'";
	}
	if( $_SESSION['log_interest_offer']['kind'] != "" ){
		$sql .= " and";
		$sql .= " t1.kind = '" . $_SESSION['log_interest_offer']['kind'] . "'";
	}
	if( $_SESSION['log_interest_offer']['main_title'] != "" ){
		$sql .= " and";
		$sql .= " t2.main_title like '%" . $_SESSION['log_interest_offer']['main_title'] . "%'";
	}
        $sql .= ")";
        $sql .= " order by serial desc";
        if( $_REQUEST['page'] == "" ) {
                $offset = 0;
        } else {
                $offset = ($_REQUEST['page']-1) * $lines;
        }
        if( $lines != "" ){
                $sql .= " limit " . $lines . " offset " . $offset;
        }
        return mysql_query( $sql );
}
function log_interest_offer_delete( $serial){
        $sql = "update log_interest_offer set";
        $sql .= " flg_delete = 1";
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " serial= " . $serial;
        common_exec_sql( $sql );
        return;
}
function log_interest_offer_csvdl(){

	$tsvFileName = '/tmp/' . time() . rand() . '.tsv';
	$res = fopen($tsvFileName, 'w');

        $line .= "項番(主キー)";
        $line .= "\tログインID";
        $line .= "\t会社名";
        $line .= "\t会社名かな";
        $line .= "\t支店名・営業所名";
        $line .= "\t郵便番号1";
        $line .= "\t郵便番号2";
        $line .= "\t住所";
        $line .= "\t電話番号1";
        $line .= "\t電話番号2";
        $line .= "\t電話番号3";
        $line .= "\tFAX1";
        $line .= "\tFAX2";
        $line .= "\tFAX3";
        $line .= "\t携帯電話1";
        $line .= "\t携帯電話2";
        $line .= "\t携帯電話3";
        $line .= "\t部署名";
        $line .= "\t役職名";
        $line .= "\t名前（姓）";
        $line .= "\t名前（名）";
        $line .= "\t名前（姓）かな";
        $line .= "\t名前（名）かな";
        $line .= "\tメールアドレス";
        $line .= "\ttarget_field1_str";
        $line .= "\ttarget_field2_str";
        $line .= "\ttarget_businessform_str";
        $line .= "\t会員ランク";
        $line .= "\tコンテンツのオリジナル識別子";
        $line .= "\t種別";
        $line .= "\t削除フラグ";
        $line .= "\tレコード更新日時";
        $line .= "\tレコード生成日時";
	$line .= "\n";

	$result = log_interest_offer_index();
	while( $arr = mysql_fetch_array($result )){
                $line .= $arr['serial'];
                $line .= "\t" . $arr['id'];
                $line .= "\t" . $arr['company_name'];
                $line .= "\t" . $arr['company_kana'];
                $line .= "\t" . $arr['branch_name'];
                $line .= "\t'" . $arr['zip1'];
                $line .= "\t'" . $arr['zip2'];
                $line .= "\t" . $arr['address'];
                $line .= "\t'" . $arr['phone_no1'];
                $line .= "\t'" . $arr['phone_no2'];
                $line .= "\t'" . $arr['phone_no3'];
                $line .= "\t'" . $arr['fax_no1'];
                $line .= "\t'" . $arr['fax_no2'];
                $line .= "\t'" . $arr['fax_no3'];
                $line .= "\t'" . $arr['k_phone_no1'];
                $line .= "\t'" . $arr['k_phone_no2'];
                $line .= "\t'" . $arr['k_phone_no3'];
                $line .= "\t" . $arr['department_name'];
                $line .= "\t" . $arr['post_name'];
                $line .= "\t" . $arr['contractor_lname'];
                $line .= "\t" . $arr['contractor_fname'];
                $line .= "\t" . $arr['contractor_lkana'];
                $line .= "\t" . $arr['contractor_fkana'];
                $line .= "\t" . $arr['email'];
                $line .= "\t" . $arr['target_field1_str'];
                $line .= "\t" . $arr['target_field2_str'];
                $line .= "\t" . $arr['target_businessform_str'];
                $line .= "\t" . $arr['rank'];
                $line .= "\t" . $arr['contents_org_id'];
                $line .= "\t" . $arr['kind'];
                $line .= "\t" . $arr['flg_delete'];
                $line .= "\t" . $arr['updated'];
                $line .= "\t" . $arr['created'];
		$line .= "\n";
	}

	$line = mb_convert_encoding($line,"SJIS","UTF-8");
	fwrite($res, $line);
	fclose($res);

	header('Content-Disposition: attachment; filename=dl.tsv'); 
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($tsvFileName));
	readfile($tsvFileName);	

	$cmd = "rm -f " . $tsvFileName;
	system($cmd);

	return;
}
?>
