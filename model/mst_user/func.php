<?php
function mst_user_index( $lines ) {
        $sql = "select";
        $sql .= " *";
        $sql .= " ,DATE_FORMAT(login_date,'%Y年%m月%d日 %k時%i分%s秒') for_login_date";
        $sql .= " from";
        $sql .= " mst_user";
        $sql .= " where";
        $sql .= " flg_delete = 0";
        $sql .= " and";
        $sql .= " (";
        $sql .= " auth_is_ok = 1";
        $sql .= " or";
        $sql .= " (";
        $sql .= " auth_is_ok = 0";
        $sql .= " and";
        $sql .= " authlimit > now()";
        $sql .= " )";
        $sql .= " )";
	if( isset($_SESSION['mst_user']['company_name'])){
		$sql .= " and";
		$sql .= " company_name like '%" . $_SESSION['mst_user']['company_name'] . "%'";
	}
	if( isset($_SESSION['mst_user']['company_kana'])){
		$sql .= " and";
		$sql .= " company_kana like '%" . $_SESSION['mst_user']['company_kana'] . "%'";
	}
	if( isset($_SESSION['mst_user']['address'])){
		$sql .= " and";
		$sql .= " address like '%" . $_SESSION['mst_user']['address'] . "%'";
	}
	if( isset($_SESSION['mst_user']['tel'])){
		$sql .= " and";
		$sql .= " concat(phone_no1,phone_no2,phone_no3) like '%" . $_SESSION['mst_user']['tel'] . "%'";
	}
	if( isset($_SESSION['mst_user']['contractor'])){
		$sql .= " and";
		$sql .= " concat(contractor_lname,contractor_fname) like '%" . $_SESSION['mst_user']['contractor'] . "%'";
	}
	if( isset($_SESSION['mst_user']['contractor_kana'])){
		$sql .= " and";
		$sql .= " concat(contractor_lkana,contractor_fkana) like '%" . $_SESSION['mst_user']['contractor_kana'] . "%'";
	}
	if( isset($_SESSION['mst_user']['email'])){
		$sql .= " and";
		$sql .= " email like '%" . $_SESSION['mst_user']['email'] . "%'";
	}
	if( isset($_SESSION['mst_user']['target_field1'])){
		foreach ( $_SESSION['mst_user']['target_field1'] as $key => $val) {
			$sql .= " and";
			$sql .= " (target_field1 like '" . $val . ",%' or target_field1 like '%," . $val . ",%' or target_field1 like '%," . $val . "' or target_field1 = '" . $val . "')";
		}
	}
	if( isset($_SESSION['mst_user']['target_field2'])){
		foreach ( $_SESSION['mst_user']['target_field2'] as $key => $val) {
			$sql .= " and";
			$sql .= " (target_field2 like '" . $val . ",%' or target_field2 like '%," . $val . ",%' or target_field2 like '%," . $val . "' or target_field2 = '" . $val . "')";
		}
	}
	if( isset($_SESSION['mst_user']['target_businessform'])){
		foreach ( $_SESSION['mst_user']['target_businessform'] as $key => $val) {
			$sql .= " and";
			$sql .= " (target_businessform like '" . $val . ",%' or target_businessform like '%," . $val . ",%' or target_businessform like '%," . $val . "' or target_businessform = '" . $val . "')";
		}
	}
	if( $_SESSION['mst_user']['auth_is_ok'] != NULL ){
		$sql .= " and";
		$sql .= " auth_is_ok = " . $_SESSION['mst_user']['auth_is_ok'];
	}
	if( $_SESSION['mst_user']['rank'] != NULL ){
		$sql .= " and";
		$sql .= " rank = " . $_SESSION['mst_user']['rank'];
	}
	if( $_SESSION['mst_user']['mailmaga'] != NULL ){
		$sql .= " and";
		$sql .= " mailmaga = " . $_SESSION['mst_user']['mailmaga'];
	}
        $sql .= " order by created desc";
        if( $_REQUEST['page'] == "" ) {
                $offset = 0;
        } else {
                $offset = ($_REQUEST['page']-1) * $lines;
        }
        if( $lines != "" ){
                $sql .= " limit " . $lines . " offset " . $offset;
        }

        return mysql_query( $sql );
}

function mst_user_count() {
        $sql = "select";
        $sql .= " count(*)";
        $sql .= " from";
        $sql .= " mst_user";
        $sql .= " where";
        $sql .= " flg_delete = 0";
        $sql .= " and";
        $sql .= " (";
        $sql .= " auth_is_ok = 1";
        $sql .= " or";
        $sql .= " (";
        $sql .= " auth_is_ok = 0";
        $sql .= " and";
        $sql .= " authlimit > now()";
        $sql .= " )";
        $sql .= " )";
	if( isset($_SESSION['mst_user']['company_name'])){
		$sql .= " and";
		$sql .= " company_name like '%" . $_SESSION['mst_user']['company_name'] . "%'";
	}
	if( isset($_SESSION['mst_user']['company_kana'])){
		$sql .= " and";
		$sql .= " company_kana like '%" . $_SESSION['mst_user']['company_kana'] . "%'";
	}
	if( isset($_SESSION['mst_user']['address'])){
		$sql .= " and";
		$sql .= " address like '%" . $_SESSION['mst_user']['address'] . "%'";
	}
	if( isset($_SESSION['mst_user']['tel'])){
		$sql .= " and";
		$sql .= " concat(phone_no1,phone_no2,phone_no3) like '%" . $_SESSION['mst_user']['tel'] . "%'";
	}
	if( isset($_SESSION['mst_user']['contractor'])){
		$sql .= " and";
		$sql .= " concat(contractor_lname,contractor_fname) like '%" . $_SESSION['mst_user']['contractor'] . "%'";
	}
	if( isset($_SESSION['mst_user']['contractor_kana'])){
		$sql .= " and";
		$sql .= " concat(contractor_lkana,contractor_fkana) like '%" . $_SESSION['mst_user']['contractor_kana'] . "%'";
	}
	if( isset($_SESSION['mst_user']['email'])){
		$sql .= " and";
		$sql .= " email like '%" . $_SESSION['mst_user']['email'] . "%'";
	}
	if( isset($_SESSION['mst_user']['target_field1'])){
		foreach ( $_SESSION['mst_user']['target_field1'] as $key => $val) {
			$sql .= " and";
			$sql .= " (target_field1 like '" . $val . ",%' or target_field1 like '%," . $val . ",%' or target_field1 like '%," . $val . "' or target_field1 = '" . $val . "')";
		}
	}
	if( isset($_SESSION['mst_user']['target_field2'])){
		foreach ( $_SESSION['mst_user']['target_field2'] as $key => $val) {
			$sql .= " and";
			$sql .= " (target_field2 like '" . $val . ",%' or target_field2 like '%," . $val . ",%' or target_field2 like '%," . $val . "' or target_field2 = '" . $val . "')";
		}
	}
	if( isset($_SESSION['mst_user']['target_businessform'])){
		foreach ( $_SESSION['mst_user']['target_businessform'] as $key => $val) {
			$sql .= " and";
			$sql .= " (target_businessform like '" . $val . ",%' or target_businessform like '%," . $val . ",%' or target_businessform like '%," . $val . "' or target_businessform = '" . $val . "')";
		}
	}
	if( $_SESSION['mst_user']['auth_is_ok'] != NULL ){
		$sql .= " and";
		$sql .= " auth_is_ok = " . $_SESSION['mst_user']['auth_is_ok'];
	}
	if( $_SESSION['mst_user']['rank'] != NULL ){
		$sql .= " and";
		$sql .= " rank = " . $_SESSION['mst_user']['rank'];
	}
	if( $_SESSION['mst_user']['mailmaga'] != NULL ){
		$sql .= " and";
		$sql .= " mailmaga = " . $_SESSION['mst_user']['mailmaga'];
	}
        $sql .= " order by created desc";

        return mysql_query( $sql );
}

function mst_user_insert(){
        $sql = "insert into mst_user(";
        $sql .= " id";
        $sql .= ",pw";
        $sql .= ",company_name";
        $sql .= ",company_kana";
        $sql .= ",branch_name";
        $sql .= ",zip1";
        $sql .= ",zip2";
        $sql .= ",address";
        $sql .= ",phone_no1";
        $sql .= ",phone_no2";
        $sql .= ",phone_no3";
        $sql .= ",fax_no1";
        $sql .= ",fax_no2";
        $sql .= ",fax_no3";
        $sql .= ",k_phone_no1";
        $sql .= ",k_phone_no2";
        $sql .= ",k_phone_no3";
        $sql .= ",department_name";
        $sql .= ",post_name";
        $sql .= ",contractor_lname";
        $sql .= ",contractor_fname";
        $sql .= ",contractor_lkana";
        $sql .= ",contractor_fkana";
        $sql .= ",email";
        $sql .= ",target_field1";
        $sql .= ",target_field1_str";
        $sql .= ",target_field2";
        $sql .= ",target_field2_str";
        $sql .= ",target_businessform";
        $sql .= ",target_businessform_str";
        $sql .= ",authlimit";
        $sql .= ",auth_is_ok";
        $sql .= ",mailmaga";
        $sql .= ",rank";
        $sql .= ",created";
        $sql .= ")values(";
        $sql .= " '" . mysql_real_escape_string($_POST['id']) . "'";
        $sql .= ",'" . md5($_POST['pw']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['company_name']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['company_kana']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['branch_name']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['zip1']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['zip2']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['address']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['phone_no1']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['phone_no2']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['phone_no3']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['fax_no1']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['fax_no2']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['fax_no3']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['k_phone_no1']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['k_phone_no2']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['k_phone_no3']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['department_name']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['post_name']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['contractor_lname']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['contractor_fname']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['contractor_lkana']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['contractor_fkana']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['email']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['target_field1']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['target_field1_str']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['target_field2']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['target_field2_str']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['target_businessform']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['target_businessform_str']) . "'";
        $sql .= ",'" . date("Y-m-d H:i:s",strtotime("+3 day")) . "'";
        $sql .= ",'" . $_POST['auth_is_ok'] . "'";
        $sql .= ",'" . $_POST['mailmaga'] . "'";
        $sql .= ",'" . $_POST['rank'] . "'";
        $sql .= ",now()";
        $sql .= ")";
        if( $_SESSION['edit_status'] == "yet" ) {
                common_exec_sql( $sql );
        }
        return;
}
function mst_user_update() {
        $sql = "update mst_user set";
        $sql .= " company_name = '" . mysql_real_escape_string($_POST['company_name']) . "'";
        $sql .= ",company_kana = '" . mysql_real_escape_string($_POST['company_kana']) . "'";
        $sql .= ",branch_name = '" . mysql_real_escape_string($_POST['branch_name']) . "'";
        $sql .= ",zip1 = '" . mysql_real_escape_string($_POST['zip1']) . "'";
        $sql .= ",zip2 = '" . mysql_real_escape_string($_POST['zip2']) . "'";
        $sql .= ",address = '" . mysql_real_escape_string($_POST['address']) . "'";
        $sql .= ",phone_no1 = '" . mysql_real_escape_string($_POST['phone_no1']) . "'";
        $sql .= ",phone_no2 = '" . mysql_real_escape_string($_POST['phone_no2']) . "'";
        $sql .= ",phone_no3 = '" . mysql_real_escape_string($_POST['phone_no3']) . "'";
        $sql .= ",fax_no1 = '" . mysql_real_escape_string($_POST['fax_no1']) . "'";
        $sql .= ",fax_no2 = '" . mysql_real_escape_string($_POST['fax_no2']) . "'";
        $sql .= ",fax_no3 = '" . mysql_real_escape_string($_POST['fax_no3']) . "'";
        $sql .= ",k_phone_no1 = '" . mysql_real_escape_string($_POST['k_phone_no1']) . "'";
        $sql .= ",k_phone_no2 = '" . mysql_real_escape_string($_POST['k_phone_no2']) . "'";
        $sql .= ",k_phone_no3 = '" . mysql_real_escape_string($_POST['k_phone_no3']) . "'";
        $sql .= ",department_name = '" . mysql_real_escape_string($_POST['department_name']) . "'";
        $sql .= ",post_name = '" . mysql_real_escape_string($_POST['post_name']) . "'";
        $sql .= ",contractor_lname = '" . mysql_real_escape_string($_POST['contractor_lname']) . "'";
        $sql .= ",contractor_fname = '" . mysql_real_escape_string($_POST['contractor_fname']) . "'";
        $sql .= ",contractor_lkana = '" . mysql_real_escape_string($_POST['contractor_lkana']) . "'";
        $sql .= ",contractor_fkana = '" . mysql_real_escape_string($_POST['contractor_fkana']) . "'";
        $sql .= ",email = '" . mysql_real_escape_string($_POST['email']) . "'";
        $sql .= ",target_field1 = '" . mysql_real_escape_string($_POST['target_field1']) . "'";
        $sql .= ",target_field1_str = '" . mysql_real_escape_string($_POST['target_field1_str']) . "'";
        $sql .= ",target_field2 = '" . mysql_real_escape_string($_POST['target_field2']) . "'";
        $sql .= ",target_field2_str = '" . mysql_real_escape_string($_POST['target_field2_str']) . "'";
        $sql .= ",target_businessform = '" . mysql_real_escape_string($_POST['target_businessform']) . "'";
        $sql .= ",target_businessform_str = '" . mysql_real_escape_string($_POST['target_businessform_str']) . "'";
        $sql .= ",auth_is_ok = '" . $_POST['auth_is_ok'] . "'";
        $sql .= ",mailmaga = '" . $_POST['mailmaga'] . "'";
        $sql .= ",rank = '" . $_POST['rank'] . "'";
	if( $_POST['pw'] != "" ){
		$sql .= ",pw = '" . md5($_POST['pw']) . "'";
	}
	if( $_POST['mailmaga_old'] != $_POST['mailmaga'] ){
		$sql .= ",mailmaga_chg = now()";
	}
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " serial = '" . $_POST['serial'] . "'";
        common_exec_sql( $sql );
        return;
}
function mst_user_delete( $serial ){
        $sql = "update mst_user set";
        $sql .= " flg_delete = 1";
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " serial = '" . $serial . "'";
        common_exec_sql( $sql );
        return;
}
function mst_user_upgrade( $serial, $rank ){
	$sql = "update mst_user set";
	$sql .= " rank = " . $rank;
	$sql .= ",updated = now()";
	$sql .= " where";
	$sql .= " serial = '" . $serial . "'";
	common_exec_sql( $sql );
	return;
}
function mst_user_csvdl(){

	$tsvFileName = '/tmp/' . time() . rand() . '.tsv';
	$res = fopen($tsvFileName, 'w');

        $line .= "項番(主キー)";
        $line .= "\tログインID";
        $line .= "\t会社名";
        $line .= "\t会社名かな";
        $line .= "\t支店名・営業所名";
        $line .= "\t郵便番号1";
        $line .= "\t郵便番号2";
        $line .= "\t住所";
        $line .= "\t電話番号1";
        $line .= "\t電話番号2";
        $line .= "\t電話番号3";
        $line .= "\tFAX番号1";
        $line .= "\tFAX番号2";
        $line .= "\tFAX番号3";
        $line .= "\t携帯電話1";
        $line .= "\t携帯電話2";
        $line .= "\t携帯電話3";
        $line .= "\t部署名";
        $line .= "\t役職名";
        $line .= "\t名前(姓)";
        $line .= "\t名前(名)";
        $line .= "\t名前(姓)かな";
        $line .= "\t名前(名)かな";
        $line .= "\tメールアドレス";
        $line .= "\tサイトを知ったきっかけ";
        $line .= "\tメールマガジン";
        $line .= "\t支援を受けたい分野1（コード）";
        $line .= "\t支援を受けたい分野1（文字列）";
        $line .= "\t支援を受けたい分野2（コード）";
        $line .= "\t支援を受けたい分野2（文字列）";
        $line .= "\t支援を受けたい事業の形態（コード）";
        $line .= "\t支援を受けたい事業の形態（文字列）";
        $line .= "\t会員登録時の認証キー";
        $line .= "\t会員登録時の認証有効期限";
        $line .= "\t認証OKフラグ(1：認証済み、0：未認証)";
        $line .= "\t会員ランク(0：無料会員、1：有料会員A、2：有料会員B、3：有料会員C)";
        $line .= "\t削除フラグ(0：有効　1：無効)";
        $line .= "\tレコード更新日時";
        $line .= "\tレコード生成日時";
        $line .= "\t最終ログイン日時";
        $line .= "\tログイン回数";
        $line .= "\tメルマガ配信フラグ変更日時";
        $line .= "\n";

	$result = mst_user_index();
	while( $arr = mysql_fetch_array($result )){
                $line .= $arr['serial'];
                $line .= "\t" . $arr['id'];
                $line .= "\t" . $arr['company_name'];
                $line .= "\t" . $arr['company_kana'];
                $line .= "\t" . $arr['branch_name'];
                $line .= "\t'" . $arr['zip1'];
                $line .= "\t'" . $arr['zip2'];
                $line .= "\t" . $arr['address'];
                $line .= "\t'" . $arr['phone_no1'];
                $line .= "\t'" . $arr['phone_no2'];
                $line .= "\t'" . $arr['phone_no3'];
                $line .= "\t'" . $arr['fax_no1'];
                $line .= "\t'" . $arr['fax_no2'];
                $line .= "\t'" . $arr['fax_no3'];
                $line .= "\t'" . $arr['k_phone_no1'];
                $line .= "\t'" . $arr['k_phone_no2'];
                $line .= "\t'" . $arr['k_phone_no3'];
                $line .= "\t" . $arr['department_name'];
                $line .= "\t" . $arr['post_name'];
                $line .= "\t" . $arr['contractor_lname'];
                $line .= "\t" . $arr['contractor_fname'];
                $line .= "\t" . $arr['contractor_lkana'];
                $line .= "\t" . $arr['contractor_fkana'];
                $line .= "\t" . $arr['email'];
                $line .= "\t" . $arr['come_from'];
                $line .= "\t" . $arr['mailmaga'];
                $line .= "\t" . $arr['target_field1'];
                $line .= "\t" . $arr['target_field1_str'];
                $line .= "\t" . $arr['target_field2'];
                $line .= "\t" . $arr['target_field2_str'];
                $line .= "\t" . $arr['target_businessform'];
                $line .= "\t" . $arr['target_businessform_str'];
                $line .= "\t" . $arr['authkey'];
                $line .= "\t" . $arr['authlimit'];
                $line .= "\t" . $arr['auth_is_ok'];
                $line .= "\t" . $arr['rank'];
                $line .= "\t" . $arr['flg_delete'];
                $line .= "\t" . $arr['updated'];
                $line .= "\t" . $arr['created'];
                $line .= "\t" . $arr['login_date'];
                $line .= "\t" . $arr['login_count'];
                $line .= "\t" . $arr['mailmaga_chg'];
		$line .= "\n";
	}

	$line = mb_convert_encoding($line,"SJIS","UTF-8");
	fwrite($res, $line);
	fclose($res);

	header('Content-Disposition: attachment; filename=dl.tsv');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($tsvFileName));
	readfile($tsvFileName);

	$cmd = "rm -f " . $tsvFileName;
	system($cmd);

	return;
}
function mst_user_err_check() {
	if( $_POST['serial'] == "" ) {
		if( $_POST['id'] == "" ) {
			$err_msg .= "ログインIDを入力して下さい。<br>";
		}else{
			if (!preg_match("/^[!-~]+$/", $_POST['id'])) {
				$err_msg .= "ログインIDは半角英数記号のみで入力して下さい。<br>";
			}
		}
		if( $_POST['pw'] == "" ) {
			$err_msg .= "パスワードを入力して下さい。<br>";
		}else{
			if (!preg_match("/^[!-~]+$/", $_POST['pw'])) {
				$err_msg .= "パスワードは半角英数記号のみで入力して下さい。<br>";
			}
		}
	}

	if( $_POST['company_name'] == "" ) {
		$err_msg .= "会社名を入力して下さい。<br>";
	}
	if( $_POST['company_kana'] == "" ) {
		$err_msg .= "会社名かなを入力して下さい。<br>";
	}
	if( $_POST['zip1'] == "" || $_POST['zip2'] == "" ) {
		$err_msg .= "郵便番号を入力して下さい。<br>";
	}else{
		if (!preg_match("/^[!-~]+$/", $_POST['zip1'])) {
			$err_msg .= "郵便番号は半角英数記号のみで入力して下さい。<br>";
		}
		if (!preg_match("/^[!-~]+$/", $_POST['zip2'])) {
			$err_msg .= "郵便番号は半角英数記号のみで入力して下さい。<br>";
		}
	}
	if( $_POST['address'] == "" ) {
		$err_msg .= "住所を入力して下さい。<br>";
	}
	if( $_POST['phone_no1'] == "" || $_POST['phone_no2'] == "" || $_POST['phone_no3'] == "" ) {
		$err_msg .= "電話番号を入力して下さい。<br>";
	}else{
		if (!preg_match("/^[!-~]+$/", $_POST['phone_no1'])) {
			$err_msg .= "電話番号は半角英数記号のみで入力して下さい。<br>";
		}
		if (!preg_match("/^[!-~]+$/", $_POST['phone_no2'])) {
			$err_msg .= "電話番号は半角英数記号のみで入力して下さい。<br>";
		}
		if (!preg_match("/^[!-~]+$/", $_POST['phone_no3'])) {
			$err_msg .= "電話番号は半角英数記号のみで入力して下さい。<br>";
		}
	}
	if( $_POST['contractor_lname'] == "" || $_POST['contractor_fname'] == "" ) {
		$err_msg .= "担当者名を入力して下さい。<br>";
	}
	if( $_POST['contractor_lkana'] == "" || $_POST['contractor_fkana'] == "" ) {
		$err_msg .= "担当者名かなを入力して下さい。<br>";
	}
	if( $_POST['email'] == "" ) {
		$err_msg .= "メールアドレスを入力して下さい。<br>";
	} else {
		if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
			$err_msg .= "メールアドレスの形式が正しくありません。<br>";
		}
	}
	if( count($_POST['target_field1']) == 0 ) {
		$err_msg .= "支援を受けたい分野1(自治体案件カテゴリ)を入力して下さい。<br />";
	}
	if( count($_POST['target_field2']) == 0 ) {
		$err_msg .= "支援を受けたい分野2(財団法人案件カテゴリ)を入力して下さい。<br />";
	}
	if( count($_POST['target_businessform']) == 0) {
		$err_msg .= "支援を受けたい事業の形態を入力して下さい。<br />";
	}
	return $err_msg;
}

function mailmagazine_conf_update( $data ){
        $sql = "update mst_user set";
        $sql .= " mailmaga = '" . $data[2] . "'";
        if($data[2] == 0){
        	$sql .= ",mailmaga_ng_date = now()";
        }
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " serial = '" . $data[0] . "'";
        $result = common_exec_sql( $sql );
        return $result;
}

function mailmagazine_flg_update( $data ){

        $sql = "update mst_user set";
        $sql .= " mailmaga = '" . $data['mailmaga'] . "'";
        $sql .= ",mailmaga_chg = now()";
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " email = '" . $data['email'] . "'";
        $sql .= " and flg_delete = 0 ";

        $result = mysql_query( $sql ) or die('query error' . mysql_error());
        return mysql_affected_rows();
}

function mst_user_serial_checke( $serial ){
        $sql = "select";
        $sql .= " count(*)";
        $sql .= " from";
        $sql .= " mst_user";
        $sql .= " where";
        $sql .= " serial = '" . $serial . "'";
		return mysql_query( $sql );
}

function select_mst_user_mailmaga( $email ){
        $sql = "select";
        $sql .= " mailmaga";
        $sql .= " from";
        $sql .= " mst_user";
        $sql .= " where";
        $sql .= " email = '" . $email . "'";
		return mysql_query( $sql );
}

function mailmagazine_conf_import() {

	if (isset($_FILES['upfile']['error']) && is_int($_FILES['upfile']['error'])) {
		try {

			$err_msg = "";
			/* ファイルアップロードエラーチェック */
			switch ($_FILES['upfile']['error']) {
				case UPLOAD_ERR_OK:
	                // エラー無し
	                break;
	            case UPLOAD_ERR_NO_FILE:
	                // ファイル未選択
	                $err_msg = 'ファイルが未選択です。<br />';
	                throw new RuntimeException();
	            case UPLOAD_ERR_INI_SIZE:
	            case UPLOAD_ERR_FORM_SIZE:
	                // 許可サイズを超過
	                $err_msg .= 'ファイルサイズが許可サイズを超えています。<br />';
	                throw new RuntimeException();
	            default:
	                $err_msg .= '不明なエラー。<br />';
	                throw new RuntimeException();
	        }

	        $tmp_name = $_FILES['upfile']['tmp_name'];
	        $detect_order = 'ASCII,JIS,UTF-8,CP51932,SJIS-win';
	        setlocale(LC_ALL, 'ja_JP.UTF-8');

	        /* 文字コードを変換してファイルを置換 */
	        $buffer = file_get_contents($tmp_name);
	        if (!$encoding = mb_detect_encoding($buffer, $detect_order, true)) {
	            // 文字コードの自動判定に失敗
	            unset($buffer);
	            $err_msg .= '不明な文字コードです。<br />';
	            throw new RuntimeException();

	        }
	        file_put_contents($tmp_name, mb_convert_encoding($buffer, 'UTF-8', $encoding));
	        unset($buffer);

            $fp = fopen($tmp_name, 'rb');
            $count = 0;
            $row_array = array();
        	$su = 0;
        	$fa = 0;
        	$fa_list = array();
            //インポートデータチェック
            while ($row = fgetcsv($fp)) {
                if ($row === array(null) or $count == 0) {
                    // 空行、先頭行はスキップ
                    $count++;
                    continue;
                }
                if (count($row) !== 3) {
                    // カラム数が異なる無効なフォーマット
                    $err_msg .= 'データ項目数が違います。<br />';
                    throw new RuntimeException();
                }

            	// バリデート
                if(!empty($row[0])){ if(!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $row[0])) {
                	$err_msg .= "{$count}行目のメールアドレスのフォーマットが合っていません<br />";
                	$fa++;
                	$fa_list[] = $row[0];
                }}

                if($row[2] == ''){
                	$err_msg .= "{$count}行目の配信可否フラグが空白です<br />";
                	$fa++;
                	$fa_list[] = $row[0];
                }
                if(!is_numeric($row[2]) or $row[2] != 0 and $row[2] != 1){
                	$err_msg .= "{$count}行目の配信可否フラグに0か1以外の値が入っています<br />";
                	$fa++;
                	$fa_list[] = $row[0];
                }

                if($err_msg == ""){
                	$row_array[($count-1)]['email'] = $row[0];
                	$row_array[($count-1)]['mailmaga'] = $row[2];
                }

                $count++;
            }

            if (!feof($fp)) {
                // ファイルポインタエラー確認
                $err_msg .= "ファイルポインタエラー";
            }


			//データ更新
            if($err_msg != ""){
            	 throw new RuntimeException();
            }else{

            	foreach ($row_array as $key => $val){
            		$result = mailmagazine_flg_update($val);

            		if($result > 0){
            			$su++;
            		}else{
            			$fa++;
            			$fa_list[] = $val['email'];
            		}
            	}
            	$msg = '<b>データの更新が完了しました<b /><br />';
            }

            fclose($fp);

	    } catch (Exception $e) {

	        /* エラーメッセージをセット */
	        $msg = $err_msg;
	    }
	}


	return array($msg,$su,$fa,$fa_list);

}

function mst_user_maimaga_csvdl(){

	$csvFileName = '/tmp/' . time() . rand() . '.csv';
	$res = fopen($tsvFileName, 'w');

        $line .= "メールアドレス";
        $line .= ",メールマガジン";

	$line .= "\n";

	$result = mst_user_index();
	while( $arr = mysql_fetch_array($result )){
                $line = $arr['email'];
                $line .= "," . $arr['mailmaga'];

		$line .= "\n";
	}

	$line = mb_convert_encoding($line,"SJIS","UTF-8");
	fwrite($res, $line);
	fclose($res);

	header('Content-Disposition: attachment; filename=dl.csv');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($csvFileName));
	readfile($csvFileName);

	$cmd = "rm -f " . $csvFileName;
	system($cmd);

	return;
}


?>
