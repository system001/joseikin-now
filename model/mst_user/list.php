<?php
define( 'MST_USER_RANK_FREE', "0" );
define( 'MST_USER_RANK_A'   , "1" );
define( 'MST_USER_RANK_B'   , "2" );
define( 'MST_USER_RANK_C'   , "3" );

$mst_user_auth_is_ok_list = array(
	"0" => "未認証"
	,"1" => "認証済"
);
$mst_user_mailmaga_list = array(
	"0" => "配信しない"
	,"1" => "配信する"
);
$mst_user_rank_list = array(
	MST_USER_RANK_FREE => "無料会員",
	MST_USER_RANK_A    => "有料会員A",
	MST_USER_RANK_B    => "有料会員B",
	MST_USER_RANK_C    => "有料会員C",
);
?>
