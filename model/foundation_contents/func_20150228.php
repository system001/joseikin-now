<?php
function foundation_contents_index( $lines ) {
        $sql = "select";
        $sql .= " *";
        $sql .= " from";
        $sql .= " foundation_contents t1";
        $sql .= " where";
        $sql .= " flg_delete = 0";
	if(isset($_SESSION['foundation_contents']['contents_id'])){
		$sql .= " and";
		$sql .= " t1.contents_id like '%" . $_SESSION['foundation_contents']['contents_id'] . "%'";
	}
	if(isset($_SESSION['foundation_contents']['main_title'])){
		$sql .= " and";
		$sql .= " t1.main_title like '%" . $_SESSION['foundation_contents']['main_title'] . "%'";
	}
	if(isset($_SESSION['foundation_contents']['businessform'])){
		$sql .= " and";
		$sql .= " t1.businessform like '%" . $_SESSION['foundation_contents']['businessform'] . "%'";
	}
	if(isset($_SESSION['foundation_contents']['field'])){
		$sql .= " and";
		$sql .= " t1.field like '%" . $_SESSION['foundation_contents']['field'] . "%'";
	}
        $sql .= " order by t1.contents_id";
        if( $_REQUEST['page'] == "" ) {
                $offset = 0;
        } else {
                $offset = ($_REQUEST['page']-1) * $lines;
        }
        if( $lines != "" ){
                $sql .= " limit " . $lines . " offset " . $offset;
        }
        return mysql_query( $sql );
}
function foundation_contents_insert(){
        $sql = "insert into foundation_contents(";
        $sql .= " main_title";
        $sql .= ",businessform";
        $sql .= ",field";
        $sql .= ",comments";
        $sql .= ",created";
        $sql .= ")values(";
        $sql .= " '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['businessform']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['field']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['comments']) . "'";
        $sql .= ",now()";
        $sql .= ")";
        if( $_SESSION['edit_status'] == "yet" ) {
                common_exec_sql( $sql );
        }
        return;
}
function foundation_contents_update() {
        $sql = "update foundation_contents set";
        $sql .= " main_title = '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ",businessform = '" . $_POST['businessform'] . "'";
        $sql .= ",field = '" . $_POST['field'] . "'";
        $sql .= ",comments = '" . mysql_real_escape_string($_POST['comments']) . "'";
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " contents_id = " . $_POST['contents_id'];
        common_exec_sql( $sql );
        return;
}
function foundation_contents_delete( $contents_id ){
        $sql = "update foundation_contents set";
        $sql .= " flg_delete = 1";
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " contents_id = " . $contents_id;
        common_exec_sql( $sql );
        return;
}
function foundation_contents_err_check() {
        if( $_POST['main_title'] == "" ) {
                $err_msg = "「案件名称」を入力して下さい。<br>";
        }
        if( $_POST['businessform'] == "" ) {
                $err_msg .= "「事業形態」を入力して下さい。<br>";
        }
        if( $_POST['field'] == "" ) {
                $err_msg .= "「分野」を入力して下さい。<br>";
        }
        return $err_msg;
}
function foundation_contents_csvdl(){

        $tsvFileName = '/tmp/' . time() . rand() . '.tsv';
        $res = fopen($tsvFileName, 'w');

        $line .= "管理番号";
        $line .= "\t表題";
        $line .= "\t事業形態";
        $line .= "\t分野";
        $line .= "\t備考";
        $line .= "\tコンテンツのオリジナル識別子";
        $line .= "\t更新日時";
        $line .= "\t登録日時";
        $line .= "\n";

        $result = foundation_contents_index();
        while( $arr = mysql_fetch_array($result )){

		$comments = str_replace(" ","",$arr['comments']);
		$comments = str_replace("\r\n","",$comments);

                $line .= $arr['contents_id'];
                $line .= "\t" . $arr['main_title'];
                $line .= "\t" . mst_foundation_businessform_code_get_name($arr['businessform']);
                $line .= "\t" . mst_foundation_field_code_get_name($arr['field']);
                $line .= "\t" . $comments;
                $line .= "\t" . $arr['contents_org_id'];
                $line .= "\t" . $arr['updated'];
                $line .= "\t" . $arr['created'];
                $line .= "\n";
        }

        $line = mb_convert_encoding($line,"SJIS","UTF-8");
        fwrite($res, $line);
        fclose($res);

        header('Content-Disposition: attachment; filename=sampaleCsv.tsv');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($tsvFileName));
        readfile($tsvFileName);

        $cmd = "rm -f " . $tsvFileName;
        system($cmd);

        return;
}
?>
