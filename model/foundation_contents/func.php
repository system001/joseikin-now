<?php
function foundation_contents_index($lines = null)
{
	$limit = empty($lines) ? null : $lines;
	$page         = isset($_REQUEST['page']) ? $_REQUEST['page'] : null;
	$businessform = isset($_SESSION['foundation_contents']['businessform']) ? $_SESSION['foundation_contents']['businessform'] : null;
	$field        = isset($_SESSION['foundation_contents']['field']) ? $_SESSION['foundation_contents']['field'] : null;
	$keyword      = isset($_SESSION['foundation_contents']['keyword']) ? trim($_SESSION['foundation_contents']['keyword']) : null;

	$search_columns = array(
		'main_title',
		'supplier',
		'target',
		'fund',
		'limitation',
		'fund_season',
		'judge_season',
		'fund_span',
		'judge_method',
		'fund_count',
		'fund_price',
		'web_page',
		'comments',
		'contact',
		'last_update',
		'contents_org_id',
	);

	$where = "flg_delete = 0";
	if(!empty($businessform) && is_numeric($businessform)){
		$where .= " AND `businessform` LIKE '%{$businessform}%'";
	}
	if(!empty($field) && is_numeric($field)){
		$where .= " AND `field` LIKE '%{$field}%'";
	}
	if(!empty($keyword)){
		$keyword = str_replace('\\', '\\\\', $keyword);
		$keyword = str_replace("'", "\\'", $keyword);
		$keyword = str_replace('%', '\\%', $keyword);
		$keyword = str_replace('_', '\\_', $keyword);
		$keyword_conditions = array();
		foreach($search_columns as $search_column){
			$keyword_conditions[] = "(`{$search_column}` LIKE '%{$keyword}%')";
		}
		$where .= " AND (" . implode(' OR ', $keyword_conditions) . ")";
	}

	$record_count = 0;
	$records = array();
	$sql = "SELECT COUNT(1) FROM foundation_contents WHERE $where";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
	if(!empty($row)){
		$record_count = reset($row);
	}


	$sql = "SELECT * FROM foundation_contents WHERE {$where} ORDER BY contents_org_id, contents_id";
	if(!empty($limit)){
		$page = max(1, $page);
		$offset = ($page - 1) * $limit;
		$sql .= " LIMIT {$limit} OFFSET $offset";
	}
	$contents_org_id_list = array();
    $result = mysql_query( $sql );
	while($row = mysql_fetch_assoc($result)){
		$contents_org_id_list[$row['contents_org_id']] = true;
		$records[] = $row;
	}

	$recommnded_list = array();
	if(!empty($contents_org_id_list)){
		ksort($contents_org_id_list);
		$sql = "SELECT contents_org_id, flg_hidden FROM recommend_contents WHERE flg_delete = 0 AND contents_org_id IN ('" . implode("','", array_keys($contents_org_id_list)) . "')";
		$result = mysql_query( $sql );
		while($row = mysql_fetch_assoc($result)){
			$recommended_list[$row['contents_org_id']] = $row['flg_hidden'];
		}
	}

	foreach($records as &$item){
		$contents_org_id = $item['contents_org_id'];
		$item['flg_hidden'] = array_key_exists($contents_org_id, $recommended_list) ? $recommended_list[$contents_org_id] : null;
	}

	return array($record_count, $records);
}

function foundation_contents_insert(){
        $sql = "insert into foundation_contents(";
        $sql .= " main_title";
        $sql .= ",businessform";
        $sql .= ",field";
        $sql .= ",comments";
        $sql .= ",created";
        $sql .= ")values(";
        $sql .= " '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['businessform']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['field']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['comments']) . "'";
        $sql .= ",now()";
        $sql .= ")";
        if( $_SESSION['edit_status'] == "yet" ) {
                common_exec_sql( $sql );
        }
        return;
}
function foundation_contents_update() {
        $sql = "update foundation_contents as t1";
		$sql .= " left join recommend_contents as t2";
		$sql .= " on (t1.contents_org_id = t2.contents_org_id";
		$sql .= " and t1.main_title = t2.main_title";
		$sql .= " and t2.callkind = 'foundation'";
		$sql .= " and t2.flg_delete = 0)";
		$sql .= " set";
        $sql .= " t1.main_title = '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ", t2.main_title = '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ", t1.businessform = '" . $_POST['businessform'] . "'";
        $sql .= ", t1.field = '" . $_POST['field'] . "'";
        $sql .= ", t1.comments = '" . mysql_real_escape_string($_POST['comments']) . "'";
        $sql .= ", t1.updated = now()";
        $sql .= ", t2.updated = now()";
        $sql .= " where";
        $sql .= " t1.contents_id = " . $_POST['contents_id'];
        common_exec_sql( $sql );
        return;
}
function edit_recommend( $title, $org_id ){
        $sql = "insert into recommend_contents";
        $sql .= " (main_title, contents_org_id, callkind, flg_hidden, flg_delete, updated, created)";
        $sql .= " values (\"".mysql_real_escape_string($title)."\", \"".mysql_real_escape_string($org_id)."\", \"foundation\", 0, 0, now(), now())";
        common_exec_sql( $sql );
        return;
}
function edit_recommend_hidden( $hidden, $title, $org_id ){
	$hidden_st = ($hidden == 0) ? '1' : '0' ;
	$sql = "update recommend_contents";
	$sql .= " set flg_hidden = " . $hidden_st;
	$sql .= " , updated = now()";
	$sql .= " where callkind = \"foundation\"";
	$sql .= " and main_title = \"" . $title . "\"";
	$sql .= " and contents_org_id = \"" . $org_id . "\"";
	common_exec_sql( $sql );
	return;
}

function edit_recommend_delete( $title, $org_id ){
	$sql = "update recommend_contents";
	$sql .= " set flg_delete = 1";
	$sql .= " , updated = now()";
	$sql .= " where callkind = \"foundation\"";
	$sql .= " and main_title = \"" . $title . "\"";
	$sql .= " and contents_org_id = \"" . $org_id . "\"";
	common_exec_sql( $sql );
	return;
}
function foundation_contents_delete( $contents_id ){
        $sql = "update foundation_contents set";
        $sql .= " flg_delete = 1";
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " contents_id = " . $contents_id;
        common_exec_sql( $sql );
        return;
}
function foundation_contents_err_check() {
        if( $_POST['main_title'] == "" ) {
                $err_msg = "「案件名称」を入力して下さい。<br>";
        }
        if( $_POST['businessform'] == "" ) {
                $err_msg .= "「事業形態」を入力して下さい。<br>";
        }
        if( $_POST['field'] == "" ) {
                $err_msg .= "「分野」を入力して下さい。<br>";
        }
        return $err_msg;
}
function foundation_contents_csvdl(){

        $tsvFileName = '/tmp/' . time() . rand() . '.tsv';
        $res = fopen($tsvFileName, 'w');

        $line .= "管理番号";
        $line .= "\t表題";
        $line .= "\t事業形態";
        $line .= "\t分野";
        $line .= "\t備考";
        $line .= "\tコンテンツのオリジナル識別子";
        $line .= "\t更新日時";
        $line .= "\t登録日時";
        $line .= "\n";

        $result = foundation_contents_index();
        while( $arr = mysql_fetch_array($result )){

		$comments = str_replace(" ","",$arr['comments']);
		$comments = str_replace("\r\n","",$comments);

                $line .= $arr['contents_id'];
                $line .= "\t" . $arr['main_title'];
                $line .= "\t" . mst_foundation_businessform_code_get_name($arr['businessform']);
                $line .= "\t" . mst_foundation_field_code_get_name($arr['field']);
                $line .= "\t" . $comments;
                $line .= "\t" . $arr['contents_org_id'];
                $line .= "\t" . $arr['updated'];
                $line .= "\t" . $arr['created'];
                $line .= "\n";
        }

        $line = mb_convert_encoding($line,"SJIS","UTF-8");
        fwrite($res, $line);
        fclose($res);

        header('Content-Disposition: attachment; filename=sampaleCsv.tsv');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($tsvFileName));
        readfile($tsvFileName);

        $cmd = "rm -f " . $tsvFileName;
        system($cmd);

        return;
}

function detail_get_value_all( $id ) {
    $sql = "select t1.*, t2.flg_hidden as flg_hidden from foundation_contents as t1";
    $sql .= " left join recommend_contents as t2";
    $sql .= " on (t1.main_title = t2.main_title";
    $sql .= " and t1.contents_org_id = t2.contents_org_id";
    $sql .= " and t2.flg_delete = 0";
    $sql .= " and t2.callkind = 'foundation')";
    $sql .= " where contents_id = '$id'";
    $result = mysql_query($sql);
    return  mysql_fetch_array($result);
}
?>
