<?php
	$announce_type_list = array(
		1 => array(
			'name'  => 'お知らせ',
			'class' => 'news',
		),
		2 => array(
			'name'  => '新着案件',
			'class' => 'matter',
		),
		3 => array(
			'name'  => 'セミナー',
			'class' => 'sem',
		),
		4 => array(
			'name'  => 'メディア',
			'class' => 'media',
		),
		5 => array(
			'name'  => 'ブログ記事',
			'class' => 'onsem',
		),
	);
