<?php
function announce_index() {
	$sql = "
SELECT * FROM announces
  WHERE
    is_deleted = 0
  ORDER BY
    start_date DESC
";
	return mysql_query( $sql );
}
function announce_insert(){
	$sql = "
INSERT INTO announces(
  announce_type_id,
  message         ,
  url             ,
  start_date      ,
  is_deleted      ,
  created
)
VALUES
(
  '" . mysql_real_escape_string($_POST['announce_type_id']) . "',
  '" . mysql_real_escape_string($_POST['message'         ]) . "',
  '" . mysql_real_escape_string($_POST['url'             ]) . "',
  '" . mysql_real_escape_string($_POST['start_date'      ]) . "',
  '" . 0 . "',
  '" . common_get_now_datetime() . "'
)";
	if( $_SESSION['edit_status'] == "yet" ) {
		common_exec_sql( $sql );
	}
	return;
}

function announce_update() {
	$sql = "
UPDATE announces
SET
  announce_type_id = '" . mysql_real_escape_string($_POST['announce_type_id']) . "',
  message          = '" . mysql_real_escape_string($_POST['message'         ]) . "',
  url              = '" . mysql_real_escape_string($_POST['url'             ]) . "',
  start_date       = '" . mysql_real_escape_string($_POST['start_date'      ]) . "',
  modified         = '" . common_get_now_datetime() . "'
WHERE
  id               = '" . mysql_real_escape_string($_POST['id']) . "'
";
	common_exec_sql( $sql );
	return;
}
function announce_delete( $id ){
	$sql = "
UPDATE announces
SET
  is_deleted = 1,
  modified   = '" . common_get_now_datetime() . "'
WHERE
  id         = '" . mysql_real_escape_string($id) . "'
";
	common_exec_sql( $sql );
	return;
}
function announce_err_check() {
	if ( $_POST['announce_type_id'] == "" ) {
		$err_msg .= "お知らせタイプを選択してください。<br>";
	}
	if ( $_POST['message'] == "" ) {
		$err_msg .= "表示文言を入力してください。<br>";
	}
	if ( $_POST['start_date'] == "" ) {
		$err_msg .= "表示開始日を入力してください。<br>";
	}
	else if (!preg_match("/^\d\d\d\d-\d+-\d+$/", $_POST['start_date'])) {
		$err_msg .= "表示開始日を「yyyy-mm-dd」形式で入力してください。<br>";
	}

	return $err_msg;
}
?>
