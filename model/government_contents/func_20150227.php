<?php
function government_contents_index( $lines ) {
        $sql = "select";
        $sql .= " *";
        $sql .= " from";
        $sql .= " government_contents t1";
        $sql .= " where";
        $sql .= " flg_delete = 0";
	if(isset($_SESSION['government_contents']['contents_id'])){
		$sql .= " and";
		$sql .= " t1.contents_id like '%" . $_SESSION['government_contents']['contents_id'] . "%'";
	}
	if(isset($_SESSION['government_contents']['main_title'])){
		$sql .= " and";
		$sql .= " t1.main_title like '%" . $_SESSION['government_contents']['main_title'] . "%'";
	}
	if(isset($_SESSION['government_contents']['kind'])){
		$sql .= " and";
		$sql .= " t1.kind like '%" . $_SESSION['government_contents']['kind'] . "%'";
	}
	if(isset($_SESSION['government_contents']['field'])){
		$sql .= " and";
		$sql .= " t1.field like '%" . $_SESSION['government_contents']['field'] . "%'";
	}
        $sql .= " order by t1.contents_id";
        if( $_REQUEST['page'] == "" ) {
                $offset = 0;
        } else {
                $offset = ($_REQUEST['page']-1) * $lines;
        }
        if( $lines != "" ){
                $sql .= " limit " . $lines . " offset " . $offset;
        }
        return mysql_query( $sql );
}
function government_contents_insert(){
        $sql = "insert into government_contents(";
        $sql .= " main_title";
        $sql .= ",kind";
        $sql .= ",field";
        $sql .= ",nation_area";
        $sql .= ",pref_area";
        //$sql .= ",city_area";
        $sql .= ",comments";
        $sql .= ",created";
        $sql .= ")values(";
        $sql .= " '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['kind']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['field']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['nation_area']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['pref_area']) . "'";
        //$sql .= ",'" . mysql_real_escape_string($_POST['city_area']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['comments']) . "'";
        $sql .= ",now()";
        $sql .= ")";
        if( $_SESSION['edit_status'] == "yet" ) {
                common_exec_sql( $sql );
        }
        return;
}
function government_contents_update() {
        $sql = "update government_contents set";
        $sql .= " main_title = '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ",kind = '" . $_POST['kind'] . "'";
        $sql .= ",field = '" . $_POST['field'] . "'";
        $sql .= ",nation_area = '" . $_POST['nation_area'] . "'";
        $sql .= ",pref_area = '" . $_POST['pref_area'] . "'";
        //$sql .= ",city_area = '" . $_POST['city_area'] . "'";
        $sql .= ",comments = '" . mysql_real_escape_string($_POST['comments']) . "'";
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " contents_id = " . $_POST['contents_id'];
        common_exec_sql( $sql );
        return;
}
function government_contents_delete( $contents_id ){
        $sql = "update government_contents set";
        $sql .= " flg_delete = 1";
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " contents_id = " . $contents_id;
        common_exec_sql( $sql );
        return;
}
function government_contents_csvdl(){

	$tsvFileName = '/tmp/' . time() . rand() . '.tsv';
	$res = fopen($tsvFileName, 'w');

	$line .= "管理番号";
	$line .= "\t表題";
	$line .= "\t支援内容";
	$line .= "\t分野";
	$line .= "\tエリア（国）";
	$line .= "\tエリア（都道府県）";
	$line .= "\t備考";
	$line .= "\tコンテンツのオリジナル識別子";
	$line .= "\t更新日時";
	$line .= "\t登録日時";
	$line .= "\n";

	$result = government_contents_index();
	while( $arr = mysql_fetch_array($result )){
		$line .= $arr['contents_id'];
		$line .= "\t" . $arr['main_title'];
		$line .= "\t" . government_contents_get_kind($arr['kind']);
		$line .= "\t" . government_contents_get_field($arr['field']);
		$line .= "\t" . government_contents_get_nation_area($arr['nation_area']);
		$line .= "\t" . government_contents_get_pref_area($arr['pref_area']);
		$line .= "\t" . $arr['comments'];
		$line .= "\t" . $arr['contents_org_id'];
		$line .= "\t" . $arr['updated'];
		$line .= "\t" . $arr['created'];
		$line .= "\n";
	}

	$line = mb_convert_encoding($line,"SJIS","UTF-8");
	fwrite($res, $line);
	fclose($res);

	header('Content-Disposition: attachment; filename=sampaleCsv.tsv'); 
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($tsvFileName));
	readfile($tsvFileName);	

	$cmd = "rm -f " . $tsvFileName;
	system($cmd);

	return;
}
function government_contents_err_check() {
        if( $_POST['main_title'] == "" ) {
                $err_msg = "「案件名称」を入力して下さい。<br>";
        }
        if( $_POST['kind'] == "" ) {
                $err_msg .= "「種別」を入力して下さい。<br>";
        }
        if( $_POST['field'] == "" ) {
                $err_msg .= "「分野」を入力して下さい。<br>";
        }
        return $err_msg;
}
function government_contents_get_kind( $kind ) {
	if(isset($kind)){
		$name = NULL;
		$tmp_arr = explode(" ",$kind);
		foreach( $tmp_arr as $code ){
			if( isset( $name )){
				$name .= "、";
			}
			$name .= $GLOBALS[government_contents_kind_list][$code];
		}
	}
	return $name;
}
function government_contents_get_field( $field ) {
	if(isset($field)){
		$name = NULL;
		$tmp_arr = explode(" ",$field);
		foreach( $tmp_arr as $code ){
			if( isset( $name )){
				$name .= "、";
			}
			$name .= common_get_value("mst_government_field_code","name",$code,"code");
		}
	}
	return $name;
}
function government_contents_get_nation_area( $nation_area ) {
	$name = NULL;
	if( $nation_area == "1" ){
		$name = "全国";
	}
	return $name;
}
function government_contents_get_pref_area( $pref_area ) {
	if(isset($pref_area)){
		$name = NULL;
		$tmp_arr = explode(" ",$pref_area);
		foreach( $tmp_arr as $code ){
			if( isset( $name )){
				$name .= "、";
			}
			$name .= common_get_value("mst_government_city_code","name",$code,"code");
		}
	}
	return $name;
}
function government_contents_get_city_area( $city_area ) {
	if(isset($city_area)){
		$name = NULL;
		$tmp_arr = explode(" ",$city_area);
		foreach( $tmp_arr as $code ){
			if( isset( $name )){
				$name .= "、";
			}
			$name .= common_get_value("mst_government_city_code","name",$code,"code");
		}
	}
	return $name;
}
?>
