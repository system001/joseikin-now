<?php
function government_contents_index($lines = null)
{
	$limit       = empty($lines) ? null : $lines;
	$page        = isset($_REQUEST['page']) ? $_REQUEST['page'] : null;
	$kind        = isset($_SESSION['government_contents']['kind'       ]) ? $_SESSION['government_contents']['kind' ] : null;
	$field       = isset($_SESSION['government_contents']['field'      ]) ? $_SESSION['government_contents']['field'] : null;
	$keyword     = isset($_SESSION['government_contents']['keyword'    ]) ? trim($_SESSION['government_contents']['keyword'    ]) : null;
	$data_type   = isset($_SESSION['government_contents']['data_type'  ]) ? trim($_SESSION['government_contents']['data_type'  ]) : null;
	$created_min = isset($_SESSION['government_contents']['created_min']) ? trim($_SESSION['government_contents']['created_min']) : null;
	$created_max = isset($_SESSION['government_contents']['created_max']) ? trim($_SESSION['government_contents']['created_max']) : null;

	$search_columns = array(
		'main_title',
		'sub_title_1',
		'sub_title_2',
		'supplier',
		'target',
		'scale',
		'season',
		'purpose',
		'period',
		'adopt',
		'contact',
		'web_page',
		'comments',
		'contents_org_id',
	);

	$where = "flg_delete = 0";
	if(!empty($kind) && is_numeric($kind)){
		$where .= " AND `kind` LIKE '%{$kind}%'";
	}
	if(!empty($field) && is_numeric($field)){
		$where .= " AND `field` LIKE '%{$field}%'";
	}
	if ($data_type == '1') {
		$where .= " AND `contents_org_id` LIKE 'g:%'";
	}
	if ($created_min) {
		$where .= " AND `created` >= '" . $created_min . "'";
	}
	if ($created_max) {
		$where .= " AND `created` <= '" . $created_max . "'";
	}

	if(!empty($keyword)){
		// フリーワード検索
		$keyword = str_replace('\\', '\\\\', $keyword);
		$keyword = str_replace("'", "\\'", $keyword);
		$keyword = str_replace('%', '\\%', $keyword);
		$keyword = str_replace('_', '\\_', $keyword);
		$keyword_conditions = array();
		foreach($search_columns as $search_column){
			$keyword_conditions[] = "(`{$search_column}` LIKE '%{$keyword}%')";
		}
		$area_conditions = GovernmentAreaMaster::get_city_code_from_text($keyword);
		if(!empty($area_conditions)){
			if(!empty($area_conditions['pref_code_list'])){
				$keyword_conditions[] = "match(`pref_area`)against('" . implode(' ', array_keys($area_conditions['pref_code_list'])) . "' in boolean mode)";
			}
			if(!empty($area_conditions['city_code_list'])){
				$keyword_conditions[] = "match(`city_area`)against('" . implode(' ', array_keys($area_conditions['city_code_list'])) . "' in boolean mode)";
			}
		}
		$where .= " AND (" . implode(' OR ', $keyword_conditions) . ")";
	}

	$record_count = 0;
	$records = array();
	$sql = "SELECT COUNT(1) FROM government_contents WHERE $where";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
	if(!empty($row)){
		$record_count = reset($row);
	}

	$sql = "SELECT * FROM government_contents WHERE {$where} ORDER BY created DESC";
	if(!empty($limit)){
		$page = max(1, $page);
		$offset = ($page - 1) * $limit;
		$sql .= " LIMIT {$limit} OFFSET $offset";
	}
	$contents_org_id_list = array();
    $result = mysql_query( $sql );
	while($row = mysql_fetch_assoc($result)){
		$contents_org_id_list[$row['contents_org_id']] = true;
		$records[] = $row;
	}

	$recommnded_list = array();
	if(!empty($contents_org_id_list)){
		ksort($contents_org_id_list);
		$sql = "SELECT contents_org_id, flg_hidden FROM recommend_contents WHERE flg_delete = 0 AND contents_org_id IN ('" . implode("','", array_keys($contents_org_id_list)) . "')";
		$result = mysql_query( $sql );
		while($row = mysql_fetch_assoc($result)){
			$recommended_list[$row['contents_org_id']] = $row['flg_hidden'];
		}
	}

	foreach($records as &$item){
		$contents_org_id = $item['contents_org_id'];
		$item['flg_hidden'] = array_key_exists($contents_org_id, $recommended_list) ? $recommended_list[$contents_org_id] : null;
	}
	return array($record_count, $records);
}


function government_contents_insert(){
        $sql = "insert into government_contents(";
        $sql .= " main_title";
        $sql .= ",kind";
        $sql .= ",field";
        $sql .= ",nation_area";
        $sql .= ",pref_area";
        //$sql .= ",city_area";
        $sql .= ",comments";
        $sql .= ",created";
        $sql .= ")values(";
        $sql .= " '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['kind']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['field']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['nation_area']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['pref_area']) . "'";
        //$sql .= ",'" . mysql_real_escape_string($_POST['city_area']) . "'";
        $sql .= ",'" . mysql_real_escape_string($_POST['comments']) . "'";
        $sql .= ",now()";
        $sql .= ")";
        if( $_SESSION['edit_status'] == "yet" ) {
                common_exec_sql( $sql );
        }
        return;
}
function government_contents_update() {
        $sql = "update government_contents as t1";
		$sql .= " left join recommend_contents as t2";
		$sql .= " on (t1.contents_org_id = t2.contents_org_id";
		$sql .= " and t1.main_title = t2.main_title";
		$sql .= " and t2.callkind = 'autonomy'";
		$sql .= " and t2.flg_delete = 0)";
		$sql .= " set";
        $sql .= " t1.main_title = '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ", t2.main_title = '" . mysql_real_escape_string($_POST['main_title']) . "'";
        $sql .= ", t1.kind = '" . $_POST['kind'] . "'";
        $sql .= ", t1.field = '" . $_POST['field'] . "'";
        $sql .= ", t1.nation_area = '" . $_POST['nation_area'] . "'";
        $sql .= ", t1.pref_area = '" . $_POST['pref_area'] . "'";
        //$sql .= ", t1.city_area = '" . $_POST['city_area'] . "'";
        $sql .= ", t1.comments = '" . mysql_real_escape_string($_POST['comments']) . "'";
        $sql .= ", t1.updated = now()";
        $sql .= ", t2.updated = now()";
        $sql .= " where";
        $sql .= " t1.contents_id = " . $_POST['contents_id'];
        common_exec_sql( $sql );
        return;
}
function edit_recommend( $title, $org_id ){
        $sql = "insert into recommend_contents";
        $sql .= " (main_title, contents_org_id, callkind, flg_hidden, flg_delete, updated, created)";
        $sql .= " values (\"".mysql_real_escape_string($title)."\", \"".mysql_real_escape_string($org_id)."\", \"autonomy\", 0, 0, now(), now())";
        common_exec_sql( $sql );
        return;
}
function edit_recommend_hidden( $hidden, $title, $org_id ){
	$hidden_st = ($hidden == 0) ? '1' : '0' ;
	$sql = "update recommend_contents";
	$sql .= " set flg_hidden = " . $hidden_st;
	$sql .= " , updated = now()";
	$sql .= " where callkind = \"autonomy\"";
	$sql .= " and main_title = \"" . $title . "\"";
	$sql .= " and contents_org_id = \"" . $org_id . "\"";
	common_exec_sql( $sql );
	return;
}

function edit_recommend_delete( $title, $org_id ){
	$sql = "update recommend_contents";
	$sql .= " set flg_delete = 1";
	$sql .= " , updated = now()";
	$sql .= " where callkind = \"autonomy\"";
	$sql .= " and main_title = \"" . $title . "\"";
	$sql .= " and contents_org_id = \"" . $org_id . "\"";
	common_exec_sql( $sql );
	return;
}
function government_contents_delete( $contents_id ){
        $sql = "update government_contents set";
        $sql .= " flg_delete = 1";
        $sql .= ",updated = now()";
        $sql .= " where";
        $sql .= " contents_id = " . $contents_id;
        common_exec_sql( $sql );
        return;
}
function government_contents_csvdl(){

	$tsvFileName = '/tmp/' . time() . rand() . '.tsv';
	$res = fopen($tsvFileName, 'w');

	$line .= "管理番号";
	$line .= "\t表題";
	$line .= "\t支援内容";
	$line .= "\t分野";
	$line .= "\tエリア（国）";
	$line .= "\tエリア（都道府県）";
	$line .= "\t備考";
	$line .= "\tコンテンツのオリジナル識別子";
	$line .= "\t更新日時";
	$line .= "\t登録日時";
	$line .= "\n";

	$result = government_contents_index();
	while( $arr = mysql_fetch_array($result )){
		$line .= $arr['contents_id'];
		$line .= "\t" . $arr['main_title'];
		$line .= "\t" . government_contents_get_kind($arr['kind']);
		$line .= "\t" . government_contents_get_field($arr['field']);
		$line .= "\t" . government_contents_get_nation_area($arr['nation_area']);
		$line .= "\t" . government_contents_get_pref_area($arr['pref_area']);
		$line .= "\t" . $arr['comments'];
		$line .= "\t" . $arr['contents_org_id'];
		$line .= "\t" . $arr['updated'];
		$line .= "\t" . $arr['created'];
		$line .= "\n";
	}

	$line = mb_convert_encoding($line,"SJIS","UTF-8");
	fwrite($res, $line);
	fclose($res);

	header('Content-Disposition: attachment; filename=sampaleCsv.tsv');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($tsvFileName));
	readfile($tsvFileName);

	$cmd = "rm -f " . $tsvFileName;
	system($cmd);

	return;
}
function government_contents_err_check() {
        if( $_POST['main_title'] == "" ) {
                $err_msg = "「案件名称」を入力して下さい。<br>";
        }
        if( $_POST['kind'] == "" ) {
                $err_msg .= "「種別」を入力して下さい。<br>";
        }
        if( $_POST['field'] == "" ) {
                $err_msg .= "「分野」を入力して下さい。<br>";
        }
        return $err_msg;
}
function government_contents_get_kind( $kind ) {
	if(isset($kind)){
		$name = NULL;
		$tmp_arr = explode(" ",$kind);
		foreach( $tmp_arr as $code ){
			if( isset( $name )){
				$name .= "、";
			}
			$name .= $GLOBALS[government_contents_kind_list][$code];
		}
	}
	return $name;
}
function government_contents_get_field( $field ) {
	if(isset($field)){
		$name = NULL;
		$tmp_arr = explode(" ",$field);
		foreach( $tmp_arr as $code ){
			if( isset( $name )){
				$name .= "、";
			}
			$name .= common_get_value("mst_government_field_code","name",$code,"code");
		}
	}
	return $name;
}
function government_contents_get_nation_area( $nation_area ) {
	$name = NULL;
	if( $nation_area == "1" ){
		$name = "全国";
	}
	return $name;
}
function government_contents_get_pref_area( $pref_area ) {
	if(isset($pref_area)){
		$name = NULL;
		$tmp_arr = explode(" ",$pref_area);
		foreach( $tmp_arr as $code ){
			if( isset( $name )){
				$name .= "、";
			}
			$name .= common_get_value("mst_government_city_code","name",$code,"code");
		}
	}
	return $name;
}
function government_contents_get_city_area( $city_area ) {
	if(isset($city_area)){
		$name = NULL;
		$tmp_arr = explode(" ",$city_area);
		foreach( $tmp_arr as $code ){
			if( isset( $name )){
				$name .= "、";
			}
			$name .= common_get_value("mst_government_city_code","name",$code,"code");
		}
	}
	return $name;
}

function detail_get_value_all( $id ) {
    $sql = "select t1.*, t2.flg_hidden as flg_hidden from government_contents as t1";
    $sql .= " left join recommend_contents as t2";
    $sql .= " on (t1.main_title = t2.main_title";
    $sql .= " and t1.contents_org_id = t2.contents_org_id";
    $sql .= " and t2.flg_delete = 0";
    $sql .= " and t2.callkind = 'autonomy')";
    $sql .= " where contents_id = '$id'";
    $result = mysql_query($sql);
    return  mysql_fetch_array($result);
}
?>
