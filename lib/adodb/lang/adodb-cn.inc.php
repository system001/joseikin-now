<?php
// Chinese language file contributed by "Cuiyan (cysoft)" cysoft#php.net.
// Encode by GB2312
// Simplified Chinese
$ADODB_LANG_ARRAY = array (
			'LANG'                      => 'cn',
            DB_ERROR                    => '隆岑危列',
            DB_ERROR_ALREADY_EXISTS     => '厮将贋壓',
            DB_ERROR_CANNOT_CREATE      => '音嬬幹秀',
            DB_ERROR_CANNOT_DELETE      => '音嬬評茅',
            DB_ERROR_CANNOT_DROP        => '音嬬卿虹',
            DB_ERROR_CONSTRAINT         => '埃崩・崙',
            DB_ERROR_DIVZERO            => '瓜0茅',
            DB_ERROR_INVALID            => '涙丼',
            DB_ERROR_INVALID_DATE       => '涙丼議晩豚賜宀扮寂',
            DB_ERROR_INVALID_NUMBER     => '涙丼議方忖',
            DB_ERROR_MISMATCH           => '音謄塘',
            DB_ERROR_NODBSELECTED       => '短嗤方象垂瓜僉夲',
            DB_ERROR_NOSUCHFIELD        => '短嗤・哘議忖粁',
            DB_ERROR_NOSUCHTABLE        => '短嗤・哘議燕',
            DB_ERROR_NOT_CAPABLE        => '方象垂朔岬音惹否',
            DB_ERROR_NOT_FOUND          => '短嗤窟・',
            DB_ERROR_NOT_LOCKED         => '短嗤瓜迄協',
            DB_ERROR_SYNTAX             => '囂隈危列',
            DB_ERROR_UNSUPPORTED        => '音屶隔',
            DB_ERROR_VALUE_COUNT_ON_ROW => '壓佩貧拙柴峙',
            DB_ERROR_INVALID_DSN        => '涙丼議方象坿 (DSN)',
            DB_ERROR_CONNECT_FAILED     => '銭俊払移',
            0	                       => '短嗤危列', // DB_OK
            DB_ERROR_NEED_MORE_DATA     => '戻工議方象音嬬憲栽勣箔',
            DB_ERROR_EXTENSION_NOT_FOUND=> '制婢短嗤瓜窟・',
            DB_ERROR_NOSUCHDB           => '短嗤・哘議方象垂',
            DB_ERROR_ACCESS_VIOLATION   => '短嗤栽癖議幡・'
);
?>